/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.personal;

import java.io.FileNotFoundException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.account.AccountActivity;
import com.ingenic.watchmanager.account.HttpAsyn;
import com.ingenic.watchmanager.account.UpdatePwdFragment;
import com.ingenic.watchmanager.account.AccountActivity.LoginListener;
import com.ingenic.watchmanager.account.HttpAsyn.CallBack;
import com.ingenic.watchmanager.util.TUtils;
import com.ingenic.watchmanager.view.CircleImageView;

public class PersonalFragment extends Fragment implements LoginListener,
        CallBack {
    public PersonalFragment() {
    }

    private static final int ACTIVITY_RESULT_CODE_0 = 0;
    private static final int ACTIVITY_RESULT_CODE_1 = 1;
    private static final int ACTIVITY_RESULT_CODE_2 = 2;

//    private CircleImageView mBackImageView;
    private CircleImageView mSaveImageView;

    private LinearLayout mHeadLayout;
    /**
     * 头像imageview
     */
    private ImageView mHeadlconImg;
    /**
     * 用户名view
     */
    private TextView mNameView;
    /**
     * 登录Button
     */
    private Button mLoginButton;
    /**
     * 修改/完善个人信息button
     */
    private Button mPerfectButton;
    /**
     * 电话textview
     */
    private EditText mValuePhoneView;
    private String mPhone;
    /**
     * email textview
     */
    private EditText mValueEmailView;
    private String mEmail;
    /**
     * 修改密码button
     */
    private Button mUpdatePwdBut;
    /**
     * 是否改变头像
     */
    boolean isUpdateHead = false;

    private LinearLayout mPhoneView;
    private LinearLayout mEmailView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_account, container,
                false);
        initViews(rootView);
        showViews();
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IwdsLog.i(this, "" + resultCode);
        if (resultCode != 0) {
            switch (requestCode) {
            case ACTIVITY_RESULT_CODE_0:
                try {
                    Bitmap bitmap = BitmapFactory.decodeStream(getActivity()
                            .getContentResolver().openInputStream(uri));
                    mHeadlconImg.setImageBitmap(bitmap);
                    isUpdateHead = true;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                break;
            case ACTIVITY_RESULT_CODE_1:
                Intent intent = new Intent("com.android.camera.action.CROP");
                intent.setDataAndType(uri, "image/*");
                intent.putExtra("crop", "true");// 可裁剪
                intent.putExtra("aspectX", 1);
                intent.putExtra("aspectY", 1);
                intent.putExtra("outputX", 500);
                intent.putExtra("outputY", 500);
                intent.putExtra("scale", true);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                intent.putExtra("return-data", false);// 若为false则表示不返回数据
                intent.putExtra("outputFormat",
                        Bitmap.CompressFormat.JPEG.toString());
                intent.putExtra("noFaceDetection", true);
                startActivityForResult(intent, ACTIVITY_RESULT_CODE_2);
                break;
            case ACTIVITY_RESULT_CODE_2:
                try {
                    Bitmap bitmap = BitmapFactory.decodeStream(getActivity()
                            .getContentResolver().openInputStream(uri));
                    mHeadlconImg.setImageBitmap(bitmap);
                    isUpdateHead = true;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    // 初始化控件
    private void initViews(View rootView) {
        // mBackImageView = (CircleImageView) rootView.findViewById(R.id.back);
        // mBackImageView.setOnClickListener(new OnClickListener() {
        //
        // @Override
        // public void onClick(View v) {
        // getActivity().onBackPressed();
        // }
        // });
        mSaveImageView = (CircleImageView) rootView.findViewById(R.id.edit);
        mSaveImageView.setOnClickListener(new OnClickListener() {

            @SuppressLint("ShowToast")
            @Override
            public void onClick(View v) {
                if (isEdit()) {
                    if (verificationInput()) {
                        // 请求修改/完善个人信息接口
                        TUtils.showToast(getActivity(), "暂未开发！");
                    }
                } else {
                    getActivity().onBackPressed();
                }
            }
        });
        mHeadLayout = (LinearLayout) rootView.findViewById(R.id.head_name);
        mHeadLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                openCamera();// 打开照相机
            }
        });
        mHeadlconImg = (ImageView) rootView.findViewById(R.id.headlcon);
        mHeadlconImg.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                openAlbum();// 打开图库
            }
        });
        mUpdatePwdBut = (Button) rootView.findViewById(R.id.update_pwd_but);
        mUpdatePwdBut.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent it = new Intent(getActivity(), AccountActivity.class);
                it.putExtra("fragment", UpdatePwdFragment.class.getName());
                it.putExtra("tag", "account");
                it.putExtra("canBack", false);
                startActivity(it);
            }
        });
        mPhoneView = (LinearLayout) rootView.findViewById(R.id.phone);
        mValuePhoneView = (EditText) rootView.findViewById(R.id.value_phone);
        mEmailView = (LinearLayout) rootView.findViewById(R.id.email);
        mValueEmailView = (EditText) rootView.findViewById(R.id.value_email);

        mNameView = (TextView) rootView.findViewById(R.id.value_name);
        mLoginButton = (Button) rootView.findViewById(R.id.login);
        mLoginButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // 跳转到登录界面
                AccountActivity.login(getActivity(), PersonalFragment.this);
            }
        });
        mPerfectButton = (Button) rootView.findViewById(R.id.cancellation);
        mPerfectButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                PersonalManager.Personal.empty();
                showViews();
            }
        });
    }

    // 显示控件值
    private void showViews() {
        if (!PersonalManager.isLogin()) {
            mSaveImageView.setVisibility(View.GONE);
            mUpdatePwdBut.setVisibility(View.GONE);
            mNameView.setText(getResources().getString(R.string.login_hint));
            mLoginButton.setVisibility(View.VISIBLE);
            mPerfectButton.setVisibility(View.GONE);
            mHeadLayout.setEnabled(false);
            mHeadlconImg.setEnabled(false);
            mPhoneView.setVisibility(View.GONE);
            mEmailView.setVisibility(View.GONE);
        } else {
            mSaveImageView.setVisibility(View.VISIBLE);
            mUpdatePwdBut.setVisibility(View.VISIBLE);
            mNameView.setText(PersonalManager.Personal.getAccountName());
            mLoginButton.setVisibility(View.GONE);
            mPerfectButton.setVisibility(View.VISIBLE);
            mHeadLayout.setEnabled(true);
            mHeadlconImg.setEnabled(true);
            mPhoneView.setVisibility(View.VISIBLE);
            mEmailView.setVisibility(View.VISIBLE);
        }
        if (PersonalManager.Personal.getHeadloadUrl() == null
                || "".equals(PersonalManager.Personal.getHeadloadUrl())) {
            mHeadlconImg.setImageResource(R.drawable.ic_launcher);
        } else {
            mHeadlconImg.setImageURI(Uri.parse(PersonalManager.Personal
                    .getHeadloadUrl()));
        }
        mValueEmailView.setText(PersonalManager.Personal.getEmail());
        mValuePhoneView.setText(PersonalManager.Personal.getPhone());
    }

    @SuppressLint("SdCardPath")
    Uri uri = Uri.parse("file:///sdcard/temp.jpg");

    // 打开相册选择图片并截图
    private void openAlbum() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_PICK);
        intent.setType("image/*");
        intent.putExtra("crop", "true");
        // 裁剪框比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // 图片输出大小
        intent.putExtra("outputX", 500);
        intent.putExtra("outputY", 500);
        intent.putExtra("scale", true);
        intent.putExtra("return-data", false);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        // 不启用人脸识别
        intent.putExtra("noFaceDetection", false);
        startActivityForResult(intent, ACTIVITY_RESULT_CODE_0);
    }

    // 打开照相机
    private void openCamera() {
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(takePhotoIntent, ACTIVITY_RESULT_CODE_1);
    }

    String checkPhone = "^1\\d{10}$";

    // 验证手机号码
    private boolean verificationPhone(String phone) {
        Pattern regex = Pattern.compile(checkPhone);
        Matcher matcher = regex.matcher(phone);
        return matcher.matches();
    }

    String checkEmail = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";

    // 验证email
    private boolean verificationEmail(String email) {
        Pattern regex = Pattern.compile(checkEmail);
        Matcher matcher = regex.matcher(email);
        return matcher.matches();
    }

    // 验证输入
    private boolean verificationInput() {
        if (mPhone != null && !"".equals(mPhone) && !verificationPhone(mPhone)) {
            TUtils.showToast(getActivity(), R.string.verificationphone_hint,
                    Gravity.CENTER);
            return false;
        } else if (mEmail != null && !"".equals(mEmail)
                && !verificationEmail(mEmail)) {
            TUtils.showToast(getActivity(), R.string.verificationemail_hint,
                    Gravity.CENTER);
            return false;
        } else {
            return true;
        }
    }

    // 是否编辑
    private boolean isEdit() {
        if (isUpdateHead) {
            return true;
        }
        mPhone = mValuePhoneView.getText().toString().trim();
        mEmail = mValueEmailView.getText().toString().trim();
        if (PersonalManager.Personal.getPhone() != null) {
            if (!PersonalManager.Personal.getPhone().equals(mPhone)) {
                return true;
            }
        } else {
            if (mPhone != null && !mPhone.equals("")) {
                return true;
            }
        }
        if (PersonalManager.Personal.getEmail() != null) {
            if (!PersonalManager.Personal.getEmail().equals(mEmail)) {
                return true;
            }
        } else {
            if (mEmail != null && !mEmail.equals("")) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void loginResult(boolean isSuccess) {
        // 登录结果是否成功回调
        if (isSuccess) {
            showViews();
        }
    }

    @SuppressLint("HandlerLeak")
    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case 1:
                TUtils.showToast(getActivity(), R.string.update_success,
                        Gravity.CENTER);
                break;
            case -1:
                TUtils.showToast(getActivity(), (String) msg.obj);
                break;
            default:
                break;
            }
        };
    };

    @Override
    public void onRequestComplete(String result, int code) {
        if (result != null) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                int resultCode = jsonObject.getInt("resultCode");
                String reason = null;
                if (resultCode != 1) {
                    if (!jsonObject.isNull("reason")) {
                        reason = jsonObject.getString("reason");
                    }
                }
                Message msg = new Message();
                msg.what = resultCode;
                msg.obj = reason;
                if (code == HttpAsyn.INFO_URI_CODE) {
                    mHandler.sendMessage(msg);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
