/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  YuanBangLin(Ahlin)<banglin.yuan@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.personal;

public class Personal {

    // 头像地址
    private String mHeadloadUrl;
    // 帐号名
    private String mAccountName;
    // 电话
    private String mPhone;
    // 电子邮箱
    private String mEmail;
    // 昵称
    private String mNickname;

    @Override
    public String toString() {
        return "headloadUrl:" + mHeadloadUrl + ",accountName:" + mAccountName
                + ",phone:" + mPhone + ",email:" + mEmail + ",nickname:"
                + mNickname;
    }

    /**
     * 清空对象
     */
    public void empty() {
        this.mAccountName = null;
        this.mHeadloadUrl = null;
        this.mPhone = null;
        this.mEmail = null;
        this.mNickname = null;
    }

    public String getNickname() {
        return mNickname;
    }

    public void setNickname(String nickname) {
        this.mNickname = nickname;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        this.mPhone = phone;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    public String getHeadloadUrl() {
        return mHeadloadUrl;
    }

    public void setHeadloadUrl(String headloadUrl) {
        this.mHeadloadUrl = headloadUrl;
    }

    public String getAccountName() {
        return mAccountName;
    }

    public void setAccountName(String accountName) {
        this.mAccountName = accountName;
    }
}
