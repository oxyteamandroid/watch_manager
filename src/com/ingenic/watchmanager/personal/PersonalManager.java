/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  YuanBangLin(Ahlin)<banglin.yuan@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.personal;


public class PersonalManager {

    //保存登陆个人信息
    public static Personal Personal = new Personal();

    /**
     * 判断是否登录
     * @return
     */
    public static boolean isLogin() {
        if (Personal.getAccountName() != null
                && !"".equals(Personal.getAccountName())) {
            return true;
        }
        return false;
    }
    
    
}
