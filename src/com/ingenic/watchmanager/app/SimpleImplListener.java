/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.app;

import java.util.List;

import android.content.pm.PackageStats;

import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceClient.ConnectionCallbacks;
import com.ingenic.iwds.remotedevice.RemoteApplicationInfo;
import com.ingenic.iwds.remotedevice.RemoteDeviceAppListener;
import com.ingenic.iwds.remotedevice.RemoteDeviceProcessListener;
import com.ingenic.iwds.remotedevice.RemoteDeviceStatusListener;
import com.ingenic.iwds.remotedevice.RemoteProcessInfo;
import com.ingenic.iwds.remotedevice.RemoteStorageInfo;

public class SimpleImplListener {
    public static class SimpleConnectCallback implements ConnectionCallbacks {

        @Override
        public void onConnected(ServiceClient serviceClient) {
        }

        @Override
        public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
        }

        @Override
        public void onConnectFailed(ServiceClient serviceClient, ConnectFailedReason reason) {
        }
    }

    public static class SimpleStatusCallback implements RemoteDeviceStatusListener {

        @Override
        public void onRemoteDeviceReady(boolean isReady) {
        }
    }

    public static class SimpleProcessCallback implements RemoteDeviceProcessListener {

        @Override
        public void onResponseSystemMemoryInfo(long availMemSize, long totalMemSize) {
        }

        @Override
        public void onResponseRunningAppProcessInfo(List<RemoteProcessInfo> processInfoList) {
        }

        @Override
        public void onDoneKillProcess(String packageName) {
        }
    }

    public static class SimpleAppCallback implements RemoteDeviceAppListener {

        @Override
        public void onRemoteAppInfoListAvailable(List<RemoteApplicationInfo> appList) {
        }

        @Override
        public void onRemoteStorageInfoAvailable(RemoteStorageInfo storageInfo) {
        }

        @Override
        public void onSendFileProgressForInstall(String packageName, int progress) {
        }

        @Override
        public void onDoneInstallApp(String packageName, int returnCode) {
        }

        @Override
        public void onDoneDeleteApp(String packageName, int returnCode) {
        }

        @Override
        public void onResponsePkgSizeInfo(PackageStats stats, int returnCode) {
        }

        @Override
        public void onResponseClearAppDataOrCache(String packageName, int requestType, int returnCode) {
        }

        @Override
        public void onResponseClearAllAppDataAndCache(int totalClearCount, int index, String packageName, int typeOfIndex, int returnCode) {
        }
    }

}
