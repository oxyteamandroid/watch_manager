/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhengchuanshu<chuanshu.zheng@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.app;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageStats;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.iwds.remotedevice.RemoteApplicationInfo;
import com.ingenic.iwds.remotedevice.RemoteDeviceManagerInfo;
import com.ingenic.iwds.remotedevice.RemoteDeviceServiceManager;
import com.ingenic.iwds.remotedevice.RemoteDeviceStatusListener;
import com.ingenic.iwds.remotedevice.RemoteProcessInfo;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.app.SimpleImplListener.SimpleAppCallback;
import com.ingenic.watchmanager.app.SimpleImplListener.SimpleProcessCallback;
import com.ingenic.watchmanager.market.AppDownloadUtils;
import com.ingenic.watchmanager.market.DownAppListInfo;
import com.ingenic.watchmanager.market.MarketCustomDialog;
import com.ingenic.watchmanager.market.RemoteDeviceUtils;
import com.ingenic.watchmanager.market.RemoteDeviceUtils.Callback;
import com.ingenic.watchmanager.market.RemoteDeviceUtils.Register;
import com.ingenic.watchmanager.util.ProgressUtils;
import com.ingenic.watchmanager.util.Utils;

public class AppManagerFragment extends Fragment implements OnClickListener, OnItemClickListener {

    private static final String TAG = AppManagerFragment.class.getSimpleName();

    private static Context mContext;
    private AppListAdapter mAppListAdapter;
    private ListView mListview;

    private LayoutInflater mInflater;
    private ProgressUtils mProgressUtils;

    private boolean mIsReady;

    private TextView mBtn_all;
    private TextView mBtn_running;

    private int mLastRequest;

    private int mTextNoSelected;
    private int mTextSelected;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mContext = getActivity();
        View rootView = inflater.inflate(R.layout.fragment_appmanager, container, false);
        mHandler = new Handler();

        mListview = (ListView) rootView.findViewById(R.id.app_list);

        mListview.setOnItemClickListener(this);
        mListview.setOnItemLongClickListener(null);
        // View emptyView = rootView.findViewById(R.id.emptyView);
        // mListview.setEmptyView(emptyView);

        mTextSelected = getResources().getColor(R.color.text_selected);
        mTextNoSelected = getResources().getColor(R.color.text_noselected);

        mInflater = LayoutInflater.from(mContext);
        mAppListAdapter = new AppListAdapter();
        mLastRequest = R.id.app_all;
        mBtn_all = (TextView) rootView.findViewById(R.id.app_all);
        mBtn_running = (TextView) rootView.findViewById(R.id.app_running);
        mBtn_all.setOnClickListener(this);
        mBtn_running.setOnClickListener(this);

        mProgressUtils = new ProgressUtils(getActivity(), rootView.findViewById(R.id.container), (ProgressUtils.Listener) null);
        mProgressUtils.setIsNetwork(false);
        isAll = true;
        changeSelectStatus(R.id.app_all);

        mBtn_all.setTextColor(mTextSelected);
        mBtn_running.setTextColor(mTextNoSelected);
        mBtn_all.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        mBtn_running.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));

        initRemoteDeviceDrive();

        return rootView;
    }

    private void changeSelectStatus(int id) {
        mLastRequest = id;
        mProgressUtils.start();
        mBtn_all.setClickable(false);
        mBtn_running.setClickable(false);
    }

    private Map<String, RemoteApplicationInfo> mAppList = new LinkedHashMap<String, RemoteApplicationInfo>();

    protected Handler mHandler;

    protected RemoteDeviceServiceManager mService;
    protected Register mRegister;

    private static RemoteDeviceUtils sRemoteDeviceUtils;

    private static Map<String, Long> mAppSize = new LinkedHashMap<String, Long>();

    private void initRemoteDeviceDrive() {

        sRemoteDeviceUtils = RemoteDeviceUtils.getInstance(mContext);
        sRemoteDeviceUtils.start(new Callback() {

            @Override
            public void onCall(RemoteDeviceServiceManager service, Register register, boolean isReady, boolean isCalledReady) {
                IwdsLog.d(TAG, "RemoteDeviceUtils onCall!  ---> isReady: " + isReady + " isCalledReady: " + isCalledReady);
                mService = service;
                mRegister = register;
                mIsReady = isReady;
                RemoteDeviceStatusListener statusListener = getStatusListener();
                if (isCalledReady) {
                    statusListener.onRemoteDeviceReady(mIsReady);
                }
                mRegister.register(AppManagerFragment.class, statusListener, getProcessListener(), getAppListener());

            }
        });
    }

    private SimpleProcessCallback getProcessListener() {
        return new SimpleImplListener.SimpleProcessCallback() {
            @Override
            public void onResponseRunningAppProcessInfo(List<RemoteProcessInfo> processInfoList) {
                IwdsLog.d(TAG, "processInfoList: " + processInfoList.size());
                if (mLastRequest == R.id.app_running) {
                    mAdapterDataSource = getAppAdapterItemListVia(processInfoList);
                    mListview.setAdapter(mAppListAdapter);
                    mProgressUtils.end();
                    mBtn_all.setClickable(true);
                    mBtn_running.setClickable(true);
                }
            }

            @Override
            public void onDoneKillProcess(String packageName) {
                IwdsLog.d(TAG, "onDoneKillProcess success or not : " + packageName);
                if (!checkRemoteDevice())
                    return;
                mService.requestRunningAppProcessInfo();
            }
        };
    }

    private SimpleAppCallback getAppListener() {
        return new SimpleImplListener.SimpleAppCallback() {

            @Override
            public void onRemoteAppInfoListAvailable(List<RemoteApplicationInfo> appList) {
                IwdsLog.d(TAG, "onRemoteAppInfoListAvailable appList: ");
                IwdsLog.d(TAG, "appList: " + appList.size());
                for (RemoteApplicationInfo info : appList) {
                    mAppList.put(info.packageName, info);
                }
                if (isAll) {
                    mAdapterDataSource = getAppAdapterItemListBy(appList);
                    mListview.setAdapter(mAppListAdapter);
                    mProgressUtils.end();
                    mBtn_all.setClickable(true);
                    mBtn_running.setClickable(true);
                    for (AppAdapterItem item : mAdapterDataSource) {
                        if (!mAppSize.containsKey(item.pknm)) {
                            mService.requestPkgSizeInfo(item.pknm);
                            // IwdsLog.d(TAG, "requestPkgSizeInfo: " + item.pknm);
                        }
                    }

                }
            }

            @Override
            public void onResponsePkgSizeInfo(PackageStats stats, int returnCode) {
                if (RemoteDeviceManagerInfo.REQUEST_SUCCEEDED == returnCode) {
                    if (null != stats) {
                        mAppSize.put(stats.packageName, stats.cacheSize + stats.codeSize + stats.dataSize);
                        mAppListAdapter.notifyDataSetChanged();
                        IwdsLog.v(TAG, "onResponsePkgSizeInfo returnCode: " + returnCode);
                    } else {
                        IwdsLog.v(TAG, "onResponsePkgSizeInfo returnCode: REQUEST_SUCCEEDED --> but error; " + returnCode);
                    }
                }
                IwdsLog.v(TAG, "onResponsePkgSizeInfo returnCode: REQUEST_SUCCEEDED --> but error; " + returnCode);
            }

            @Override
            public void onDoneDeleteApp(String packageName, int returnCode) {
                IwdsLog.d(TAG, "onDoneDeleteApp : " + packageName + " : " + (RemoteDeviceManagerInfo.DELETE_SUCCEEDED == returnCode ? "succeeded" : "failed:"));
                if (RemoteDeviceManagerInfo.DELETE_SUCCEEDED == returnCode) {
                    Toast.makeText(mContext, R.string.uninstall_success, Toast.LENGTH_SHORT).show();
                    AppDownloadUtils.changeDownloadInfoStatus(packageName, DownAppListInfo.STATUS_COMPLETE);
                } else {
                    Toast.makeText(mContext, R.string.uninstall_fail, Toast.LENGTH_SHORT).show();
                }
                if (!checkRemoteDevice())
                    return;
                mService.requestGetAppList();
            }
        };
    }

    private RemoteDeviceStatusListener getStatusListener() {
        return new RemoteDeviceStatusListener() {

            private boolean mIsInit = true;
            private boolean mIsFirst = true;

            @Override
            public void onRemoteDeviceReady(boolean isReady) {
                IwdsLog.d(TAG, "isReady: " + isReady);
                if (mIsFirst && !isReady) {
                    IwdsLog.d(TAG, "into first!!");
                    mHandler.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            if (!checkRemoteDevice()) {
                                mProgressUtils.end();
                                mBtn_all.setClickable(true);
                                mBtn_running.setClickable(true);
                                IwdsLog.d(TAG, "into first!! post runnable + isready: " + false);
                            }
                            IwdsLog.d(TAG, "into first!! post runnable + isready: " + true);
                        }
                    }, 1000);
                    mIsFirst = false;
                    return;
                }
                mIsFirst = false;
                mIsReady = isReady;
                if (isReady && mIsInit) {
                    mProgressUtils.start();
                    mService.requestGetAppList();
                    mIsInit = false;
                }
                if (!checkRemoteDevice()) {
                    mProgressUtils.end();
                    mBtn_all.setClickable(true);
                    mBtn_running.setClickable(true);
                    IwdsLog.d(TAG, "link is disconnection! ");
                }
            }
        };
    }

    private static class AppAdapterItem {
        public Bitmap icon;
        public String name;
        public String pknm;
        public boolean isSystem;

        public AppAdapterItem(Bitmap ic, String na, String pk, boolean system) {
            icon = ic;
            name = na;
            pknm = pk;
            isSystem = system;
        }
    }

    public boolean checkRemoteDevice() {
        if (!mIsReady) {
            Toast.makeText(mContext, R.string.check_watch_link, Toast.LENGTH_SHORT).show();
            mBtn_all.setClickable(true);
            mBtn_running.setClickable(true);
        }
        return mIsReady;
    }

    private List<AppAdapterItem> mAdapterDataSource = new ArrayList<AppManagerFragment.AppAdapterItem>();
    private Map<String, String> mPkname2Label = new LinkedHashMap<String, String>();
    private Map<String, Bitmap> mPkname2Icon = new LinkedHashMap<String, Bitmap>();

    private List<AppAdapterItem> getAppAdapterItemListBy(List<RemoteApplicationInfo> list) {
        ArrayList<AppAdapterItem> temp = new ArrayList<AppAdapterItem>();

        for (RemoteApplicationInfo item : list) {
            if (isFilter(item.packageName))
                continue;

            boolean isSystemTemp = (mAppList.get(item.packageName).applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0;
            AppAdapterItem it = new AppAdapterItem(item.iconBitmap, item.label.toString(), item.packageName, isSystemTemp);
            if (it.isSystem) {
                temp.add(it);
            } else {
                temp.add(0, it);
            }
            mPkname2Label.put(item.packageName, item.label.toString());
            mPkname2Icon.put(item.packageName, item.iconBitmap);
            IwdsLog.v(TAG, "item.packageName: " + item.packageName);
        }

        return temp;
    }

    private static final Set<String> filter = new HashSet<String>();
    static {
        filter.add("com.ingenic.theme");
        filter.add("com.ingenic.iwds.device");
        filter.add("com.ingenic.provider.notification");

        filter.add("com.ingenic.provider.calendar");
        filter.add("system");
    }

    private boolean isFilter(String packageName) {
        String trimStr = packageName.trim();
        return trimStr.startsWith("com.android.") || trimStr.startsWith("android") || filter.contains(packageName);
    }

    private List<AppAdapterItem> getAppAdapterItemListVia(List<RemoteProcessInfo> list) {
        ArrayList<AppAdapterItem> temp = new ArrayList<AppAdapterItem>();

        for (RemoteProcessInfo item : list) {
            if (isFilter(item.processName))
                continue;

            String name = mPkname2Label.get(item.processName);
            name = null == name ? item.processName : name;
            RemoteApplicationInfo itemTemp = mAppList.get(item.processName);
            boolean isSystemTemp = null == itemTemp ? false : ((itemTemp.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
            AppAdapterItem it = new AppAdapterItem(mPkname2Icon.get(item.processName), name, item.processName, isSystemTemp);
            if (it.isSystem) {
                temp.add(it);
            } else {
                temp.add(0, it);
            }
            IwdsLog.v(TAG, "item.packageName: " + item.processName);
        }
        return temp;
    }

    private class AppListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mAdapterDataSource.size();
        }

        @Override
        public Object getItem(int position) {
            return mAdapterDataSource.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.fragment_appmanager_item, parent, false);
                viewHolder.icon = (ImageView) convertView.findViewById(R.id.app_icon);
                viewHolder.appName = (TextView) convertView.findViewById(R.id.app_name);
                viewHolder.appDesc = (TextView) convertView.findViewById(R.id.app_desc);
                viewHolder.operate = (TextView) convertView.findViewById(R.id.btn_operate);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            final AppAdapterItem appInfo = mAdapterDataSource.get(position);
            if (null == appInfo.icon) {
                viewHolder.icon.setImageResource(R.drawable.ic_tabsel);
            } else {
                viewHolder.icon.setImageBitmap(appInfo.icon);
            }

            if (isAll) {
                viewHolder.operate.setText(mContext.getString(R.string.uninstall));
            } else {
                viewHolder.operate.setText(mContext.getString(R.string.force_stop));
            }

            if (appInfo.isSystem) {
                viewHolder.operate.setTextColor(getResources().getColor(R.color.app_manager_operate_btn));// 0xFF666666);
                viewHolder.operate.setBackgroundResource(R.drawable.selector_tv_round_border);
                // IwdsLog.v(TAG, appInfo.name + " is system app!");
            } else {
                viewHolder.operate.setTextColor(getResources().getColor(android.R.color.white));// 0xFFFFFFFF);
                viewHolder.operate.setBackgroundResource(R.drawable.selector_app_manager_round);
            }

            viewHolder.operate.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (appInfo.isSystem) {
                        // IwdsLog.v(TAG, appInfo.name + " is system app too !");
                        return;
                    } else {
                        // IwdsLog.v(TAG, appInfo.name + " is not system app too !");
                    }
                    showOperateDialog(position, appInfo);
                }
            });

            viewHolder.appName.setText(appInfo.name);
            // viewHolder.appDesc.setText(appInfo.pknm);
            // viewHolder.appDesc.setVisibility(View.GONE);
            if (mAppSize.containsKey(appInfo.pknm)) {
                viewHolder.appDesc.setText(Utils.formatSize(mAppSize.get(appInfo.pknm).intValue()));
            }
            return convertView;
        }
    }

    private static class ViewHolder {
        public TextView operate;
        public ImageView icon;
        public TextView appName;
        public TextView appDesc;
    }

    @Override
    public void onClick(View v) {
        IwdsLog.d(TAG, "onclick id: " + v.getId());
        mAdapterDataSource.clear();
        mAppListAdapter.notifyDataSetChanged();
        switch (v.getId()) {
        case R.id.app_all:
            isAll = true;
            mBtn_all.setTextColor(mTextSelected);
            mBtn_running.setTextColor(mTextNoSelected);
            mBtn_all.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            mBtn_running.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));

            changeSelectStatus(R.id.app_all);
            if (!checkRemoteDevice()) {
                mProgressUtils.end();
                return;
            }
            mService.requestGetAppList();
            break;

        case R.id.app_running:
            isAll = false;
            mBtn_all.setTextColor(mTextNoSelected);
            mBtn_running.setTextColor(mTextSelected);
            mBtn_all.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            mBtn_running.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

            changeSelectStatus(R.id.app_running);
            if (!checkRemoteDevice()) {
                mProgressUtils.end();
                return;
            }
            mService.requestRunningAppProcessInfo();
            break;
        }
    }

    @SuppressLint("InflateParams")
    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        // AppAdapterItem appInfo = mAdapterDataSource.get(position);
        // showOperateDialog(position, appInfo);
    }

    private MarketCustomDialog mDialog;

    private boolean isAll = true;

    private void showOperateDialog(final int position, final AppAdapterItem appInfo) {
        final boolean isAllApp = isAll;
        String content = getString(R.string.uninstall_app);
        if (!isAllApp) {
            content = getString(R.string.stop_app);
        }
        mDialog = new MarketCustomDialog(mContext, content, new OnClickListener() {

            @Override
            public void onClick(View v) {
                String packageName = appInfo.pknm;
                if (!checkRemoteDevice())
                    return;
                if (isAllApp) {
                    IwdsLog.d(TAG, "delete app --> packageName:" + packageName);
                    mService.requestDeleteApp(packageName);
                } else {
                    IwdsLog.d(TAG, "RemoteProcessInfo --> packageName:" + packageName);
                    mService.requestKillProcess(packageName);
                }
                mDialog.dismiss();
                mProgressUtils.start();
            }
        });
        mDialog.setName(appInfo.name);
        mDialog.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroy();
        IwdsLog.d(TAG, "onDestroy!");
        mRegister.unregister(AppManagerFragment.class);
    }
}