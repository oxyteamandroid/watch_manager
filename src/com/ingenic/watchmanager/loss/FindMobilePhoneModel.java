package com.ingenic.watchmanager.loss;

import java.io.IOException;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.util.Log;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactorCallback;
import com.ingenic.watchmanager.util.UUIDS;

public class FindMobilePhoneModel implements DataTransactorCallback {

    private static FindMobilePhoneModel mInstance = null;
    private static DataTransactor mDataTransactor = null;

    private Context mContext;
    private MediaPlayer mMediaPlayer;
    private Vibrator vibrator;

    public static FindMobilePhoneModel getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new FindMobilePhoneModel(context);
        }
        return mInstance;
    }

    public FindMobilePhoneModel(Context context) {
        mContext = context;
        if (mDataTransactor == null) {
            mDataTransactor = new DataTransactor(mContext, this,
                    UUIDS.FIND_PHONE);
        }
    }

    public void startDataTransactor() {
        if (mDataTransactor != null)
            mDataTransactor.start();
    }

    public void stopDataTransactor() {
        if (mDataTransactor != null)
            mDataTransactor.stop();
    }

    // 获取系统默认铃声的Uri
    private Uri getSystemDefultRingtoneUri() {
        return RingtoneManager.getActualDefaultRingtoneUri(mContext,
                RingtoneManager.TYPE_RINGTONE);
    }

    private void startAlarm() {
        stopAlarm();
        mMediaPlayer = MediaPlayer.create(mContext,
                getSystemDefultRingtoneUri());
        vibrator = (Vibrator) mContext
                .getSystemService(Context.VIBRATOR_SERVICE);

        try {
            mMediaPlayer.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            long[] pattern = { 100, 400, 100, 400 };
            vibrator.vibrate(pattern, -1);
        } catch (Exception e) {
            // TODO: handle exception
        }

        mMediaPlayer.start();
        sendDataTransactor("mobileStartRing");
    }

    private void sendDataTransactor(String cmd) {
        if (mDataTransactor != null)
            mDataTransactor.send(cmd);
    }

    private void stopAlarm() {
        if (mMediaPlayer != null) {
            mMediaPlayer.pause();

            try {
                if (vibrator != null && vibrator.hasVibrator())
                    vibrator.cancel();
            } catch (Exception e) {
                // TODO: handle exception
            }
            sendDataTransactor("mobileStopRing");
        }
    }

    @Override
    public void onChannelAvailable(boolean arg0) {
        if (!arg0)
            stopAlarm();
    }

    @Override
    public void onDataArrived(Object arg0) {
        String cmd = (String) arg0;
        if (cmd.equals("startFindMobile")) {
            startAlarm();
        } else if (cmd.equals("stopFindMobile")) {
            stopAlarm();
        }

    }

    @Override
    public void onLinkConnected(DeviceDescriptor arg0, boolean arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onRecvFileProgress(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSendFileProgress(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSendResult(DataTransactResult arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSendFileInterrupted(int index) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onRecvFileInterrupted(int index) {
        // TODO Auto-generated method stub

    }

}
