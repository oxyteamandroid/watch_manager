
package com.ingenic.watchmanager.ota;

import com.ingenic.watchmanager.R;

import android.app.Activity;
import android.os.Bundle;

/**
 * 提示用户，当前已经是最新版本
 * @author jbnong
 */
public class LatestActivity extends Activity {

    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ota_activity_latest);
    }
}
