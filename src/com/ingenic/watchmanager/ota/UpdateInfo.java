/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * nongjiabao<jiabao.nong@ingenic.com>
 *
 * Elf/IDWS Project
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.watchmanager.ota;

import android.os.Parcel;
import android.os.Parcelable;

import com.ingenic.iwds.utils.IwdsLog;

/**
 * @author nongjiabao
 */
public class UpdateInfo implements Parcelable {
    private String TAG = "UpdateInfo";
    public String index;
    public String version_from;
    public String version_to;
    public String description;
    public String url;
    public String size;
    public String md5;

    public UpdateInfo() {

    }

    private UpdateInfo(Parcel in) {
        index = in.readString();
        version_from = in.readString();
        version_to = in.readString();
        description = in.readString();
        url = in.readString();
        size = in.readString();
        md5 = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int arg1) {
        out.writeString(index);
        out.writeString(version_from);
        out.writeString(version_to);
        out.writeString(description);
        out.writeString(url);
        out.writeString(size);
        out.writeString(md5);
    }

    public static final Parcelable.Creator<UpdateInfo> CREATOR = new Parcelable.Creator<UpdateInfo>() {

        @Override
        public UpdateInfo createFromParcel(Parcel in) {
            return new UpdateInfo(in);
        }

        @Override
        public UpdateInfo[] newArray(int size) {
            return new UpdateInfo[size];
        }

    };

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(index + ";");
        builder.append(version_from + ";");
        builder.append(version_to + ";");
        builder.append(description + ";");
        builder.append(url + ";");
        builder.append(size + ";");
        builder.append(md5 + ";");

        return builder.toString();
    }

    public static UpdateInfo createFromString(String s) {
        if (s.equals(""))
            return null;
        UpdateInfo info = new UpdateInfo();
        String[] values = s.split(";");
        int i = 0;
        try {
            info.index = values[i++];
            info.version_from = values[i++];
            info.version_to = values[i++];
            info.description = values[i++];
            info.url = values[i++];
            info.size = values[i++];
            info.md5 = values[i++];
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return info;
    }

    public void dump() {
        IwdsLog.d(TAG, "=========UpdateInfo===========");
        IwdsLog.d(TAG, "index			:" + index);
        IwdsLog.d(TAG, "version_from	:" + version_from);
        IwdsLog.d(TAG, "version_to	:" + version_to);
        IwdsLog.d(TAG, "description	:" + description);
        IwdsLog.d(TAG, "url			:" + url);
        IwdsLog.d(TAG, "size			:" + size);
        IwdsLog.d(TAG, "md5			:" + md5);
    }
}
