/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * nongjiabao<jiabao.nong@ingenic.com>
 *
 * Elf/IDWS Project
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.watchmanager.ota;

import com.ingenic.watchmanager.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class SetUrlActivity extends Activity {
    private EditText mUrl;
    private Button mBtnSave;
	protected void onDestroy(){
	    super.onDestroy();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ota_activity_set_url);
        mBtnSave = (Button)findViewById(R.id.btn_saveurl);
        mUrl = (EditText) findViewById(R.id.Edit_http);
        mUrl.setText(UpdateUtils.getStringFromSP(this, UpdateUtils.CONFIG_URL));
        mBtnSave.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String mUrlstr = mUrl.getText().toString();
                UpdateUtils.putStringToSP(getApplicationContext().getApplicationContext(), UpdateUtils.CONFIG_URL, mUrlstr);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
