/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.ota;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.WMKeyEventCallback;
import com.ingenic.watchmanager.ota.UpdateInfo;
import com.ingenic.watchmanager.ota.UpdateManager;
import com.ingenic.watchmanager.ota.UpdateUtils;
import com.ingenic.watchmanager.util.Utils;
import com.ingenic.watchmanager.view.OtaVersionListView;
import com.ingenic.watchmanager.view.OtaVersionListView.OnVersionCheckedListener;

public class OtaFragment extends Fragment implements OnClickListener,
        OnVersionCheckedListener, WMKeyEventCallback, UpdateManager.managerCallback
        , OtaModel.Callback{
    private String TAG ="OtaFragment";
    private static final int MSG_SYNC_START = 2;
    private static final int MSG_DOWNLOAD_ZIP = 5;
    private static final int MSD_DOWNLOAD_FINISHED = 6;
    private static final int MSD_DOWNLOAD_FAILED = 8;
    private static final int MSG_DISPLAY_NEWVERSION = 9;

    private final static int MSG_FILE_IS_EXIST = 20;
    private final static int MSG_SEND_CMD2WATCH = 43;
    private final static int MSG_SEND_CMDCANCEL = 44;

    private enum prompt {
        SUCCESS, UPDATE_FAILURE, DOWNLOAD_FAILURE, CANCEL, PROMPT
    };

    private Context  mContext = null;
    private TextView mModel_tv;
    private TextView mDisplayid_tv;
    private TextView mPercent;
    private TextView mRecoverytips;
    private TextView mUpdateListtime;
    private TextView mCur_version;
    private TextView mLatest_version;
    private TextView mUpdatezip_size;
    private TextView mNewDescriptor;

    private Button mCheck_btn;
    private Button mSeturl_btn;
    private Button mSelect_btn;
    private Button mUpdatenow_btn;
    private Button mUpdatelater_btn;
    private ProgressBar mPbar;
    private ProgressBar mParChecknet;
    private ViewGroup mDeviceInfo_layout;
    private ViewGroup mVersionInfo_layout;
    private ViewGroup mDownloadInfo_layout;
    private ViewGroup mNewversion_layout;
    private OtaModel mOtaModel = null;
    private UpdateManager mManager = null;
    private UpdateInfo mUpdateInfo = null;
    private UpdateInfo mNewUpdateInfo = null;
    private String mModel = null;
    private String mDisplayid = null;
    private OtaVersionListView mVersionList;
    private AlertDialog mConfirm_dlg = null;

    private String mUpdatepath = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/" + "update.zip";
    private void showOtaConfirmDialog(String tips){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.tips)
        .setMessage(tips)
        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mOtahandler.obtainMessage(MSG_SEND_CMD2WATCH).sendToTarget();
                            }
                        }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mOtahandler.obtainMessage(MSG_SEND_CMDCANCEL).sendToTarget();
                            }
                        });

        mConfirm_dlg = builder.create();
        mConfirm_dlg.setCancelable(false);
        mConfirm_dlg.show();
    }

    private void dimissConfirmDialog(){
        if (mConfirm_dlg != null) {
            mConfirm_dlg.dismiss();
            mConfirm_dlg = null;
        }
    }

    /**
     * watchRecovery 显示提示信息
     * @param ret
     */
    private void watchRecovery(prompt ret) {
        mDeviceInfo_layout.setVisibility(View.GONE);
        mVersionInfo_layout.setVisibility(View.GONE);
        mNewversion_layout.setVisibility(View.GONE);
        mDownloadInfo_layout.setVisibility(View.VISIBLE);
        mPercent.setVisibility(View.GONE);
        mPbar.setVisibility(View.GONE);
        mRecoverytips.setVisibility(View.VISIBLE);
        switch (ret){
        case SUCCESS:
            mRecoverytips.setText(R.string.recovery_success);
            break;
        case DOWNLOAD_FAILURE:
            mRecoverytips.setText(R.string.download_failure);
            break;
        case UPDATE_FAILURE:
            mRecoverytips.setText(R.string.recovery_failure);
            break;
        case CANCEL:
            mRecoverytips.setText(R.string.recovery_cancel);
            break;
        case PROMPT:
            mRecoverytips.setText(R.string.recovery_tips);
            break;
            default:
                break;
        }
    }

    /**
     * displayWatchInfomation 显示手表当前系统的信息
     * @param model
     * @param version
     */
    private void displayWatchInfomation(String model, String version) {
        mModel_tv.setText("unknown".equals(model) ? " " : model);
        mDisplayid_tv.setText("unknown".equals(version) ? " " : version);
        if (!"unknown".equals(model)) {
            mCheck_btn.setText(R.string.system_updates);
            mCheck_btn.setEnabled(true);
            mParChecknet.setVisibility(View.GONE);
            mDeviceInfo_layout.setVisibility(View.VISIBLE);
            mVersionInfo_layout.setVisibility(View.GONE);
            mDownloadInfo_layout.setVisibility(View.GONE);
            mNewversion_layout.setVisibility(View.GONE);
        }
    }

    private Handler mOtahandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MSG_SYNC_START:
                mCheck_btn.setEnabled(false);
                mCheck_btn.setText(R.string.checking);
                mParChecknet.setVisibility(View.VISIBLE);
                mManager.sync(null);
                break;
            case MSG_DISPLAY_NEWVERSION:
                if (mNewUpdateInfo == null){
                    mUpdateInfo = mManager.getLatestUpdateInfo();
                } else {
                  //后台服务检索到最新的升级信息
                    mUpdateInfo = mNewUpdateInfo;
                }
                IwdsLog.d(TAG, "+++++MSG_DISPLAY_NEWVERSION: " + mUpdateInfo);

                if (mUpdateInfo !=null){
                    UpdateUtils.setUpdateInfo(mContext, false, mUpdateInfo);
                    mCur_version.setText(mUpdateInfo.version_from);
                    mLatest_version.setText(mUpdateInfo.version_to);
                    String sizetext = UpdateUtils.FormetFileSize(Long.valueOf(mUpdateInfo.size));
                    mUpdatezip_size.setText(getString(R.string.filesize) + sizetext);
                    mNewDescriptor.setText(mUpdateInfo.description.replace("|", "\n"));
                    mDeviceInfo_layout.setVisibility(View.GONE);
                    mVersionInfo_layout.setVisibility(View.GONE);
                    mDownloadInfo_layout.setVisibility(View.GONE);
                    mUpdatenow_btn.setEnabled(true);
                    mUpdatelater_btn.setEnabled(true);
                    mNewversion_layout.setVisibility(View.VISIBLE);
                }
                break;
            case MSG_FILE_IS_EXIST:
                if(msg.arg1 == 1)
                    showOtaConfirmDialog(getString(R.string.install_exist_tips));
                else
                    showOtaConfirmDialog(getString(R.string.install_tips));
                break;
            case MSG_DOWNLOAD_ZIP:
                mDeviceInfo_layout.setVisibility(View.GONE);
                mVersionInfo_layout.setVisibility(View.GONE);
                mNewversion_layout.setVisibility(View.GONE);
                mDownloadInfo_layout.setVisibility(View.VISIBLE);
                mManager.downloadUpatezip(mUpdateInfo, mUpdatepath);
                break;
            case MSD_DOWNLOAD_FAILED:
                UpdateUtils.setUpdateState(mContext, UpdateUtils.STATE_DEFAULT);
                watchRecovery(prompt.DOWNLOAD_FAILURE);
                break;
            case MSG_SEND_CMD2WATCH:
                UpdateUtils.setUpdateState(mContext, UpdateUtils.STATE_RECOVERING);
                mOtaModel.sendCmd2watch("updates");
                break;
            case MSG_SEND_CMDCANCEL:
                UpdateUtils.setUpdateState(mContext, UpdateUtils.STATE_DEFAULT);
                UpdateUtils.setUpdateInfo(mContext, false, null);
                watchRecovery(prompt.CANCEL);
                break;
            default:
                break;
            }
            super.handleMessage(msg);
        }
    };
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_check_now:
                String url = UpdateUtils.getStringFromSP(getActivity(), UpdateUtils.CONFIG_URL);
                IwdsLog.d(TAG, "=========Url:" + url);

                if (url.equals("") == true) {
                    startActivity(new Intent(mContext, SetUrlActivity.class));
                    return;
                }
                mOtahandler.obtainMessage(MSG_SYNC_START).sendToTarget();
                break;
            case R.id.btn_update_now:
            case R.id.btn_ver_selected:

                if (!mOtaModel.IsOtaLinkConnect()) {
                    Toast.makeText(v.getContext(), R.string.Channel_unAvailable, Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                /**判断在手表中是否存在一样的update.zip*,校验码正确而且文件存在,直接通知手表更新 */
                if (UpdateUtils.getUpdatezipMD5(mContext).equals(mUpdateInfo.md5) &&
                        UpdateUtils.getUpdatezipExist(mContext).equals("true")) {
                    mOtahandler.obtainMessage(MSG_FILE_IS_EXIST, 1, 0).sendToTarget();
                    return;
                }

                ConnectivityManager net = (ConnectivityManager) getActivity().getSystemService(
                        Context.CONNECTIVITY_SERVICE);
                if ((net == null) || (net.getActiveNetworkInfo() == null)
                        || (!net.getActiveNetworkInfo().isConnected())) {
                    Toast.makeText(v.getContext(), R.string.update_no_net, Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                mOtahandler.obtainMessage(MSG_DOWNLOAD_ZIP).sendToTarget();
                break;
            case R.id.btn_seturl:
                startActivity(new Intent(mContext, SetUrlActivity.class));
                break;
            case R.id.btn_update_later:
                UpdateUtils.setUpdateInfo(mContext, false, null);
                UpdateUtils.setUpdateLasttime(mContext, UpdateUtils.STATE_DEFAULT);
                getActivity().onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public View getView() {
        return super.getView();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        int state;
        super.onActivityCreated(savedInstanceState);

        mNewUpdateInfo = UpdateUtils.getUpdateInfo(mContext);
        state = UpdateUtils.getUpdateState(mContext);
        /*显示最新更新时间*/
        long updatelasttime = UpdateUtils.getUpdateLasttime(mContext);
        if (updatelasttime != 0){
            mUpdateListtime.setText(Utils.TimetoString(updatelasttime));
        } else {
            mUpdateListtime.setText(R.string.notupdate);
        }
        mModel     = UpdateUtils.getStringFromSP(mContext, UpdateUtils.CONFIG_WATCH_MODEL);
        mDisplayid = UpdateUtils.getStringFromSP(mContext, UpdateUtils.CONFIG_WATCH_DISPLAYID);
        mManager.setVersionModel(mModel, mDisplayid);
        displayWatchInfomation(mModel, mDisplayid);
        switch(state){
            case UpdateUtils.STATE_DOWNLOADING:
            case UpdateUtils.STATE_DOWNLOADED:
                IwdsLog.d(TAG,"display downloading srceen");
                int progress = mManager.getDownloadProgress();
                updateDownloadProgress(progress);
                mDeviceInfo_layout.setVisibility(View.GONE);
                mVersionInfo_layout.setVisibility(View.GONE);
                mNewversion_layout.setVisibility(View.GONE);
                mDownloadInfo_layout.setVisibility(View.VISIBLE);
                break;

            case UpdateUtils.STATE_FILE_TRANSING:
                IwdsLog.d(TAG,"display transing srceen");
                mDeviceInfo_layout.setVisibility(View.GONE);
                mVersionInfo_layout.setVisibility(View.GONE);
                mNewversion_layout.setVisibility(View.GONE);
                mDownloadInfo_layout.setVisibility(View.VISIBLE);
                mPercent.setVisibility(View.VISIBLE);
                mPbar.setVisibility(View.VISIBLE);
                mRecoverytips.setVisibility(View.GONE);
                break;
            case UpdateUtils.STATE_FILE_TRANSED:
                IwdsLog.d(TAG,"display transed srceen");
                mOtahandler.obtainMessage(MSG_FILE_IS_EXIST,0,0).sendToTarget();
                break;
            case UpdateUtils.STATE_RECOVERING:
                IwdsLog.d(TAG,"display recovering srceen");
                break;
            case UpdateUtils.STATE_RECOVERED:
                IwdsLog.d(TAG,"display recovered srceen");
                break;
            default:
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_ota, null);
        mDeviceInfo_layout = (ViewGroup) rootView
                .findViewById(R.id.device_info_layout);
        mVersionInfo_layout = (ViewGroup) rootView
                .findViewById(R.id.version_info_layout);
        mDownloadInfo_layout = (ViewGroup) rootView
                .findViewById(R.id.download_version_layout);
        mNewversion_layout = (ViewGroup) rootView
                .findViewById(R.id.newversion_info_layout);

        //deivceinfo layout
        mModel_tv = (TextView) rootView.findViewById(R.id.model);
        mDisplayid_tv = (TextView) rootView.findViewById(R.id.displayid);
        mUpdateListtime = (TextView) rootView.findViewById(R.id.updatelasttime);
        mParChecknet = (ProgressBar) rootView.findViewById(R.id.checknetbar);
        mParChecknet.setVisibility(View.GONE);

        mCheck_btn = (Button) rootView.findViewById(R.id.btn_check_now);
        mCheck_btn.setOnClickListener(this);
        mCheck_btn.setEnabled(false);

        mSeturl_btn = (Button) rootView.findViewById(R.id.btn_seturl);
        mSeturl_btn.setOnClickListener(this);
        // versionlist layout
        mSelect_btn = (Button) rootView.findViewById(R.id.btn_ver_selected);
        mSelect_btn.setOnClickListener(this);

        mVersionList = (OtaVersionListView) rootView
                .findViewById(R.id.version_list);
        mVersionList.setOnVersionCheckedListener(this);

        // download layout
        mPbar = (ProgressBar) rootView.findViewById(R.id.updatebar);
        mPercent = (TextView) rootView.findViewById(R.id.percent);
        mRecoverytips = (TextView) rootView.findViewById(R.id.recoverytips_id);

        //newversion layout
        mUpdatenow_btn = (Button)rootView.findViewById(R.id.btn_update_now);
        mUpdatenow_btn.setOnClickListener(this);
        mUpdatenow_btn.setEnabled(false);
        mUpdatelater_btn = (Button)rootView.findViewById(R.id.btn_update_later);
        mUpdatelater_btn.setOnClickListener(this);
        mUpdatelater_btn.setEnabled(false);
        mCur_version      = (TextView)rootView.findViewById(R.id.cur_version);
        mLatest_version  = (TextView)rootView.findViewById(R.id.latest_version);
        mUpdatezip_size   = (TextView)rootView.findViewById(R.id.updatezip_size);
        mNewDescriptor = (TextView)rootView.findViewById(R.id.new_descriptor);
        setupTypeFace();
        return rootView;
    }
    private void setupTypeFace() {
        Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/watch_font.ttf");
        mModel_tv.setTypeface(typeFace);
        mDisplayid_tv.setTypeface(typeFace);
        mCheck_btn.setTypeface(typeFace);
        mSeturl_btn.setTypeface(typeFace);
        mSelect_btn.setTypeface(typeFace);
        mPercent.setTypeface(typeFace);
        mRecoverytips.setTypeface(typeFace);
        mUpdatenow_btn.setTypeface(typeFace);
        mUpdatelater_btn.setTypeface(typeFace);
        mCur_version.setTypeface(typeFace);
        mLatest_version.setTypeface(typeFace);
        mNewDescriptor.setTypeface(typeFace);
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (UpdateUtils.getStringFromSP(activity, UpdateUtils.CONFIG_URL)
                .equals("")) {
            UpdateUtils.putStringToSP(activity, UpdateUtils.CONFIG_URL, UpdateUtils.URL_PRODUCTS_UPDATE);
        }
        mContext = activity;
        mManager = UpdateManager.getInstance(mContext);
        mManager.registCallback(this);
        mOtaModel = OtaModel.getInstance(mContext);
        mOtaModel.registCallback(this);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        mContext = null;
        mManager.setVersionModel(null, null);
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        mManager.unRegistCallback(this);
        mOtaModel.unRegistCallback(this);
        super.onDetach();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void OnVersionChanged(String version, boolean update) {
        int res = update ? R.string.update_to_version
                : R.string.rollback_to_version;
        mSelect_btn.setEnabled(true);
        mSelect_btn.setText(getString(res, version));
        // 当到当前选择的版本信息。
        mUpdateInfo = mManager.getUpdateInfoTo(version);
        IwdsLog.d(TAG, "mUpdateInfo: " + mUpdateInfo.toString());
    }

    public static boolean onKeyDown(int keyCode, KeyEvent event) {
        return true;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
      return false;
    }

    @Override
    public void syncFinished(int ret) {
        IwdsLog.d(TAG, "syncFinished ret: " + ret);
        mCheck_btn.setEnabled(true);
        mCheck_btn.setText(R.string.system_updates);
        mParChecknet.setVisibility(View.GONE);
        switch (ret) {
        case UpdateManager.SYNC_SUCCESS:
            List<String> list1 = mManager.getVersionListBaseCurrent();
            if(list1.size() > 1  ){
                mOtahandler.obtainMessage(MSG_DISPLAY_NEWVERSION).sendToTarget();
            } else {
                startActivity(new Intent(mContext, LatestActivity.class));
            }
            break;
        case UpdateManager.SYNC_FAIL:
            Toast.makeText(getActivity(),R.string.sync_failed, Toast.LENGTH_SHORT).show();
            break;
        }
    }

    @Override
    public void updateDownloadProgress(int per) {
        String txt = getResources().getString(R.string.update_downloading) + per + "% ";
        mPbar.setProgress(per);
        mPercent.setText(txt);
    }

    @Override
    public void downloadUpgrade(int ret) {
        if (ret == UpdateManager.DOWNLOAD_FAIL){
            mOtahandler.obtainMessage(MSD_DOWNLOAD_FAILED).sendToTarget();
        }else{
            mOtahandler.obtainMessage(MSD_DOWNLOAD_FINISHED).sendToTarget();
        }

    }

    @Override
    public void onSendFileProgress(int progress) {
        mPbar.setProgress(progress);
        String protxt = getResources().getString(R.string.sendfiletowatch) + progress + "% ";
        mPercent.setText(protxt);
    }

    @Override
    public void onConfirmForReceiveFile() {
        mPercent.setVisibility(View.VISIBLE);
        mPbar.setVisibility(View.VISIBLE);
        mRecoverytips.setVisibility(View.GONE);
    }

    @Override
    public void onCancelForReceiveFile() {

    }

    @Override
    public void onWatchRecovery(int ret) {
        switch (ret){
            case 0:
                watchRecovery(prompt.PROMPT);
                break;
            case 1:
                watchRecovery(prompt.SUCCESS);
                break;
            case 2:
                watchRecovery(prompt.UPDATE_FAILURE);
                break;
        }
    }

    @Override
    public void OnSendfileFinished() {
        mOtahandler.obtainMessage(MSG_FILE_IS_EXIST, 0 ,0).sendToTarget();
    }

    @Override
    public void onChannelAvailable(boolean isAvailable) {
        if (!isAvailable){
            dimissConfirmDialog();
            if (UpdateUtils.getUpdateState(getActivity()) != UpdateUtils.STATE_RECOVERING){
                watchRecovery(prompt.UPDATE_FAILURE);
            }
        }
        
    }
}
