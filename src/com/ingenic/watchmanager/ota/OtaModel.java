/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * nongjiabao<jiabao.nong@ingenic.com>
 *
 * Elf/IDWS Project
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.watchmanager.ota;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.FileInfo;
import com.ingenic.iwds.datatransactor.FileTransactionModel;
import com.ingenic.iwds.datatransactor.FileTransferErrorCode;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.FileTransactionModel.FileTransactionModelCallback;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.util.UUIDS;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * OtaModel OTA模块
 * @author jbnong
 *
 */
public class OtaModel {

    private final static String TAG = "OtaModel";
    private final static int MSG_FILE_SEND_OK = 20;
    private final static int MSG_SEND_ERROR = 21;
    private final static int MSG_DATA_CHANNEL_AVAILABLE = 24;
    private final static int MSG_DATA_CHANNEL_UNAVAILABLE = 25;
    private final static int MSG_CONFIRM_RECEIVEFILE = 26;
    private final static int MSG_CANCEL_RECEIVEFILE = 27;
    private final static int MSG_UPDATE_PROGRESS = 34;
    private final static int MSG_WATCH_RECOVERY = 41;
    private static OtaModel sInstance;
    private Context mContext;
    private static boolean mChannelisAvailable = false;
    private static MyFiletransModel mFiletransModel;
    private UpdateManager mManager = null;
    private Callback mCallback;
    public OtaModel(Context context) {
        mContext = context;
        mFiletransModel = new MyFiletransModel(mContext, new OtaFileTransfer(), UUIDS.OTAFILE);
    }

    public synchronized static OtaModel getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new OtaModel(context);
        }
        return sInstance;
    }

    public void startTransaction() {
        mFiletransModel.start();
    }

    public void stopTransaction() {
        mFiletransModel.stop();
    }

    public boolean IsOtaLinkConnect() {
        return mChannelisAvailable;
    }

    public void transferUpdateFiletoWatch(String updatepath) {
        File zipfile = new File(updatepath);

        if (!zipfile.exists()) {
            IwdsLog.i(TAG, "update.zip file is not exists");
            return;
        }
        if (!mChannelisAvailable) {
            IwdsLog.i(TAG, "+++++isChannelAvailable is false");
            return;
        }
        try {
            int index = UpdateUtils.getSendIndex(mContext);
            IwdsLog.i(TAG, "+++++requestSendFile index: " + index);
            mFiletransModel.requestSendFile(zipfile.getAbsolutePath() , index);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void sendCmd2watch(String cmd){
        if(mChannelisAvailable){
            mFiletransModel.sendCmd(cmd);
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_UPDATE_PROGRESS:
                    if (mCallback != null) {
                        mCallback.onSendFileProgress(msg.arg1);
                    }
                    break;
                case MSG_FILE_SEND_OK:
                    if (mCallback != null) {
                        mCallback.OnSendfileFinished();
                    }
                    UpdateUtils.saveSendIndex(mContext, 0);
                    UpdateUtils.setUpdateState(mContext, UpdateUtils.STATE_FILE_TRANSED);
                    break;
                case MSG_CONFIRM_RECEIVEFILE:
                    if (mCallback != null) {
                        mCallback.onConfirmForReceiveFile();
                    }
                    UpdateUtils.setUpdateState(mContext, UpdateUtils.STATE_FILE_TRANSING);
                    break;
                case MSG_CANCEL_RECEIVEFILE:
                    if (mCallback != null) {
                        mCallback.onCancelForReceiveFile();
                    }
                    break;
                case MSG_DATA_CHANNEL_UNAVAILABLE:
                    if (mCallback != null) {
                        mCallback.onChannelAvailable(false);
                    }
                    /**
                     * 当是在STATE_RECOVERYING 转换到 UNAVAILABLE时， 不修改
                     * state的值,因为是recovery 手表重新启动导致的
                     **/
                    if (UpdateUtils.getUpdateState(mContext) != UpdateUtils.STATE_RECOVERING) {
                        UpdateUtils.setUpdateState(mContext, UpdateUtils.STATE_DEFAULT);
                    }
                    break;
                case MSG_WATCH_RECOVERY:
                    if (mCallback != null) {
                        mCallback.onWatchRecovery(msg.arg1);
                    }
                    UpdateUtils.setUpdateState(mContext, UpdateUtils.STATE_RECOVERING);
                    break;
                default:
                    break;
            }
            super.handleMessage(msg);
        }
    };

    private class MyFiletransModel extends FileTransactionModel {

        public MyFiletransModel(Context context,
                FileTransactionModelCallback callback, String uuid) {
            super(context, callback, uuid);
        }

        public void sendCmd(String cmd) {
            this.m_transactor.send(cmd);
        }

        @Override
        public void onDataArrived(Object object) {
            super.onDataArrived(object);
            if (object instanceof String){
                IwdsLog.d(TAG, "onDataArrived command:"  + object.toString());
                /*解析命令*/
                String cmd[] = ((String) object).split("#");
                if(cmd[0].equals("recovery")){
                    /*保存手表最后一次更新时间*/
                    UpdateUtils.setUpdateLasttime(mContext, System.currentTimeMillis());
                    mHandler.obtainMessage(MSG_WATCH_RECOVERY, 0, 0).sendToTarget();
                }
                if(cmd[0].equals("md5")){
                    //命令格式"md5#md5值#文件是否存在"
                    //比如：  md5#b4dbb82ee817eb373cebfbad7245e880#true
                    UpdateUtils.setUpdatezipMD5(mContext, cmd[1]);
                    UpdateUtils.setUpdatezipExist(mContext, cmd[2]);
                    if (cmd[2].equals("false")){
                        //假如手表的update.zip文件不存在或是被删除，把index清零。
                        UpdateUtils.saveSendIndex(mContext, 0);
                    }
                }
                if(cmd[0].equals("otaurl")){
                    UpdateUtils.putStringToSP(mContext, UpdateUtils.CONFIG_URL, cmd[1]);
                    IwdsLog.i(TAG,"otaurl is: " + cmd[1]);
                }
            }
        }
    }

    private class OtaFileTransfer implements FileTransactionModelCallback {

        @Override
        public void onSendResult(DataTransactResult result) {
            if (result.getResultCode() == DataTransactResult.RESULT_OK) {
                if (result.getTransferedObject() instanceof File) {
                    IwdsLog.d(TAG, "OtaFileTransfer Send file success");
                    mHandler.obtainMessage(MSG_FILE_SEND_OK).sendToTarget();
                } else {
                    IwdsLog.d(TAG, "OtaFileTransfer Send cmd success");
                }
            } else {
                mHandler.obtainMessage(MSG_SEND_ERROR, result.getResultCode()).sendToTarget();
                if (result.getTransferedObject() instanceof File) {
                    IwdsLog.i(TAG, "OtaFileTransfer Send file failed by error code: " + result.getResultCode());
                } else {
                    IwdsLog.i(TAG, "OtaFileTransfer Send cmd failed by error code: " + result.getResultCode());
                }
            }
        }

        @Override
        public void onSendFileProgress(int progress) {
            // TODO Auto-generated method stub
            mHandler.obtainMessage(MSG_UPDATE_PROGRESS, progress, 0).sendToTarget();
        }

        @Override
        public void onRequestSendFile(FileInfo info) {
        }

        @Override
        public void onRecvFileProgress(int progress) {
        }

        @Override
        public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
            IwdsLog.d(TAG, "OtaModel onLinkConnected model: " + descriptor.model + " display: " + descriptor.displayID + " isConnected: " + isConnected);
            if (isConnected) {
                //记录当前手表的版本信息
                String model,displayid;
                UpdateUtils.setWatchVersionInfo(mContext, descriptor.model, descriptor.displayID);
                model     = UpdateUtils.getStringFromSP(mContext, UpdateUtils.CONFIG_WATCH_MODEL);
                displayid = UpdateUtils.getStringFromSP(mContext, UpdateUtils.CONFIG_WATCH_DISPLAYID);
                mManager = UpdateManager.getInstance(mContext);
                mManager.setVersionModel(model, displayid);

                int state = UpdateUtils.getUpdateState(mContext);

                if (state == UpdateUtils.STATE_RECOVERING){

                    UpdateInfo info = UpdateUtils.getUpdateInfo(mContext);
                    if (info !=null){
                        if (displayid.equals(info.version_to)){
                            mHandler.obtainMessage(MSG_WATCH_RECOVERY, 1, 0).sendToTarget();
                            Toast.makeText(mContext, R.string.recovery_success, Toast.LENGTH_LONG).show();
                        }else {
                            mHandler.obtainMessage(MSG_WATCH_RECOVERY, 2, 0).sendToTarget();
                            Toast.makeText(mContext, R.string.recovery_failure, Toast.LENGTH_LONG).show();
                        }
                    }
                }
                UpdateUtils.setUpdateState(mContext, UpdateUtils.STATE_DEFAULT);
                UpdateUtils.setUpdateInfo(mContext, false, null);
             }
        }

        @Override
        public void onFileArrived(File file) {
        }

        @Override
        public void onConfirmForReceiveFile() {
            IwdsLog.d(TAG, "OtaModel onConfirmForReceiveFile ");
            mHandler.obtainMessage(MSG_CONFIRM_RECEIVEFILE).sendToTarget();
        }

        @Override
        public void onCancelForReceiveFile() {
            IwdsLog.d(TAG, "OtaModel onCancelForReceiveFile ");
            mHandler.obtainMessage(MSG_CANCEL_RECEIVEFILE).sendToTarget();
        }

        @Override
        public void onChannelAvailable(boolean isAvailable) {
            IwdsLog.d(TAG, "OtaModel onChannelAvailable: " + isAvailable);
            mChannelisAvailable = isAvailable;
            if (!mChannelisAvailable)
                mHandler.obtainMessage(MSG_DATA_CHANNEL_UNAVAILABLE).sendToTarget();
            else
                mHandler.obtainMessage(MSG_DATA_CHANNEL_AVAILABLE).sendToTarget();
        }

        @Override
        public void onFileTransferError(int errorCode) {
            IwdsLog.i(TAG, "FileTransferError error " + FileTransferErrorCode.errorString(errorCode));

        }

        @Override
        public void onSendFileInterrupted(int index) {
            UpdateUtils.saveSendIndex(mContext, index);
            IwdsLog.i(this, "onSendFileInterrupted index " + index);
        }

        @Override
        public void onRecvFileInterrupted(int index) {
            UpdateUtils.saveRecvIndex(mContext, index);
            IwdsLog.i(this, "onRecvFileInterrupted index " + index);
        }
    }

    public void registCallback(Callback callback) {
            mCallback = callback;
    }

    public void unRegistCallback(Callback callback) {
            mCallback = null;
    }

    public interface Callback {
        void onSendFileProgress(int progress);
        void onConfirmForReceiveFile();
        void onCancelForReceiveFile();
        void OnSendfileFinished();
        void onWatchRecovery(int ret);
        void onChannelAvailable(boolean isAvailable);
    }
}
