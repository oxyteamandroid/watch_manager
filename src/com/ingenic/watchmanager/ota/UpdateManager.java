/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * nongjiabao<jiabao.nong@ingenic.com>
 *
 * Elf/IDWS Project
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.watchmanager.ota;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import com.ingenic.iwds.utils.IwdsLog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

/**
 * OTA包下载管理
 * @author nongjiabao
 */
public class UpdateManager {
    private String TAG = "UpdateManager";
    private static final int SYNC_MESSAGE = 100;
    private static final int PROGRESS_MASSAGE = 7;
    private static final int MSD_DOWNLOAD_FAILED = 8;
    private static final int DOWNLOAD_MESSAGE = 200;

    public static final int DOWNLOAD_SUCCESS = 0;
    public static final int DOWNLOAD_FAIL = 1;
    public static final int SYNC_SUCCESS = 0;
    public static final int SYNC_FAIL = 1;

    public static final int CHECK_FAILED = 1;
    public static final int CHECK_NO_UPDATE = 2;
    public static final int CHECK_HAS_UPDATE = 3;
    public static final int CHECK_NO_ROLLBACK = 4;
    public static final int CHECK_HAS_ROLLBACK = 5;
    private static int mProgress = 0;
    private static UpdateManager sInstance = null;
    private Context mContext;
    private List<UpdateInfo> mUpdateList;
    private List<String> mVersionList;
    private String mCurrentVersion, mDeviceName;
    private String mUpdatezipname;
    private managerCallback mCallback;
    private myDownThread    mDownloadThread = null;
    private Comparator<Object> mComparator = new Comparator<Object>() {
        @Override
        public int compare(Object l, Object r) {
            // if l > r , swap them
            return mVersionList.indexOf(l) - mVersionList.indexOf(r);
        }
    };

    private UpdateManager(Context context) {
        mContext = context;
        mUpdatezipname = null;
    }

    public static UpdateManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new UpdateManager(context);
        }
        return sInstance;
    }

    public void sync(Message callback) {
        new SyncDataTask(callback).execute("");
    }

    /** get available version list ,base on current version */
    public List<String> getVersionListBaseCurrent() {
        List<String> list = new ArrayList<String>();
        String currentVersion = mCurrentVersion;
        list.add(currentVersion);
        for (UpdateInfo info : mUpdateList) {
            if (currentVersion.equals(info.version_from)) {
                list.add(info.version_to);
            }
        }
        Collections.sort(list, mComparator);
        return list;
    }

    public UpdateInfo getUpdateInfoTo(String version) {
        String currentVersion = mCurrentVersion;
        UpdateInfo update_info = null;
        for (UpdateInfo info : mUpdateList) {
            if (info.version_from.equals(currentVersion)
                    && info.version_to.equals(version)) {
                update_info = info;
                break;
            }
        }
        return update_info;
    }

    // 得到服务器当前最新的版本
    public UpdateInfo getLatestUpdateInfo() {
        UpdateInfo update_info = null;
        IwdsLog.d(this, "mUpdateList size: " + mUpdateList.size());
        for (UpdateInfo info : mUpdateList) {
            if (info.version_from.equals(mCurrentVersion)) {
                update_info = info;
                break;
            }
        }
        return update_info;
    }

    public void setVersionModel(String model, String displayid) {
        mDeviceName = model;
        mCurrentVersion = displayid;
    }

    public String getCurrentVersion() {
        if (null == mCurrentVersion || "unknown".equals(mCurrentVersion)){
            mCurrentVersion = "unknow";
        }
        return mCurrentVersion;
    }

    private class SyncDataTask extends AsyncTask<Object, Object, Object> {

        public SyncDataTask(Message msg) {

        }

        @Override
        protected Object doInBackground(Object... arg0) {
            String url = getUpdateUrl();
            if (url == null) {
                return SYNC_FAIL;
            } else if (url == "") {
                mUpdateList = new ArrayList<UpdateInfo>();
                return SYNC_SUCCESS;
            } else {
                return SyncData(url);
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            mManagerhandler.obtainMessage(SYNC_MESSAGE, (Integer)result, 0).sendToTarget();
        }

        private String getUpdateUrl() {
            String url = null;
            String mUrl = UpdateUtils.getStringFromSP(mContext, UpdateUtils.CONFIG_URL);
            IwdsLog.d(TAG,  "=====mUrl:" + mUrl);
            HttpGet getMethod;
            try {
                getMethod = new HttpGet(mUrl);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return null;
            }

            HttpClient httpClient = new DefaultHttpClient();

            // set connection timeout to 10s
            // set request timeout to 10s
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 10 * 1000);
            HttpConnectionParams.setSoTimeout(params, 10 * 1000);
            getMethod.setParams(params);

            List<ProductInfo> productList = new ArrayList<ProductInfo>();
            try {
                HttpResponse response = httpClient.execute(getMethod);
                String result = EntityUtils.toString(response.getEntity(),
                        UpdateUtils.ENCODE);
                IwdsLog.d(TAG,  "=====result:" + result);
                if (200 == response.getStatusLine().getStatusCode()) {
                    productList = ProductInfoHelper.getInstance()
                            .getProductList(result);
                    String this_model = mDeviceName.toLowerCase();
                    for (ProductInfo info : productList) {
                        String model = info.model.toLowerCase(
                                Locale.getDefault()).trim();
                        if (this_model.equals(model)) {
                            url = info.url;
                            return url;
                        }
                    }
                    IwdsLog.d(TAG, "Products not in products list! : " + this_model);
                    return "";
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException npe) {
                npe.printStackTrace();
            } catch (Exception er) {
                Log.e("UpdateManager", "Error...........");
                er.printStackTrace();
            }
            return null;
        }

        private int SyncData(String syncUrl) {
            int res = SYNC_SUCCESS;
            HttpGet getMethod = new HttpGet(syncUrl);

            // set connection timeout to 10s
            // set request timeout to 10s
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 10 * 1000);
            HttpConnectionParams.setSoTimeout(params, 10 * 1000);
            getMethod.setParams(params);

            HttpClient httpClient = new DefaultHttpClient();
            try {
                HttpResponse response = httpClient.execute(getMethod);
                String result = EntityUtils.toString(response.getEntity(),
                        UpdateUtils.ENCODE);
                int code = response.getStatusLine().getStatusCode();
                if (200 == code) { // get code of httpresponse
                    UpdateInfoHelper helper = new UpdateInfoHelper(result);
                    mVersionList = helper.getVersionList();
                    mUpdateList = helper.getUpdateList();
                } else {
                    IwdsLog.e(TAG, "Http response error, code:" + code);
                    res = SYNC_FAIL;
                }
            } catch (ClientProtocolException e) {
                res = SYNC_FAIL;
                e.printStackTrace();
            } catch (IOException ie) {
                res = SYNC_FAIL;
                ie.printStackTrace();
            } catch (Exception e) {
                res = SYNC_FAIL;
                e.printStackTrace();
            }
            return res;
        }
    }

    public void downloadUpatezip(UpdateInfo info, String filename) {
        IwdsLog.d(TAG,  "start download thread");
        mProgress = 0;
        mUpdatezipname = filename;
        UpdateUtils.setUpdateState(mContext, UpdateUtils.STATE_DOWNLOADING);
        mDownloadThread = new myDownThread(info, filename);
        mDownloadThread.start();

    }
    public void cancelDownload(){
        mProgress = 0;
        if (mDownloadThread != null){
            mDownloadThread.shut();
            mDownloadThread = null;
        }
    }
    private class myDownThread extends Thread {
        private MessageDigest md5 = null;
        private String Checkmd5Sum;
        private String Oldmd5Sum;
        private String urlpath;
        private String zipfilename;
        private byte[] Newmd5Sum;
        private long   length;
        private File   zipfile;

        private boolean runable = true;
        private UpdateInfo mInfo;

        public void shut() {
            this.runable = false;
        }

        public boolean isRuning() {
            return this.runable;
        }

        myDownThread(UpdateInfo info, String filename){
            mInfo = info;
            length = Long.valueOf(mInfo.size).longValue();
            Oldmd5Sum = mInfo.md5;
            urlpath = mInfo.url;
            zipfilename = filename;
            zipfile = new File(zipfilename);
            IwdsLog.d(TAG,  "zipfile:" + zipfile.getAbsolutePath());
            IwdsLog.d(TAG,  "download mInfo:" + mInfo.toString());
        }

        @Override
        public void run() {
            // TODO Auto-generated method stub
            long total = 0;
            int  per = 0, len = 0;
            byte[] buf = new byte[2048];
            FileOutputStream fos;
            /*判断update.zip文件是否已经下载过*/
            if(UpdateUtils.updatezipIsExist(mInfo, zipfilename)){
                IwdsLog.d(TAG,  "updatezipIsExist is true");
                mProgress = 100;
                mManagerhandler.obtainMessage(DOWNLOAD_MESSAGE, DOWNLOAD_SUCCESS, 0).sendToTarget();
                return;
            }

            try {
                fos = new FileOutputStream(zipfile);
                try {
                    mManagerhandler.obtainMessage(PROGRESS_MASSAGE, 0, 0).sendToTarget();
                    URL url = new URL(urlpath);
                    URLConnection connection = url.openConnection();
                    connection.connect();
                    connection.getHeaderField("Last-Modified");

                    InputStream is = connection.getInputStream();
                    md5 = MessageDigest.getInstance("MD5");
                    try {
                        while (isRuning()) {
                            len = is.read(buf);
                            if (len < 0)
                                break;
                            else {
                                md5.update(buf, 0, len);
                                fos.write(buf, 0, len);
                                total += len;
                                if (total * 100 / length > per) {
                                    per = (int) (total * 100 / length);
                                    mManagerhandler.obtainMessage(PROGRESS_MASSAGE, per, 0).sendToTarget();
                                }
                            }
                        }
                        fos.flush();
                        Newmd5Sum = md5.digest();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    fos.close();
                    is.close();
                } catch (Exception e) {
                }

                if (!isRuning() || total < length) {
                    IwdsLog.d(TAG,  "thread break:" + total);
                    if (zipfile.exists())
                        zipfile.delete();
                    mManagerhandler.obtainMessage(DOWNLOAD_MESSAGE, DOWNLOAD_FAIL, 0).sendToTarget();
                    IwdsLog.d(TAG,  "download updatezip failed");
                    return;
                }

                String result = "";
                for (int j = 0; j < Newmd5Sum.length; j++) {
                    result += Integer.toString((Newmd5Sum[j] & 0xff) + 0x100,
                            16).substring(1);
                }
                Checkmd5Sum = result;
                IwdsLog.d(TAG,  "Checkmd5Sum:" + Checkmd5Sum);
                if ((total < length) || !Oldmd5Sum.equals(result)) {
                    if (zipfile.exists())
                        zipfile.delete();
                }
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }

            if(Checkmd5Sum !=null){
                if(Checkmd5Sum.equals(Oldmd5Sum)){
                    mManagerhandler.obtainMessage(DOWNLOAD_MESSAGE, DOWNLOAD_SUCCESS, 0).sendToTarget();
                    IwdsLog.d(TAG,  "download updatezip finished");
                } else {
                    mManagerhandler.obtainMessage(DOWNLOAD_MESSAGE, DOWNLOAD_FAIL, 0).sendToTarget();
                    IwdsLog.d(TAG,  "download updatezip failed");
                }
            } else {
                mManagerhandler.obtainMessage(MSD_DOWNLOAD_FAILED, DOWNLOAD_FAIL, 0).sendToTarget();
                IwdsLog.d(TAG,  "download updatezip failed");
            }
            UpdateUtils.saveSendIndex(mContext, 0);
        }
    }
    private Handler mManagerhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case SYNC_MESSAGE:
                int ret = (Integer)msg.arg1;
                IwdsLog.d(TAG,  "+++++get version info finished, ret: " + ret);
                if (mCallback!=null ){
                    mCallback.syncFinished(ret);
                }
            case PROGRESS_MASSAGE:
                mProgress = msg.arg1;
                if (mCallback!=null ){
                    mCallback.updateDownloadProgress(msg.arg1);
                }
                break;
            case DOWNLOAD_MESSAGE:
                if (msg.arg1 == DOWNLOAD_SUCCESS){
                    UpdateUtils.setUpdateState(mContext, UpdateUtils.STATE_DOWNLOADED);
                    OtaModel otamodel = OtaModel.getInstance(mContext);
                    otamodel.transferUpdateFiletoWatch(mUpdatezipname);
                } else {
                    UpdateUtils.setUpdateState(mContext, UpdateUtils.STATE_DEFAULT);
                }

                if (mCallback!=null ){
                    mCallback.downloadUpgrade(msg.arg1);
                }
                break;
            default:
                break;
            }
            super.handleMessage(msg);
        }
    };
    public int getDownloadProgress(){
        return mProgress;
    }
    public void registCallback(managerCallback callback) {
        mCallback = callback;
    }

    public void unRegistCallback(managerCallback callback) {
        mCallback = null;
    }

    public interface managerCallback {
        void syncFinished(int ret);
        void downloadUpgrade(int ret);
        void updateDownloadProgress(int progress);
    }
}
