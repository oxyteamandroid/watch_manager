/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * nongjiabao<jiabao.nong@ingenic.com>
 *
 * Elf/IDWS Project
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.watchmanager.ota;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.util.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

/**
 * @author nongjiabao
 */

/**
 * @author jbnong
 *
 */
/**
 * @author jbnong
 *
 */
/**
 * @author jbnong
 *
 */
public class UpdateUtils {
    static final String TAG = "UpdateUtils";
    static final String ENCODE = "UTF-8";
    /**
     * the URL to check update
     */
    public static final String URL_PRODUCTS_UPDATE = "http://iwop.ingenic.com/static/iwds-ota/update-list.xml";
    //public static final String URL_PRODUCTS_UPDATE = "http://194.169.2.39/update_products_list.xml";
    public static final String PREFERENCE_NAME          = "update_config";
    public static final String CONFIG_DOWNLOAD_ID       = "download_id";
    public static final String CONFIG_UPDATE_INFO       = "update_info";
    public static final String CONFIG_NEW_UPDATE_INFO   = "new_update_info";
    public static final String CONFIG_RECOVERY_STATE    = "recoverystate";
    public static final String CONFIG_URL             = "update_url";
    public static final String CONFIG_WATCH_MODEL     = "model";
    public static final String CONFIG_WATCH_DISPLAYID = "displayid";
    public static final String WATCH_UPDATE_ZIP_MD5   = "md5";
    public static final String DEFAULT_UPDATE_ZIP_MD5 = "unknown";
    public static final String WATCH_UPDATE_ZIP_EXIST = "exist";
    public static final String CONFIG_UPDATELISTTIME  = "updatelisttime";

    private static final String INTERRUPTED_SEND_INDEX = "send_index";
    private static final String INTERRUPTED_RECV_INDEX = "recv_index";

    /**
     * 保存当前OTA的状态，比如正在下载文件，文件已经下载完毕，等
     */
    public static final String UPDATE_STATE = "update_state";

    /**
     * STATE_DEFAULT       = 0xFF;   // 默认
     * STATE_DOWNLOADING   = 0x01;   // 正在下载
     * STATE_DOWNLOADED    = 0x02;   // 下载完成
     * STATE_FILE_TRANSING = 0x03;   // 正在传文件
     * STATE_RECOVERING    = 0x05;   // 正在升级
     * STATE_RECOVERING    = 0x05;   // 正在升级
     * STATE_RECOVERED     = 0x06;   // 升级完成
     */
    public static final int STATE_DEFAULT       = 0xFF;
    public static final int STATE_DOWNLOADING   = 0x01;
    public static final int STATE_DOWNLOADED    = 0x02;
    public static final int STATE_FILE_TRANSING = 0x03;
    public static final int STATE_FILE_TRANSED  = 0x04;
    public static final int STATE_RECOVERING    = 0x05;
    public static final int STATE_RECOVERED     = 0x06;

    public static void saveSendIndex(Context context, int index) {
        SharedPreferences pref = context.getSharedPreferences(PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(INTERRUPTED_SEND_INDEX, index);
        editor.commit();
    }

    public static void saveRecvIndex(Context context, int index) {
        SharedPreferences pref = context.getSharedPreferences(PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(INTERRUPTED_RECV_INDEX, index);
        editor.commit();
    }

    public static int getSendIndex(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREFERENCE_NAME, 0);
        return pref.getInt(INTERRUPTED_SEND_INDEX, 0);
    }

    public static int getRecvIndex(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREFERENCE_NAME, 0);
        return pref.getInt(INTERRUPTED_RECV_INDEX, 0);
    }
    /**
     * 保存最新的ota信息。
     *
     * @param context Context
     * @param state ：false 表示手表么更新，true 表示已经更新
     * @param info ：最新ota版本信息
     */
    public synchronized static void setUpdateInfo(Context context, boolean state, UpdateInfo info) {
        SharedPreferences pref = context.getSharedPreferences(PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(CONFIG_RECOVERY_STATE, state);
        editor.putString(CONFIG_NEW_UPDATE_INFO, info == null ? "" : info.toString());
        editor.commit();
    }

    /**
     * 从SharedPreferences获取最新的ota信息。
     *
     * @param context Context
     */
    public static UpdateInfo getUpdateInfo(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREFERENCE_NAME, 0);
        UpdateInfo info = UpdateInfo.createFromString(pref.getString(CONFIG_NEW_UPDATE_INFO, ""));
        IwdsLog.i(TAG, "getUpdateInfo: info" + info);
        return info;
    }

    /**
     * 保存升级状态
     * @param context
     * @param state
     */
    public static void setUpdateState(Context context, int state) {
        switch (state) {
            case STATE_DEFAULT:
                IwdsLog.i(TAG, "setUpdateState: " + state + " default state");
                break;
            case STATE_DOWNLOADING:
                IwdsLog.i(TAG, "setUpdateState: " + state + " zip downloading");
                break;
            case STATE_DOWNLOADED:
                IwdsLog.i(TAG, "setUpdateState: " + state + " zip downloaded");
                break;
            case STATE_FILE_TRANSING:
                IwdsLog.i(TAG, "setUpdateState: " + state + " file transing");
                break;
            case STATE_FILE_TRANSED:
                IwdsLog.i(TAG, "setUpdateState: " + state + " file transed");
                break;
            case STATE_RECOVERING:
                IwdsLog.i(TAG, "setUpdateState: " + state + " recovering");
                break;
            case STATE_RECOVERED:
                IwdsLog.i(TAG, "setUpdateState: " + state + " recoveryed");
                break;
            default:
                IwdsLog.i(TAG, "setUpdateState  unkown");
                break;
        }
        SharedPreferences pref = context.getSharedPreferences(PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(UPDATE_STATE, state);
        editor.commit();
    }

    /**
     * 获取升级状态
     * @param context
     * @return
     */
    public static int getUpdateState(Context context) {
        int state;
        SharedPreferences pref = context.getSharedPreferences(PREFERENCE_NAME, 0);
        state = pref.getInt(UPDATE_STATE, STATE_DEFAULT);
        IwdsLog.i(TAG, "getUpdateState:" + state);
        return state;
    }

    /**
     * 从SharedPreferences获取对应info的Recovery状态。
     * @param context Context
     */
    public static boolean getRecoveryState(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREFERENCE_NAME, 0);
        return pref.getBoolean(CONFIG_RECOVERY_STATE, false);
    }

    /**
     * 保存升级服务器的url地址
     * @param context
     * @param url
     */
    public static void putUrlInfo(Context context, String url) {
        SharedPreferences pref = context.getSharedPreferences(PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(CONFIG_URL, url);
        editor.commit();
    }

    public static void putStringToSP(Context con, String key, String value) {
        SharedPreferences pref = con.getSharedPreferences(PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getStringFromSP(Context con, String key) {
        SharedPreferences pref = con.getSharedPreferences(PREFERENCE_NAME, 0);
        return pref.getString(key, "");
    }

    /**
     * 记录与手机连接的手表的model和displayid
     * @param context
     * @param model ：手表的型号
     * @param displayid ：手表的版本号
     */
    public static void setWatchVersionInfo(Context context, String model, String displayid) {
        SharedPreferences pref = context.getSharedPreferences(PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = pref.edit();
        if (model.equals("") || displayid.equals("")) {
            editor.putString(CONFIG_WATCH_MODEL, model);
            editor.putString(CONFIG_WATCH_DISPLAYID, displayid);
            editor.commit();
            return;
        }
        editor.putString(CONFIG_WATCH_MODEL, model);
        editor.putString(CONFIG_WATCH_DISPLAYID, displayid);
        editor.commit();
    }

    /**
     * 改变OTA Metro的状态
     * @param context
     * @param state false：不翻转， true：翻转，提示用户服务器有新的手表固件版本可以更新
     */
    public static void SetOtaOppsiteviewState(Context context, boolean state) {
        Intent it = new Intent(Utils.HAS_NEW_OTA_ACTION);
        it.putExtra("setoppsiteview", state);
        context.sendBroadcast(it);
    }

    /**
     * 清空本地OTA信息
     * @param context
     */
    public static void clearUpdateInfo(Context context) {
        /** 绑定之前清空本地OTA信息 **/
        setUpdateInfo(context, false, null);
        setWatchVersionInfo(context, "", "");
        setUpdateState(context, STATE_DEFAULT);
    }

    /**
     * 保存updatezip的Md5码
     * @param context
     * @param md5
     */
    public static void setUpdatezipMD5(Context context, String md5) {
        SharedPreferences sp = context.getSharedPreferences(PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        sp.edit().putString(WATCH_UPDATE_ZIP_MD5, md5).commit();
    }

    /**
     * 读取updatezip的Md5码
     * @param context
     * @return
     */
    public static String getUpdatezipMD5(Context context) {
        SharedPreferences sp = context.getSharedPreferences(PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        return sp.getString(WATCH_UPDATE_ZIP_MD5, DEFAULT_UPDATE_ZIP_MD5);
    }

    /**
     * 保存update.zip在手表的状态，
     * @param context
     * @param exist true 表示要升级的update包，在手表已经存在，不用再次传输，可以直接升级 false 表示不存在。
     */
    public static void setUpdatezipExist(Context context, String exist) {
        SharedPreferences sp = context.getSharedPreferences(PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        sp.edit().putString(WATCH_UPDATE_ZIP_EXIST, exist).commit();
    }

    /**
     * 获取update.zip在手表的状态
     * @param context
     * @return
     */
    public static String getUpdatezipExist(Context context) {
        SharedPreferences sp = context.getSharedPreferences(PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        return sp.getString(WATCH_UPDATE_ZIP_EXIST, "false");
    }

    /**
     * 保存最后更新时间
     * @param context
     * @param date
     */
    public static void setUpdateLasttime(Context context, long date) {
        SharedPreferences sp = context.getSharedPreferences(PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        sp.edit().putLong(CONFIG_UPDATELISTTIME, date).commit();
    }

    /**
     * 读取最后更新时间
     * @param context
     * @return
     */
    public static long getUpdateLasttime(Context context) {
        SharedPreferences sp = context.getSharedPreferences(PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        return sp.getLong(CONFIG_UPDATELISTTIME, 0);
    }

    /**
     * 等到文件的MD5码
     * @param file
     * @return
     * @throws FileNotFoundException
     */
    public static String getMd5ByFile(File file) throws FileNotFoundException {
        int len = 0;
        String md5Sum = null;
        MessageDigest md5 = null;
        byte[] buf = new byte[4096];
        byte[] localmd5Sum = null;
        try {
            FileInputStream in = new FileInputStream(file);
            md5 = MessageDigest.getInstance("MD5");
            try {
                while (true) {
                    len = in.read(buf);
                    if (len < 0)
                        break;
                    else {
                        md5.update(buf, 0, len);
                    }
                }
                localmd5Sum = md5.digest();
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }

        String result = "";
        if (localmd5Sum != null) {
            for (int j = 0; j < localmd5Sum.length; j++) {
                result += Integer.toString((localmd5Sum[j] & 0xff) + 0x100,
                        16).substring(1);
            }
        }

        md5Sum = result;
        return md5Sum;
    }

    /**
     * 判断update.zip是否存在
     * @param info
     * @param updatepath
     * @return
     */
    public static boolean updatezipIsExist(UpdateInfo info, String updatepath) {
        File zipfile = new File(updatepath);
        String MD5 = null;
        if (zipfile.exists()) {
            try {
                MD5 = UpdateUtils.getMd5ByFile(zipfile);
                if (info.md5.equals(MD5)) {
                    return true;
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    /**
     * 格式化文件长度
     * @param fileS
     * @return
     */
    public static String FormetFileSize(long fileS) {
        DecimalFormat df = new DecimalFormat("#.00");
        String fileSizeString = "";
        if (fileS < 1024) {
            fileSizeString = df.format((double) fileS) + "B";
        } else if (fileS < 1048576) {
            fileSizeString = df.format((double) fileS / 1024) + "K";
        } else if (fileS < 1073741824) {
            fileSizeString = df.format((double) fileS / 1048576) + "M";
        } else {
            fileSizeString = df.format((double) fileS / 1073741824) + "G";
        }
        return fileSizeString;
    }
}
