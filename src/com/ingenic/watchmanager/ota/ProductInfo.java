/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * nongjiabao<jiabao.nong@ingenic.com>
 *
 * Elf/IDWS Project
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.watchmanager.ota;

import android.os.Parcel;
import android.os.Parcelable;

import com.ingenic.iwds.utils.IwdsLog;

/**
 * @author nongjiabao
 */

public class ProductInfo implements Parcelable {
    private String TAG = "ProductInfo";
    public String model;
    public String url;

    public ProductInfo() {

    }

    private ProductInfo(Parcel in) {
        model = in.readString();
        url = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int arg1) {
        out.writeString(model);
        out.writeString(url);
    }

    public static final Parcelable.Creator<ProductInfo> CREATOR = new Parcelable.Creator<ProductInfo>() {

        @Override
        public ProductInfo createFromParcel(Parcel in) {
            return new ProductInfo(in);
        }

        @Override
        public ProductInfo[] newArray(int size) {
            return new ProductInfo[size];
        }

    };

    public void dump() {
        IwdsLog.d(TAG, "=========ProductInfo===========");
        IwdsLog.d(TAG, "model			:" + model);
        IwdsLog.d(TAG, "url			:" + url);
    }
}
