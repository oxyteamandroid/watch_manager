/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/???/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.calendar;

import java.util.List;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.provider.CalendarContract;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.ScheduleInfo;
import com.ingenic.iwds.datatransactor.elf.ScheduleInfo.Event;
import com.ingenic.iwds.datatransactor.elf.ScheduleTransactionModel;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.util.UUIDS;

/**
 * 负责同步日程信息到本地数据库
 * 
 * @author tZhang
 */

public class CalendarSyncService extends Service implements
        ScheduleTransactionModel.ScheduleTransactionModelCallback {

    static final String TAG = CalendarSyncService.class.getSimpleName();

    private static final int MSG_REQUEST_CALENDAR = 0;
    private static final int MSG_UPDATE_CALENDAR = 1;
    private static final int MSG_SEND_CALENDAR = 2;

    private ContentResolver mResolver;
    private CalendarDatabaseContentObserver mContentObserver;
    private EventOperator mEventOperator;
    private ReminderOperator mReminderOperator;

    private static ScheduleTransactionModel mModel;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mResolver = getContentResolver();
        mContentObserver = new CalendarDatabaseContentObserver(new Handler());
        mResolver.registerContentObserver(CalendarContract.CONTENT_URI, true,
                mContentObserver);

        mEventOperator = new EventOperator(this);
        mReminderOperator = new ReminderOperator(this);

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        filter.addAction(Intent.ACTION_TIME_CHANGED);
        filter.addAction(Intent.ACTION_DATE_CHANGED);
        registerReceiver(mReceiver, filter);

        if (mModel == null) {
            mModel = new ScheduleTransactionModel(this, this, UUIDS.SCHEDULE);
        }
        mModel.start();
        IwdsLog.d(TAG, "ScheduleTransactionModel is started!");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // 关闭日程数据传输模型
        if (mModel != null) {
            mModel.stop();
            IwdsLog.d(TAG, "ScheduleTransactionModel is stoped!");
        }

        // 注销日历数据的内容观察者
        if (mResolver != null && mContentObserver != null) {
            mResolver.registerContentObserver(CalendarContract.CONTENT_URI,
                    true, mContentObserver);
        }

        // 注销日期改变的广播接收器
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }
    }

    /**
     * 日历数据的内容观察者
     * 
     * @author tZhang
     */
    private class CalendarDatabaseContentObserver extends ContentObserver {

        public CalendarDatabaseContentObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            IwdsLog.d(TAG, "The calendar data has been changed!");
            mHandler.sendEmptyMessage(MSG_UPDATE_CALENDAR);
        }

    }

    /**
     * 更新日程信息
     */
    private void updateScheduleEvents() {
        new AsyncTask<Void, Void, List<Event>>() {

            @Override
            protected List<Event> doInBackground(Void... params) {
                // 当日历数据有更新时，更新本地的数据
                List<Event> events = CalendarUtil.diff(mResolver,
                        mEventOperator, mReminderOperator);
                backupScheduleEvents(events);
                return events;
            }

            @Override
            protected void onPostExecute(List<Event> result) {
                super.onPostExecute(result);
                // 发送更新的日程信息
                mHandler.obtainMessage(MSG_SEND_CALENDAR,
                        CalendarUtil.EventListToscheduleInfo(result))
                        .sendToTarget();
            }
        }.execute();

    }

    /**
     * 备份日程信息
     * 
     * @param events
     */
    private void backupScheduleEvents(List<Event> events) {
        // 备份日程数据，以便下次更新时diff
        if (events != null && events.size() > 0) {
            for (Event event : events) {
                if (event.dtStart < 0 || event.dtEnd < 0) {
                    // 删除
                    synchronized (this) {
                        if (event.reminder != null) {
                            mReminderOperator.delete(event.reminder);
                        }
                        mEventOperator.delete(event);
                    }
                } else {
                    if (mEventOperator.hasData(event.id)) {
                        // 修改
                        mEventOperator.update(event);
                    } else {
                        // 添加
                        mEventOperator.insert(event);
                    }

                    if (event.reminder != null) {
                        if (mReminderOperator.hasData(event.reminder.id)) {
                            mReminderOperator.update(event.reminder);
                        } else {
                            mReminderOperator.insert(event.reminder);
                        }
                    }
                }
            }
            IwdsLog.d(TAG, "Calendar event Backup successful!");
        }
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case MSG_REQUEST_CALENDAR:
            case MSG_UPDATE_CALENDAR:
                updateScheduleEvents();
                break;
            case MSG_SEND_CALENDAR:
                sendData(msg.obj);
                break;
            default:
                break;
            }
        };
    };

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // 监听到系统日期改变，需要更新
            mHandler.sendEmptyMessage(MSG_UPDATE_CALENDAR);
        }
    };

    @Override
    public void onRequest() {
        mHandler.sendEmptyMessage(MSG_REQUEST_CALENDAR);
    }

    @Override
    public void onRequestFailed() {
    }

    @Override
    public void onObjectArrived(ScheduleInfo object) {
    }

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        IwdsLog.d(TAG, "Calendar - On Link Connected " + isConnected);
        if (!isConnected) {
            mEventOperator.deleteAll();
            mReminderOperator.deleteAll();
            IwdsLog.d(TAG, "Remove all backup event successful!");
        }
    }

    @Override
    public void onChannelAvailable(boolean isAvailable) {
        IwdsLog.d(TAG, "Calendar - On Channel Available " + isAvailable);
    }

    @Override
    public void onSendResult(DataTransactResult result) {
    }

    /**
     * 将数据发送到手表端
     */
    private void sendData(Object info) {
        if (mModel == null) {
            return;
        }

        if (info != null) {
            // 发送数据
            IwdsLog.d(TAG, "send info " + info.toString());
            mModel.send((ScheduleInfo) info);
            // } else {
            // // 没有日程信息／日程没有任何改变
            // mModel.notifyRequestFailed();
        }
    }
}
