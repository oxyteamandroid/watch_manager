/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/WatchManager/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.calendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.ingenic.iwds.datatransactor.elf.ScheduleInfo;
import com.ingenic.iwds.datatransactor.elf.ScheduleInfo.Event;
import com.ingenic.watchmanager.db.Operator;
import com.ingenic.watchmanager.db.WatchManagerContracts;

/**
 * 日程事件数据操作对象
 * 
 * @author tZhang
 */

public class EventOperator extends Operator<ScheduleInfo.Event> {

    private String SELECTION = WatchManagerContracts.EventColumns._ID
            + " = ? and " + WatchManagerContracts.EventColumns.TITLE
            + " = ? and " + WatchManagerContracts.EventColumns.EVENTLOCATION
            + " = ? and " + WatchManagerContracts.EventColumns.DESCRIPTION
            + " = ? and " + WatchManagerContracts.EventColumns.DTSTART
            + " = ? and " + WatchManagerContracts.EventColumns.DTEND
            + " = ? and " + WatchManagerContracts.EventColumns.DURATION
            + " = ? and " + WatchManagerContracts.EventColumns.EVENTTIMEZONE
            + " = ? and " + WatchManagerContracts.EventColumns.ALLDAY
            + " = ? and " + WatchManagerContracts.EventColumns.HASALARM
            + " = ? and " + WatchManagerContracts.EventColumns.RRULE + " = ? ";

    public EventOperator(Context context) {
        super(context, WatchManagerContracts.Tables.EVENT);
    }

    @Override
    protected ContentValues toValues(ScheduleInfo.Event event) {
        ContentValues values = new ContentValues();
        values.put(WatchManagerContracts.EventColumns._ID, event.id);
        values.put(WatchManagerContracts.EventColumns.TITLE,
                event.title == null ? "null" : event.title);
        values.put(WatchManagerContracts.EventColumns.EVENTLOCATION,
                event.eventLocation == null ? "null" : event.eventLocation);
        values.put(WatchManagerContracts.EventColumns.DESCRIPTION,
                event.description == null ? "null" : event.description);
        values.put(WatchManagerContracts.EventColumns.DTSTART, event.dtStart);
        values.put(WatchManagerContracts.EventColumns.DTEND, event.dtEnd);
        values.put(WatchManagerContracts.EventColumns.DURATION,
                event.duration == null ? "null" : event.duration);
        values.put(WatchManagerContracts.EventColumns.EVENTTIMEZONE,
                event.eventTimezone == null ? "null" : event.eventTimezone);
        values.put(WatchManagerContracts.EventColumns.ALLDAY, event.allDay);
        values.put(WatchManagerContracts.EventColumns.HASALARM, event.hasAlarm);
        values.put(WatchManagerContracts.EventColumns.RRULE,
                event.rrule == null ? "null" : event.rrule);
        return values;
    }

    @Override
    public int update(ScheduleInfo.Event event) {
        return update(toValues(event), WatchManagerContracts.EventColumns._ID
                + " = ? ", new String[] { String.valueOf(event.id) });
    }

    @Override
    protected ScheduleInfo.Event fromCursor(Cursor cursor) {
        Event event = new Event();
        event.id = cursor.getLong(cursor
                .getColumnIndex(WatchManagerContracts.EventColumns._ID));
        event.title = cursor.getString(cursor
                .getColumnIndex(WatchManagerContracts.EventColumns.TITLE));
        event.eventLocation = cursor
                .getString(cursor
                        .getColumnIndex(WatchManagerContracts.EventColumns.EVENTLOCATION));
        event.description = cursor
                .getString(cursor
                        .getColumnIndex(WatchManagerContracts.EventColumns.DESCRIPTION));
        event.dtStart = cursor.getLong(cursor
                .getColumnIndex(WatchManagerContracts.EventColumns.DTSTART));
        event.dtEnd = cursor.getLong(cursor
                .getColumnIndex(WatchManagerContracts.EventColumns.DTEND));
        event.duration = cursor.getString(cursor
                .getColumnIndex(WatchManagerContracts.EventColumns.DURATION));
        event.eventTimezone = cursor
                .getString(cursor
                        .getColumnIndex(WatchManagerContracts.EventColumns.EVENTTIMEZONE));
        event.allDay = cursor.getInt(cursor
                .getColumnIndex(WatchManagerContracts.EventColumns.ALLDAY));
        event.hasAlarm = cursor.getInt(cursor
                .getColumnIndex(WatchManagerContracts.EventColumns.HASALARM));
        event.rrule = cursor.getString(cursor
                .getColumnIndex(WatchManagerContracts.EventColumns.RRULE));
        return event;
    }

    @Override
    public int delete(ScheduleInfo.Event event) {
        return delete(WatchManagerContracts.EventColumns._ID + " = ? ",
                new String[] { String.valueOf(event.id) });
    }

    @Override
    public boolean hasData(ScheduleInfo.Event event) {
        String[] selectionArgs = { String.valueOf(event.id),
                event.title == null ? "null" : event.title,
                event.eventLocation == null ? "null" : event.eventLocation,
                event.description == null ? "null" : event.description,
                String.valueOf(event.dtStart), String.valueOf(event.dtEnd),
                event.duration == null ? "null" : event.duration,
                event.eventTimezone == null ? "null" : event.eventTimezone,
                String.valueOf(event.allDay), String.valueOf(event.hasAlarm),
                event.rrule == null ? "null" : event.rrule };

        return query(null, SELECTION, selectionArgs, null, null, null) != null;
    }

    public boolean hasData(long event_id) {
        return query(null, WatchManagerContracts.EventColumns._ID + " = ? ",
                new String[] { String.valueOf(event_id) }, null, null, null) != null;
    }
}
