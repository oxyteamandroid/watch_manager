/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/WatchManager/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.calendar;

import com.ingenic.iwds.datatransactor.elf.ScheduleInfo;

/**
 * 记录了日程的事件信息，以及这条日程是否已经同步到手表端
 * 
 * @author tZhang
 */

public class EventInfo {

    /**
     * 存储日程的事件信息
     */
    ScheduleInfo.Event event;

    /**
     * 手否同步到了手表端
     */
    boolean isSync;

    public EventInfo() {
    }

    public EventInfo(ScheduleInfo.Event event, boolean isSync) {
        this.event = event;
        this.isSync = isSync;
    }

    @Override
    public String toString() {
        return event.toString() + ", isSync = " + isSync;
    }
}
