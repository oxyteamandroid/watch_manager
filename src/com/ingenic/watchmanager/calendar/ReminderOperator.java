/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/WatchManager/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.calendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.ingenic.iwds.datatransactor.elf.ScheduleInfo;
import com.ingenic.iwds.datatransactor.elf.ScheduleInfo.Reminder;
import com.ingenic.watchmanager.db.Operator;
import com.ingenic.watchmanager.db.WatchManagerContracts;

/**
 * 日程提醒数据操作对象
 * 
 * @author tZhang
 */

public class ReminderOperator extends Operator<ScheduleInfo.Reminder> {

    private static final String SQL_WHERE_HASDATA = WatchManagerContracts.ReminderColumns._ID + " = ? and "
            + WatchManagerContracts.ReminderColumns.EVENTID + " = ? and "
            + WatchManagerContracts.ReminderColumns.MINUTES + " = ? and "
            + WatchManagerContracts.ReminderColumns.METHOD + " = ? ";

    public ReminderOperator(Context context) {
        super(context, WatchManagerContracts.Tables.REMINDER);
    }

    @Override
    protected ContentValues toValues(Reminder reminder) {
        ContentValues values = new ContentValues();
        values.put(WatchManagerContracts.ReminderColumns._ID, reminder.id);
        values.put(WatchManagerContracts.ReminderColumns.EVENTID,
                reminder.eventId);
        values.put(WatchManagerContracts.ReminderColumns.MINUTES,
                reminder.minutes);
        values.put(WatchManagerContracts.ReminderColumns.METHOD,
                reminder.method);
        return values;
    }

    @Override
    public int update(Reminder reminder) {
        return update(toValues(reminder),
                WatchManagerContracts.ReminderColumns.EVENTID + " = ? ",
                new String[] { String.valueOf(reminder.eventId) });
    }

    @Override
    protected Reminder fromCursor(Cursor cursor) {
        Reminder reminder = new Reminder();
        reminder.id = cursor.getLong(cursor
                .getColumnIndex(WatchManagerContracts.ReminderColumns._ID));
        reminder.eventId = cursor.getLong(cursor
                .getColumnIndex(WatchManagerContracts.ReminderColumns.EVENTID));
        reminder.minutes = cursor.getLong(cursor
                .getColumnIndex(WatchManagerContracts.ReminderColumns.MINUTES));
        reminder.method = cursor.getLong(cursor
                .getColumnIndex(WatchManagerContracts.ReminderColumns.METHOD));
        return reminder;
    }

    @Override
    public int delete(Reminder reminder) {
        if (reminder.eventId >= 0) {
            return delete(WatchManagerContracts.ReminderColumns.EVENTID
                    + " = ? ",
                    new String[] { String.valueOf(reminder.eventId) });
        }

        return delete(WatchManagerContracts.ReminderColumns._ID + " = ? ",
                new String[] { String.valueOf(reminder.id) });
    }

    @Override
    public boolean hasData(Reminder reminder) {
        return query(
                null,
                SQL_WHERE_HASDATA,
                new String[] { String.valueOf(reminder.id),
                        String.valueOf(reminder.eventId),
                        String.valueOf(reminder.minutes),
                        String.valueOf(reminder.method) }, null, null, null) != null;
    }

    public boolean hasData(long reminder_id) {
        return query(null, WatchManagerContracts.ReminderColumns._ID + " = ? ",
                new String[] { String.valueOf(reminder_id) }, null, null, null) != null;
    }
}
