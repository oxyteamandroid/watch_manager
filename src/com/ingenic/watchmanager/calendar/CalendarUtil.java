/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/WatchManager/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.calendar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Instances;
import android.provider.CalendarContract.Reminders;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.format.Time;

import com.ingenic.iwds.datatransactor.elf.ScheduleInfo;
import com.ingenic.iwds.datatransactor.elf.ScheduleInfo.Event;
import com.ingenic.iwds.datatransactor.elf.ScheduleInfo.Reminder;

/**
 * 日程同步工具类
 * 
 * @author tZhang
 */

public class CalendarUtil {

    static final String TAG = CalendarUtil.class.getSimpleName();

    public static final String REMIDER_WHERE = Instances.EVENT_ID + "=?";
    private static final String DISPLAY_AS_ALLDAY = "dispAllday";
    private static final String EVENTS_WHERE = DISPLAY_AS_ALLDAY + "=0";
    private static final String ALLDAY_WHERE = DISPLAY_AS_ALLDAY + "=1";
    private static final String SORT_EVENTS_BY = "begin ASC, end DESC, title ASC";
    private static final String SORT_ALLDAY_BY = "startDay ASC, endDay DESC, title ASC";

    private static final String[] EVENT_PROJECTION = new String[] {
            Instances.EVENT_ID,
            Instances.TITLE,
            Instances.EVENT_LOCATION,
            Instances.DESCRIPTION,
            Instances.BEGIN,
            Instances.END,
            Instances.EVENT_TIMEZONE,
            Instances.DURATION,
            Instances.ALL_DAY,
            Instances.HAS_ALARM,
            Instances.RRULE,
            Instances.ALL_DAY + "=1 OR (" + Instances.END + "-"
                    + Instances.BEGIN + ")>=" + DateUtils.DAY_IN_MILLIS
                    + " AS " + DISPLAY_AS_ALLDAY };

    // The indices for the projection array above.
    private static final int PROJECTION_EVENT_ID_INDEX = 0;
    private static final int PROJECTION_TITLE_INDEX = 1;
    private static final int PROJECTION_LOCATION_INDEX = 2;
    private static final int PROJECTION_DESCRIPTION_INDEX = 3;
    private static final int PROJECTION_BEGIN_INDEX = 4;
    private static final int PROJECTION_END_INDEX = 5;
    private static final int PROJECTION_TIMEZONE_INDEX = 6;
    private static final int PROJECTION_DURATION_INDEX = 7;
    private static final int PROJECTION_ALL_DAY_INDEX = 8;
    private static final int PROJECTION_HAS_ALARM_INDEX = 9;
    private static final int PROJECTION_RRULE_INDEX = 10;

    private static final String[] REMINDERS_PROJECTION = new String[] {
            Reminders._ID, Reminders.MINUTES, Reminders.METHOD };

    // The indices for the projection array above.
    private static final int PROJECTION_REMINDERS_ID_INDEX = 0;
    private static final int PROJECTION_MINUTES_INDEX = 1;
    private static final int PROJECTION_METHOD_INDEX = 2;

    /**
     * 从手机获取今天前后一个月的日程信息(包括今天)
     * 
     * @return
     */
    public static List<Event> getScheduleFromPhone(ContentResolver resolver) {
        Time time = new Time();
        time.setToNow();
        int start = Time.getJulianDay(time.toMillis(false), time.gmtoff) - 30;
        int end = start + 60;

        Cursor cEvents = instancesQuery(resolver, EVENT_PROJECTION, start, end,
                EVENTS_WHERE, null, SORT_EVENTS_BY);

        Cursor cAllday = instancesQuery(resolver, EVENT_PROJECTION, start, end,
                ALLDAY_WHERE, null, SORT_ALLDAY_BY);

        List<ScheduleInfo.Event> events = new ArrayList<ScheduleInfo.Event>();
        buildEventsFromCursor(cEvents, events);
        buildEventsFromCursor(cAllday, events);

        buildReminderForEvents(resolver, events);

        return events;
    }

    /**
     * 获取所有备份的日程信息
     * 
     * @return
     */
    public static List<Event> getScheduleFromBackups(
            EventOperator eventOperator, ReminderOperator reminderOperator) {

        // 查询所有备份的日程信息
        List<ScheduleInfo.Event> events = eventOperator.queryAll();

        // 查询日程提醒
        if (events != null) {
            for (ScheduleInfo.Event event : events) {
                if (event.hasAlarm != 0) {
                    List<Reminder> reminders = reminderOperator.queryAll(
                            CalendarUtil.REMIDER_WHERE,
                            new String[] { String.valueOf(event.id) }, null);
                    if (reminders != null && reminders.size() > 0) {
                        event.reminder = reminders.get(0);
                    }
                }
            }
        }

        return events;
    }

    /**
     * Performs a query to return all visible instances in the given range that
     * match the given selection. This is a blocking function and should not be
     * done on the UI thread. This will cause an expansion of recurring events
     * to fill this time range if they are not already expanded and will slow
     * down for larger time ranges with many recurring events.
     * 
     * @param cr
     *            The ContentResolver to use for the query
     * @param projection
     *            The columns to return
     * @param begin
     *            The start of the time range to query in UTC millis since epoch
     * @param end
     *            The end of the time range to query in UTC millis since epoch
     * @param selection
     *            Filter on the query as an SQL WHERE statement
     * @param selectionArgs
     *            Args to replace any '?'s in the selection
     * @param orderBy
     *            How to order the rows as an SQL ORDER BY statement
     * @return A Cursor of instances matching the selection
     */
    private static final Cursor instancesQuery(ContentResolver cr,
            String[] projection, int startDay, int endDay, String selection,
            String[] selectionArgs, String orderBy) {
        String WHERE_CALENDARS_SELECTED = Calendars.VISIBLE + "=?";
        String[] WHERE_CALENDARS_ARGS = { "1" };
        String DEFAULT_SORT_ORDER = "begin ASC";

        Uri.Builder builder = Instances.CONTENT_BY_DAY_URI.buildUpon();
        ContentUris.appendId(builder, startDay);
        ContentUris.appendId(builder, endDay);
        if (TextUtils.isEmpty(selection)) {
            selection = WHERE_CALENDARS_SELECTED;
            selectionArgs = WHERE_CALENDARS_ARGS;
        } else {
            selection = "(" + selection + ") AND " + WHERE_CALENDARS_SELECTED;
            if (selectionArgs != null && selectionArgs.length > 0) {
                selectionArgs = Arrays.copyOf(selectionArgs,
                        selectionArgs.length + 1);
                selectionArgs[selectionArgs.length - 1] = WHERE_CALENDARS_ARGS[0];
            } else {
                selectionArgs = WHERE_CALENDARS_ARGS;
            }
        }
        return cr.query(builder.build(), projection, selection, selectionArgs,
                orderBy == null ? DEFAULT_SORT_ORDER : orderBy);
    }

    private static void buildEventsFromCursor(Cursor cursor,
            List<ScheduleInfo.Event> events) {
        if (cursor == null) {
            return;
        }

        if (events == null) {
            events = new ArrayList<ScheduleInfo.Event>();
        }

        if (cursor.moveToFirst()) {
            do {
                ScheduleInfo.Event event = buildEventFromCursor(cursor);
                events.add(event);
            } while (cursor.moveToNext());
        }
        cursor.close();
    }

    private static ScheduleInfo.Event buildEventFromCursor(Cursor cursor) {
        ScheduleInfo.Event event = new ScheduleInfo.Event();
        event.id = cursor.getLong(PROJECTION_EVENT_ID_INDEX);
        event.title = cursor.getString(PROJECTION_TITLE_INDEX);
        event.eventLocation = cursor.getString(PROJECTION_LOCATION_INDEX);
        event.description = cursor.getString(PROJECTION_DESCRIPTION_INDEX);
        event.dtStart = cursor.getLong(PROJECTION_BEGIN_INDEX);
        event.dtEnd = cursor.getLong(PROJECTION_END_INDEX);
        event.eventTimezone = cursor.getString(PROJECTION_TIMEZONE_INDEX);
        event.duration = cursor.getString(PROJECTION_DURATION_INDEX);
        event.allDay = cursor.getInt(PROJECTION_ALL_DAY_INDEX);
        event.hasAlarm = cursor.getInt(PROJECTION_HAS_ALARM_INDEX);
        event.rrule = cursor.getString(PROJECTION_RRULE_INDEX);
        return event;
    }

    private static void buildReminderForEvents(ContentResolver cr,
            List<ScheduleInfo.Event> events) {
        for (ScheduleInfo.Event event : events) {
            if (event.hasAlarm != 0) {
                Cursor cursor = cr.query(Reminders.CONTENT_URI,
                        REMINDERS_PROJECTION, REMIDER_WHERE,
                        new String[] { String.valueOf(event.id) }, null);

                ScheduleInfo.Reminder reminder = buildReminderFromCursor(cursor);
                if (reminder != null) {
                    reminder.eventId = event.id;
                    event.reminder = reminder;
                }
            }
        }
    }

    private static Reminder buildReminderFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }

        ScheduleInfo.Reminder reminder = null;
        if (cursor.moveToFirst()) {
            reminder = new ScheduleInfo.Reminder();
            reminder.id = cursor.getLong(PROJECTION_REMINDERS_ID_INDEX);
            reminder.minutes = cursor.getLong(PROJECTION_MINUTES_INDEX);
            reminder.method = cursor.getLong(PROJECTION_METHOD_INDEX);
        }

        cursor.close();
        return reminder;
    }

    public static List<ScheduleInfo.Event> diff(ContentResolver resolver,
            EventOperator eventOperator, ReminderOperator reminderOperator) {
        List<ScheduleInfo.Event> newEvents = getScheduleFromPhone(resolver);
        List<ScheduleInfo.Event> backupEvents = getScheduleFromBackups(
                eventOperator, reminderOperator);

        if (backupEvents == null) {
            // 没有备份日程信息，判定为全部添加
            return newEvents;
        }

        if (newEvents != null) {
            // 有添加/删除/修改，需要遍历比较
            for (int i = 0; i < newEvents.size(); i++) {
                for (int j = 0; j < backupEvents.size(); j++) {
                    if ((newEvents.get(i).toString()).equals(backupEvents
                            .get(j).toString())) {
                        // IwdsLog.d(TAG, "已同步，不需要再发送 "
                        // + backupEvents.get(j).toString());
                        // 没有改变的日程事件
                        newEvents.remove(i);
                        backupEvents.remove(j);
                        i--;
                        break;
                    } else {
                        if (newEvents.get(i).id == backupEvents.get(j).id) {
                            // IwdsLog.d(TAG, "已同步，但有修改，任需要再发送 "
                            // + newEvents.get(i).toString());
                            // 修改了的日程事件
                            backupEvents.remove(j);
                            j--;
                        }
                    }
                }
            }
        }

        // 需要删除的日程事件做特殊处理
        for (int i = 0; i < backupEvents.size(); i++) {
            backupEvents.get(i).dtStart = -1;
            backupEvents.get(i).dtEnd = -1;
        }

        if (newEvents == null) {
            // 历史日程事件被全部清除
            return backupEvents;
        } else {
            // diff完后需要添加/修改/或删除的日程事件
            newEvents.addAll(backupEvents);
            return newEvents;
        }

    }

    public static List<Event> scheduleInfoToEventList(ScheduleInfo info) {
        if (info == null) {
            return null;
        }

        List<ScheduleInfo.Event> events = new ArrayList<ScheduleInfo.Event>();
        ScheduleInfo.Event[] eventArray = info.event;

        for (ScheduleInfo.Event event : eventArray) {
            events.add(event);
        }

        return events;
    }

    public static ScheduleInfo EventListToscheduleInfo(
            List<ScheduleInfo.Event> events) {
        if (events == null || events.size() <= 0) {
            return null;
        }

        int count = events.size();
        ScheduleInfo info = new ScheduleInfo(count);

        for (int i = 0; i < count; i++) {
            info.event[i] = events.get(i);
        }

        return info;
    }
}
