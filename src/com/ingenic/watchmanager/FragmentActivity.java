/*
 * Copyright (C) 2014 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

/**
 * FragmentActivity用来辅助显示Fragment的视图界面。
 */
public class FragmentActivity extends WMActivity {
    /**
     * Fragment的标识值在传入的{@link Intent}中对应的键。
     */
    public static final String EXTRA_TAG = "tag";

    /**
     * 可用于创建Fragment实例的字符串在传入的{@link Intent}中对应的键。
     */
    public static final String EXTRA_FRAGMENT = "fragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        init(intent);
    }

    private void init(Intent intent) {
        String fragmentString = intent.getStringExtra(EXTRA_FRAGMENT);
        if (fragmentString != null) {
            Fragment fragment = Fragment.instantiate(this, fragmentString);
            String tag = intent.getStringExtra(EXTRA_TAG);
            if (tag == null) {
                tag = WMFragment.DEFAULT_FRAGMENT_TAG;
            }

            show(fragment, false, tag);
        }
    }
}
