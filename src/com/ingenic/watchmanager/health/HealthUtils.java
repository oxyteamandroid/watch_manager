/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/???/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.health;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.animation.OvershootInterpolator;

import com.ingenic.watchmanager.view.CircularProgressDrawable;

/**
 * 健康与运动工具类
 *
 * @author tZhang
 */

public class HealthUtils {

    /**
     * 一段动画的播放时长
     */
    public static int ANIM_DURATION = 1200;

    /**
     * 加载动画设置
     * @param from
     * @param to
     * @param startDelay
     * @return
     */
    public static Animator prepareProgressAnimation(
            CircularProgressDrawable drawable, Animator anim, float from,
            float to, long startDelay) {
        if (anim != null) {
            anim.cancel();
        }

        anim = new AnimatorSet();
        ObjectAnimator progressBackAnimation = ObjectAnimator.ofFloat(drawable,
                CircularProgressDrawable.PROGRESS_PROPERTY, from, 0f);
        progressBackAnimation.setStartDelay(startDelay);
        progressBackAnimation.setDuration(ANIM_DURATION);
        progressBackAnimation.setInterpolator(new OvershootInterpolator());

        ObjectAnimator progressAnimation = ObjectAnimator.ofFloat(drawable,
                CircularProgressDrawable.PROGRESS_PROPERTY, 0f, to);
        progressAnimation.setStartDelay(ANIM_DURATION);
        progressAnimation.setDuration(ANIM_DURATION);
        progressAnimation.setInterpolator(new OvershootInterpolator());

        ((AnimatorSet) anim).playTogether(progressBackAnimation,
                progressAnimation);
        return anim;
    }

    public static int dp2Px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }
}
