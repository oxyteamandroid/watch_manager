/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.health;

import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.ingenic.watchmanager.db.Operator;
import com.ingenic.watchmanager.db.WatchManagerContracts;

public class EverydayStepsOperator extends Operator<EverydayStepsInfo> {

    public EverydayStepsOperator(Context context) {
        super(context, WatchManagerContracts.Tables.EVERYDAY_STEPS);
    }

    @Override
    protected ContentValues toValues(EverydayStepsInfo info) {
        ContentValues values = new ContentValues();
        // values.put(WatchManagerContracts.EverydayStepsColumns.ID, info.id);
        values.put(WatchManagerContracts.EverydayStepsColumns.STEPS, info.steps);
        values.put(WatchManagerContracts.EverydayStepsColumns.DATE, info.date);
        return values;

    }

    @Override
    public int update(EverydayStepsInfo t) {
        ContentValues values = new ContentValues();
        // values.put(WatchManagerContracts.EverydayStepsColumns.ID, t.id);
        values.put(WatchManagerContracts.EverydayStepsColumns.STEPS, t.steps);
        values.put(WatchManagerContracts.EverydayStepsColumns.DATE, t.date);
        return update(values, WatchManagerContracts.EverydayStepsColumns.DATE
                + " = ? ", new String[] { "" + t.date });
    }

    @Override
    protected EverydayStepsInfo fromCursor(Cursor cursor) {
        EverydayStepsInfo info = new EverydayStepsInfo();
        info.id = cursor.getInt(cursor
                .getColumnIndex(WatchManagerContracts.EverydayStepsColumns.ID));
        info.steps = cursor
                .getInt(cursor
                        .getColumnIndex(WatchManagerContracts.EverydayStepsColumns.STEPS));
        info.date = cursor
                .getString(cursor
                        .getColumnIndex(WatchManagerContracts.EverydayStepsColumns.DATE));
        return info;
    }

    @Override
    public int delete(EverydayStepsInfo t) {
        return delete(
                WatchManagerContracts.EverydayStepsColumns.DATE + " = ? ",
                new String[] { "" + t.date });
    }

    @Override
    public boolean hasData(EverydayStepsInfo t) {
        EverydayStepsInfo c = query(null,
                WatchManagerContracts.EverydayStepsColumns.DATE + " = ? ",
                new String[] { "" + t.date }, null, null, null);
        return c != null;
    }

    @Override
    public List<EverydayStepsInfo> queryAll() {
        return queryAll(WatchManagerContracts.EverydayStepsColumns.DATE);
    }
}
