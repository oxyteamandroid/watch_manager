/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/watchmanager Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.health;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import a_vcard.android.text.Spanned;
import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.SubscriptSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.db.WatchManagerContracts;

/**
 * 心率界面
 * 
 * @author tzhang
 * 
 */
public class HeartRateFragment extends Fragment {

    static final String TAG = "HeartRateFragment";

    /**
     * 日期
     */
    private TextView mDate;

    /**
     * 心率字体
     */
    private Typeface mHeartRateTf;

    /**
     * 日期设置
     */
    private SimpleDateFormat format = new SimpleDateFormat("M/d/yyyy");
    private Date date;

    /**
     * 最近一次测量的心率Text
     */
    private TextView mCurrentHeartRate;

    /**
     * 最近一次测量的心率Number
     */
    private int mCurrentHeartRateNumber = 0;

    /**
     * 本日最低Text
     */
    private TextView mTodayMinRate;

    /**
     * 本日最高Text
     */
    private TextView mTodayMaxRate;

    /**
     * 本日最低Number
     */
    private int mTodayMinRateNum = 0;

    /**
     * 本日最高
     */
    private int mTodayMaxRateNum = 0;

    /**
     * 心率数据操作对象
     */
    private HeartRateOperator mHeartRateOperator;

    /**
     * 今天开始时间
     */
    private long today_start_time = 0;

    /**
     * 今天结束时间
     */
    private long today_end_time = 0;

    /**
     * 心率折线图
     */
    private LineChart mRateLineChart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.health_heart_rate_layout, null);

        initView(view);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        setDate();

        mHeartRateTf = Typeface.createFromAsset(getResources().getAssets(),
                "fonts/watch_font.ttf");

        IntentFilter filter = new IntentFilter();
        filter.addAction(HealthExerciseService.PLAY_LINECHART_ANIM_ACTION);
        filter.addAction(HealthExerciseService.UPDATE_UI_ACTION);
        filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        filter.addAction(Intent.ACTION_TIME_CHANGED);
        filter.addAction(Intent.ACTION_DATE_CHANGED);
        activity.registerReceiver(mReceiver, filter);
        mHeartRateOperator = new HeartRateOperator(activity);

    }

    /**
     * 设置日期
     */
    private void setDate() {
        date = new Date(System.currentTimeMillis());
        Calendar mCalendar = Calendar.getInstance();
        mCalendar.setTimeInMillis(System.currentTimeMillis());

        // 获取开始时间,将当前时间的时、分、秒、毫秒清0
        mCalendar.set(Calendar.HOUR_OF_DAY, 0);
        mCalendar.set(Calendar.MINUTE, 0);
        mCalendar.set(Calendar.SECOND, 0);
        mCalendar.set(Calendar.MILLISECOND, 0);
        today_start_time = mCalendar.getTimeInMillis();

        // 获取结束时间
        today_end_time = today_start_time + 24 * 60 * 60 * 1000;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(mReceiver);
    }

    /**
     * 初始化
     * 
     * @param view
     */
    private void initView(View view) {

        // 设置日期
        mDate = (TextView) view.findViewById(R.id.health_heart_rate_date);
        mDate.setTypeface(mHeartRateTf);
        mDate.setText(format.format(date));

        // 设置心率值
        mCurrentHeartRate = (TextView) view
                .findViewById(R.id.health_heart_rate_number);
        mCurrentHeartRate.setTypeface(mHeartRateTf);
        mCurrentHeartRate.setText(mCurrentHeartRateNumber + "");

        // 设置本日最高
        mTodayMinRate = (TextView) view
                .findViewById(R.id.health_heart_rate_min_num);
        mTodayMinRate.setTypeface(mHeartRateTf);
        mTodayMinRate.setText(setSubscriptUnit(mTodayMinRateNum + ""));

        // 设置本日最高
        mTodayMaxRate = (TextView) view
                .findViewById(R.id.health_heart_rate_max_num);
        mTodayMaxRate.setTypeface(mHeartRateTf);
        mTodayMaxRate.setText(setSubscriptUnit(mTodayMaxRateNum + ""));

        // 设置心率折线图
        mRateLineChart = (LineChart) view.findViewById(R.id.heart_rate_chart);
        mRateLineChart.setDrawGridBackground(false);
        mRateLineChart.setDescription("");
        mRateLineChart.getXAxis();
        mRateLineChart.setNoDataText(getResources().getString(R.string.health_rate_no_data));

        // 设置横向坐标
        mRateLineChart.getLegend().setEnabled(false);

        XAxis xAxis = mRateLineChart.getXAxis();
        xAxis.setTextSize(12f);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);
        xAxis.setSpaceBetweenLabels(1);

        YAxis leftAxis = mRateLineChart.getAxisLeft();
        leftAxis.setTextColor(ColorTemplate.getHoloBlue());
        leftAxis.setAxisMaxValue(200f);
        leftAxis.setDrawGridLines(true);

        YAxis rightAxis = mRateLineChart.getAxisRight();
        rightAxis.setTextColor(Color.RED);
        rightAxis.setAxisMaxValue(200f);
        rightAxis.setDrawGridLines(false);

        // 加载数据
        loadData();

    }

    /**
     * 加载心率数据
     */
    private void loadData() {
        new AsyncTask<Void, Void, List<HeartRateInfo>>() {

            @Override
            protected List<HeartRateInfo> doInBackground(Void... params) {
                return mHeartRateOperator.queryAll(
                        WatchManagerContracts.HeartRatesColumns.TIME
                                + " >= ? and "
                                + WatchManagerContracts.HeartRatesColumns.TIME
                                + " < ?", new String[] { today_start_time + "",
                                today_end_time + "" },
                        WatchManagerContracts.HeartRatesColumns.TIME + " asc");
            }

            @Override
            protected void onPostExecute(List<HeartRateInfo> result) {
                super.onPostExecute(result);

                if (result != null && result.size() > 0) {
                    int size = result.size();

                    // 记录当前
                    mCurrentHeartRateNumber = result.get(size - 1).rate;

                    // 统计最低心率和最高心率值
                    mTodayMinRateNum = result.get(0).rate;
                    mTodayMaxRateNum = result.get(0).rate;

                    for (int i = 1; i < size; i++) {
                        int rate = result.get(i).rate;
                        if (mTodayMinRateNum > rate) {
                            mTodayMinRateNum = rate;
                        }

                        if (mTodayMaxRateNum < rate) {
                            mTodayMaxRateNum = rate;
                        }
                    }

                    // 设置显示数据
                    mCurrentHeartRate.setText(mCurrentHeartRateNumber + "");
                    mTodayMinRate.setText(setSubscriptUnit(mTodayMinRateNum
                            + ""));
                    mTodayMaxRate.setText(setSubscriptUnit(mTodayMaxRateNum
                            + ""));

                    // 设置折线图数据
                    setData(result);
                }

            }
        }.execute();
    }

    /**
     * 设置心率下标单位
     * 
     * @param str
     * @return
     */
    private SpannableString setSubscriptUnit(String str) {
        SpannableString msp = new SpannableString(str + " bpm");
        int len = msp.length();
        msp.setSpan(new SubscriptSpan(), len - 3, len,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        msp.setSpan(new AbsoluteSizeSpan(14, true), len - 3, len,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return msp;
    }

    private SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");

    private void setData(List<HeartRateInfo> rates) {

        if (rates == null || rates.size() == 0) {
            return;
        }

        int count = rates.size();

        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<Entry> yVals1 = new ArrayList<Entry>();
        for (int i = 0; i < count; i++) {
            date = new Date(rates.get(i).time);

            xVals.add(formatTime.format(date));

            yVals1.add(new Entry(rates.get(i).rate, i));
        }

        // create a dataset and give it a type
        LineDataSet dataset = new LineDataSet(yVals1, null);
        dataset.setAxisDependency(AxisDependency.LEFT);
        dataset.setColor(ColorTemplate.getHoloBlue());
        dataset.setCircleColor(Color.WHITE);
        dataset.setLineWidth(2f);
        dataset.setCircleSize(3f);
        dataset.setFillAlpha(65);
        dataset.setFillColor(ColorTemplate.getHoloBlue());
        dataset.setHighLightColor(Color.rgb(244, 117, 117));
        dataset.setDrawCircleHole(false);

        // create a data object with the datasets
        LineData data = new LineData(xVals);
        data.addDataSet(dataset);
        data.setValueTextColor(Color.WHITE);
        data.setValueTextSize(9f);

        // set data
        mRateLineChart.setData(data);
    }

    /**
     * 广播接受器（播放折线图动画和刷新界面）
     */
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (HealthExerciseService.PLAY_LINECHART_ANIM_ACTION.equals(action)) {
                mRateLineChart.animateX(1000);
            } else if (HealthExerciseService.UPDATE_UI_ACTION.equals(action)) {
                // 刷新界面
                IwdsLog.d(TAG, "Refresh UI");
                update();
            } else {
                // 时间改变
                setDate();
                if(mDate != null){
                    mDate.setText(format.format(date));
                }
                update();
            }
        }

        private void update() {
            loadData();
            mRateLineChart.invalidate();
            mRateLineChart.animateX(1000);
        }
    };

}