/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/watchmanager Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.health;

import a_vcard.android.provider.BaseColumns;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.ingenic.watchmanager.db.Operator;
import com.ingenic.watchmanager.db.WatchManagerContracts;

public class HeartRateOperator extends Operator<HeartRateInfo> {

    public HeartRateOperator(Context context) {
        super(context, WatchManagerContracts.Tables.HEART_RATES);
    }

    @Override
    protected ContentValues toValues(HeartRateInfo info) {
        ContentValues values = new ContentValues();
        values.put(WatchManagerContracts.HeartRatesColumns.RATE, info.rate);
        values.put(WatchManagerContracts.HeartRatesColumns.TIME, info.time);
        return values;
    }

    @Override
    public int update(HeartRateInfo info) {
        return update(toValues(info),
                WatchManagerContracts.HeartRatesColumns.TIME + " = ? ",
                new String[] { "" + info.time });
    }

    @Override
    protected HeartRateInfo fromCursor(Cursor cursor) {
        HeartRateInfo info = new HeartRateInfo();
        info.id = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID));
        info.rate = cursor.getInt(cursor
                .getColumnIndex(WatchManagerContracts.HeartRatesColumns.RATE));
        info.time = cursor.getLong(cursor
                .getColumnIndex(WatchManagerContracts.HeartRatesColumns.TIME));
        return info;
    }

    @Override
    public int delete(HeartRateInfo info) {
        return delete(WatchManagerContracts.HeartRatesColumns.TIME + " = ? ",
                new String[] { "" + info.time });
    }

    @Override
    public boolean hasData(HeartRateInfo info) {
        HeartRateInfo c = query(null,
                WatchManagerContracts.HeartRatesColumns.TIME + " = ? ",
                new String[] { "" + info.time }, null, null, null);
        return c != null;
    }
}
