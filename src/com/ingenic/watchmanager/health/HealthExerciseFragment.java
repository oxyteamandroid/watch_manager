/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/watchmanager Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.health;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.view.UnderlinePageIndicator;

public class HealthExerciseFragment extends Fragment implements
        OnPageChangeListener {

    /**
     * 各页面index
     */
    private final int HEALTH_STEP_INDEX = 0;
//    private final int HEALTH_SLEEP_INDEX = 1;
//    private final int HEALTH_RATE_INDEX = 2;
    private final int HEALTH_RATE_INDEX = 1;
    private final int HEALTH_SLEEP_INDEX = 2;

    /**
     * 健康运动主ViewPager
     */
    private ViewPager mViewPager;

    /**
     * 指示器
     */
    private UnderlinePageIndicator mPageIndicator;

    /**
     * 指示器Radio
     */
    private RadioGroup mRadioGroup;
    private RadioButton mStepRadio; // 计步
    private RadioButton mSleepRadio; // 睡眠监测
    private RadioButton mRateRadio; // 心率

    /**
     * 计步/心率Fragment List
     */
    private ArrayList<Fragment> mList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_health_exercise, null);

        init(view);

        return view;
    }

    /**
     * 初始化
     */
    @SuppressLint("NewApi")
    private void init(View view) {
        mViewPager = (ViewPager) view.findViewById(R.id.health_exercise_vp);

        mRadioGroup = (RadioGroup) view
                .findViewById(R.id.health_exercise_title);
        mStepRadio = (RadioButton) view.findViewById(R.id.step_radio_btn);
        mSleepRadio = (RadioButton) view.findViewById(R.id.sleep_radio_btn);
        mRateRadio = (RadioButton) view.findViewById(R.id.rate_radio_btn);

        mPageIndicator = (UnderlinePageIndicator) view
                .findViewById(R.id.health_exercise_page_indicator);

        mList = new ArrayList<Fragment>();

        StepCounterFragment stepCounterFragment = new StepCounterFragment();
        mList.add(HEALTH_STEP_INDEX, stepCounterFragment);

//        SleepMonitorFragment sleepMonitorFragment = new SleepMonitorFragment();
//        mList.add(HEALTH_SLEEP_INDEX, sleepMonitorFragment);

        HeartRateFragment heartRateFragment = new HeartRateFragment();
        mList.add(HEALTH_RATE_INDEX, heartRateFragment);

        HealthFragmentPagerAdapter mAdapter = null;
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            mAdapter = new HealthFragmentPagerAdapter(getChildFragmentManager());

        } else {
            mAdapter = new HealthFragmentPagerAdapter(getFragmentManager());

        }

        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setAdapter(mAdapter);

        mPageIndicator.setViewPager(mViewPager);
        mPageIndicator.setOnPageChangeListener(this);

        // 设置标题（单选）按钮监听
        mRadioGroup
                .setOnCheckedChangeListener(new OnCheckedChangeListenerImpl());
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
        case HEALTH_STEP_INDEX:
            mStepRadio.setChecked(true);
            mSleepRadio.setChecked(false);
            mRateRadio.setChecked(false);

            // 播放柱状图动画
            getActivity()
                    .sendBroadcast(
                            new Intent(
                                    HealthExerciseService.PLAY_BARCHART_ANIM_ACTION));
            break;

        case HEALTH_SLEEP_INDEX:
            mStepRadio.setChecked(false);
            mSleepRadio.setChecked(true);
            mRateRadio.setChecked(false);

            // 播放圆形进度动画
            getActivity()
                    .sendBroadcast(
                            new Intent(
                                    HealthExerciseService.PLAY_SLEEP_ANIM_ACTION));
            break;

        case HEALTH_RATE_INDEX:
            mStepRadio.setChecked(false);
            mSleepRadio.setChecked(false);
            mRateRadio.setChecked(true);

            // 播放折线图动画
            getActivity()
                    .sendBroadcast(
                            new Intent(
                                    HealthExerciseService.PLAY_LINECHART_ANIM_ACTION));
            break;
        default:
            break;
        }
    }

    private class HealthFragmentPagerAdapter extends FragmentPagerAdapter {

        public HealthFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            if (mList != null) {
                return mList.size();
            }
            return 0;
        }

        @Override
        public Fragment getItem(int position) {
            return mList.get(position);
        }

    }

    private class OnCheckedChangeListenerImpl implements
            OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
            case R.id.step_radio_btn:
                mViewPager.setCurrentItem(HEALTH_STEP_INDEX);
                break;

            case R.id.sleep_radio_btn:
                mViewPager.setCurrentItem(HEALTH_SLEEP_INDEX);
                break;

            case R.id.rate_radio_btn:
                mViewPager.setCurrentItem(HEALTH_RATE_INDEX);
                break;

            default:
                break;
            }
        }

    }
}
