/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/watchmanager Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.health;

import java.util.Calendar;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.HealthInfo;
import com.ingenic.iwds.datatransactor.elf.HealthTransactionModel;
import com.ingenic.iwds.datatransactor.elf.HealthTransactionModel.HealthTransactionModelCallback;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.util.UUIDS;

/**
 * 健康&运动同步服务
 * 
 * @author tzhang
 * 
 */
public class HealthExerciseService extends Service implements
        HealthTransactionModelCallback {

    static final String TAG = "HealthExerciseService";

    public static final String PLAY_BARCHART_ANIM_ACTION = "com.ingenic.action.STEP_BARCHART_ANIM";
    public static final String PLAY_LINECHART_ANIM_ACTION = "com.ingenic.action.RATE_LINECHART_ANIM";
    public static final String PLAY_SLEEP_ANIM_ACTION = "com.ingenic.action.PLAY_SLEEP_ANIM_ACTION";
    public static final String UPDATE_UI_ACTION = "com.ingenic.action.UPDATE_HEALTH_UI";

    private static HealthTransactionModel mTransactionModel;

    private TodayStepsOperator mTodayStepsOperator;
    private EverydayStepsOperator mEverydayStepsOperator;
    private HeartRateOperator mHeartRateOperator;

    private Calendar mCalendar = Calendar.getInstance();

    private Intent mUpdateIntent;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // 初始化数据传输模型
        if (mTransactionModel == null) {
            mTransactionModel = new HealthTransactionModel(this, this,
                    UUIDS.HEALTH);
        }

        mTransactionModel.start();

        // 初始化计步数据操作对象
        mTodayStepsOperator = new TodayStepsOperator(this);
        mEverydayStepsOperator = new EverydayStepsOperator(this);
        mHeartRateOperator = new HeartRateOperator(this);

        mUpdateIntent = new Intent(UPDATE_UI_ACTION);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // 关闭数据传输模型
        if (mTransactionModel != null) {
            mTransactionModel.stop();
            IwdsLog.d(TAG, "HealthTransactionModel is stoped!");
        }

        // 重启到当前服务
        sendBroadcast(new Intent(RestartHealthServiceReceiver.RESTART_SERVICE));
    }

    @Override
    public void onRequest() {

    }

    @Override
    public void onRequestFailed() {

    }

    @Override
    public void onObjectArrived(HealthInfo info) {
        if (info == null) {
            IwdsLog.i(TAG, "HealthModel - HealthInfo is null!");
            return;
        }

        IwdsLog.i(TAG,
                "HealthModel - on object arrived is : " + info.toString());

        long time = info.nowDate;

        // 将当前事件的时、分、秒、毫秒清0，从而获得年月日的TimeInMillis
        mCalendar.setTimeInMillis(time);
        mCalendar.set(Calendar.HOUR_OF_DAY, 0);
        mCalendar.set(Calendar.MINUTE, 0);
        mCalendar.set(Calendar.SECOND, 0);
        mCalendar.set(Calendar.MILLISECOND, 0);
        time = mCalendar.getTimeInMillis();

        // 保存今天的健康运动数据到本地
        for (int i = 0; i < info.days.length; i++) {

            if (null != info.days[i] && info.days[i].trim().length() > 0) {
                TodayStepsInfo tsi = new TodayStepsInfo();
                tsi.hour = (i * 2);
                tsi.steps = Integer.parseInt(info.days[i]);
                tsi.date = String.valueOf(time);
                mTodayStepsOperator.save(tsi);
            }
        }

        // 保存本周的健康运动数据到本地
        for (int i = 0; i < info.weeks.length; i++) {
            if (null != info.weeks[i] && info.weeks[i].trim().length() > 0) {
                EverydayStepsInfo tsi = new EverydayStepsInfo();
                tsi.steps = Integer.parseInt(info.weeks[i]);
                tsi.date = String.valueOf(time - (6 - i) * 24 * 60 * 60 * 1000);
                mEverydayStepsOperator.save(tsi);
            }
        }

        // 保存心率数据到本地
        String[] rates = info.rates.split("A");

        for (int i = 0; i < rates.length; i++) {
            if (null != rates[i] && rates[i].trim().length() > 0) {
                String[] s = rates[i].split(",");
                HeartRateInfo hri = new HeartRateInfo();
                hri.rate = Integer.parseInt(s[0]);
                hri.time = Long.parseLong(s[1]);
                mHeartRateOperator.save(hri);
            }
        }

        // 更新界面UI
        sendBroadcast(mUpdateIntent);
    }

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        IwdsLog.i(TAG,
                "HealthModel - DeviceDescriptor:" + descriptor.toString()
                        + " Link connected is : " + isConnected);
    }

    @Override
    public void onChannelAvailable(boolean isAvailable) {
        IwdsLog.i(TAG, "HealthModel - on Channel Available is : " + isAvailable);

        if (isAvailable) {
            mTransactionModel.request();
        }
    }

    @Override
    public void onSendResult(DataTransactResult result) {
        if (result.getResultCode() == DataTransactResult.RESULT_OK) {
            IwdsLog.i(TAG, "HealthModel - requset send success");
        } else {
            IwdsLog.i(TAG, "HealthModel - requset send failed by error code: "
                    + result.getResultCode());
        }
    }

}
