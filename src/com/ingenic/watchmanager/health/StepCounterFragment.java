/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/watchmanager Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.health;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import a_vcard.android.text.Spanned;
import android.animation.Animator;
import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.SubscriptSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.StepDetailsActivity;
import com.ingenic.watchmanager.db.WatchManagerContracts;
import com.ingenic.watchmanager.view.CircularProgressDrawable;

@SuppressWarnings("rawtypes")
public class StepCounterFragment extends Fragment implements OnClickListener,
        OnPageChangeListener {

    static final String TAG = "StepCounterFragment";

    /**
     * 计步详情
     */
    private ImageButton mStepDetail;

    /**
     * 日期
     */
    private TextView mDate;

    /**
     * 今天
     */
    private TextView mStepToday;

    /**
     * 计步圆形View
     */
    private ImageView mStepImageView;

    /**
     * 计步数TextView
     */
    private TextView mStepNumber;

    /**
     * 计步圆形Drawable
     */
    private CircularProgressDrawable mStepDrawable;

    /**
     * 每日目标
     */
    private TextView mTargetStep;

    /**
     * 完成度(百分比)
     */
    private TextView mTargetCompleteness;

    /**
     * 默认目标值（计步）
     */
    private int mTargetStepNum = 5000;

    /**
     * 计步统计柱状图Pager
     */
    private ViewPager mChartPager;

    /**
     * 计步统计柱状图PagerAdapter
     */
    private ChartDataAdapter mChartDataAdapter;

    /**
     * 计步统计柱状图列表
     */
    private List<BarChart> mList = new ArrayList<BarChart>();

    /**
     * 计步数字字体
     */
    private Typeface mStepNumberTf;

    /**
     * 柱状图Title
     */
    private TextView mStepBarChartTitle;

    /**
     * 日期设置
     */
    private Calendar mCalendar;

    /**
     * 记录当前日期（今天的起始事件0时0分0秒0毫秒）
     */
    private long currentDate;
    private Date cDate;

    /**
     * 日期格式化
     */
    private SimpleDateFormat format = new SimpleDateFormat("M/d/yyyy");

    /**
     * 指示器
     */
    private LinearLayout mIndicator;

    /** 记录指示器前一个指示的位置 */
    private int previousSelectPosition = 0;

    /**
     * 计步数据操作对象
     */
    private TodayStepsOperator mTodayStepsOperator;
    private EverydayStepsOperator mEverydayStepsOperator;

    /**
     * 记录当前计步数
     */
    private int dayStepCount = 0;
    private int weekStepCount = 0;
    private int monthStepCount = 0;
    private int yearStepCount = 0;

    private static final String ORDER = "date asc";

    /**
     * 当前目标完成度（百分比）
     */
    private float currentCompleteness = 0f;

    /**
     * 当前显示完成度（可能时日/周/月/年的完成度）
     */
    private float currentProgress = 0f;

    /**
     * 统计一月内每周的计步信息
     */
    private List<EverydayStepsInfo> weekOfMonth = new ArrayList<EverydayStepsInfo>();

    /**
     * 统计一年内每月的计步信息
     */
    private List<EverydayStepsInfo> monthOfYear = new ArrayList<EverydayStepsInfo>();

    /**
     * 记录当前时间
     */
    @SuppressWarnings("unused")
    private int currentHour = 0;
    private int currentWeek = 0;
    private int currentMonth = 0;

    @SuppressWarnings("unused")
    private int currentPage = 0;
    private boolean monthDataIsNull = true;
    private boolean yearDataIsNull = true;

    /**
     * 加载动画
     */
    private Animator mAnim;

    // =============== 需要用到的资源 =============
    private int outlineWidth = 0; // 计步圆形View圆环宽度
    private int outlineColor = 0; // 计步圆形View圆环颜色
    private int ringWidth = 0; // 计步圆形View进度圆弧宽度
    private int ringColor = 0; // 计步圆形View进度圆弧颜色
    private int normalColor = 0; // 柱状图默认颜色
    private String chartNoData = "No Data Avilable"; // 柱状图没有数据时的提示
    private String[] weeks = null; // 柱状图
    private String[] weekNums = null; // 一个月的四周
    private String[] months = null; // 月份
    private String[] titles = null; // 柱状图上方标题

    /**
     * 柱状图个数
     */
    private static final int CHART_COUNT = 4;

    // 柱状图index
    private static final int TODAY_CHART_INDEX = 0;
    private static final int WEEK_CHART_INDEX = 1;
    private static final int MONTH_CHART_INDEX = 2;
    private static final int YEAR_CHART_INDEX = 3;

    // 柱状图
    private BarChart mTodayChart;
    private BarChart mWeekChart;
    private BarChart mMonthChart;
    private BarChart mYearChart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.health_step_layout, null);

        init(view);

        return view;
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            if (HealthExerciseService.PLAY_BARCHART_ANIM_ACTION.equals(action)) {
                if (mList.size() > mChartPager.getCurrentItem()) {
                    // 播放当前页面柱状图动画
                    BarChart barChart = mList.get(mChartPager.getCurrentItem());
                    barChart.invalidate();
                    barChart.animateY(700, Easing.EasingOption.EaseInCubic);
                }
            } else if (HealthExerciseService.UPDATE_UI_ACTION.equals(action)) {
                update();
            } else {
                // 时间改变
                setDate();
                if (mDate != null) {
                    mDate.setText(format.format(cDate));
                }
                update();
            }

        }

        private void update() {
            // 记录当前页
            currentPage = mChartPager.getCurrentItem();

            // 重新加载数据
            loadData();
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        /**
         * 在这里把加载把需要的资源加载好，防止出现not attached activity
         */
        chartNoData = getResources().getString(R.string.health_step_no_data);
        normalColor = getResources().getColor(R.color.color_step_drawable_ring);
        weeks = getResources().getStringArray(R.array.day_of_week_entries);
        weekNums = getResources().getStringArray(R.array.week_of_month_entries);
        months = getResources().getStringArray(R.array.month_entries);
        titles = getResources().getStringArray(R.array.chart_title_entries);

        outlineWidth = getResources().getDimensionPixelSize(
                R.dimen.step_drawable_outline_size);
        outlineColor = getResources().getColor(android.R.color.darker_gray);
        ringWidth = getResources().getDimensionPixelSize(
                R.dimen.step_drawable_ring_size);
        ringColor = getResources().getColor(R.color.color_step_drawable_ring);

        mTodayStepsOperator = new TodayStepsOperator(activity);
        mEverydayStepsOperator = new EverydayStepsOperator(activity);

        setDate();

        mStepNumberTf = Typeface.createFromAsset(getResources().getAssets(),
                "fonts/watch_font.ttf");

        IntentFilter filter = new IntentFilter();
        filter.addAction(HealthExerciseService.PLAY_BARCHART_ANIM_ACTION);
        filter.addAction(HealthExerciseService.UPDATE_UI_ACTION);
        filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        filter.addAction(Intent.ACTION_TIME_CHANGED);
        filter.addAction(Intent.ACTION_DATE_CHANGED);
        getActivity().registerReceiver(mReceiver, filter);
    }

    /**
     * 设置日期
     */
    private void setDate() {
        mCalendar = Calendar.getInstance();
        mCalendar.setTimeInMillis(System.currentTimeMillis());
        currentHour = mCalendar.get(Calendar.HOUR_OF_DAY);
        currentWeek = mCalendar.get(Calendar.DAY_OF_WEEK);
        currentMonth = mCalendar.get(Calendar.MONTH) + 1;

        currentDate = getDateTimeInMillis();
        cDate = new Date(System.currentTimeMillis());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getActivity().unregisterReceiver(mReceiver);
    }

    /**
     * 初始化BarChart
     */
    private BarChart initBarChart() {

        BarChart chart = new BarChart(getActivity());

        chart.setDescription("");
        chart.setNoDataText(chartNoData);
        chart.setDrawGridBackground(false);
        chart.getLegend().setEnabled(false);
        chart.setHighlightEnabled(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setTextColor(Color.WHITE);

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setLabelCount(5);
        leftAxis.setSpaceTop(15f);
        leftAxis.setDrawGridLines(false);
        leftAxis.setTextColor(Color.WHITE);

        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setLabelCount(5);
        rightAxis.setSpaceTop(15f);
        rightAxis.setDrawGridLines(false);
        rightAxis.setTextColor(Color.WHITE);

        return chart;
    }

    /**
     * 初始化
     */
    private void init(View view) {

        // 计步详情
        mStepDetail = (ImageButton) view.findViewById(R.id.health_step_details);
        mStepDetail.setOnClickListener(this);

        // 日期
        mDate = (TextView) view.findViewById(R.id.health_step_date);
        mDate.setText(format.format(cDate));
        mDate.setTypeface(mStepNumberTf);

        // TODAY
        mStepToday = (TextView) view.findViewById(R.id.health_step_today);

        // 计步圆形View
        mStepImageView = (ImageView) view.findViewById(R.id.health_step_iv);

        // 计步数
        mStepNumber = (TextView) view.findViewById(R.id.health_step_number);
        mStepNumber.setText(dayStepCount + "");
        mStepNumber.setTypeface(mStepNumberTf);

        // 每日目标
        mTargetStep = (TextView) view.findViewById(R.id.health_step_target_num);
        mTargetStep.setText(setSubscriptUnit(mTargetStepNum + ""));
        mTargetStep.setTypeface(mStepNumberTf);

        // 完成度
        mTargetCompleteness = (TextView) view
                .findViewById(R.id.health_step_completeness_num);
        mTargetCompleteness.setTypeface(mStepNumberTf);

        // 计步下方页面标题（日/周/月/年）
        mStepBarChartTitle = (TextView) view
                .findViewById(R.id.health_step_title);
        mStepBarChartTitle.setText(getPageTitle(0));

        // 计步
        mChartPager = (ViewPager) view.findViewById(R.id.health_step_chart);

        // 指示器
        mIndicator = (LinearLayout) view.findViewById(R.id.health_indicator);

        // 计步圆形Drawable
        mStepDrawable = new CircularProgressDrawable.Builder()
                .setOutlineWidth(outlineWidth).setOutlineColor(outlineColor)
                .setRingWidth(ringWidth).setRingColor(ringColor).create();

        // 计步圆形View
        mStepImageView.setImageDrawable(mStepDrawable);

        mList.clear();
        // 柱状图
        for (int i = 0; i < CHART_COUNT; i++) {
            BarChart chart = initBarChart();

            switch (i) {
            case TODAY_CHART_INDEX:
                mTodayChart = chart;
                break;

            case WEEK_CHART_INDEX:
                mWeekChart = chart;
                break;

            case MONTH_CHART_INDEX:
                mMonthChart = chart;
                break;

            case YEAR_CHART_INDEX:
                mYearChart = chart;
                break;

            default:
                break;
            }

            mList.add(chart);
        }

        mChartDataAdapter = new ChartDataAdapter();
        mChartPager.setAdapter(mChartDataAdapter);
        mChartPager.setOnPageChangeListener(this);

        // 加载数据
        loadData();

        mStepToday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // 回到今天
                mAnim = HealthUtils.prepareProgressAnimation(mStepDrawable,
                        mAnim, currentProgress, currentCompleteness, 0);
                mAnim.start();
                mChartPager.setCurrentItem(0, true);
            }
        });

    }

    /**
     * 读取计步数据
     */
    private void loadData() {

        // 加载当天的计步数据
        loadToday();

        // 加载周的计步数据
        loadWeek();

        // 加载月的计步数据
        loadMonth();

        // 加载年的计步数据
        loadYear();

    }

    /**
     * 加载'年'（包括今年在内向前的12个月，分为12段，每段代表一个'月'）的计步数据到柱状图并刷新界面
     */
    private void loadYear() {
        Calendar calendar = Calendar.getInstance();

        long min = currentDate;
        long max = currentDate;

        monthOfYear.clear();
        yearStepCount = 0;

        for (int i = 0; i < 12; i++) {
            final int index = i;
            calendar.setTimeInMillis(max);
            calendar.add(Calendar.MONTH, -1);
            min = calendar.getTimeInMillis();

            new AsyncTask<String, Void, List<EverydayStepsInfo>>() {

                @Override
                protected List<EverydayStepsInfo> doInBackground(
                        String... params) {
                    return mEverydayStepsOperator
                            .queryAll(
                                    WatchManagerContracts.TodayStepsColumns.DATE
                                            + " > ? and "
                                            + WatchManagerContracts.TodayStepsColumns.DATE
                                            + " <= ?", new String[] {
                                            params[0], params[1] }, ORDER);
                }

                @Override
                protected void onPostExecute(List<EverydayStepsInfo> result) {
                    super.onPostExecute(result);
                    if (result != null && result.size() > 0) {
                        yearDataIsNull = false;
                    }

                    // 统计一年内每月计步数
                    EverydayStepsInfo info = new EverydayStepsInfo();
                    info.steps = getStepTotalCount(YEAR_CHART_INDEX, result);
                    monthOfYear.add(info);

                    // 累计一年内总步数
                    yearStepCount += info.steps;

                    if (index == 11) {
                        // 设置年的柱状图数据并刷新界面
                        mYearChart.setData(loadChartDatas(YEAR_CHART_INDEX,
                                null));
                        mYearChart.notifyDataSetChanged();

                        setSteps(mChartPager.getCurrentItem());
                    }

                }

            }.execute(min + "", max + "");

            max = min;
        }
    }

    /**
     * 加载'月'（包括今天在内向前的28天，分为4段，每7天代表一个'周'）的计步数据到柱状图并刷新界面
     */
    private void loadMonth() {
        Calendar calendar = Calendar.getInstance();
        long min = currentDate;
        long max = currentDate;

        weekOfMonth.clear();
        monthStepCount = 0;

        for (int i = 0; i < 4; i++) {
            final int index = i;
            calendar.setTimeInMillis(max);
            calendar.add(Calendar.DAY_OF_MONTH, -7);
            min = calendar.getTimeInMillis();

            new AsyncTask<String, Void, List<EverydayStepsInfo>>() {

                @Override
                protected List<EverydayStepsInfo> doInBackground(
                        String... params) {
                    return mEverydayStepsOperator
                            .queryAll(
                                    WatchManagerContracts.TodayStepsColumns.DATE
                                            + " > ? and "
                                            + WatchManagerContracts.TodayStepsColumns.DATE
                                            + " <= ?", new String[] {
                                            params[0], params[1] }, ORDER);
                }

                @Override
                protected void onPostExecute(List<EverydayStepsInfo> result) {
                    super.onPostExecute(result);

                    if (result != null && result.size() > 0) {
                        monthDataIsNull = false;
                    }

                    // 统计一月内每周计步数
                    EverydayStepsInfo info = new EverydayStepsInfo();
                    info.steps = getStepTotalCount(MONTH_CHART_INDEX, result);
                    weekOfMonth.add(info);

                    // 累计一月内总步数
                    monthStepCount += info.steps;

                    if (index == 3) {
                        mMonthChart.setData(loadChartDatas(MONTH_CHART_INDEX,
                                null));
                        mMonthChart.notifyDataSetChanged();
                    }
                }

            }.execute(min + "", max + "");

            max = min;
        }
    }

    /**
     * 加载'周'（包括今天在内向前的7天）的计步数据到柱状图并刷新界面
     */
    private void loadWeek() {
        Calendar calendar = Calendar.getInstance();
        long max = currentDate;
        calendar.setTimeInMillis(currentDate);
        calendar.add(Calendar.DAY_OF_MONTH, -7);
        long min = calendar.getTimeInMillis();

        new AsyncTask<String, Void, List>() {

            @Override
            protected List doInBackground(String... params) {
                return mEverydayStepsOperator.queryAll(
                        WatchManagerContracts.TodayStepsColumns.DATE
                                + " > ? and "
                                + WatchManagerContracts.TodayStepsColumns.DATE
                                + " <= ?",
                        new String[] { params[0], params[1] }, ORDER);
            }

            @Override
            protected void onPostExecute(List result) {
                super.onPostExecute(result);
                // 记录一周内总步数
                weekStepCount = getStepTotalCount(WEEK_CHART_INDEX, result);

                mWeekChart.setData(loadChartDatas(WEEK_CHART_INDEX, result));
                mWeekChart.notifyDataSetChanged();
            }
        }.execute(min + "", max + "");
    }

    /**
     * 加载当天的计步数据到柱状图并刷新界面
     */
    private void loadToday() {
        new AsyncTask<String, Void, List>() {
            @Override
            protected List doInBackground(String... params) {

                return mTodayStepsOperator.queryAll(
                        WatchManagerContracts.TodayStepsColumns.DATE + " = ?",
                        new String[] { params[0] }, ORDER);
            }

            @Override
            protected void onPostExecute(List result) {
                super.onPostExecute(result);

                // 记录并设置当日总步数
                dayStepCount = getStepTotalCount(TODAY_CHART_INDEX, result);

                // 记录当前目标完成度
                currentCompleteness = (float) (1.0 * dayStepCount / mTargetStepNum);
                currentProgress = currentCompleteness;

                // 设置完成度
                if (currentCompleteness < 1.0f) {
                    mTargetCompleteness.setText(String.format("%.2f",
                            currentCompleteness * 100) + "%");
                } else {
                    mTargetCompleteness.setText("100%");
                }

                // 加载数据
                mTodayChart.setData(loadChartDatas(TODAY_CHART_INDEX, result));
                mTodayChart.notifyDataSetChanged();

            }
        }.execute(currentDate + "");
    }

    /**
     * 计步柱状图适配器
     *
     * @author tzhang
     *
     */
    private class ChartDataAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            if (mList != null) {
                return mList.size();
            }

            return 0;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View child = mList.get(position);
            ((ViewPager) container).addView(child);
            return child;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView(mList.get(position));
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }
    }

    /**
     * 加载柱状图数据
     * 
     * @param type
     * @param datas
     * @return
     */
    private BarData loadChartDatas(int type, List datas) {
        ArrayList<BarEntry> entries = new ArrayList<BarEntry>();
        boolean emptyDate = true;

        switch (type) {
        case TODAY_CHART_INDEX:
            for (int i = 0; i < 12; i++) {

                if (datas != null && datas.size() > (i + 1)
                        && ((TodayStepsInfo) datas.get(i)).hour == (i * 2)) {
                    emptyDate = false;
                    entries.add(new BarEntry(
                            ((TodayStepsInfo) datas.get(i + 1)).steps, i));
                } else {
                    entries.add(new BarEntry(0, i));
                }
            }
            break;

        case WEEK_CHART_INDEX:
            for (int i = 0; i < 7; i++) {

                if (datas != null && datas.size() > i) {
                    emptyDate = false;
                    entries.add(new BarEntry(
                            ((EverydayStepsInfo) datas.get(i)).steps, i));
                } else {
                    entries.add(new BarEntry(0, i));
                }
            }
            break;

        case MONTH_CHART_INDEX:
            if (!monthDataIsNull) {
                for (int i = 0; i < 4; i++) {

                    if (weekOfMonth != null && weekOfMonth.size() > i) {
                        emptyDate = false;
                        entries.add(new BarEntry(weekOfMonth.get(i).steps,
                                3 - i));
                    } else {
                        entries.add(new BarEntry(0, 3 - i));
                    }
                }
            }
            break;

        case YEAR_CHART_INDEX:
            if (!yearDataIsNull) {
                for (int i = 0; i < 12; i++) {

                    if (monthOfYear != null && monthOfYear.size() > i) {
                        emptyDate = false;
                        entries.add(new BarEntry(monthOfYear.get(i).steps,
                                11 - i));
                    } else {
                        entries.add(new BarEntry(0, 11 - i));
                    }
                }
            }
            break;

        default:
            break;
        }

        // 没有数据
        if (emptyDate) {
            entries.clear();
            // return null;
        }

        BarDataSet d = new BarDataSet(entries, null);
        d.setBarSpacePercent(20f);
        int[] colors = { normalColor, normalColor, normalColor, normalColor,
                normalColor, normalColor, normalColor, normalColor,
                normalColor, normalColor, normalColor, normalColor };
        d.setColors(colors);

        BarData data = new BarData(getXvals(type));
        data.addDataSet(d);
        data.setValueTextColor(Color.WHITE);

        return data;
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {

    }

    @Override
    public void onPageSelected(int position) {

        // 切换指示器
        if (mIndicator != null && mIndicator.getChildCount() > position) {
            // 把前一个点置为normal状态
            mIndicator.getChildAt(previousSelectPosition)
                    .setBackgroundResource(R.drawable.health_indicator_normal);
            // 把当前选中的position对应的点置为enabled状态
            mIndicator.getChildAt(position).setBackgroundResource(
                    R.drawable.health_indicator_selected);

            // 重新记录前一个点的位置
            previousSelectPosition = position;
        }

        // 重新设置柱状图标题
        mStepBarChartTitle.setText(getPageTitle(position));

        // 重设计步数据
        setSteps(position);
    }

    private void setSteps(int position) {
        // 记录当前的进度
        final float oldProgress = currentProgress;

        // 重新计算进度
        switch (position) {
        case TODAY_CHART_INDEX:
            // 计算当前进度(日)
            currentProgress = (float) (1.0 * dayStepCount / mTargetStepNum);
            mStepNumber.setText(dayStepCount + "");
            break;

        case WEEK_CHART_INDEX:
            // 计算当前进度(周)
            currentProgress = (float) (1.0 * weekStepCount / mTargetStepNum / 7);
            mStepNumber.setText(weekStepCount + "");
            break;

        case MONTH_CHART_INDEX:
            // 计算当前进度(月)
            currentProgress = (float) (1.0 * monthStepCount / mTargetStepNum / 30);
            mStepNumber.setText(monthStepCount + "");
            break;

        case YEAR_CHART_INDEX:
            // 计算当前进度(年)
            currentProgress = (float) (1.0 * yearStepCount / mTargetStepNum / 365);
            mStepNumber.setText(yearStepCount + "");
            break;

        default:
            break;
        }

        // 播放圆形View加载进度动画
        mAnim = HealthUtils.prepareProgressAnimation(mStepDrawable, mAnim,
                oldProgress, currentProgress, 0);
        mAnim.start();

        // 播放柱状图加载动画
        if (position < mList.size()) {
            BarChart barChart = (BarChart) mList.get(position);

            if (barChart != null) {
                barChart.animateY(700, Easing.EasingOption.EaseInCubic);
            }
        }
    }

    /**
     * 计算总步数
     * 
     * @param datas
     * @return
     */
    private int getStepTotalCount(int type, List datas) {
        if (datas == null || datas.size() == 0) {
            return 0;
        }

        int totalCount = 0;

        switch (type) {
        case TODAY_CHART_INDEX:
            TodayStepsInfo todayStepsInfo = null;
            for (int i = 0; i < datas.size(); i++) {
                todayStepsInfo = (TodayStepsInfo) datas.get(i);
                totalCount = totalCount + todayStepsInfo.steps;
            }
            break;

        case WEEK_CHART_INDEX:
        case MONTH_CHART_INDEX:
        case YEAR_CHART_INDEX:
            EverydayStepsInfo everydayStepsInfo = null;
            for (int i = 0; i < datas.size(); i++) {
                everydayStepsInfo = (EverydayStepsInfo) datas.get(i);
                totalCount = totalCount + everydayStepsInfo.steps;
            }
            break;

        default:
            break;
        }
        return totalCount;
    }

    /**
     * 获取当前日期的TimeInMillis
     * 
     * @return
     */
    private long getDateTimeInMillis() {
        // 设置为当前时间
        mCalendar.setTimeInMillis(System.currentTimeMillis());

        // 将当天的时、分、秒、毫秒清0，从而获得年月日的TimeInMillis
        mCalendar.set(Calendar.HOUR_OF_DAY, 0);
        mCalendar.set(Calendar.MINUTE, 0);
        mCalendar.set(Calendar.SECOND, 0);
        mCalendar.set(Calendar.MILLISECOND, 0);

        return mCalendar.getTimeInMillis();
    }

    /**
     * 获取X坐标值
     * 
     * @param type
     * @return
     */
    private ArrayList<String> getXvals(int type) {

        ArrayList<String> xVals = new ArrayList<String>();

        switch (type) {
        case TODAY_CHART_INDEX:
            for (int i = 1; i <= 12; i++) {
                xVals.add(i * 2 + "h");
            }
            break;

        case WEEK_CHART_INDEX:
            for (int i = 0; i < 7; i++) {
                xVals.add(getDayOfWeek((currentWeek + 1 + i) % 7));
            }
            break;

        case MONTH_CHART_INDEX:
            for (int i = 0; i < 4; i++) {
                xVals.add(getWeekOfMonth(i));
            }
            break;

        case YEAR_CHART_INDEX:
            for (int i = 0; i < 12; i++) {
                xVals.add(getMonth((currentMonth + 1 + i) % 12));
            }
            break;
        }

        return xVals;
    }

    /**
     * 获取周X坐标字符串
     * 
     * @param dayOfWeek
     * @return
     */
    private String getDayOfWeek(int dayOfWeek) {
        if (dayOfWeek <= 0 || dayOfWeek > weeks.length) {
            return weeks[weeks.length - 1];
        } else {
            return weeks[dayOfWeek - 1];
        }
    }

    /**
     * 获取月X坐标字符串
     * 
     * @param weekOfMonth
     * @return
     */
    private String getWeekOfMonth(int weekOfMonth) {
        if (weekOfMonth < 0 || weekOfMonth > weekNums.length) {
            return weekNums[weekNums.length - 1];
        } else {
            return weekNums[weekOfMonth];
        }
    }

    /**
     * 获取年X坐标字符串
     * 
     * @param month
     * @return
     */
    private String getMonth(int month) {
        if (month <= 0 || month > months.length) {
            return months[months.length - 1];
        } else {
            return months[month - 1];
        }
    }

    /**
     * 获取柱状图标题字符串
     * 
     * @param index
     * @return
     */
    private String getPageTitle(int index) {
        if (index < 0 || index >= titles.length) {
            return titles[titles.length - 1];
        } else {
            return titles[index];
        }
    }

    /**
     * 设置今日目标和今日完成计步下标单位
     * 
     * @param str
     * @return
     */
    private SpannableString setSubscriptUnit(String str) {
        SpannableString msp = new SpannableString(str + " steps");
        int len = msp.length();
        msp.setSpan(new SubscriptSpan(), len - 5, len,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        msp.setSpan(new AbsoluteSizeSpan(14, true), len - 5, len,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return msp;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
        case R.id.health_step_details:
            getActivity().startActivity(
                    new Intent(getActivity(), StepDetailsActivity.class));
            break;

        default:
            break;
        }

    }
}