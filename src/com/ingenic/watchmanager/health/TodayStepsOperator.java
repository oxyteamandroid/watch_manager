/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.health;

import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.ingenic.watchmanager.db.Operator;
import com.ingenic.watchmanager.db.WatchManagerContracts;

public class TodayStepsOperator extends Operator<TodayStepsInfo> {

    public TodayStepsOperator(Context context) {
        super(context, WatchManagerContracts.Tables.TODAY_STEPS);
    }

    @Override
    protected ContentValues toValues(TodayStepsInfo info) {
        ContentValues values = new ContentValues();
        // values.put(WatchManagerContracts.TodayStepsColumns.ID, info.id);
        values.put(WatchManagerContracts.TodayStepsColumns.HOUR, info.hour);
        values.put(WatchManagerContracts.TodayStepsColumns.STEPS, info.steps);
        values.put(WatchManagerContracts.TodayStepsColumns.DATE, info.date);
        return values;

    }

    @Override
    public int update(TodayStepsInfo t) {
        ContentValues values = new ContentValues();
        // values.put(WatchManagerContracts.TodayStepsColumns.ID, t.id);
        values.put(WatchManagerContracts.TodayStepsColumns.HOUR, t.hour);
        values.put(WatchManagerContracts.TodayStepsColumns.STEPS, t.steps);
        values.put(WatchManagerContracts.TodayStepsColumns.DATE, t.date);
        return update(values, WatchManagerContracts.TodayStepsColumns.DATE
                + " = ? and " + WatchManagerContracts.TodayStepsColumns.HOUR
                + " = ? ", new String[] { "" + t.date, t.hour + "" });
    }

    @Override
    protected TodayStepsInfo fromCursor(Cursor cursor) {
        TodayStepsInfo info = new TodayStepsInfo();
        info.id = cursor.getInt(cursor
                .getColumnIndex(WatchManagerContracts.TodayStepsColumns.ID));
        info.hour = cursor.getInt(cursor
                .getColumnIndex(WatchManagerContracts.TodayStepsColumns.HOUR));
        info.steps = cursor.getInt(cursor
                .getColumnIndex(WatchManagerContracts.TodayStepsColumns.STEPS));
        info.date = cursor.getString(cursor
                .getColumnIndex(WatchManagerContracts.TodayStepsColumns.DATE));
        return info;
    }

    @Override
    public int delete(TodayStepsInfo t) {
        return delete(WatchManagerContracts.TodayStepsColumns.DATE
                + " = ? and " + WatchManagerContracts.TodayStepsColumns.HOUR
                + " = ? ", new String[] { "" + t.date, t.hour + "" });
    }

    @Override
    public boolean hasData(TodayStepsInfo t) {
        TodayStepsInfo c = query(null,
                WatchManagerContracts.TodayStepsColumns.DATE + " = ? and "
                        + WatchManagerContracts.TodayStepsColumns.HOUR
                        + " = ? ", new String[] { "" + t.date, t.hour + "" },
                null, null, null);
        return c != null;
    }

    @Override
    public List<TodayStepsInfo> queryAll() {
        return queryAll(null);
    }
}
