/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/watchmanager Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.health;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ingenic.iwds.utils.IwdsLog;

/**
 * 用来重启服务的广播接收器
 * 
 * @author tZhang
 * 
 */
public class RestartHealthServiceReceiver extends BroadcastReceiver {

    static final String TAG = "RestartHealthServiceReceiver";
    public static final String RESTART_SERVICE = "ingenic.intent.action.RESTART_SERVICE";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        // 重启健康的通讯服务
        if (RESTART_SERVICE.equals(action)) {
            context.startService(new Intent(context,
                    HealthExerciseService.class));
            IwdsLog.d(TAG, "HealthExerciseService restarted!");
        }
    }

}
