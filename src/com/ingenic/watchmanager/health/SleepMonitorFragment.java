/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/WatchManager/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.health;

import java.sql.Date;
import java.text.SimpleDateFormat;

import android.animation.Animator;
import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.SleepMonitorDetailsActivity;
import com.ingenic.watchmanager.view.CircularProgressDrawable;

/**
 * 睡眠监测
 *
 * @author tZhang
 */

public class SleepMonitorFragment extends Fragment implements
        View.OnClickListener {

    static final String TAG = "SleepMonitorFragment";
    /**
     * 日期
     */
    private TextView mDate;
    private Date cDate;

    /**
     * 日期格式化
     */
    private SimpleDateFormat mFormat = new SimpleDateFormat("M/d/yyyy");

    /**
     * 睡眠监测圆形View
     */
    private ImageView mSleepImageView;

    /**
     * 睡眠监测圆形Drawable
     */
    private CircularProgressDrawable mSleepDrawable;

    /**
     * 更多睡眠信息
     */
    private ImageButton mMoreSleep;

    /**
     * 睡眠质量(等级)
     */
    private LinearLayout mSleepRank;

    /**
     * 睡眠状况描述
     */
    private TextView mSleepDescription;

    /**
     * 加载动画
     */
    private Animator mAnim;

    // =============== 需要用到的资源 =============
    private int mOutlineWidth = 0; // 圆形View圆环宽度
    private int mOutlineColor = 0; // 圆形View圆环颜色
    private int mRingWidth = 0; // 圆形View进度圆弧宽度
    private int mRingColor = 0; // 圆形View进度圆弧颜色

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        cDate = new Date(System.currentTimeMillis());

        mOutlineWidth = getResources().getDimensionPixelSize(
                R.dimen.sleep_drawable_outline_size);
        mOutlineColor = getResources().getColor(android.R.color.darker_gray);
        mRingWidth = getResources().getDimensionPixelSize(
                R.dimen.sleep_drawable_ring_size);
        mRingColor = getResources().getColor(R.color.color_sleep_drawable_ring);

        IntentFilter filter = new IntentFilter();
        filter.addAction(HealthExerciseService.PLAY_SLEEP_ANIM_ACTION);
        filter.addAction(HealthExerciseService.UPDATE_UI_ACTION);
        getActivity().registerReceiver(mReceiver, filter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater
                .inflate(R.layout.health_sleep_monitor_layout, null);

        init(view);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getActivity().unregisterReceiver(mReceiver);
    }

    /**
     * 初始化
     * @param view
     */
    private void init(View view) {
        mDate = (TextView) view.findViewById(R.id.health_sleep_date);
        mDate.setText(mFormat.format(cDate));

        mSleepImageView = (ImageView) view.findViewById(R.id.health_sleep_iv);

        // 计步圆形Drawable
        mSleepDrawable = new CircularProgressDrawable.Builder()
                .setOutlineWidth(mOutlineWidth).setOutlineColor(mOutlineColor)
                .setRingWidth(mRingWidth).setRingColor(mRingColor).create();

        mSleepImageView.setImageDrawable(mSleepDrawable);

        mMoreSleep = (ImageButton) view.findViewById(R.id.health_more_sleep);
        mMoreSleep.setOnClickListener(this);

        mSleepRank = (LinearLayout) view.findViewById(R.id.health_sleep_rank);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.leftMargin = HealthUtils.dp2Px(getActivity(), 5);
        for (int i = 0; i < 3; i++) {
            ImageView rank = new ImageView(getActivity());
            rank.setImageResource(R.drawable.health_sleep_rank);
            mSleepRank.addView(rank, params);
        }

        mSleepDescription = (TextView) view
                .findViewById(R.id.health_sleep_description);
        mSleepDescription.setText("您今天睡的好极了，记得多运动哦～");

    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            if (HealthExerciseService.PLAY_SLEEP_ANIM_ACTION.equals(action)) {
                // 播放进度动画
                mAnim = HealthUtils.prepareProgressAnimation(mSleepDrawable,
                        mAnim, 0.2f, 362f / 480, 0);
                mAnim.start();

            } else if (HealthExerciseService.UPDATE_UI_ACTION.equals(action)) {
                // 重新加载数据,刷新界面
            }

        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.health_more_sleep:
            getActivity()
                    .startActivity(
                            new Intent(getActivity(),
                                    SleepMonitorDetailsActivity.class));
            break;

        default:
            break;
        }
    }

}
