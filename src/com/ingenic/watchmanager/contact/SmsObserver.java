/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/IDWS Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.contact;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;

import com.ingenic.iwds.datatransactor.elf.SmsInfo;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.util.Utils;

public class SmsObserver extends ContentObserver {

    private static final String[] PROJECTION = new String[] { SMS._ID,//0
            SMS.TYPE,//1
            SMS.ADDRESS,//2
            SMS.BODY,//3
            SMS.DATE,//4
            SMS.THREAD_ID,//5
            SMS.READ,//6
            SMS.PROTOCOL //7
    };

    private static final String SELECTION = SMS.DATE
            + " > %s"
            +
            //" and " + SMS.PROTOCOL + " = null" +
            //" or " + SMS.PROTOCOL + " = " + SMS.PROTOCOL_SMS + ")" +
            " and (" + SMS.TYPE + " = " + SMS.MESSAGE_TYPE_INBOX + " or " + SMS.TYPE + " = "
            + SMS.MESSAGE_TYPE_SENT + ")";

    private static final int COLUMN_INDEX_ID = 0;
    private static final int COLUMN_INDEX_TYPE = 1;
    private static final int COLUMN_INDEX_PHONE = 2;
    private static final int COLUMN_INDEX_BODY = 3;
    private static final int COLUMN_INDEX_DATE = 4;
    private static final int COLUMN_INDEX_THREAD_ID = 5;
    private static final int COLUMN_INDEX_READ = 6;
    private static final int COLUMN_INDEX_PROTOCOL = 7;

    private ContentResolver mResolver;
    private Handler mHandler;
    private Context mContext;

    public SmsObserver(Context context, Handler handler) {
        super(handler);

        mContext = context;
        mResolver = context.getContentResolver();
        mHandler = handler;
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);

        SharedPreferences sp = mContext.getSharedPreferences("last_sms", Context.MODE_PRIVATE);
        long last = sp.getLong("last", 0);

        Cursor cursor = mResolver.query(SMS.CONTENT_URI, PROJECTION,
                String.format(SELECTION, last), null, null);
        IwdsLog.d(SmsService.TAG, "observer onchange");
        while (cursor.moveToNext()) {
            int id = cursor.getInt(COLUMN_INDEX_ID);
            int type = cursor.getInt(COLUMN_INDEX_TYPE);
            String phone = cursor.getString(COLUMN_INDEX_PHONE);
            String body = cursor.getString(COLUMN_INDEX_BODY);
            long date = cursor.getLong(COLUMN_INDEX_DATE);
            int protocol = cursor.getInt(COLUMN_INDEX_PROTOCOL);
            int read = cursor.getInt(COLUMN_INDEX_READ);
            int thread_id = cursor.getInt(COLUMN_INDEX_THREAD_ID);
            String person = Utils.queryNameByNum(phone, mContext);

            SmsInfo info = new SmsInfo();
            info.setId(id);
            info.setType(type);
            info.setAddress(configAddress(phone));
            info.setBody(body);
            info.setProtocol(protocol);
            info.setRead(read);
            info.setDate(date);
            info.setThreadId(thread_id);
            info.setPerson(person);

            IwdsLog.d(this, "Received a message:" + info);
            mHandler.obtainMessage(SmsService.MSG_NEW_SMS, info).sendToTarget();

            if (last == 0) {
                last = date;
                break;
            } else {
                last = Math.max(last, date);
            }
            IwdsLog.d(SmsService.TAG, "sms body: " + info.getBody());
        }

        sp.edit().putLong("last", last).commit();
    }

    private String configAddress(String address) {
        if (address == null) return null;

        if (address.startsWith("+86")) {
            address = address.substring(3);
        }

        return address.replaceAll("\\D", "");
    }
}
