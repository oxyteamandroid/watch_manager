/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * nongjiabao<jiabao.nong@ingenic.com>
 *
 * Elf/IDWS Project
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.watchmanager.contact;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.provider.CallLog.Calls;

import com.ingenic.iwds.datatransactor.elf.CalllogInfo;
import com.ingenic.iwds.utils.IwdsLog;

/**
 * CalllogObserver 监听calllog服务
 * @author jbnong
 *
 */
public class CalllogObserver extends ContentObserver {
    private static final String TAG = "CalllogObserver";
    private ContentResolver mResolver;
    private Handler mHandler;
    private Context mContext;

    public CalllogObserver(Context context, ContentResolver resolver, Handler handler) {
        super(handler);
        mResolver = resolver;
        mHandler = handler;
        mContext = context;
        IwdsLog.i(TAG, "+++++CalllogObserver");
    }
    @Override
    public void onChange(boolean selfChange) {
        IwdsLog.i(TAG, "+++++CalllogObserver onChange");
        Cursor calllogcsr ;
        /*********读取旧的的id************/
        String oldid_str;
        SharedPreferences sp = mContext.getSharedPreferences("Calllog_id", Context.MODE_PRIVATE);
        oldid_str = sp.getString("id", "");
        String[] oldcalllog_id = oldid_str.split("#");
        /*********保存新的的id************/
        String newid_str = "";
        Editor editor1 = sp.edit();
        editor1.clear();
        editor1.commit();
        calllogcsr = mResolver.query(Calls.CONTENT_URI, null, Calls.TYPE + "!=4", null, Calls.DEFAULT_SORT_ORDER + " limit " + ContactUtils.CALLLOG_LIMIT_NUM );
        if (calllogcsr != null) {
            while (calllogcsr.moveToNext()){
                newid_str += calllogcsr.getInt(calllogcsr.getColumnIndex(Calls._ID)) + "#";
                editor1.putString("id", newid_str);
                editor1.commit();
            }
            calllogcsr.close();
        }

        int length = oldcalllog_id.length;
        for (int i=0; i<length; i++){
            if (!newid_str.contains(oldcalllog_id[i])){
                /*如果id不在newid_str中，删除*/
                CalllogInfo delinfo = new CalllogInfo();
                delinfo.set_id(Integer.valueOf(oldcalllog_id[i]));
                delinfo.operation = CalllogInfo.OPT_DEL;
                mHandler.obtainMessage(CalllogService.MSG_DEL_CALLLOG, delinfo).sendToTarget();
                IwdsLog.i(TAG, "Delete calllog :" + delinfo.toString());
            }
        }

        String[] newcalllog_id = newid_str.split("#");
        length = newcalllog_id.length;
        for (int i=0; i<length; i++) {
            if (!oldid_str.contains(newcalllog_id[i])){
                /*如果id不在oldid_str中，添加*/
                int new_id = Integer.valueOf(newcalllog_id[i]);
                calllogcsr = mResolver.query(Calls.CONTENT_URI, null, Calls._ID +"=?" , new String[]{ new_id + ""}, null );
                if (calllogcsr!=null) {
                    if (calllogcsr.moveToFirst()) {
                        int _id       = calllogcsr.getInt(calllogcsr.getColumnIndex(Calls._ID));
                        int duration  = calllogcsr.getInt(calllogcsr.getColumnIndex(Calls.DURATION));;
                        int type      = calllogcsr.getInt(calllogcsr.getColumnIndex(Calls.TYPE));
                        int news      = calllogcsr.getInt(calllogcsr.getColumnIndex(Calls.NEW));;
                        int is_read   = calllogcsr.getInt(calllogcsr.getColumnIndex(Calls.IS_READ));
                        long date     = calllogcsr.getLong(calllogcsr.getColumnIndex(Calls.DATE));
                        String number = calllogcsr.getString(calllogcsr.getColumnIndex(Calls.NUMBER));
                        String name   = calllogcsr.getString(calllogcsr.getColumnIndex(Calls.CACHED_NAME));

                        CalllogInfo info = new CalllogInfo();
                        info.operation = CalllogInfo.OPT_ADD;
                        info.set_id(_id);
                        info.setDate(date);
                        info.setDuration(duration);
                        info.setIs_read(is_read);
                        info.setName(name);
                        info.setNewflag(news);
                        info.setNumber(number);
                        info.setType(type);
                        mHandler.obtainMessage(CalllogService.MSG_NEW_CALLLOG, info).sendToTarget();
                        IwdsLog.i(TAG, "Add Calllog : " + info.toString());
                        info = null;
                    }
                    calllogcsr.close();
                }
            }
        }
        mHandler.obtainMessage(CalllogService.MSG_READ_CALLLOG).sendToTarget();
    }
}
