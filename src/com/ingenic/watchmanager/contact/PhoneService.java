/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.contact;

import java.lang.reflect.Method;

import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothProfile;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;

import com.android.internal.telephony.ITelephony;
import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.PhoneState;
import com.ingenic.iwds.datatransactor.elf.PhoneStateTransactionModel;
import com.ingenic.iwds.datatransactor.elf.PhoneStateTransactionModel.PhoneStateCallback;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.util.UUIDS;
import com.ingenic.watchmanager.util.Utils;

public class PhoneService extends Service implements PhoneStateCallback {
    private static PhoneStateTransactionModel sModel;
    private TelephonyManager mTelephonyManager;
    private BluetoothAdapter mAdapter;
    private String[] mDefaultSms;

    private PhoneStateListener mStateListener = new PhoneStateListener() {
        public void onCallStateChanged(int state, String incomingNumber) {
            IwdsLog.d(this, "Phone state has changed.The current state is " + state
                    + "of the number is " + incomingNumber);

            boolean isHeadsetConnected = mAdapter
                    .getProfileConnectionState(BluetoothProfile.HEADSET) == BluetoothProfile.STATE_CONNECTED;
            if (isHeadsetConnected) {
                IwdsLog.w(this, "BluetoothHeadset is connected.Use Bluetooth to control this call.");
                return;
            }

            SharedPreferences sp = getSharedPreferences(Utils.SP_CONTACT, MODE_PRIVATE);
            boolean enabled = sp.getBoolean(Utils.KEY_PHONE, true);
            if (!enabled) {
                IwdsLog.w(this, "Phonecall's notice is disabled by User!");
                return;
            }

            PhoneState phoneState = new PhoneState();
            phoneState.state = state;
            phoneState.number = incomingNumber;
            phoneState.name = Utils.queryNameByNum(incomingNumber, PhoneService.this);

            IwdsLog.d(this, "Send this PhoneState to watch:" + phoneState);
            sModel.send(phoneState);
        };
    };

    @Override
    public IBinder onBind(Intent intent) {
        return PhoneBinder.getInstance(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mTelephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if (sModel == null) {
            sModel = new PhoneStateTransactionModel(this, this, UUIDS.PONESTATE);
        }
        sModel.start();

        mDefaultSms = getResources().getStringArray(R.array.sms);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mTelephonyManager.listen(mStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        if (sModel != null) {
            sModel.stop();
        }

        super.onDestroy();
    }

    @Override
    public void onRequest() {}

    @Override
    public void onRequestFailed() {}

    @Override
    public void onObjectArrived(PhoneState state) {
        IwdsLog.i(this, "Received a PhoneState from watch:" + state);
        switch (state.state) {
        case PhoneState.STATE_IDLE:
            hangup();
            break;
        case PhoneState.STATE_SEND_SMS:
            sendSMS(state.number, state.name);
            hangup();
            break;
        default:
            break;
        }
    }

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {}

    @Override
    public void onChannelAvailable(boolean isAvailable) {
        if (isAvailable) {
            for (int i = 0; i < mDefaultSms.length; i++) {
                sendFastSMS(i);
            }
        }
    }

    @Override
    public void onSendResult(DataTransactResult result) {}

    private void sendFastSMS(int index) {
        SharedPreferences sp = getSharedPreferences("sms", MODE_PRIVATE);
        String sms = index < mDefaultSms.length ? mDefaultSms[index] : null;
        sms = sp.getString("sms_" + index, sms);
        if (sms != null) {
            PhoneState state = new PhoneState();
            state.state = -1;
            state.number = "sms_" + index;
            state.name = sms;
            sModel.send(state);
        }
    }

    private void hangup() {
        Class<?> cls = mTelephonyManager.getClass();
        try {
            Method method = cls.getDeclaredMethod("getITelephony", (Class<?>[]) null);
            method.setAccessible(true);
            ITelephony t = (ITelephony) method.invoke(mTelephonyManager, (Object[]) null);
            t.endCall();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendSMS(String number, String sms) {
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, new Intent(), 0);
        SmsManager sm = SmsManager.getDefault();
        sm.sendTextMessage(number, null, sms, pendingIntent, null);

        ContentValues values = new ContentValues(9);
        values.put("address", number);
        values.put("date", System.currentTimeMillis());
        values.put("type", 2);
        values.put("body", sms);
        getContentResolver().insert(Uri.parse("content://sms"), values);
    }

    private PhoneStateTransactionModel getModel() {
        return sModel;
    }

    public static class PhoneBinder extends Binder {
        private static PhoneBinder sInstance;
        private PhoneService mService;
        private PhoneBinder(PhoneService service) {
            mService = service;
        }

        public synchronized static PhoneBinder getInstance(PhoneService service) {
            if (sInstance == null) {
                sInstance = new PhoneBinder(service);
            }
            return sInstance;
        }

        public PhoneStateTransactionModel getTransactionModel() {
            if (mService == null) return null;
            return mService.getModel();
        }
    }
}