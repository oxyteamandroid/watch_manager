/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  nongjiabao<jiabao.nong@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.contact;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * 同步联系人工具类
 * @author jbnong
 *
 */
public class ContactUtils {
    public static final String CONTACTS_BACKUP_STATE ="contacts_backup_state";
    public static final String WATCH_ADDRESS="watch-address";
    public static final String WATCH_ADDRESS_DEFUALT="00:00:00:00:00:00";
    public static final String SHARE_PREFERENCE = "watch-manager";
    //通讯录同步状态
    public static final int CONTACTS_STATE_UNKNOWN      = -1;
    public static final int CONTACTS_STATE_BACKUPED     = 1;
    public static final int CONTACTS_STATE_SYNC_SUCCESS = 2;
    public static final int CONTACTS_STATE_SYNC_FAILED  = 3;
    public static final int CONTACTS_STATE_PHONE_MAC_CHANGE = 4;
    public static final int CALLLOG_LIMIT_NUM = 10;
    public static final String CMD_VCARD_EMPTY ="vcard-empty";
    public static final String CMD_SYNC_FAILED ="sync-failed";

    public static void setContactsSyncstate(Context context, int state){
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCE,
                Context.MODE_PRIVATE);
        sp.edit().putInt(CONTACTS_BACKUP_STATE, state).commit();
    }

    public static int getContactsSyncstate(Context context){
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCE,
                Context.MODE_PRIVATE);
        return sp.getInt(CONTACTS_BACKUP_STATE, CONTACTS_STATE_UNKNOWN);
    }

    /** 绑定连接后，保存 watch 的 mac 地址
     * @param context
     * @param watch_address
     */

    public static void setWatchMacAddress(Context context, String watch_address){
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCE, Context.MODE_PRIVATE);
        sp.edit().putString(WATCH_ADDRESS, watch_address).commit();
    }

    /** 获取 watch 的 mac 地址
     * @param context
     * @return
     */
    public static String getWatchMacAddress(Context context){
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCE, Context.MODE_PRIVATE);
        return sp.getString(WATCH_ADDRESS, WATCH_ADDRESS_DEFUALT);
    }
}
