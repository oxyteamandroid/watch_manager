/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * nongjiabao<jiabao.nong@ingenic.com>
 *
 * Elf/IDWS Project
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.watchmanager.contact;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.provider.ContactsContract.RawContacts;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.FileInfo;
import com.ingenic.iwds.datatransactor.FileTransactionModel;
import com.ingenic.iwds.datatransactor.FileTransactionModel.FileTransactionModelCallback;
import com.ingenic.iwds.datatransactor.elf.CalllogInfo;
import com.ingenic.iwds.datatransactor.elf.ContactInfo;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.util.UUIDS;

/**
 * 同步联系人服务
 * @author jbnong
 *
 */
public class ContactSyncService extends Service {

    private static final String TAG = "ContactSyncService";
    public static final String CONTACTS_SERVICE_ACTION = "com.ingenic.watchmanager.contact.ContactSyncService";
    public static final String CONTACTS_BACKUPED_ACTION = "com.ingenic.watchmanager.contact.backuped";
    private static MyContactFiletransModel mFiletransModel;
    private ContactsListener mContactsListener;
    private Context mContext;
    private static boolean mIsAvailable = false;
    private boolean mIsSameWatch = false;

    private BroadcastReceiver mContactsReceiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(CONTACTS_BACKUPED_ACTION)){
                IwdsLog.i(TAG, "contacts back up finish, start sync to watch.");
                Contactshandler.obtainMessage(CONTACTS_BACKUP_FINISHED).sendToTarget();
            }
        }
    };
    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate() {
        IwdsLog.i(TAG, "++++ContactsService create.");
        /**
         * 保持服务一直在运行
         */
        CharSequence label = getApplication().getApplicationInfo().loadLabel(
                getPackageManager());

        Notification notification = new Notification(getApplication()
                .getApplicationInfo().icon, label, System.currentTimeMillis());

        Intent it = new Intent(
                ServiceManagerContext.ACTION_NOTIFICATION_CLICKED);
        it.setFlags(it.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, it, 0);
        notification.setLatestEventInfo(this, label, "", pendingIntent);
        notification.flags = notification.flags
                | Notification.FLAG_ONGOING_EVENT;

        startForeground(9999, notification);

        mContext = getBaseContext();
        /*注册一个Receiver，等contacts.vcf生成后，接收CONTACTS_BACKUPED_ACTION*/
        IntentFilter filter = new IntentFilter(CONTACTS_BACKUPED_ACTION);
        registerReceiver(mContactsReceiver, filter);
        MyContactsThread myThread = new MyContactsThread();
        new Thread(myThread).start();

        if (mFiletransModel == null){
            mFiletransModel = new MyContactFiletransModel(mContext, new ContactFiletransCallback(),UUIDS.CONTACTS);
        }
        mFiletransModel.start();
        mContactsListener =  ContactsListener.getInstance(mContext);
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        if (mFiletransModel != null) {
            mFiletransModel.stop();
        }
        unregisterReceiver(mContactsReceiver);
        mContactsReceiver = null;
        if (mContactsListener !=null){
            mContactsListener.unregisterContentObservers();
        }
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        return super.onStartCommand(intent, flags, startId);
    }

    private class MyContactFiletransModel extends FileTransactionModel {

        public MyContactFiletransModel(Context context, FileTransactionModelCallback callback,
                String uuid) {
            super(context, callback, uuid);
            // TODO Auto-generated constructor stub
        }

        public void sendContactsInfo2Watch(ContactInfo info){
            IwdsLog.i(TAG, "send to watch Contact info: _id: " + info.getRaw_id() + " name: " + info.getName());
            this.m_transactor.send(info);
        }
        public void sendcmd2watch(String cmd) {
            this.m_transactor.send(cmd);
        }
        @Override
        public void onDataArrived(Object object) {

            super.onDataArrived(object);
            if (object instanceof String){
                String cmd = (String)object;
                if (cmd.equals("first_run")){
                    IwdsLog.i(TAG, "the watch is first run");
                    if (mIsSameWatch){
                        Contactshandler.obtainMessage(MSG_PHONE_MAC_CHANGE).sendToTarget();
                    }
                }
                if (cmd.equals("phone_mac_ischange")){
                    IwdsLog.i(TAG, "phone mac is change");
                    if (mIsSameWatch) {
                        Contactshandler.obtainMessage(MSG_PHONE_MAC_CHANGE).sendToTarget();
                    }
                } 
                if (cmd.equals("contacts_sync_success")){
                    IwdsLog.i(TAG, "contacts sync success");
                    ContactUtils.setContactsSyncstate(mContext, ContactUtils.CONTACTS_STATE_SYNC_SUCCESS);
                    mContactsListener.syncNewContacts();
                }
            }
        }
    }

    private class ContactFiletransCallback implements FileTransactionModelCallback {

        @Override
        public void onRequestSendFile(FileInfo info) {


        }

        @Override
        public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
            if ( isConnected ){
                Contactshandler.obtainMessage(MSG_LINK_CONNECTED, descriptor).sendToTarget();
            }
        }

        @Override
        public void onChannelAvailable(boolean isAvailable) {
            mIsAvailable = isAvailable;
            if (isAvailable){
                IwdsLog.i(TAG, "+++onChannelAvailable: " + isAvailable);
                Contactshandler.obtainMessage(MSG_DATA_CHANNEL_AVAILABLE).sendToTarget();
            }
        }

        @Override
        public void onSendResult(DataTransactResult result) {
            if (result.getResultCode() == DataTransactResult.RESULT_OK) {
                if (result.getTransferedObject() instanceof File) {
                    IwdsLog.i(TAG, "Send contactsFile success");
                    Contactshandler.obtainMessage(MSG_FILE_SEND_OK).sendToTarget();
                } else if (result.getTransferedObject() instanceof ContactInfo ) {
                    ContactInfo info = (ContactInfo)result.getTransferedObject();
                    IwdsLog.i(TAG, "Send contactInfo success : _id: " + info.getRaw_id());
                }
            } else {
                if (result.getTransferedObject() instanceof File) {
                    IwdsLog.i(TAG, "Send contactsFile failed by error code: " + result.getResultCode());
                    Contactshandler.obtainMessage(MSG_FILE_SEND_FAILED).sendToTarget();
                } else if (result.getTransferedObject() instanceof ContactInfo ) {
                    ContactInfo info = (ContactInfo)result.getTransferedObject();
                    IwdsLog.i(TAG, "Send contactInfo failed by error code: " + result.getResultCode() + " _id: " + info.getRaw_id());
                }
            }
        }

        @Override
        public void onFileArrived(File file) {


        }

        @Override
        public void onSendFileProgress(int progress) {

        }

        @Override
        public void onRecvFileProgress(int progress) {


        }

        @Override
        public void onConfirmForReceiveFile() {


        }

        @Override
        public void onCancelForReceiveFile() {


        }

        @Override
        public void onFileTransferError(int errorCode) {

        }

        @Override
        public void onSendFileInterrupted(int index) {

        }

        @Override
        public void onRecvFileInterrupted(int index) {

        }

    }
    private final static int CONCACTSMODLE = 0x10;
    private final static int MSG_DATA_CHANNEL_AVAILABLE = CONCACTSMODLE | 0;
    private final static int MSG_FILE_SEND_OK           = CONCACTSMODLE | 1;
    private final static int MSG_FILE_SEND_FAILED       = CONCACTSMODLE | 2;
    private final static int CONTACTS_BACKUP_FINISHED   = CONCACTSMODLE | 3;
    private final static int MSG_LINK_CONNECTED         = CONCACTSMODLE | 4;
    private final static int MSG_PHONE_MAC_CHANGE       = CONCACTSMODLE | 5;
    private Handler Contactshandler;
    class MyContactsThread implements Runnable{
        @Override
        public void run() {
            Looper.prepare();
            Contactshandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    switch (msg.what) {
                        case MSG_LINK_CONNECTED:
                            DeviceDescriptor descriptor = (DeviceDescriptor)msg.obj;
                            String MAC = ContactUtils.getWatchMacAddress(mContext);
                            if(MAC.equals(descriptor.devAddress)){
                                mIsSameWatch = true;
                            } else {
                                mIsSameWatch = false;
                                ContactUtils.setContactsSyncstate(mContext, ContactUtils.CONTACTS_STATE_UNKNOWN);
                                ContactUtils.setWatchMacAddress(mContext, descriptor.devAddress);
                            }
                            break;
                        case MSG_PHONE_MAC_CHANGE:
                            ContactHandler handler = ContactHandler.getInstance(getApplicationContext());
                            /**切换手机，重新生成contacts.vcf文件**/
                            mContactsListener.queryIdAndVersion();
                            handler.StartReadContactsThread(mContext);
                            break;

                        case CONTACTS_BACKUP_FINISHED:
                            if (mIsAvailable) {
                                String vcfpath = Environment.getExternalStorageDirectory() + "/contacts.vcf";
                                File vcffile = new File(vcfpath);
                                if (!vcffile.exists()) {
                                    IwdsLog.e(TAG, "contacts.vcf file is not exists");
                                    break;
                                }
                                if (vcffile.length() == 0) {
                                    mFiletransModel.sendcmd2watch(ContactUtils.CMD_VCARD_EMPTY);
                                    IwdsLog.w(TAG, "vcard file is empty");
                                    break;
                                }
                                ContactUtils.setContactsSyncstate(mContext, ContactUtils.CONTACTS_STATE_SYNC_FAILED);
                                try {
                                    IwdsLog.i(TAG, "send file to watch :" + vcffile.getAbsolutePath());
                                    mFiletransModel.requestSendFile(vcffile.getAbsolutePath());
                                } catch (FileNotFoundException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            } else {
                                ContactUtils.setContactsSyncstate(mContext, ContactUtils.CONTACTS_STATE_SYNC_FAILED);
                            }
                            break;

                        case MSG_DATA_CHANNEL_AVAILABLE:
                            int syncstate = ContactUtils.getContactsSyncstate(mContext);
                            IwdsLog.i(TAG, "sync contacts mIsSameWatch:" + mIsSameWatch + " syncstate:" + syncstate);

                            if (mIsSameWatch == false) {
                                ContactHandler handler1 = ContactHandler.getInstance(getApplicationContext());
                                /**生成contacts.vcf文件**/
                                mContactsListener.queryIdAndVersion();
                                handler1.StartReadContactsThread(mContext);
                            }

                            /*同一手表，假如同步文件失败，链接正常时，再同步一次*/
                            if ((mIsSameWatch == true && syncstate == ContactUtils.CONTACTS_STATE_SYNC_FAILED)) {
                                String vcfpath = Environment.getExternalStorageDirectory() + "/contacts.vcf";
                                File vcffile = new File(vcfpath);
                                if (!vcffile.exists()) {
                                    IwdsLog.e(TAG, "contacts.vcf file is not exists");
                                    break;
                                }
                                if (vcffile.length() == 0) {
                                    mFiletransModel.sendcmd2watch(ContactUtils.CMD_VCARD_EMPTY);
                                    IwdsLog.w(TAG, "vcard file is empty");
                                    break;
                                }
                                ContactUtils.setContactsSyncstate(mContext, ContactUtils.CONTACTS_STATE_SYNC_FAILED);
                                try {
                                    IwdsLog.i(TAG, "send file to watch :" + vcffile.getAbsolutePath());
                                    mFiletransModel.requestSendFile(vcffile.getAbsolutePath());
                                } catch (FileNotFoundException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }

                            if (syncstate == ContactUtils.CONTACTS_STATE_SYNC_SUCCESS){
                                mContactsListener.syncNewContacts();
                            }
                            break;
                        case MSG_FILE_SEND_OK:
                            /* 同步contacts.vcf 结束后，把状态设置成CONTACTS_STATE_SYNC_SUCCESS*/
                            //ContactUtils.setContactsSyncstate(mContext, ContactUtils.CONTACTS_STATE_SYNC_SUCCESS);
                            break;
                        case MSG_FILE_SEND_FAILED:
                            /* 同步contacts.vcf  失败，把状态设置成CONTACTS_STATE_SYNC_FAILED*/
                            mFiletransModel.sendcmd2watch(ContactUtils.CMD_SYNC_FAILED);
                            ContactUtils.setContactsSyncstate(mContext, ContactUtils.CONTACTS_STATE_SYNC_FAILED);
                        default:
                            break;
                    }
                    super.handleMessage(msg);
                }
            };
            Looper.loop();
        }
    };

    public static class ContactsListener {
        private String TAG = "ContactSyncService";
        private Context mContext = null;
        private static ContactsListener sInstance = null;
        private static final int ELAPSE_TIME = 100;
        private static final int CONTACT_CHANGE = 1;
        private Handler mHandler;
        ArrayList<String> mChangedContacts = new ArrayList<String>();
        ArrayList<String> mDeletedContacts = new ArrayList<String>();
        ArrayList<String> mAddedContacts = new ArrayList<String>();

        private static final String[] PHONES_PROJECTION = new String[] {
                RawContacts._ID, RawContacts.VERSION
        };

        ContactHandler mContactshandler;

        private ContentObserver mObContactsserver = new ContentObserver(new Handler()) {

            @Override
            public void onChange(boolean selfChange) {
                IwdsLog.i(TAG, "contacts ContentObserver");
                mHandler.sendEmptyMessageDelayed(CONTACT_CHANGE, ELAPSE_TIME);
            }

        };

        private void registerContentObservers() {
            mContext.getContentResolver().registerContentObserver(RawContacts.CONTENT_URI, true,
                    mObContactsserver);

        }

        public void unregisterContentObservers() {
            mContext.getContentResolver().unregisterContentObserver(mObContactsserver);

        }

        public static ContactsListener getInstance(Context context) {
            if (sInstance == null) {
                sInstance = new ContactsListener(context);
            }
            return sInstance;
        }
        class MyThread implements Runnable{
            @Override
            public void run() {
                Looper.prepare();
                mHandler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        switch (msg.what)
                        {
                            case CONTACT_CHANGE:
                                int syncstate = ContactUtils.getContactsSyncstate(mContext);
                                if (syncstate != ContactUtils.CONTACTS_STATE_SYNC_SUCCESS || !mIsAvailable){
                                    return;
                                }

                                ContactsChange();
                                break;
                            default:
                                break;
                        }
                        super.handleMessage(msg);
                    }
                };
                Looper.loop();
            }
        };

        public ContactsListener(Context context) {
            mContext = context;
            mContactshandler = ContactHandler.getInstance(mContext);
            MyThread myThread = new MyThread();
            new Thread(myThread).start();
            registerContentObservers();
        }

        public void syncNewContacts(){
            IwdsLog.i(TAG, "+++syncNewContacts");
            mHandler.sendEmptyMessageDelayed(CONTACT_CHANGE, ELAPSE_TIME);
        }

        /**
         * 进一部判断是否修改通讯录，注意：打电话也会出发了此方法，因为监听的是uri
         */
        public void ContactsChange()
        {
            /**
             * 清除数据列表
             */
            IwdsLog.i(TAG, "+++ContactsChange");
            clearArrayList();
            String idStr;
            String versionStr;
            ArrayList<String> newid = new ArrayList<String>();
            ArrayList<String> newversion = new ArrayList<String>();
            /*********读取旧的的id 和 version************/
            SharedPreferences sp = mContext.getSharedPreferences("sd", Context.MODE_PRIVATE);
            idStr = sp.getString("id", "");
            versionStr = sp.getString("version", "");
            String[] mid = idStr.split("#");
            String[] mversion = versionStr.split("#");

            ContentResolver resolver = mContext.getContentResolver();
            Cursor phoneCursor = resolver.query(RawContacts.CONTENT_URI, PHONES_PROJECTION,
                    RawContacts.DELETED + "==0 and 1==" + RawContacts.DIRTY, null, null);

            if(phoneCursor == null) {
                IwdsLog.i(TAG, "phoneCursor is null,return");
                return ;
            }

            /*********保存新的id 和 version************/
            String id="",version="";
            SharedPreferences sp1 = mContext.getSharedPreferences("sd", Context.MODE_PRIVATE);
            Editor editor1 = sp1.edit();
            editor1.clear();
            editor1.commit();
            while (phoneCursor.moveToNext())
            {
                id += phoneCursor.getString(0) + "#";
                version += phoneCursor.getString(1) + "#";
                Editor editor = sp1.edit();
                editor.putString("id", id);
                editor.putString("version", version);
                editor.commit();

                newid.add(phoneCursor.getString(0));
                newversion.add(phoneCursor.getString(1));
            }
            phoneCursor.close();
            for (int i = 0; i < mid.length; i++)
            {
                int k = newid.size();
                int j;
                for (j = 0; j < k; j++)
                {
                    /**
                     * 找到了，但版本不一样，说明联系人发生了更新。
                     */
                    if (mid[i].equals(newid.get(j)))
                    {
                        if (!(mversion[i].equals(newversion.get(j))))
                        {
                            mChangedContacts.add(newid.get(j) + "#" + newversion.get(j));
                            newid.remove(j);
                            newversion.remove(j);
                            break;
                        }
                        if (mversion[i].equals(newversion.get(j)))
                        {
                            newid.remove(j);
                            newversion.remove(j);
                            break;
                        }
                    }
                }
                /**
                 * 如果在新的链表中找不到联系人
                 */
                if (j >= k)
                {
                    mDeletedContacts.add(mid[i] + "#" + mversion[i]);
                }
            }
            /**
             *  查找新增加的成员
             */
            int n = newid.size();
            for (int m = 0; m < n; m++)
            {
                mAddedContacts.add(newid.get(m) + "#" + newversion.get(m));
            }
            dumpChangeContacts();
        }

        private void clearArrayList() {
            mChangedContacts.clear();
            mAddedContacts.clear();
            mDeletedContacts.clear();
        }

        private void dumpChangeContacts() {
            int i;
            int size;
            String temp[];

            size = mChangedContacts.size();
            for (i = 0; i < size; i++) {
                IwdsLog.i(TAG, "changedContacts: " + mChangedContacts.get(i));
                temp = mChangedContacts.get(i).split("#");

                if (temp.length == 0)
                    continue;

                List<ContactInfo> _infoList = mContactshandler.getContactInfo(temp[0]);
                for (ContactInfo info :_infoList){
                    info.operation = ContactInfo.OPT_UPDATE;
                    mFiletransModel.sendContactsInfo2Watch(info);
                }
            }

            size = mDeletedContacts.size();
            for (i = 0; i < size; i++) {
                IwdsLog.i(TAG, "deletedContacts: " + mDeletedContacts.get(i));
                temp = mDeletedContacts.get(i).split("#");

                if (temp.length == 0)
                    continue;

                ContactInfo info = new ContactInfo(temp[0], "");
                info.operation = ContactInfo.OPT_DEL;
                mFiletransModel.sendContactsInfo2Watch(info);
            }

            size = mAddedContacts.size();
            for (i = 0; i < size; i++) {

                IwdsLog.i(TAG, "addedContacts: " + mAddedContacts.get(i));
                temp = mAddedContacts.get(i).split("#");

                if (temp.length == 0)
                    continue;

                List<ContactInfo> _infoList = mContactshandler.getContactInfo(temp[0]);
                for (ContactInfo info :_infoList){
                    info.operation = ContactInfo.OPT_ADD;
                    mFiletransModel.sendContactsInfo2Watch(info);
                }
            }
        }
        /**
         * 程序刚开始运行时，存入sd.xml,后使用。
         */
        public void queryIdAndVersion()
        {
            IwdsLog.i(TAG, "+++queryIdAndVersion");
            String id = "";
            String version = "";
            /**
             * 存之前，先清理原来的数据
             */
            SharedPreferences sp1 = mContext.getSharedPreferences("sd", Context.MODE_PRIVATE);
            Editor editor1 = sp1.edit();
            editor1.clear();
            editor1.commit();

            ContentResolver resolver = mContext.getContentResolver();
            /**
             *  此处取判断dirty为1的原因是我发现我的通讯录的db会被手机QQ改变，
             * 手机qq会把dirty变成0.。。安卓通讯录数据的删除只是把deleted置为1
             */
            Cursor phoneCursor = resolver.query(RawContacts.CONTENT_URI, PHONES_PROJECTION,
                    RawContacts.DELETED + "==0 and 1==" + RawContacts.DIRTY, null, null);

            if (phoneCursor == null) {
                IwdsLog.i(TAG, "phoneCursor is null");
                return;
            }
            while (phoneCursor.moveToNext())
            {

                id += phoneCursor.getString(0) + "#";
                version += phoneCursor.getString(1) + "#";
            }
            SharedPreferences sp = mContext.getSharedPreferences("sd", Context.MODE_PRIVATE);
            Editor editor = sp.edit();
            editor.putString("id", id);
            editor.putString("version", version);
            editor.commit();
            phoneCursor.close();
        }
    }
}
