/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * nongjiabao<jiabao.nong@ingenic.com>
 *
 * Elf/IDWS Project
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.watchmanager.contact;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import a_vcard.android.provider.Contacts;
import a_vcard.android.syncml.pim.PropertyNode;
import a_vcard.android.syncml.pim.VDataBuilder;
import a_vcard.android.syncml.pim.VNode;
import a_vcard.android.syncml.pim.vcard.ContactStruct;
import a_vcard.android.syncml.pim.vcard.ContactStruct.ContactMethod;
import a_vcard.android.syncml.pim.vcard.ContactStruct.PhoneData;
import a_vcard.android.syncml.pim.vcard.VCardComposer;
import a_vcard.android.syncml.pim.vcard.VCardException;
import a_vcard.android.syncml.pim.vcard.VCardParser;
import android.content.ContentProviderOperation;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.PowerManager;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.RawContacts;
import android.provider.ContactsContract.RawContacts.Data;

import com.ingenic.iwds.datatransactor.elf.ContactInfo;
import com.ingenic.iwds.utils.IwdsLog;

/**
 * ContactHandler 电话本备份和恢复
 * @author jbnong
 *
 */
public class ContactHandler {
    private static String TAG = "ContactHandler";
    private static String mContacts_vcf_file = Environment.getExternalStorageDirectory() + "/contacts.vcf";
    private static ContactHandler mInstance = null;
    private static Context mContext;
    private static PowerManager.WakeLock mWakeLock = null;

    public ContactHandler(Context context) {
        mContext = context;
        PowerManager powerMgr = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        mWakeLock = powerMgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "restoreContactsThread");
        mWakeLock.setReferenceCounted(false);
    }

    public synchronized void acquireWakeLock() {
        if (!mWakeLock.isHeld()) {
            IwdsLog.i(TAG, "acquire WakeLock.");
            mWakeLock.acquire();
        } else {
            IwdsLog.i(TAG, "WakeLock already acquire.");
        }
    }

    public synchronized void releaseWakeLock() {
        synchronized (mWakeLock) {
            if (mWakeLock.isHeld()) {
                IwdsLog.i(TAG, "release WakeLock");
                mWakeLock.release();
            } else {
                IwdsLog.i(TAG, "WakeLock not locked");
            }
        }
    }

    /** 获取实例 */
    public static ContactHandler getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ContactHandler(context);
        }
        return mInstance;
    }

    /**
     * 获取联系人指定信息
     *
     * @param projection 指定要获取的列数组, 获取全部列则设置为null
     * @return
     * @throws Exception
     */
    public Cursor queryContact(String[] projection, String raw_contact_id) {
        // 获取联系人的所需信息
        Cursor cur;
        if (raw_contact_id == null) {
            cur = mContext.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                    projection, null, null, null);
        } else {
            cur = mContext.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                    projection, "name_raw_contact_id" + "==" + raw_contact_id, null, null);
        }
        return cur;
    }

    /**
     * 获取联系人信息
     *
     * @param context
     * @return
     */
    public List<ContactInfo> getContactInfo(String raw_id) {
        List<ContactInfo> infoList = new ArrayList<ContactInfo>();

        Cursor cur = queryContact(null, raw_id);

        if (cur.moveToFirst()) {
            do {

                // 获取联系人id号
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String rawid = cur.getString(cur.getColumnIndex("name_raw_contact_id"));
                // 获取联系人姓名
                String displayName = cur.getString(cur
                        .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                ContactInfo info = new ContactInfo(rawid, displayName);// 初始化联系人信息

                // 查看联系人有多少电话号码, 如果没有返回0
                int phoneCount = cur.getInt(cur
                        .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                if (phoneCount > 0) {

                    Cursor phonesCursor = mContext.getContentResolver().query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + id, null,
                            null);

                    if (phonesCursor != null) {
                        if (phonesCursor.moveToFirst()) {
                            List<ContactInfo.PhoneInfo> phoneNumberList = new ArrayList<ContactInfo.PhoneInfo>();
                            do {
                                // 遍历所有电话号码
                                String phoneNumber = phonesCursor
                                        .getString(phonesCursor
                                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                // 对应的联系人类型
                                int type = phonesCursor
                                        .getInt(phonesCursor
                                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                                // 初始化联系人电话信息
                                ContactInfo.PhoneInfo phoneInfo = new ContactInfo.PhoneInfo();
                                phoneInfo.type = type;
                                phoneInfo.number = phoneNumber;
                                phoneNumberList.add(phoneInfo);
                            } while (phonesCursor.moveToNext());
                            // 设置联系人电话信息
                            phonesCursor.close();
                            info.setPhoneList(phoneNumberList);
                        }
                    }
                }

                // 获得联系人的EMAIL
                Cursor emailCur = mContext.getContentResolver().query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + "=" + id, null,
                        null);

                if (emailCur != null) {
                    if (emailCur.moveToFirst()) {
                        List<ContactInfo.EmailInfo> emailList = new ArrayList<ContactInfo.EmailInfo>();
                        do {
                            // 遍历所有的email
                            String email = emailCur.getString(emailCur
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA1));
                            int type = emailCur.getInt(emailCur
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
                            // 初始化联系人邮箱信息
                            ContactInfo.EmailInfo emailInfo = new ContactInfo.EmailInfo();
                            emailInfo.type = type;
                            emailInfo.email = email;
                            emailList.add(emailInfo);

                        } while (emailCur.moveToNext());
                        emailCur.close();
                        info.setEmail(emailList);
                    }
                }
                infoList.add(info);
            } while (cur.moveToNext());
        }
        cur.close();
        return infoList;
    }

    /**
     * 备份联系人
     */
    public void backupContacts(List<ContactInfo> infos) {

        try {

            String path = mContacts_vcf_file;

            OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(path),
                    "UTF-8");

            VCardComposer composer = new VCardComposer();

            for (ContactInfo info : infos)
            {
                ContactStruct contact = new ContactStruct();
                contact.name = info.getName();
                // 获取联系人电话信息, 添加至 ContactStruct
                List<ContactInfo.PhoneInfo> numberList = info
                        .getPhoneList();
                for (ContactInfo.PhoneInfo phoneInfo : numberList)
                {
                    contact.addPhone(phoneInfo.type, phoneInfo.number,
                            null, true);
                }
                // 获取联系人Email信息, 添加至 ContactStruct
                List<ContactInfo.EmailInfo> emailList = info.getEmail();
                for (ContactInfo.EmailInfo emailInfo : emailList)
                {
                    contact.addContactmethod(Contacts.KIND_EMAIL,
                            emailInfo.type, emailInfo.email, null, true);
                }

                List<String> list = new ArrayList<String>();
                list.add(info.raw_id);
                PropertyNode node;
                node = new PropertyNode("UID", "UID", list, null, null, null, null);
                contact.addExtension(node);

                String vcardString = composer.createVCard(contact,
                        VCardComposer.VERSION_VCARD30_INT);
                writer.write(vcardString);
                writer.write("\n");

                writer.flush();
            }
            writer.close();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (VCardException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        IwdsLog.i(TAG, "backup contacts success!");
    }

    /**
     * 获取vCard文件中的联系人信息
     *
     * @return
     */
    @SuppressWarnings("resource")
    public List<ContactInfo> restoreContacts() throws Exception {
        long start;
        List<ContactInfo> contactInfoList = new ArrayList<ContactInfo>();

        VCardParser parse = new VCardParser();
        VDataBuilder builder = new VDataBuilder();
        String file = mContacts_vcf_file;
        IwdsLog.i(TAG, "contacts file:" + file);

        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(
                file), "UTF-8"));

        String vcardString = "";
        String line;
        start = System.currentTimeMillis();
        while ((line = reader.readLine()) != null) {
            vcardString += line + "\n";
            if (line.contains("END:VCARD")) {
                builder.vNodeList.clear(); /* 解析前清除list数据 */
                boolean parsed = parse.parse(vcardString, "UTF-8", builder);
                if (!parsed) {
                    throw new VCardException("Could not parse vCard file: " + file);
                }

                List<VNode> pimContacts = builder.vNodeList;
                for (VNode contact : pimContacts) {

                    ContactStruct contactStruct = ContactStruct.constructContactFromVNode(
                            contact, 1);
                    // 获取备份文件中的联系人电话信息
                    List<PhoneData> phoneDataList = contactStruct.phoneList;
                    List<ContactInfo.PhoneInfo> phoneInfoList = new ArrayList<ContactInfo.PhoneInfo>();
                    if (null != phoneDataList) {
                        for (PhoneData phoneData : phoneDataList) {
                            ContactInfo.PhoneInfo phoneInfo = new ContactInfo.PhoneInfo();
                            phoneInfo.number = phoneData.data;
                            phoneInfo.type = phoneData.type;
                            phoneInfoList.add(phoneInfo);
                        }
                    }
                    // 获取备份文件中的联系人邮箱信息
                    List<ContactMethod> emailList = contactStruct.contactmethodList;
                    List<ContactInfo.EmailInfo> emailInfoList = new ArrayList<ContactInfo.EmailInfo>();
                    // 存在 Email 信息
                    if (null != emailList)
                    {
                        for (ContactMethod contactMethod : emailList)
                        {
                            if (Contacts.KIND_EMAIL == contactMethod.kind)
                            {
                                ContactInfo.EmailInfo emailInfo = new ContactInfo.EmailInfo();
                                emailInfo.email = contactMethod.data;
                                emailInfo.type = contactMethod.type;
                                emailInfoList.add(emailInfo);
                            }
                        }
                    }
                    String raw_id = null;
                    Map<String, List<String>> extensionmap = contactStruct.extensionMap;
                    if (null != extensionmap) {
                        if (extensionmap.containsKey("UID")) {
                            PropertyNode node = PropertyNode.decode(extensionmap.get("UID")
                                    .get(0));
                            raw_id = node.propValue;
                        }
                    }
                    ContactInfo info = new ContactInfo(raw_id, contactStruct.name);
                    info.setPhoneList(phoneInfoList);
                    info.setEmail(emailInfoList);
                    contactInfoList.add(info);
                }/* end for pimContacts */
                vcardString = "";/* 读完一个，在读下一个，如此循环 */
            }/* end if line.contains("END:VCARD") */
        }
        IwdsLog.i(TAG, "++++++++++parse vcard used time:" + (System.currentTimeMillis() - start)
                + "contactInfoList size:" + contactInfoList.size());
        reader.close();
        return contactInfoList;
    }

    /**
     * 向手机中录入联系人信息
     * 
     * @param info 要录入的联系人信息
     */
    public void addContacts(ContactInfo info) {
        ContentValues values = new ContentValues();
        // 首先向RawContacts.CONTENT_URI执行一个空值插入，_ID为info的raw_id
        values.put(RawContacts._ID, info.raw_id);
        Uri rawContactUri = mContext.getContentResolver()
                .insert(RawContacts.CONTENT_URI, values);
        long rawContactId = ContentUris.parseId(rawContactUri);
        // 往data表入姓名数据
        values.clear();
        values.put(Data.RAW_CONTACT_ID, rawContactId);
        values.put(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE);
        values.put(StructuredName.GIVEN_NAME, info.getName());
        mContext.getContentResolver().insert(
                android.provider.ContactsContract.Data.CONTENT_URI, values);

        // 获取联系人电话信息
        List<ContactInfo.PhoneInfo> phoneList = info.getPhoneList();
        /** 录入联系电话 */
        for (ContactInfo.PhoneInfo phoneInfo : phoneList) {
            values.clear();
            values.put(android.provider.ContactsContract.Contacts.Data.RAW_CONTACT_ID,
                    rawContactId);
            values.put(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE);
            // 设置录入联系人电话信息
            values.put(Phone.NUMBER, phoneInfo.number);
            values.put(Phone.TYPE, phoneInfo.type);
            // 往data表入电话数据
            mContext.getContentResolver().insert(
                    android.provider.ContactsContract.Data.CONTENT_URI, values);
        }

        // 获取联系人邮箱信息
        List<ContactInfo.EmailInfo> emailList = info.getEmail();

        /** 录入联系人邮箱信息 */
        for (ContactInfo.EmailInfo email : emailList) {
            values.clear();
            values.put(android.provider.ContactsContract.Contacts.Data.RAW_CONTACT_ID,
                    rawContactId);
            values.put(Data.MIMETYPE, Email.CONTENT_ITEM_TYPE);
            // 设置录入的邮箱信息
            values.put(Email.DATA, email.email);
            values.put(Email.TYPE, email.type);
            // 往data表入Email数据
            mContext.getContentResolver().insert(
                    android.provider.ContactsContract.Data.CONTENT_URI, values);
        }
    }

    public void deleteContactsfile() {
        File contactsfile;
        contactsfile = new File(mContacts_vcf_file);
        IwdsLog.i(TAG, "++++deleteContactsfile: " + contactsfile.getAbsolutePath());
        if (contactsfile.exists()) {
            contactsfile.delete();
        }
    }

    public boolean isContactsfileExist() {
        if (new File(mContacts_vcf_file).exists()) {
            IwdsLog.i(TAG, mContacts_vcf_file + " is exists");
            return true;
        } else {
            IwdsLog.i(TAG, mContacts_vcf_file + " is not exists");
            return false;
        }
    }

    /**
     * 批量添加联系人，处于同一个事务中
     */
    public void BatchRestoreContacts(List<ContactInfo> infoList) throws Throwable {
        // 文档位置：reference\android\provider\ContactsContract.RawContacts.html
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        int rawContactInsertIndex = 0;
        final int opsnumlimit = 400;
        // 以在手机读到的_ID，插入一条记录到RawContacts表中，目的是让手表的_ID和手表一致，方便查询和更新。
        IwdsLog.i(TAG, "begin commit infoList,size is : " + infoList.size());
        for (ContactInfo info : infoList) {
            rawContactInsertIndex = Integer.parseInt(info.raw_id);
            ops.add(ContentProviderOperation.newInsert(RawContacts.CONTENT_URI)
                    .withValue(RawContacts._ID, rawContactInsertIndex)
                    .withValue(RawContacts.ACCOUNT_TYPE, null)
                    .withValue(RawContacts.ACCOUNT_NAME, null)
                    .build());

            // 文档位置：reference\android\provider\ContactsContract.Data.html
            ops.add(ContentProviderOperation
                    .newInsert(android.provider.ContactsContract.Data.CONTENT_URI)
                    .withValue(Data.RAW_CONTACT_ID, rawContactInsertIndex)
                    .withValue(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(StructuredName.GIVEN_NAME, info.name)
                    .build());
            // 获取联系人电话信息
            List<ContactInfo.PhoneInfo> phoneList = info.getPhoneList();
            for (ContactInfo.PhoneInfo phoneInfo : phoneList) {
                ops.add(ContentProviderOperation
                        .newInsert(android.provider.ContactsContract.Data.CONTENT_URI)
                        .withValue(Data.RAW_CONTACT_ID, rawContactInsertIndex)
                        .withValue(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
                        .withValue(Phone.NUMBER, phoneInfo.number)
                        .withValue(Phone.TYPE, phoneInfo.type)
                        .build());

            }
            // 获取联系人邮箱信息
            List<ContactInfo.EmailInfo> emailList = info.getEmail();

            /* 录入联系人邮箱信息 */
            for (ContactInfo.EmailInfo email : emailList) {
                ops.add(ContentProviderOperation
                        .newInsert(android.provider.ContactsContract.Data.CONTENT_URI)
                        .withValue(Data.RAW_CONTACT_ID, rawContactInsertIndex)
                        .withValue(Data.MIMETYPE, Email.CONTENT_ITEM_TYPE)
                        .withValue(Email.DATA, email.email)
                        .withValue(Email.TYPE, email.type)
                        .build());
            }

            //分批提交，但是每批不能找过500个数据。
            if (ops.size() > opsnumlimit) {
                mContext.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
                ops.clear();
            }
        }

        //把剩下的事物全部提交。
        if (ops.size() > 0) {
            mContext.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            ops.clear();
        }
        IwdsLog.i(TAG, "end commit.");
    }

    /**
     * 清楚数据库
     **/
    public void DeleteRawContacts() {
        mContext.getContentResolver().delete(
                RawContacts.CONTENT_URI.buildUpon()
                        .appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER, "true")
                        .build(), null, null);
    }

    private class restoreContactsThread extends Thread {
        Context mRestoreContext;

        public restoreContactsThread(Context context) {
            mRestoreContext = context;
        }

        @Override
        public void run() {
            // TODO Auto-generated method stub
            long start = 0, end = 0;
            long sbt = 0, ebt = 0;
            IwdsLog.i(TAG, "begin restore contacts");
            start = System.currentTimeMillis();
            ContactHandler handler = ContactHandler.getInstance(mRestoreContext);
            handler.acquireWakeLock();
            handler.DeleteRawContacts();
            try {
                // 获取要恢复的联系人信息
                IwdsLog.i(TAG, "begin read infoList");
                List<ContactInfo> infoList = handler.restoreContacts();
                IwdsLog.i(TAG, "begin Batch");
                sbt = System.currentTimeMillis();
                handler.BatchRestoreContacts(infoList);
                ebt = System.currentTimeMillis();
                IwdsLog.i(TAG, "end Batch, used time: " + (ebt - sbt));
            } catch (Exception e) {
                e.printStackTrace();
            } catch (Throwable e) {
                e.printStackTrace();
            }
            handler.releaseWakeLock();
            end = System.currentTimeMillis();
            IwdsLog.i(TAG, "end restore contacts, used time:" + (end - start));
        }
    }

    public void StartRestoreContactsThread() {
        new restoreContactsThread(mContext).start();
    }

    private class ReadContactsThread extends Thread {
        Context mReadContext;

        public ReadContactsThread(Context context) {
            mReadContext = context;
        }

        @Override
        public void run() {
            long start = 0, end = 0;
            start = System.currentTimeMillis();
            IwdsLog.i(TAG, "start read contacts ...");
            ContactHandler handler = ContactHandler
                    .getInstance(mReadContext);
            // 获取要备份的信息
            List<ContactInfo> _infoList = handler.getContactInfo(null);
            // 备份联系人信息
            handler.backupContacts(_infoList);// 备份联系人信息
            end = System.currentTimeMillis();
            IwdsLog.i(TAG, "read contacts vcard used time:" + (end - start) + " _infoList size is:"
                    + _infoList.size());
            ContactUtils.setContactsSyncstate(mReadContext, ContactUtils.CONTACTS_STATE_BACKUPED);
            mReadContext.sendBroadcast(new Intent(ContactSyncService.CONTACTS_BACKUPED_ACTION));
        }
    }

    public void StartReadContactsThread(Context context) {
        new ReadContactsThread(context).start();
    }
}
