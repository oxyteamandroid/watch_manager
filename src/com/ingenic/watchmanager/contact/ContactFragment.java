/*
 * Copyright (C) 2014 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.contact;

import java.lang.reflect.Method;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingenic.iwds.datatransactor.elf.PhoneState;
import com.ingenic.iwds.datatransactor.elf.PhoneStateTransactionModel;
import com.ingenic.watchmanager.FragmentActivity;
import com.ingenic.watchmanager.IngenicSwitchPreference;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.contact.PhoneService.PhoneBinder;
import com.ingenic.watchmanager.util.Utils;

public class ContactFragment extends PreferenceFragment implements
        BluetoothProfile.ServiceListener, Preference.OnPreferenceChangeListener {
    public ContactFragment() {}

    private IngenicSwitchPreference mHeadsetPreference;
    private BluetoothHeadset mHeadset;
    private String mRemoteAddress;
    private IngenicSwitchPreference mSmsPreference;
    private IngenicSwitchPreference mPhonePreference;

    private HeadsetStateReceiver mHeadsetStateReceiver;
    private PhoneService.PhoneBinder mBinder;

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {}

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBinder = (PhoneBinder) service;
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        SharedPreferences sp = activity.getSharedPreferences(Utils.SP_CONNECTED_DEVICE,
                Context.MODE_PRIVATE);
        mRemoteAddress = sp.getString(Utils.KEY_DEVICE_ADDRESS, null);

        BluetoothAdapter.getDefaultAdapter().getProfileProxy(activity, this,
                BluetoothProfile.HEADSET);

        mHeadsetStateReceiver = new HeadsetStateReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED);
        activity.registerReceiver(mHeadsetStateReceiver, intentFilter);
        activity.bindService(new Intent(activity, PhoneService.class), mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        addPreferencesFromResource(R.xml.contact_settings);

        mHeadsetPreference = (IngenicSwitchPreference) findPreference("bluetooth_headset");
        mHeadsetPreference.setEnabled(false);
        mHeadsetPreference.setOnPreferenceChangeListener(this);

        Preference preference = findPreference("fast_sms");
        getPreferenceScreen().removePreference(preference);

        SharedPreferences sp = getActivity().getSharedPreferences(Utils.SP_CONTACT,
                Context.MODE_PRIVATE);

        mSmsPreference = (IngenicSwitchPreference) findPreference("sync_sms");
        mSmsPreference.setChecked(sp.getBoolean(Utils.KEY_SMS, true));
        mSmsPreference.setOnPreferenceChangeListener(this);

        mPhonePreference = (IngenicSwitchPreference) findPreference("sync_phone");
        mPhonePreference.setChecked(sp.getBoolean(Utils.KEY_PHONE, true));
        mPhonePreference.setOnPreferenceChangeListener(this);
    }

    @Override
    public void onDetach() {
        if (mHeadset != null) {
            BluetoothAdapter.getDefaultAdapter().closeProfileProxy(BluetoothProfile.HEADSET,
                    mHeadset);
            mHeadset = null;
        }

        getActivity().unregisterReceiver(mHeadsetStateReceiver);
        getActivity().unbindService(mConnection);
        mBinder = null;
        super.onDetach();
    }

    private class HeadsetStateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED)) {
                int state = intent.getIntExtra(BluetoothHeadset.EXTRA_STATE,
                        BluetoothHeadset.STATE_DISCONNECTED);
                handleHeadsetState(state);
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contactsettings, container, false);
        return rootView;
    }

    private boolean connectHeadset() {
        if (mHeadset == null)
            return false;
        Class<?> clazz = mHeadset.getClass();
        try {
            Method method = clazz.getDeclaredMethod("connect", BluetoothDevice.class);
            method.setAccessible(true);

            BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(
                    mRemoteAddress);
            return (Boolean) method.invoke(mHeadset, device);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    private boolean disconnectHeadset() {
        if (mHeadset == null)
            return false;
        Class<?> clazz = mHeadset.getClass();
        try {
            Method method = clazz.getDeclaredMethod("disconnect", BluetoothDevice.class);
            method.setAccessible(true);

            BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(
                    mRemoteAddress);
            return (Boolean) method.invoke(mHeadset, device);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        String fragment = preference.getFragment();
        if (fragment != null) {
            startFragment(fragment);
            return true;
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    private void startFragment(String fragment) {
        if (fragment == null) return;
        Intent it = new Intent(getActivity(), FragmentActivity.class);
        it.putExtra(FragmentActivity.EXTRA_FRAGMENT, fragment);
        startActivity(it);
    }

    @Override
    public void onServiceConnected(int profile, BluetoothProfile proxy) {
        mHeadset = (BluetoothHeadset) proxy;
        if (mRemoteAddress == null || !BluetoothAdapter.checkBluetoothAddress(mRemoteAddress)) {
            return;
        }

        BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(
                mRemoteAddress);
        handleHeadsetState(mHeadset.getConnectionState(device));
    }

    @Override
    public void onServiceDisconnected(int profile) {
        mHeadset = null;
    }

    private void handleHeadsetState(int state) {
        switch (state) {
        case BluetoothProfile.STATE_CONNECTED:
            mHeadsetPreference.setEnabled(true);
            mHeadsetPreference.setChecked(true);
            mHeadsetPreference.setSummary(R.string.label_connected);
            break;
        case BluetoothProfile.STATE_DISCONNECTED:
            mHeadsetPreference.setEnabled(true);
            mHeadsetPreference.setChecked(false);
            mHeadsetPreference.setSummary(R.string.label_disconnected);
            break;
        case BluetoothProfile.STATE_CONNECTING:
            mHeadsetPreference.setEnabled(false);
            mHeadsetPreference.setChecked(true);
            mHeadsetPreference.setSummary(R.string.label_connecting);
            break;
        case BluetoothProfile.STATE_DISCONNECTING:
            mHeadsetPreference.setSummary(R.string.text_disconnecting);
        default:
            mHeadsetPreference.setEnabled(false);
            mHeadsetPreference.setChecked(false);
            break;
        }
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        SharedPreferences sp = getActivity().getSharedPreferences(Utils.SP_CONTACT, Context.MODE_PRIVATE);

        if (preference == mHeadsetPreference) {
            boolean isChecked = (Boolean) newValue;
            if (isChecked) {
                return connectHeadset();
            } else {
                return disconnectHeadset();
            }
        } else if (preference == mSmsPreference) {
            boolean isChecked = (Boolean) newValue;//mSmsPreference.isChecked();
            sp.edit().putBoolean(Utils.KEY_SMS, isChecked).commit();
            return true;
        } else if (preference == mPhonePreference) {
            boolean isChecked = (Boolean) newValue;//mPhonePreference.isChecked();
            sp.edit().putBoolean(Utils.KEY_PHONE, isChecked).commit();
            if (!isChecked && mBinder != null) {
                PhoneStateTransactionModel model = mBinder.getTransactionModel();
                if (model != null && model.isStarted()) {
                    PhoneState state = new PhoneState();
                    state.state = PhoneState.STATE_SYNC_PHONE_DISABLED;
                    model.send(state);
                }
            }
            return true;
        }

        return false;
    }
}
