/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/IDWS Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.contact;

import android.net.Uri;
import android.provider.BaseColumns;

public interface SMS extends BaseColumns {
    public static final Uri CONTENT_URI = Uri.parse("content://sms");

    public static final String FILTER = "!imichat";

    public static final String TYPE = "type";

    public static final String THREAD_ID = "thread_id";

    public static final String ADDRESS = "address";

    public static final String PERSON_ID = "person";

    public static final String DATE = "date";

    public static final String READ = "read";

    public static final String BODY = "body";

    public static final String PROTOCOL = "protocol";

    public static final int MESSAGE_TYPE_ALL = 0;

    public static final int MESSAGE_TYPE_INBOX = 1;

    public static final int MESSAGE_TYPE_SENT = 2;

    public static final int MESSAGE_TYPE_DRAFT = 3;

    public static final int MESSAGE_TYPE_OUTBOX = 4;

    public static final int MESSAGE_TYPE_FAILED = 5; // for failed outgoing messages

    public static final int MESSAGE_TYPE_QUEUED = 6; // for messages to send later

    public static final int PROTOCOL_SMS = 0;//SMS_PROTO

    public static final int PROTOCOL_MMS = 1;//MMS_PROTO
}
