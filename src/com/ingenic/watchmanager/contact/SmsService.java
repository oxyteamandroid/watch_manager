/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/IDWS Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.contact;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.SmsInfo;
import com.ingenic.iwds.datatransactor.elf.SmsTransactionModel;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.util.UUIDS;

public class SmsService extends Service implements SmsTransactionModel.SmsTransactionCallback {

    static final int MSG_NEW_SMS = 0;

    public static final String TAG = SmsService.class.getSimpleName();

    private SmsObserver mObserver;
    private static SmsTransactionModel sModel;
    private static List<SmsInfo> set = new LinkedList<SmsInfo>();
    private static SmsInfo sCurrentInfo;
    private static boolean mIsAvailable;

    private Handler mHandle = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case MSG_NEW_SMS:
                SharedPreferences sp = getSharedPreferences("contact", MODE_PRIVATE);
                boolean enabled = sp.getBoolean("sms", true);

                if (!enabled) {
                    IwdsLog.w(this, "Sms is disabled by User!");
                    return;
                }

                SmsInfo info = (SmsInfo) msg.obj;
                set.add(info);
                sendSms2Watch();
                break;
            default:
                break;
            }
        }
    };

    private void sendSms2Watch() {
        if(!mIsAvailable)
            return;
        if (null == sCurrentInfo && set.size() > 0) {
            sCurrentInfo = set.get(0);
            sModel.send(sCurrentInfo);
            IwdsLog.d(TAG, "send sms! --->" + sCurrentInfo.getBody());
        }
    }

    @SuppressWarnings("deprecation")
    public void onCreate() {
        super.onCreate();
        /**
         * 保持服务一直在运行
         */
        CharSequence label = getApplication().getApplicationInfo().loadLabel(
                getPackageManager());

        Notification notification = new Notification(getApplication()
                .getApplicationInfo().icon, label, System.currentTimeMillis());

        Intent it = new Intent(
                ServiceManagerContext.ACTION_NOTIFICATION_CLICKED);
        it.setFlags(it.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, it, 0);
        notification.setLatestEventInfo(this, label, "", pendingIntent);
        notification.flags = notification.flags
                | Notification.FLAG_ONGOING_EVENT;

        startForeground(9999, notification);
        addSMSObserver();

        if (sModel == null) {
            sModel = new SmsTransactionModel(this, this, UUIDS.SMS);
        }
        sModel.start();
        IwdsLog.d(TAG, TAG + " oncreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        SharedPreferences sp = getSharedPreferences("last_sms", MODE_PRIVATE);
        sp.edit().putLong("last", System.currentTimeMillis()).commit();
        return START_STICKY;
    }

    public void addSMSObserver() {
        mObserver = new SmsObserver(this, mHandle);
        ContentResolver resolver = getContentResolver();
        resolver.registerContentObserver(SMS.CONTENT_URI, true, mObserver);
        IwdsLog.d(TAG, "register sms observer");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        // getContentResolver().unregisterContentObserver(mObserver);
        // if (sModel != null) {
        // sModel.stop();
        // }
        super.onDestroy();
        IwdsLog.d(TAG, TAG + " ondestroy");
    }

    @Override
    public void onRequest() {}

    @Override
    public void onRequestFailed() {}

    @Override
    public void onObjectArrived(SmsInfo object) {}

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        if (!isConnected) {
            IwdsLog.w(this, "Message model is disconnected!");
        }
    }

    @Override
    public void onChannelAvailable(boolean isAvailable) {
        mIsAvailable = isAvailable;
        IwdsLog.d(TAG, "onChannelAvailable " + isAvailable);
        if (!isAvailable) {
            IwdsLog.w(this, "Message modle is unavailabled!");
        }else {
            sCurrentInfo = null;
            sendSms2Watch();
        }
    }

    @Override
    public void onSendResult(DataTransactResult result) {
        int resultCode = result.getResultCode();
        if (resultCode != DataTransactResult.RESULT_OK) {
            IwdsLog.w(this, "Message send to watch failed:" + resultCode);
        }else {
            IwdsLog.d(this, "set contain this object " + (sCurrentInfo == result.getTransferedObject()));
            set.remove(result.getTransferedObject());
            IwdsLog.d(TAG, "send success! ---> " + ((SmsInfo)result.getTransferedObject()).getBody());
        }
        sCurrentInfo = null;
        sendSms2Watch();
    }
}
