/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/IDWS Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.contact;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.ingenic.watchmanager.R;

public class SmsListFragment extends ListFragment {

    private AlertDialog mEditDialog;
    private EditText mEditText;
    private String[] mSmss;
    private ArrayAdapter<CharSequence> mAdapter;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mSmss = getResources().getStringArray(R.array.sms);
        mAdapter = new ArrayAdapter<CharSequence>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, mSmss);
        setListAdapter(mAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        showEditDialog(position, (CharSequence) l.getItemAtPosition(position));
        super.onListItemClick(l, v, position, id);
    }

    private void showEditDialog(final int position, CharSequence text) {
        if (mEditText == null) {
            mEditText = new EditText(getActivity());
        }

        if (mEditDialog == null) {
            mEditDialog = new AlertDialog.Builder(getActivity()).setTitle(R.string.title_edit_sms)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            CharSequence c = mEditText.getText();
                            if (c == null) {
                                Toast.makeText(getActivity(), R.string.null_sms_tips, Toast.LENGTH_SHORT)
                                        .show();
                                return;
                            }

                            mSmss[position] = c.toString();
                            mAdapter.notifyDataSetChanged();
                        }
                    }).setNegativeButton(android.R.string.cancel, null).setView(mEditText).create();
        }

        mEditText.setText(text);
        mEditDialog.show();
    }
}
