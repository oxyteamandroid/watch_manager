/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * nongjiabao<jiabao.nong@ingenic.com>
 *
 * Elf/IDWS Project
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.contact;

import java.util.ArrayList;
import java.util.List;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.os.Handler;
import android.os.IBinder;
import android.provider.CallLog;
import android.provider.CallLog.Calls;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.CalllogInfo;
import com.ingenic.iwds.datatransactor.elf.CalllogTransactionModel;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.util.UUIDS;

/**
 * 通话记录同步服务
 * @author jbnong
 *
 */
public class CalllogService extends Service implements CalllogTransactionModel.CalllogTransactionCallback {
    private static final String TAG = "CalllogService";
    public static final int MSG_NEW_CALLLOG = 0;
    public static final int MSG_ISAVAILABLE = 1;
    public static final int MSG_DEL_CALLLOG = 2;
    public static final int MSG_READ_CALLLOG = 3;
    public static final int COM_ERROR = -1;
    public static final int COM_START = 1;
    public static final int COM_STOP = 2;
    private static final int SYNC_CALLLOG_START = -1;
    private static final int SYNC_CALLLOG_END = -2;
    private boolean mIsAvailable = false;
    private Context mContext = null;
    private CalllogObserver mObserver;
    private CalllogTransactionModel mModel;
    private List<CalllogInfo> mCallloglist = new ArrayList<CalllogInfo>();

    private Handler mHandle = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case MSG_NEW_CALLLOG:
                CalllogInfo info = (CalllogInfo) msg.obj;
                if(mIsAvailable){
                    mModel.send(info);
                }
                break;
            case MSG_DEL_CALLLOG:
                CalllogInfo delinfo = (CalllogInfo) msg.obj;
                if(mIsAvailable){
                    mModel.send(delinfo);
                }
                break;
            case MSG_READ_CALLLOG:
                synchronized (mCallloglist){
                    mCallloglist = queryCalllogList();
                }
                break;
            case MSG_ISAVAILABLE:
                synchronized (mCallloglist){
                    if (mCallloglist.size() > 0) {
                        /* 先发一个清除calllog命令给手表：命令为：calls._id = -1 */
                        CalllogInfo startInfo = new CalllogInfo();
                        startInfo.operation = CalllogInfo.OPT_START;
                        startInfo.set_id(SYNC_CALLLOG_START);
                        mHandle.obtainMessage(MSG_NEW_CALLLOG, startInfo).sendToTarget();
                        /* 然后同步要同步的通话记录 */
                        for (CalllogInfo callloginfo : mCallloglist) {
                            mHandle.obtainMessage(MSG_NEW_CALLLOG, callloginfo).sendToTarget();
                        }
                        CalllogInfo finishInfo = new CalllogInfo();
                        finishInfo.set_id(SYNC_CALLLOG_END);
                        finishInfo.operation = CalllogInfo.OPT_END;
                        mHandle.obtainMessage(MSG_NEW_CALLLOG, finishInfo).sendToTarget();
                    }
                    mCallloglist.clear();
                }
                break;
            default:
                break;
            }
        };
    };

    private List<CalllogInfo> queryCalllogList() {
        List<CalllogInfo> mCalllogList = new ArrayList<CalllogInfo>();
        Cursor csr = getContentResolver().query(Calls.CONTENT_URI, null, Calls.TYPE + "!=4", null, Calls.DEFAULT_SORT_ORDER + " limit " + ContactUtils.CALLLOG_LIMIT_NUM);
        if (csr != null) {
            if (csr.moveToFirst()) {
                do {
                    int _id = csr.getInt(csr.getColumnIndex(Calls._ID));
                    int duration = csr.getInt(csr.getColumnIndex(Calls.DURATION));
                    ;
                    int type = csr.getInt(csr.getColumnIndex(Calls.TYPE));
                    int news = csr.getInt(csr.getColumnIndex(Calls.NEW));
                    ;
                    int is_read = csr.getInt(csr.getColumnIndex(Calls.IS_READ));
                    long date = csr.getLong(csr.getColumnIndex(Calls.DATE));
                    String number = csr.getString(csr.getColumnIndex(Calls.NUMBER));
                    String name = csr.getString(csr.getColumnIndex(Calls.CACHED_NAME));

                    CalllogInfo info = new CalllogInfo();
                    info.operation = CalllogInfo.OPT_ADD;
                    info.set_id(_id);
                    info.setDate(date);
                    info.setDuration(duration);
                    info.setIs_read(is_read);
                    info.setName(name);
                    info.setNewflag(news);
                    info.setNumber(number);
                    info.setType(type);
                    mCalllogList.add(info);
                    info = null;
                } while (csr.moveToNext());
            }
            // release resource
            csr.close();
        }
        /**
         * 存之前，先清理原来的数据
         */
        SharedPreferences sp1 = mContext.getSharedPreferences("Calllog_id", Context.MODE_PRIVATE);
        Editor editor1 = sp1.edit();
        editor1.clear();
        editor1.commit();
        String calllog_id = "";
        csr = getContentResolver().query(Calls.CONTENT_URI, null, Calls.TYPE + "!=4", null, Calls.DEFAULT_SORT_ORDER + " limit " + ContactUtils.CALLLOG_LIMIT_NUM);
        if (csr!=null) {
            while (csr.moveToNext())
            {
                calllog_id += csr.getInt(csr.getColumnIndex(Calls._ID)) + "#";
            }
            csr.close();
            editor1.putString("id", calllog_id);
            editor1.commit();
        }
        return mCalllogList;
    }

    @SuppressWarnings("deprecation")
    public void onCreate() {
        super.onCreate();
        /**
         * 保持服务一直在运行
         */
        CharSequence label = getApplication().getApplicationInfo().loadLabel(
                getPackageManager());

        Notification notification = new Notification(getApplication()
                .getApplicationInfo().icon, label, System.currentTimeMillis());

        Intent it = new Intent(
                ServiceManagerContext.ACTION_NOTIFICATION_CLICKED);
        it.setFlags(it.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, it, 0);
        notification.setLatestEventInfo(this, label, "", pendingIntent);
        notification.flags = notification.flags
                | Notification.FLAG_ONGOING_EVENT;

        startForeground(9999, notification);
        /**
         * 服务创建时，扫描通话记录
         */
        mContext = getBaseContext();
        synchronized (mCallloglist) {
            mCallloglist = queryCalllogList();
        }
        addCalllogObserver();
        if (mModel == null) {
            mModel = new CalllogTransactionModel(this, this, UUIDS.CALLLOG);
        }
        mModel.start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        int cmd = intent.getIntExtra("commandId", COM_ERROR);
        switch(cmd){
            case COM_START:
                /**
                 * 绑定时，重新扫描通话记录
                 */
                synchronized (mCallloglist){
                    mCallloglist = queryCalllogList();
                }
                break;
            case COM_STOP:
                break;
        }
        return START_NOT_STICKY;
    }

    public void addCalllogObserver() {
        IwdsLog.d(TAG, "add a calllog observer. ");
        ContentResolver resolver = getContentResolver();

        mObserver = new CalllogObserver(mContext, resolver, mHandle);
        resolver.registerContentObserver(CallLog.Calls.CONTENT_URI, true, mObserver);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        getContentResolver().unregisterContentObserver(mObserver);
        if (mModel != null) {
            mModel.stop();
        }
        super.onDestroy();
    }

    @Override
    public void onRequest() {}

    @Override
    public void onRequestFailed() {}

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {}

    @Override
    public void onChannelAvailable(boolean isAvailable) {
        mIsAvailable = isAvailable;
        if (isAvailable ==true){
            mHandle.obtainMessage(MSG_ISAVAILABLE).sendToTarget();
        }
    }

    @Override
    public void onSendResult(DataTransactResult result) {}

    @Override
    public void onObjectArrived(CalllogInfo object) {
    }
}
