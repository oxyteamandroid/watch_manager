/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhengchuanshu<chuanshu.zheng@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.weather;

/**
 * @author chuanshu.zheng 搜索城市用到的字段
 */
public class QueryCity {
    // 城市ID
    public String woeid;

    // 城市名称
    public String name;

    // 国家
    public String country;

    // 省
    public String admin1;

    // 时区
    public String timezone;

    // 语言
    public String lang;

}
