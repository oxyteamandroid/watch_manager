/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhengchuanshu<chuanshu.zheng@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.weather;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.ingenic.watchmanager.Model;
import com.ingenic.watchmanager.db.Operator;

public class WeatherModel extends Model<WeatherInfo> implements
        BDLocationListener {
    protected LocationClient mClient;
    protected WeatherFromNet mFromNet;
    protected Context mContext;
    protected Handler mNetworkHandler;
    protected HandlerThread mBackThread;
    protected Runnable mCityRunnable;
    protected Runnable mWeatherRunnable;
    protected City2Operator mCity2Operator;

    public WeatherModel(Context context) {
        super(context);
        this.mContext = context;
        // 百度位置服务
        mClient = new LocationClient(context.getApplicationContext(),
                getLocationOption());
        mClient.registerLocationListener(this);
        if (mBackThread == null) {
            mBackThread = new HandlerThread("locate_network");
            mBackThread.start();
        }

        mNetworkHandler = new Handler(mBackThread.getLooper());
        mCity2Operator = new City2Operator(context);
    }

    @Override
    public Operator<WeatherInfo> getOperator(Context context) {
        return new WeatherOperator(context);
    }

    /** 百度位置配置 */
    private LocationClientOption getLocationOption() {
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationMode.Hight_Accuracy);
        option.setCoorType("gcj02");
        option.setScanSpan(2000);
        option.setTimeOut(5000);
        option.setIsNeedAddress(true);
        return option;
    }

    @Override
    public void onReceiveLocation(BDLocation arg0) {

    }

    public void getWeatherOfHere() {
        if (!mClient.isStarted()) {
            mClient.start();
        }
    }

    public void cancelGetCity() {
        if (mCityRunnable != null) {
            mNetworkHandler.removeCallbacks(mCityRunnable);
            mCityRunnable = null;
        }
    }

    public void cancelGetWeather() {
        if (mWeatherRunnable != null) {
            mNetworkHandler.removeCallbacks(mWeatherRunnable);
            mWeatherRunnable = null;
        }
    }

    public interface WeatherCallback extends Callback<WeatherInfo> {
    }

    @Override
    protected void finalize() throws Throwable {

        cancelGetCity();
        cancelGetWeather();

        if (mBackThread != null) {
            mBackThread.interrupt();
            mBackThread = null;
        }
        super.finalize();
    }

}
