/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhengchuanshu<chuanshu.zheng@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.weather;

import java.lang.reflect.Field;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.WMFragment;

public class WeatherPagerFragment extends WMFragment implements
        OnClickListener, OnPageChangeListener {
    public static String mCurrentTempUnit = Weather_Constant.TEMP_UNIT_CENTIGRADE;
    private ViewPager mViewPager;
    private int mCurrentPage = 0;
    private Context mContext;
    private Dialog mDialog;

    private TextView mTvUnit, mTvDefaultCity, mTvDelete;
    private LinearLayout mDefaultCity, mDeleteCity, mSwitchUnit;
    private boolean canClick = true;
    private Handler mHandler = new Handler();
    private LoadCityTask mLoadCityTask;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        SharedPreferences sp = activity.getSharedPreferences(
                Weather_Constant.SP_WEATHER, Context.MODE_PRIVATE);
        mCurrentTempUnit = sp.getString("temp_unit",
                Weather_Constant.TEMP_UNIT_CENTIGRADE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_weather_pager,
                container, false);
        mDefaultCity = (LinearLayout) rootView
                .findViewById(R.id.ll_default_city);
        mDeleteCity = (LinearLayout) rootView.findViewById(R.id.ll_delete_city);
        mSwitchUnit = (LinearLayout) rootView.findViewById(R.id.ll_switch_unit);
        mViewPager = (ViewPager) rootView.findViewById(R.id.view_pager);
        mTvUnit = (TextView) rootView.findViewById(R.id.tv_unit);
        mTvDefaultCity = (TextView) rootView.findViewById(R.id.tv_default);
        mTvDelete = (TextView) rootView.findViewById(R.id.tv_delete);
        mDefaultCity.setOnClickListener(this);
        mDeleteCity.setOnClickListener(this);
        mSwitchUnit.setOnClickListener(this);
        mViewPager.setOnPageChangeListener(this);
        mViewPager.setOffscreenPageLimit(10);
        mDialog = new Dialog(mContext, R.style.MyDialog);
        mDialog.setContentView(R.layout.dialog_waiting);
        mDialog.setCancelable(false);
        if (mCurrentTempUnit.equals(Weather_Constant.TEMP_UNIT_CENTIGRADE)) {
            Drawable drawableTop = getResources().getDrawable(
                    R.drawable.ic_temp_unit_f);
            mTvUnit.setCompoundDrawablesWithIntrinsicBounds(null, drawableTop,
                    null, null);
        } else {
            Drawable drawableTop = getResources().getDrawable(
                    R.drawable.ic_temp_unit_c);
            mTvUnit.setCompoundDrawablesWithIntrinsicBounds(null, drawableTop,
                    null, null);
        }

        setupTypeFace();

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mLoadCityTask = new LoadCityTask();
        mLoadCityTask.execute("");
    }

    private void setupTypeFace() {
        Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/watch_font.ttf");
        mTvUnit.setTypeface(typeFace);
        mTvDelete.setTypeface(typeFace);
        mTvDefaultCity.setTypeface(typeFace);
    }

    public void saveTempUnitToSP(String unit) {
        SharedPreferences sp = mContext.getSharedPreferences(
                Weather_Constant.SP_WEATHER, Context.MODE_PRIVATE);
        sp.edit().putString("temp_unit", unit).commit();
    }

    @Override
    public void onClick(View v) {
        WeatherPagerAdapter mAdapter = ((WeatherPagerAdapter) mViewPager
                .getAdapter());
        switch (v.getId()) {
        case R.id.ll_default_city:
            if (mAdapter.isDefaultCity(mCurrentPage))
                mAdapter.setDefaultCity(mCurrentPage, 0);
            else
                mAdapter.setDefaultCity(mCurrentPage, 1);
            mAdapter.syncWeather();
            onPageSelected(mCurrentPage);
            break;
        case R.id.ll_delete_city:
            mAdapter.deleteCity(mCurrentPage);
            mAdapter.removeAllFragment();
            mAdapter = new WeatherPagerAdapter(getFragmentManagerByVersion(),
                    getActivity());
            mViewPager.setAdapter(mAdapter);
            onPageSelected(0);
            break;
        case R.id.ll_switch_unit:
            // 切換溫度單位
            if (canClick) {
                canClick = false;
                resetDrawable();
                mCurrentTempUnit = mCurrentTempUnit
                        .equals(Weather_Constant.TEMP_UNIT_CENTIGRADE) ? Weather_Constant.TEMP_UNIT_FAHRENHEIT
                        : Weather_Constant.TEMP_UNIT_CENTIGRADE;
                saveTempUnitToSP(mCurrentTempUnit);
                WeatherPagerAdapter adapter = new WeatherPagerAdapter(
                        getFragmentManagerByVersion(), getActivity());
                mAdapter.removeAllFragment();
                mViewPager.setAdapter(adapter);
                mViewPager.setCurrentItem(mCurrentPage);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        canClick = true;
                    }
                }, 3000);
            } else {
                Toast.makeText(mContext, R.string.warning_click,
                        Toast.LENGTH_SHORT).show();
            }
            break;
        }
    }

    private void resetDrawable() {
        Drawable drawableTop;
        if (mCurrentTempUnit.equals(Weather_Constant.TEMP_UNIT_CENTIGRADE)) {
            drawableTop = getResources().getDrawable(R.drawable.ic_temp_unit_c);
            mTvUnit.setCompoundDrawablesWithIntrinsicBounds(null, drawableTop,
                    null, null);
        } else {
            drawableTop = getResources().getDrawable(R.drawable.ic_temp_unit_f);
            mTvUnit.setCompoundDrawablesWithIntrinsicBounds(null, drawableTop,
                    null, null);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLoadCityTask.cancel(true);
        mLoadCityTask = null;
        WeatherPagerAdapter adapter = (WeatherPagerAdapter) mViewPager
                .getAdapter();
        if (adapter != null)
            adapter.clearData();
        if (mDialog.isShowing())
            mDialog.dismiss();
    }

    @Override
    public void onDetach() {
        try {
            Field f = Fragment.class.getDeclaredField("mChildFragmentManager");
            f.setAccessible(true);
            f.set(this, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDetach();
    }

    @Override
    public void onPageSelected(int arg0) {
        mCurrentPage = arg0;
        WeatherPagerAdapter adapter = (WeatherPagerAdapter) mViewPager
                .getAdapter();
        if (arg0 == 0) {
            mDeleteCity.setVisibility(View.GONE);
        } else {
            mDeleteCity.setVisibility(View.VISIBLE);
        }
        Drawable drawableTop;
        if (adapter.isDefaultCity(mCurrentPage)) {
            mTvDefaultCity.setText(R.string.set_as_default);
            drawableTop = getResources().getDrawable(
                    R.drawable.ic_default_city_selected);
            mTvDefaultCity.setCompoundDrawablesWithIntrinsicBounds(null,
                    drawableTop, null, null);
        } else {
            mTvDefaultCity.setText(R.string.cancel_default);
            drawableTop = getResources()
                    .getDrawable(R.drawable.ic_default_city);
            mTvDefaultCity.setCompoundDrawablesWithIntrinsicBounds(null,
                    drawableTop, null, null);
        }
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {

    }

    @Override
    public void onPageScrollStateChanged(int arg0) {

    }

    private class LoadCityTask extends
            AsyncTask<String, Integer, List<City2Show>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!mDialog.isShowing())
                mDialog.show();
        }

        @Override
        protected List<City2Show> doInBackground(String... params) {
            if (isCancelled())
                return null;
            City2Operator operator = new City2Operator(mContext);
            return operator.queryCitys();
        }

        @Override
        protected void onPostExecute(List<City2Show> result) {
            super.onPostExecute(result);
            if (isDetached())
                return;
            WeatherPagerAdapter adapter = new WeatherPagerAdapter(
                    getFragmentManagerByVersion(), mContext, result);
            mViewPager.setAdapter(adapter);
            onPageSelected(mCurrentPage);
            if (mDialog.isShowing())
                mDialog.dismiss();
        }

    }

    @SuppressLint("NewApi")
    public FragmentManager getFragmentManagerByVersion() {
        if (VERSION.SDK_INT >= 17)
            return getChildFragmentManager();
        return getFragmentManager();
    }

}
