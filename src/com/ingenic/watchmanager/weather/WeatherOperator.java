/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhengchuanshu<chuanshu.zheng@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.weather;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.ingenic.watchmanager.db.Operator;
import com.ingenic.watchmanager.db.WatchManagerContracts;

public class WeatherOperator extends Operator<WeatherInfo> {

    public WeatherOperator(Context context) {
        super(context, WatchManagerContracts.Tables.WEATHER);
    }

    @Override
    protected ContentValues toValues(WeatherInfo info) {
        ContentValues values = new ContentValues();
        values.put(WatchManagerContracts.WeatherColumns.CITY, info.city);
        values.put(WatchManagerContracts.WeatherColumns.DATE, info.date);
        values.put(WatchManagerContracts.WeatherColumns.DAY_INDEX,
                info.dayIndex);
        values.put(WatchManagerContracts.WeatherColumns.DAY_OF_WEEK,
                info.dayOfWeek);
        values.put(WatchManagerContracts.WeatherColumns.UPDATE_TIME,
                info.updateTime);
        values.put(WatchManagerContracts.WeatherColumns.WEATHER_CODE,
                info.weatherCode);
        values.put(WatchManagerContracts.WeatherColumns.WEATHER, info.weather);
        values.put(WatchManagerContracts.WeatherColumns.TEMP_UNIT,
                info.tempUnit);
        values.put(WatchManagerContracts.WeatherColumns.CURRENT_TEMP,
                info.currentTemp);
        values.put(WatchManagerContracts.WeatherColumns.MINIMUNTEMP,
                info.minimumTemp);
        values.put(WatchManagerContracts.WeatherColumns.MAXIMUMTEMP,
                info.maximumTemp);
        values.put(WatchManagerContracts.WeatherColumns.WOEID, info.woeid);
        return values;
    }

    @Override
    public int update(WeatherInfo info) {
        return update(info, WatchManagerContracts.WeatherColumns.DAY_INDEX
                + " = ? and " + WatchManagerContracts.WeatherColumns.WOEID
                + "=? and " + WatchManagerContracts.WeatherColumns.TEMP_UNIT
                + "=?", new String[] { String.valueOf(info.dayIndex),
                info.woeid, info.tempUnit });
    }

    @Override
    protected WeatherInfo fromCursor(Cursor cursor) {
        WeatherInfo info = new WeatherInfo();
        info.city = cursor.getString(cursor
                .getColumnIndex(WatchManagerContracts.WeatherColumns.CITY));
        info.date = cursor.getString(cursor
                .getColumnIndex(WatchManagerContracts.WeatherColumns.DATE));
        info.dayIndex = cursor
                .getInt(cursor
                        .getColumnIndex(WatchManagerContracts.WeatherColumns.DAY_INDEX));
        info.dayOfWeek = cursor
                .getString(cursor
                        .getColumnIndex(WatchManagerContracts.WeatherColumns.DAY_OF_WEEK));
        info.updateTime = cursor
                .getString(cursor
                        .getColumnIndex(WatchManagerContracts.WeatherColumns.UPDATE_TIME));
        info.weatherCode = cursor
                .getString(cursor
                        .getColumnIndex(WatchManagerContracts.WeatherColumns.WEATHER_CODE));
        info.weather = cursor.getString(cursor
                .getColumnIndex(WatchManagerContracts.WeatherColumns.WEATHER));
        info.tempUnit = cursor
                .getString(cursor
                        .getColumnIndex(WatchManagerContracts.WeatherColumns.TEMP_UNIT));
        info.currentTemp = cursor
                .getInt(cursor
                        .getColumnIndex(WatchManagerContracts.WeatherColumns.CURRENT_TEMP));
        info.minimumTemp = cursor
                .getInt(cursor
                        .getColumnIndex(WatchManagerContracts.WeatherColumns.MINIMUNTEMP));
        info.maximumTemp = cursor
                .getInt(cursor
                        .getColumnIndex(WatchManagerContracts.WeatherColumns.MAXIMUMTEMP));
        info.woeid = cursor.getString(cursor
                .getColumnIndex(WatchManagerContracts.WeatherColumns.WOEID));
        return info;
    }

    @Override
    public int delete(WeatherInfo info) {
        return delete(WatchManagerContracts.WeatherColumns.DAY_INDEX + " = ? ",
                new String[] { String.valueOf(info.dayIndex) });
    }

    @Override
    public boolean hasData(WeatherInfo info) {
        WeatherInfo inf = query(
                null,
                WatchManagerContracts.WeatherColumns.DAY_INDEX + " = ?  and "
                        + WatchManagerContracts.WeatherColumns.WOEID
                        + " = ? and "
                        + WatchManagerContracts.WeatherColumns.TEMP_UNIT + "=?",
                new String[] { String.valueOf(info.dayIndex), info.woeid,
                        info.tempUnit }, null, null, null);
        return inf != null;
    }

    public WeatherInfo queryToday() {
        return query(null, WatchManagerContracts.WeatherColumns.DAY_INDEX
                + " = ? ", new String[] { String.valueOf(0) }, null, null, null);
    }
}
