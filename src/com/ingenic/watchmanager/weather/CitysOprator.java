/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.weather;

import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.ingenic.watchmanager.db.Operator;
import com.ingenic.watchmanager.db.WatchManagerContracts;

public class CitysOprator extends Operator<OffenCity> {

    public CitysOprator(Context context) {
        super(context, WatchManagerContracts.Tables.CITY);
    }

    @Override
    protected ContentValues toValues(OffenCity t) {
        ContentValues values = new ContentValues();
        values.put(WatchManagerContracts.CityColumns.NAME, t.name);
        values.put(WatchManagerContracts.CityColumns.TIME, t.time);
        values.put(WatchManagerContracts.CityColumns.WOEID, t.woeid);
        return values;
    }

    @Override
    public int update(OffenCity t) {
        return update(t, WatchManagerContracts.CityColumns.NAME + " = ? ",
                new String[] { t.name });
    }

    @Override
    protected OffenCity fromCursor(Cursor cursor) {
        String name = cursor.getString(cursor
                .getColumnIndex(WatchManagerContracts.CityColumns.NAME));
        OffenCity city = new OffenCity(name);
        city.time = cursor.getInt(cursor
                .getColumnIndex(WatchManagerContracts.CityColumns.TIME));
        city.woeid = cursor.getString(cursor
                .getColumnIndex(WatchManagerContracts.CityColumns.WOEID));
        return city;
    }

    @Override
    public int delete(OffenCity t) {
        return delete(WatchManagerContracts.CityColumns.WOEID + " = ? ",
                new String[] { t.woeid });
    }

    @Override
    public boolean hasData(OffenCity t) {
        OffenCity c = query(null, WatchManagerContracts.CityColumns.WOEID
                + " = ? ", new String[] { t.woeid }, null, null, null);
        return c != null;
    }

    private OffenCity queryCity(String name) {
        OffenCity city = query(null, WatchManagerContracts.CityColumns.NAME
                + "=?", new String[] { name }, null, null, null);
        return city;
    }

    public void save(String name, String woeid) {
        OffenCity city = queryCity(name);
        if (city == null) {
            city = new OffenCity(name);
            city.woeid = woeid;
        }
        city.time++;

        save(city);
    }

    @Override
    public List<OffenCity> queryAll() {
        String orderBy = WatchManagerContracts.CityColumns.TIME + " DESC";
        return queryAll(orderBy);
    }
}
