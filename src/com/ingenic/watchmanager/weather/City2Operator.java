/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhengchuanshu<chuanshu.zheng@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.weather;

import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;

import com.ingenic.watchmanager.db.Operator;
import com.ingenic.watchmanager.db.WatchManagerContracts;

public class City2Operator extends Operator<City2Show> {
    private Context mContext;

    public City2Operator(Context context) {
        super(context, WatchManagerContracts.Tables.CITY2);
        mContext = context;
    }

    @Override
    protected ContentValues toValues(City2Show t) {
        ContentValues values = new ContentValues();
        values.put(WatchManagerContracts.City2Columns.NAME, t.name);
        values.put(WatchManagerContracts.City2Columns.UPDATE_TIME,
                t.update_time);
        values.put(WatchManagerContracts.City2Columns.LOCATED, t.isLocated ? 1
                : 0);
        values.put(WatchManagerContracts.City2Columns.ISDEFAULT,
                t.isDefault ? 1 : 0);
        values.put(WatchManagerContracts.City2Columns.UNIT, t.tempUnit);
        values.put(WatchManagerContracts.City2Columns.WOEID, t.woeid);
        values.put(WatchManagerContracts.City2Columns.TIMEZONE, t.timezone);
        values.put(WatchManagerContracts.City2Columns.LANG, t.lang);
        return values;
    }

    public int updateTime(City2Show city) {
        ContentValues values = new ContentValues();
        values.put(WatchManagerContracts.City2Columns.UPDATE_TIME,
                city.update_time);
        return update(values, WatchManagerContracts.City2Columns.WOEID
                + " = ? and " + WatchManagerContracts.City2Columns.UNIT + "=?",
                new String[] { city.woeid, city.tempUnit });
    }

    /*
     * 更新的时候不更新是否是定位和默认的
     */
    @Override
    public int update(City2Show t) {
        ContentValues values = new ContentValues();
        values.put(WatchManagerContracts.City2Columns.WOEID, t.woeid);
        values.put(WatchManagerContracts.City2Columns.NAME, t.name);
        values.put(WatchManagerContracts.City2Columns.LANG, t.lang);
        values.put(WatchManagerContracts.City2Columns.TIMEZONE, t.timezone);
        if (t.isLocated)
            return update(values, WatchManagerContracts.City2Columns.LOCATED
                    + " = ? and " + WatchManagerContracts.City2Columns.UNIT
                    + "=?", new String[] { "1", t.tempUnit });
        return update(values, WatchManagerContracts.City2Columns.WOEID
                + " = ? and " + WatchManagerContracts.City2Columns.UNIT + "=?",
                new String[] { t.woeid, t.tempUnit });
    }

    /**
     * 设置默认城市，需要先把上一个默认城市取消掉
     * 
     * @param city
     * @return
     */
    public int setDefaultCity(String woeid, int isdefault) {
        ContentValues values = new ContentValues();
        values.put(WatchManagerContracts.City2Columns.ISDEFAULT, 0);
        update(values, WatchManagerContracts.City2Columns.ISDEFAULT + " = ? ",
                new String[] { "1" });
        int count = 0;
        if (woeid == null)
            return count;
        values = new ContentValues();
        values.put(WatchManagerContracts.City2Columns.ISDEFAULT, isdefault);
        count = update(values, WatchManagerContracts.City2Columns.WOEID
                + " = ? ", new String[] { woeid });
        return count;
    }

    @Override
    protected City2Show fromCursor(Cursor cursor) {
        String name = cursor.getString(cursor
                .getColumnIndex(WatchManagerContracts.City2Columns.NAME));
        long update_time = cursor
                .getLong(cursor
                        .getColumnIndex(WatchManagerContracts.City2Columns.UPDATE_TIME));
        int isLocated = cursor.getInt(cursor
                .getColumnIndex(WatchManagerContracts.City2Columns.LOCATED));
        int isDefault = cursor.getInt(cursor
                .getColumnIndex(WatchManagerContracts.City2Columns.ISDEFAULT));
        String unit = cursor.getString(cursor
                .getColumnIndex(WatchManagerContracts.City2Columns.UNIT));
        String woeid = cursor.getString(cursor
                .getColumnIndex(WatchManagerContracts.City2Columns.WOEID));
        String timezone = cursor.getString(cursor
                .getColumnIndex(WatchManagerContracts.City2Columns.TIMEZONE));
        String lang = cursor.getString(cursor
                .getColumnIndex(WatchManagerContracts.City2Columns.LANG));
        City2Show city = new City2Show(name);
        city.update_time = update_time;
        city.isDefault = isDefault == 1 ? true : false;
        city.isLocated = isLocated == 1 ? true : false;
        city.tempUnit = unit;
        city.woeid = woeid;
        city.timezone = timezone;
        city.lang = lang;
        return city;
    }

    @Override
    public int delete(City2Show t) {
        return delete(WatchManagerContracts.City2Columns.WOEID + " = ? ",
                new String[] { t.woeid });
    }

    @Override
    public boolean hasData(City2Show t) {
        City2Show c;
        if (t.isLocated)
            c = query(null, WatchManagerContracts.City2Columns.LOCATED
                    + " = ? and " + WatchManagerContracts.City2Columns.UNIT
                    + "= ?", new String[] { String.valueOf(1), t.tempUnit },
                    null, null, null);
        else
            c = query(null, WatchManagerContracts.City2Columns.WOEID
                    + " = ? and " + WatchManagerContracts.City2Columns.UNIT
                    + "= ?", new String[] { t.woeid, t.tempUnit }, null, null,
                    null);
        return c != null;
    }

    public City2Show queryCity(String woeid) {
        SharedPreferences sp = mContext.getSharedPreferences(
                Weather_Constant.SP_WEATHER, Context.MODE_PRIVATE);
        String tempUnit = sp.getString("temp_unit", "c");
        City2Show city = query(null, WatchManagerContracts.City2Columns.WOEID
                + "=? and " + WatchManagerContracts.City2Columns.UNIT + "=?",
                new String[] { woeid, tempUnit }, null, null, null);
        return city;
    }

    public List<City2Show> queryCitys() {
        SharedPreferences sp = mContext.getSharedPreferences(
                Weather_Constant.SP_WEATHER, Context.MODE_PRIVATE);
        String tempUnit = sp.getString("temp_unit", "c");
        return queryAll(WatchManagerContracts.City2Columns.LOCATED
                + "=? and unit=?",
                new String[] { String.valueOf(0), tempUnit }, null);
    }

    /*
     * public void save(String woeid) { City2Show city = queryCity(woeid); if
     * (city == null) { city = new City2Show(); city.woeid = woeid;
     * city.isLocated = false; } SharedPreferences sp =
     * mContext.getSharedPreferences( WeatherModel.SP_WEATHER,
     * Context.MODE_PRIVATE); city.tempUnit = sp.getString("temp_unit", "c");
     * save(city); }
     */

    @Override
    public List<City2Show> queryAll() {
        return queryAll(null);
    }
}
