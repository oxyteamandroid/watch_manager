/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhengchuanshu<chuanshu.zheng@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.weather;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;

public class WeatherPagerAdapter extends FragmentPagerAdapter {
    private Context mContext;
    private List<City2Show> mCitys;
    private City2Operator mOperator;
    private WeatherOperator mWeatherOperator;
    private boolean mNeedAddCity;// 需不需要显示添加城市，最多能添加四个城市
    @SuppressWarnings("unused")
    private int mCount;
    private List<Fragment> mFragments;

    public WeatherPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.mContext = context;
        mOperator = new City2Operator(mContext);
        mCitys = mOperator.queryCitys();
        mWeatherOperator = new WeatherOperator(context);
        mFragments = new ArrayList<Fragment>();
    }

    public WeatherPagerAdapter(FragmentManager fm, Context context,
            List<City2Show> citys) {
        super(fm);
        this.mContext = context;
        mOperator = new City2Operator(mContext);
        mCitys = citys;
        mWeatherOperator = new WeatherOperator(context);
        mFragments = new ArrayList<Fragment>();
    }

    public void clearData() {
        if (mCitys != null && mCitys.size() > 0) {
            mCitys.clear();
            mCitys = null;
            System.gc();
        }

        if (mFragments != null && mFragments.size() > 0) {
            mFragments.clear();
            mFragments = null;
            System.gc();
        }

    }

    @Override
    public Fragment getItem(int arg0) {
        Fragment fragment;
        Bundle bundle = new Bundle();
        if (arg0 == 0) {
            fragment = new LocateWeatherFragment();
        } else {
            fragment = new WeatherFragment();
            bundle.putParcelable("city", mCitys.get(arg0 - 1));
        }
        fragment.setArguments(bundle);
        mFragments.add(fragment);
        return fragment;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    /**
     * 一共可以显示五页 第一页为百度定位到的当前城市
     */
    @Override
    public int getCount() {
        int count = 0;
        if (mCitys == null || mCitys.size() <= 0) {
            count = 1;// 当前城市页面和添加页面
            mNeedAddCity = true;
        } else if (mCitys.size() + 1 >= 5) {
            count = 5;// 已添加的城市大于等于4个
            mNeedAddCity = false;
        } else {
            count = mCitys.size() + 1;
            mNeedAddCity = true;
        }
        mCount = count;
        return count;
    }

    /**
     * 清空FragmentPagerAdapter里缓存的所有Framgent
     */
    public void removeAllFragment() {
        for (Fragment fragment : mFragments) {
            FragmentManager fm = fragment.getFragmentManager();
            fm.beginTransaction().remove(fragment).commitAllowingStateLoss();
            fm.executePendingTransactions();
        }
        mFragments.clear();
    }

    public boolean getmNeedAddCity() {
        return mNeedAddCity;
    }

    /**
     * 删除当前页
     */
    public void deleteCity(int page) {
        City2Show city = mCitys.get(page - 1);
        city = mOperator.queryCity(city.woeid);
        mOperator.delete(city);
        mWeatherOperator.delete("woeid=?", new String[] { city.woeid });
        if (city.isDefault)
            syncWeather();
    }

    public void setDefaultCity(int page, int isdefault) {
        if (page == 0) {
            if (isdefault == 0)
                return;
            mOperator.setDefaultCity(null, isdefault);
        } else
            mOperator.setDefaultCity(mCitys.get(page - 1).woeid, isdefault);
    }

    public City2Show getDefaultCity() {
        City2Show city = mOperator.query(null, "isdefault=? and unit=?",
                new String[] { String.valueOf(1),
                        WeatherPagerFragment.mCurrentTempUnit }, null, null,
                null);
        return city;
    }

    public boolean isDefaultCity(int page) {
        City2Show city = getDefaultCity();
        // 如果没有默认城市就设置定位到的城市为默认城市
        if (page == 0) {
            if (city == null)
                return true;
            return false;
        } else {
            if (city == null)
                return false;
            return mCitys.get(page - 1).woeid.equals(city.woeid);
        }
    }

    public void syncWeather() {
        WeatherInfoTransatorModel.getInstance(mContext).onRequest();
    }

}
