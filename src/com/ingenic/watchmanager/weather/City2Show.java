/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhengchuanshu<chuanshu.zheng@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.weather;

import android.os.Parcel;
import android.os.Parcelable;

/*
 *  ViewPager中要显示的城市
 * */
public class City2Show implements Parcelable {

    public String name; // 城市名称
    public long update_time; // 雅虎上一次天气更新时间
    public boolean isDefault;// 是否是默认城市
    public boolean isLocated;// 是否是定位出的城市
    public String tempUnit;// 这个城市的温度单位
    public String woeid;// 雅虎城市ID
    public String timezone; // 城市所在时区
    public String lang; // 城市语言 zh-CN格式 注意Locale.getDefault()返回的是zh_CN

    public City2Show() {
    }

    public City2Show(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeLong(update_time);
        dest.writeByte((byte) (isDefault ? 1 : 0));
        dest.writeByte((byte) (isLocated ? 1 : 0));
        dest.writeString(tempUnit);
        dest.writeString(woeid);
        dest.writeString(timezone);
        dest.writeString(lang);
    }

    public static final Creator<City2Show> CREATOR = new Creator<City2Show>() {
        @Override
        public City2Show createFromParcel(Parcel source) {
            City2Show city = new City2Show();

            city.name = source.readString();
            city.update_time = source.readLong();
            city.isDefault = source.readByte() != 0;
            city.isLocated = source.readByte() != 0;
            city.tempUnit = source.readString();
            city.woeid = source.readString();
            city.timezone = source.readString();
            city.lang = source.readString();

            return city;
        }

        @Override
        public City2Show[] newArray(int size) {
            return new City2Show[size];
        }
    };

    @Override
    public String toString() {
        return "name:  " + name + "--woeid:  " + woeid;
    }

}
