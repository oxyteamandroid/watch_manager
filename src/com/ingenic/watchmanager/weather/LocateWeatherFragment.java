/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhengchuanshu<chuanshu.zheng@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.weather;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.ptr.PtrFrameLayout;
import com.ingenic.watchmanager.ptr.PtrHandler;

public class LocateWeatherFragment extends WeatherFragment {

    @Override
    public void onAttach(Activity activity) {
        super.callSuperOnAttach(activity);
        mModel = new WeatherInfoModel(activity, true);
        mModel.registCallback(this);
        mViewPager = (ViewPager) activity.findViewById(R.id.view_pager);
        mCurrentCity = mModel.getLocatedCity();
        mModel.getWeatherOfHere();
    }

    @Override
    protected void initTimeAndCityName() {
        Calendar calendar = null;
        SimpleDateFormat format2 = null;
        if (mCurrentCity != null) {
            mCity.setText(mCurrentCity.name);
            TimeZone timeZone = null;
            if (mCurrentCity.timezone != null)
                timeZone = TimeZone.getTimeZone(mCurrentCity.timezone);
            else
                timeZone = TimeZone.getDefault();
            calendar = Calendar.getInstance(timeZone);
            format2 = new SimpleDateFormat("MM/dd", Locale.getDefault());
            format2.setTimeZone(timeZone);
        } else {
            calendar = Calendar.getInstance();
            format2 = new SimpleDateFormat("MM/dd", Locale.getDefault());
        }
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        String dateStr = format2.format(calendar.getTime());
        String day = getResources().getStringArray(R.array.day_of_week)[dayOfWeek - 1];
        mWeek.setText(day);
        mDate.setText(dateStr);
        calendar.clear();
    }

    @Override
    protected void initPtr() {
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame,
                    View content, View header) {
                return true;
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame, Object data) {
                if (data == null)
                    doRefresh();
                else
                    mModel.getWeatherFromNet((City2Show) data,
                            WeatherPagerFragment.mCurrentTempUnit,
                            new WeatherRunnableCallBack() {

                                @Override
                                public void before() {

                                }

                                @Override
                                public void afterGetCity() {
                                    SharedPreferences sp = getActivity()
                                            .getSharedPreferences(
                                                    Weather_Constant.SP_WEATHER,
                                                    Context.MODE_PRIVATE);
                                    sp.edit()
                                            .putBoolean(
                                                    Weather_Constant.PROMPT,
                                                    false).commit();
                                }

                                @Override
                                public void afterGetWeather() {

                                }
                            });
            }
        });
    }

    private void doRefresh() {
        mCurrentCity = mModel.getLocatedCity();
        if (mCurrentCity == null) {
            mModel.getWeatherOfHere();
        } else {// 已经定位过
            mModel.getWeatherFromNet(mCurrentCity,
                    WeatherPagerFragment.mCurrentTempUnit);
        }
    }

    @Override
    public void onLocalDataSuccess(List<WeatherInfo> infos, int where) {
        if (isDetached())
            return;
        mLineChart.updateWeatherForecast(infos);
        WeatherInfo info = getToday(infos);
        updateToday(info);
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }
        // 100表示从网络上获取的， 1000表示从数据库获取到的
        if (where == 1000)
            return;
        sIsSuccess = true;
        mPtrFrame.refreshComplete(true);
        mCurrentCity = mModel.getLocatedCity();
    }

}
