/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.weather;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.WMFragment;
import com.ingenic.watchmanager.ptr.PtrClassicFrameLayout;
import com.ingenic.watchmanager.ptr.PtrFrameLayout;
import com.ingenic.watchmanager.ptr.PtrHandler;
import com.ingenic.watchmanager.util.Utils;
import com.ingenic.watchmanager.view.WeatherLineChart;

public class WeatherFragment extends WMFragment implements
        WeatherInfoModel.WeatherCallback, OnClickListener {
    protected TextView mCity;
    protected TextView mCurrentTemp;
    protected TextView mWeather;
    protected TextView mTemp;
    protected TextView mUpdate;
    protected TextView mDate;
    protected TextView mWeek;
    protected ImageView mIvAddCity;
    protected WeatherLineChart mLineChart;
    protected ImageView mIconView;
    protected WeatherInfoModel mModel;
    protected ProgressBar mProgressBar;
    protected City2Show mCurrentCity;
    protected ViewPager mViewPager;
    protected PtrClassicFrameLayout mPtrFrame;
    public static boolean sIsSuccess;

    protected void updateToday(WeatherInfo info) {
        if (getActivity() == null)
            return;
        String degress = Utils.getWeatherDegress(getActivity()
                .getApplicationContext(), info);
        mCurrentTemp.setText(info.currentTemp + degress);
        mWeather.setText(Utils.getWeatherByLocal(getResources(),
                info.weatherCode));
        mTemp.setText(info.minimumTemp + "~" + info.maximumTemp + degress);
        mUpdate.setText(getString(R.string.update_time, info.updateTime));
        int code = Integer.parseInt(info.weatherCode);
        int iconId = Utils.getWeatherIconResource(getActivity(), code);
        mIconView.setImageResource(iconId);
        mCity.setText(info.city);
    }

    protected static WeatherInfo getToday(List<WeatherInfo> infos) {
        for (WeatherInfo weatherInfo : infos) {
            if (weatherInfo.dayIndex == 0) {
                return weatherInfo;
            }
        }
        return null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCurrentCity = getArguments().getParcelable("city");
        mModel = new WeatherInfoModel(activity, false);
        mModel.registCallback(this);
        mViewPager = (ViewPager) activity.findViewById(R.id.view_pager);
    }

    protected void callSuperOnAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_weather, container,
                false);
        initView(rootView);
        setupTypeFace();
        initTimeAndCityName();
        initPtr();
        return rootView;
    }

    protected void initView(View rootView) {
        mCity = (TextView) rootView.findViewById(R.id.city);
        mCurrentTemp = (TextView) rootView.findViewById(R.id.current_temp);
        mWeather = (TextView) rootView.findViewById(R.id.weather);
        mTemp = (TextView) rootView.findViewById(R.id.temps);
        mUpdate = (TextView) rootView.findViewById(R.id.update_time);
        mIconView = (ImageView) rootView.findViewById(R.id.weather_icon);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progress);
        mLineChart = (WeatherLineChart) rootView
                .findViewById(R.id.weatherLineChart1);
        mDate = (TextView) rootView.findViewById(R.id.date);
        mWeek = (TextView) rootView.findViewById(R.id.week);
        mIvAddCity = (ImageView) rootView.findViewById(R.id.add_city);
        mIvAddCity.setOnClickListener(this);
        mPtrFrame = (PtrClassicFrameLayout) rootView
                .findViewById(R.id.view_pager_ptr_frame);
        mPtrFrame.disableWhenHorizontalMove(true);
    }

    protected void initTimeAndCityName() {
        // 初始化城市名稱
        if (mCurrentCity != null) {
            mCity.setText(mCurrentCity.name);
            TimeZone timeZone = null;
            if (mCurrentCity.timezone != null)
                timeZone = TimeZone.getTimeZone(mCurrentCity.timezone);
            else
                timeZone = TimeZone.getDefault();
            Calendar calendar = Calendar.getInstance(timeZone);
            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
            SimpleDateFormat format2 = new SimpleDateFormat("MM/dd",
                    Locale.getDefault());
            format2.setTimeZone(timeZone);
            String dateStr = format2.format(calendar.getTime());
            String day = getResources().getStringArray(R.array.day_of_week)[dayOfWeek - 1];
            mWeek.setText(day);
            mDate.setText(dateStr);
            calendar.clear();
        }
    }

    protected void initPtr() {
        // mPtrFrame.setLastUpdateTimeRelateObject(this);
        // mPtrFrame.setPinContent(true);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame,
                    View content, View header) {
                return true;
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame, Object data) {
                doRefresh();
            }
        });
    }

    protected void setupTypeFace() {
        Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/watch_font.ttf");
        mCity.setTypeface(typeFace);
        mUpdate.setTypeface(typeFace);
        mWeather.setTypeface(typeFace);
        mWeek.setTypeface(typeFace);
        mDate.setTypeface(typeFace);
        mCurrentTemp.setTypeface(typeFace);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mCurrentCity != null) {
            mModel.getWeatherFromDatabase(mCurrentCity.woeid);
            long last = mCurrentCity.update_time;
            long now = System.currentTimeMillis();
            String language = Locale.getDefault().toString().replace("_", "-");
            if (Math.abs(now - last) > Weather_Constant.TWO_HOURS
                    || !language.equals(mCurrentCity.lang)) {
                if (!Utils.isNetworkConnected(getActivity()))
                    return;
                mPtrFrame.post(new Runnable() {
                    @Override
                    public void run() {
                        mPtrFrame.autoRefresh(null);
                    }
                });
            }
        }
    }

    private void doRefresh() {
        if (mModel != null)
            mModel.getWeatherFromNet(mCurrentCity,
                    WeatherPagerFragment.mCurrentTempUnit);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mModel.cancelGetCity();
        mModel.cancelGetWeather();
        mModel.unRegistCallback(this);
        mModel = null;
    }

    @Override
    public void onLocalDataSuccess(List<WeatherInfo> infos, int where) {
        if (isDetached())
            return;
        mLineChart.updateWeatherForecast(infos);
        WeatherInfo info = getToday(infos);
        updateToday(info);
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }
        // 100表示从网络上获取的， 1000表示从数据库获取到的
        if (where == 1000)
            return;
        sIsSuccess = true;
        mPtrFrame.refreshComplete(true);
        City2Show city = mModel.getCityByWoeid(mCurrentCity.woeid);
        if (city != null)
            mCurrentCity = city;
    }

    @Override
    public void onLocalDataFailed(int reasonCode) {
        if (isDetached())
            return;
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }
        if (reasonCode == -1000) {// 从数据库获取失败
            mPtrFrame.autoRefresh(null);
            return;
        }
        sIsSuccess = false;
        mPtrFrame.refreshComplete(false);
        if (!isVisible())
            return;
        int reasonRes = reasonCode == -101 ? R.string.error_city
                : R.string.network_error;
        Toast.makeText(getActivity(), reasonRes, Toast.LENGTH_SHORT).show();
    }

    public void showProgress() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.add_city:
            if (mViewPager.getAdapter().getCount() >= 5) {
                Toast.makeText(getActivity(), R.string.warning_city,
                        Toast.LENGTH_SHORT).show();
                return;
            }
            Intent it = new Intent(getActivity(), SelectCityActivity.class);
            startActivityForResult(it, R.id.weather);
            break;

        default:
            break;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case R.id.weather:
            if (resultCode == Activity.RESULT_OK && data != null) {
                // String city = data.getStringExtra("city");
                WeatherPagerAdapter adapter = (WeatherPagerAdapter) mViewPager
                        .getAdapter();
                WeatherPagerAdapter mAdapter = new WeatherPagerAdapter(
                        getFragmentManager(), getActivity());
                adapter.removeAllFragment();
                mViewPager.setAdapter(mAdapter);
                int count = mAdapter.getCount();
                mViewPager.setCurrentItem(count - 1);
                mViewPager = null;
            }
            break;

        default:
            break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
