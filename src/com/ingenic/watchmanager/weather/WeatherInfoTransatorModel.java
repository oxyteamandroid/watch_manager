/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhengchuanshu<chuanshu.zheng@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.weather;

import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;

import com.baidu.location.BDLocation;
import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.WeatherInfoArray;
import com.ingenic.iwds.datatransactor.elf.WeatherTransactionModel;
import com.ingenic.watchmanager.db.WatchManagerContracts;
import com.ingenic.watchmanager.util.UUIDS;

public class WeatherInfoTransatorModel extends WeatherModel implements
        WeatherTransactionModel.WeatherInfoTransactionModelCallback,
        WeatherFromNet.WeatherNetCallback {
    private static WeatherInfoTransatorModel sInstance;
    private static WeatherTransactionModel mTransactionModel;
    private volatile boolean mRequesting = false;
    private String mTempUnit = "c";
    private String mCity;

    private WeatherInfoTransatorModel(Context context) {
        super(context);
        mFromNet = new WeatherFromNet(context.getApplicationContext(), true);
        mFromNet.registCallback(this);

        if (mTransactionModel == null) {
            mTransactionModel = new WeatherTransactionModel(context, this,
                    UUIDS.WEATHER);
        }
    }

    public void startTransaction() {
        mTransactionModel.start();
    }

    public void stopTransaction() {
        mTransactionModel.stop();
    }

    public synchronized static WeatherInfoTransatorModel getInstance(
            Context context) {
        if (sInstance == null) {
            sInstance = new WeatherInfoTransatorModel(context);
        }
        return sInstance;
    }

    public List<WeatherInfo> getWeatherFromDatabase(String woeid) {
        return mOperator.queryAll("woeid=? and temp_unit=?", new String[] {
                woeid, mTempUnit }, null);
    }

    public void getWeatherFromNet(City2Show city, String tempUnit) {
        cancelGetWeather();

        mWeatherRunnable = new WeatherRunnable(city, tempUnit);
        mNetworkHandler.post(mWeatherRunnable);
    }

    private void sendWeatherFromDatabase(String woeid) {
        if (woeid == null || "".equals(woeid)) {
            mTransactionModel.notifyRequestFailed();
            return;
        }
        if (mRequesting) {
            List<WeatherInfo> infos = getWeatherFromDatabase(woeid);
            if (infos == null || infos.size() <= 0) {
                mTransactionModel.notifyRequestFailed();
                return;
            }

            WeatherInfoArray.WeatherInfo[] infoArray = infos
                    .toArray(new WeatherInfoArray.WeatherInfo[] {});
            WeatherInfoArray array = new WeatherInfoArray(infoArray);
            mTransactionModel.send(array);
            mRequesting = false;
        }
    }

    @Override
    public void onRequest() {
        mRequesting = true;
        mTempUnit = mContext.getSharedPreferences(Weather_Constant.SP_WEATHER,
                Context.MODE_PRIVATE).getString("temp_unit", "c");

        City2Show city2Show = mCity2Operator.query(null,
                "isdefault=? and unit=?", new String[] { String.valueOf(1),
                        mTempUnit }, null, null, null);

        if (city2Show != null) {
            // 如果设置了默认城市就发送默认城市的天气
            mCity = city2Show.woeid;
            sendWeather(city2Show);
        } else {
            city2Show = mCity2Operator.query(null, "located=? and unit=?",
                    new String[] { String.valueOf(1), mTempUnit }, null, null,
                    null);
            // 如果没有默认城市就发送已经定位到的城市天气
            if (city2Show != null) {
                mCity = city2Show.woeid;
                sendWeather(city2Show);
            } else {
                // 默认城市和定位到的城市都为空就从网络获取
                if (!mClient.isStarted()) {
                    mClient.start();
                }
            }
        }
    }

    private void sendWeather(City2Show city) {
        long last_update = city.update_time;
        long now = System.currentTimeMillis();

        String language = Locale.getDefault().toString().replace("_", "-");
        if (Math.abs(now - last_update) > Weather_Constant.TWO_HOURS
                || !language.equals(city.lang)) {
            getWeatherFromNet(city, mTempUnit);
        } else {
            sendWeatherFromDatabase(city.woeid);
        }
    }

    @Override
    public void onRequestFailed() {

    }

    @Override
    public void onObjectArrived(WeatherInfoArray object) {

    }

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        if (!isConnected) {
            mRequesting = false;
        }
    }

    @Override
    public void onChannelAvailable(boolean isAvailable) {
        if (!isAvailable) {
            mRequesting = false;
        }

    }

    @Override
    public void onSendResult(DataTransactResult result) {
        mRequesting = false;

        int resultCode = result.getResultCode();
        if (resultCode == DataTransactResult.RESULT_OK) {
        } else {
            // Failed
        }
    }

    @Override
    public void onCityError() {
        mPrivateHandler.obtainMessage(MSG_DATA_FAILED, -101, -1).sendToTarget();
        if (mCity == null || "".equals(mCity))
            mTransactionModel.notifyRequestFailed();
        else
            sendWeatherFromDatabase(mCity);
    }

    @Override
    public void onNetworkError() {
        mPrivateHandler.obtainMessage(MSG_DATA_FAILED, -100, -1).sendToTarget();
        if (mCity == null || "".equals(mCity))
            mTransactionModel.notifyRequestFailed();
        else
            sendWeatherFromDatabase(mCity);
    }

    @Override
    public void onReceiveLocation(BDLocation location) {
        if (location != null) {
            mClient.stop();

            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            cancelGetCity();

            mCityRunnable = new CityRunnable(latitude, longitude);
            mNetworkHandler.post(mCityRunnable);
        }
    }

    private class CityRunnable implements Runnable {
        private double mLatitude;
        private double mLongitude;

        public CityRunnable(double latitude, double longitude) {
            mLatitude = latitude;
            mLongitude = longitude;
        }

        @Override
        public void run() {
            String city = mFromNet.getCity(mLatitude, mLongitude);
            if (city == null) {
                if (mRequesting) {
                    sendWeatherFromDatabase(mCity);
                }
                return;
            }
            SharedPreferences sp = mContext.getSharedPreferences(
                    Weather_Constant.SP_WEATHER, Context.MODE_PRIVATE);
            String last_locate_city = sp.getString(
                    Weather_Constant.LAST_LOCATE_CITY, "-1");
            if ("-1".equals(last_locate_city)) {
                getWeatherFromNet(new City2Show(city),
                        WeatherPagerFragment.mCurrentTempUnit);
                sp.edit().putString(Weather_Constant.LAST_LOCATE_CITY, city)
                        .putBoolean(Weather_Constant.PROMPT, false).commit();
            }
            getWeatherFromNet(new City2Show(city), mTempUnit);
        }

    }

    private class WeatherRunnable implements Runnable {
        private City2Show mCity;
        private String mTempUnit;

        public WeatherRunnable(City2Show city, String tempUnit) {
            mCity = city;
            mTempUnit = tempUnit;
        }

        @Override
        public void run() {

            mCity = mFromNet.getWoeid(mCity);

            if (mCity == null) {
                return;
            }

            List<WeatherInfo> infos = mFromNet.getWeathersOfCity(mCity,
                    mTempUnit);
            // 获取到完整的五天数据才允许更新数据库
            if (infos == null || infos.size() < 5) {
                if (mRequesting)
                    sendWeatherFromDatabase(mCity.woeid);
                return;
            }

            mPrivateHandler.obtainMessage(MSG_DATA_SUCCESS, infos)
                    .sendToTarget();
            if (mRequesting) {
                WeatherInfoArray.WeatherInfo[] infoArray = infos
                        .toArray(new WeatherInfoArray.WeatherInfo[] {});
                WeatherInfoArray array = new WeatherInfoArray(infoArray);
                mTransactionModel.send(array);
            }

            if (mCity != null && mCity.woeid != null) {
                mOperator
                        .delete(WatchManagerContracts.WeatherColumns.WOEID
                                + "=? and "
                                + WatchManagerContracts.WeatherColumns.TEMP_UNIT
                                + "=?", new String[] { mCity.woeid, mTempUnit });
            }

            for (WeatherInfo weatherInfo : infos) {
                writeToDatabase(weatherInfo);
            }

        }
    }

    public boolean isRequesting() {
        return mRequesting;
    }

}
