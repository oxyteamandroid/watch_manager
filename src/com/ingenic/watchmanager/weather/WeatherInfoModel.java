/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhengchuanshu<chuanshu.zheng@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.weather;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.widget.CheckBox;

import com.baidu.location.BDLocation;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.db.WatchManagerContracts;
import com.ingenic.watchmanager.ptr.PtrClassicFrameLayout;

public class WeatherInfoModel extends WeatherModel implements
        WeatherFromNet.WeatherNetCallback {
    private Runnable mLocalDataRunnable;
    private boolean mIsLocated;
    private boolean detached = false;

    public WeatherInfoModel(Context context, boolean isLocated) {
        super(context);
        mFromNet = new WeatherFromNet(context, isLocated);
        mFromNet.registCallback(this);
        mIsLocated = isLocated;
    }

    public void getWeatherFromDatabase(String woeid) {
        cancelLocalRunnable();// 取消上一次的
        mLocalDataRunnable = new LocalDataRunnable(woeid);
        mPrivateHandler.post(mLocalDataRunnable);
    }

    public void getWeatherFromNet(City2Show city, String tempUnit) {
        cancelGetWeather();

        mWeatherRunnable = new WeatherRunnable(city, tempUnit, null);
        mNetworkHandler.post(mWeatherRunnable);
    }

    public void getWeatherFromNet(City2Show city, String tempUnit,
            WeatherRunnableCallBack callBack) {
        cancelGetWeather();

        mWeatherRunnable = new WeatherRunnable(city, tempUnit, callBack);
        mNetworkHandler.post(mWeatherRunnable);
    }

    private class LocalDataRunnable implements Runnable {
        private String woeid;

        public LocalDataRunnable(String woeid) {
            this.woeid = woeid;
        }

        @Override
        public void run() {
            List<WeatherInfo> infos = mOperator.queryAll(
                    "woeid=? and temp_unit=?", new String[] { woeid,
                            WeatherPagerFragment.mCurrentTempUnit }, null);
            if (infos != null && infos.size() > 0)
                mPrivateHandler.obtainMessage(MSG_DATA_SUCCESS, 1000, 0, infos)
                        .sendToTarget();
            else
                mPrivateHandler.obtainMessage(MSG_DATA_FAILED, -1000, -1)
                        .sendToTarget();
            return;
        }

    }

    private class CityRunnable implements Runnable {
        private double mLatitude;
        private double mLongitude;

        public CityRunnable(double latitude, double longitude) {
            mLatitude = latitude;
            mLongitude = longitude;
        }

        @Override
        public void run() {
            String city = mFromNet.getCity(mLatitude, mLongitude);
            if (detached)
                return;
            if (city == null) {
                mPrivateHandler.obtainMessage(MSG_DATA_FAILED, -101)
                        .sendToTarget();
                return;
            }
            SharedPreferences sp = mContext.getSharedPreferences(
                    Weather_Constant.SP_WEATHER, Context.MODE_PRIVATE);
            String last_locate_city = sp.getString(
                    Weather_Constant.LAST_LOCATE_CITY, "-1");
            if ("-1".equals(last_locate_city)) {
                getWeatherFromNet(new City2Show(city),
                        WeatherPagerFragment.mCurrentTempUnit);
                sp.edit().putString(Weather_Constant.LAST_LOCATE_CITY, city)
                        .putBoolean(Weather_Constant.PROMPT, false).commit();
            } else {
                if (!city.equals(last_locate_city)) {
                    sp.edit()
                            .putString(Weather_Constant.LAST_LOCATE_CITY, city)
                            .putBoolean(Weather_Constant.PROMPT, true).commit();
                    showSwitchCityDialog(sp, city);
                } else {
                    boolean prompt = sp.getBoolean(Weather_Constant.PROMPT,
                            true);
                    if (prompt) {
                        showSwitchCityDialog(sp, city);
                    }
                }
            }
        }

    }

    private class WeatherRunnable implements Runnable {
        private City2Show mCity;
        private String mTempUnit;
        private WeatherRunnableCallBack mCallBack;

        public WeatherRunnable(City2Show city, String tempUnit,
                WeatherRunnableCallBack callBack) {
            mCity = city;
            mTempUnit = tempUnit;
            mCallBack = callBack;
        }

        @Override
        public void run() {
            if (detached)
                return;

            if (mCallBack != null)
                mCallBack.before();

            City2Show oldCity = getLocatedCity();
            City2Show city2Show = mFromNet.getWoeid(mCity);
            if (city2Show == null) {
                return;
            }

            if (mCallBack != null)
                mCallBack.afterGetCity();

            List<WeatherInfo> infos = mFromNet.getWeathersOfCity(city2Show,
                    mTempUnit);

            if (detached)
                return;
            // 获取到完整的五天数据才允许更新数据库
            if (infos == null || infos.size() < 5) {
                onNetworkError();
                return;
            }

            if (mCallBack != null)
                mCallBack.afterGetWeather();

            mPrivateHandler.obtainMessage(MSG_DATA_SUCCESS, 100, 0, infos)
                    .sendToTarget();

            if (mIsLocated
                    && WeatherInfoTransatorModel.getInstance(mContext)
                            .isRequesting()) {
                return;
            }

            if (city2Show != null && city2Show.woeid != null) {
                if (mIsLocated) {
                    if (oldCity != null && oldCity.woeid != null)
                        mOperator
                                .delete(WatchManagerContracts.WeatherColumns.WOEID
                                        + "=? and "
                                        + WatchManagerContracts.WeatherColumns.TEMP_UNIT
                                        + "=?", new String[] { oldCity.woeid,
                                        mTempUnit });
                } else
                    mOperator.delete(WatchManagerContracts.WeatherColumns.WOEID
                            + "=? and "
                            + WatchManagerContracts.WeatherColumns.TEMP_UNIT
                            + "=?", new String[] { mCity.woeid, mTempUnit });
            }

            for (WeatherInfo weatherInfo : infos) {
                writeToDatabase(weatherInfo);
            }
        }

    }

    public void cancelLocalRunnable() {
        if (mLocalDataRunnable != null) {
            mNetworkHandler.removeCallbacks(mLocalDataRunnable);
            mLocalDataRunnable = null;
        }
    }

    @Override
    public void onReceiveLocation(BDLocation location) {
        if (location != null && !detached) {
            mClient.stop();

            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            cancelGetCity();
            mCityRunnable = new CityRunnable(latitude, longitude);
            mNetworkHandler.post(mCityRunnable);
        }
    }

    @Override
    public void onCityError() {
        mPrivateHandler.obtainMessage(MSG_DATA_FAILED, -101, -1).sendToTarget();
    }

    @Override
    public void onNetworkError() {
        mPrivateHandler.obtainMessage(MSG_DATA_FAILED, -100, -1).sendToTarget();
    }

    public City2Show getLocatedCity() {
        City2Show city = null;
        try {
            city = mCity2Operator
                    .query(null, "located=? and unit=?", new String[] { "1",
                            WeatherPagerFragment.mCurrentTempUnit }, null,
                            null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return city;
    }

    public City2Show getCityByWoeid(String woeid) {
        return mCity2Operator.queryCity(woeid);
    }

    @Override
    public void unRegistCallback(
            com.ingenic.watchmanager.Model.Callback<WeatherInfo> callback) {
        super.unRegistCallback(callback);
        mFromNet.removeCallback();
        mClient.unRegisterLocationListener(this);
        detached = true;
    }

    private void showSwitchCityDialog(final SharedPreferences sp,
            final String city) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(mContext.getString(R.string.location_changed) + city
                + "?");
        final CheckBox view = (CheckBox) LayoutInflater.from(mContext).inflate(
                R.layout.dialog_weather_locate_prompt, null);
        builder.setView(view);
        builder.setNegativeButton(R.string.cancel,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (view.isChecked()) {
                            sp.edit()
                                    .putBoolean(Weather_Constant.PROMPT, false)
                                    .commit();
                        }
                    }
                })
                .setPositiveButton(R.string.confirm,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                mPrivateHandler.post(new Runnable() {

                                    @Override
                                    public void run() {
                                        ViewPager viewPager = (ViewPager) ((Activity) mContext)
                                                .findViewById(R.id.view_pager);
                                        if (viewPager.getCurrentItem() != 0)
                                            viewPager.setCurrentItem(0);

                                        PtrClassicFrameLayout mPtrFrame = (PtrClassicFrameLayout) ((Activity) mContext)
                                                .findViewById(R.id.view_pager_ptr_frame);
                                        mPtrFrame.autoRefresh(new City2Show(
                                                city));
                                    }
                                });
                            }
                        }).show();
    }
}
