/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhengchuanshu<chuanshu.zheng@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.weather;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.weather.WeatherFromNet.WeatherNetCallback;

public class SelectCityActivity extends Activity implements WeatherNetCallback {
    private SearchView mSearchView;
    private TabHost mTabHost;
    private ListView mOffenList;
    private ListView mHotList;
    private ListView mQueryList;
    private City2Operator mOperator;
    private CitysOprator mCitysOperator;
    private List<String> mAddedCitys = new ArrayList<String>();
    private WeatherFromNet mWeatherFromNer;
    private Typeface mTypeFace;
    private Dialog mDialog;
    private List<QueryCity> mQueryCitys;
    private List<OffenCity> mOffenCitys;
    private QueryTask mCurrentQueryTask;
    private RelativeLayout mSearchRelativeLayout;
    private LinearLayout mSearchLinearLayout;
    private TextView mSearchTextView;
    private ProgressBar mSearchProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_weather_citys);
        mOperator = new City2Operator(this);
        mCitysOperator = new CitysOprator(this);
        mTypeFace = Typeface.createFromAsset(getAssets(),
                "fonts/watch_font.ttf");
        mWeatherFromNer = new WeatherFromNet(getApplicationContext(), false);
        mWeatherFromNer.registCallback(this);
        List<City2Show> lists = mOperator.queryAll();
        if (lists != null && lists.size() > 0) {
            for (City2Show city : lists) {
                mAddedCitys.add(city.woeid);
            }
        }
        initViews();
    }

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWeatherFromNer.removeCallback();
        if (mDialog.isShowing())
            mDialog.dismiss();
        if (mCurrentQueryTask != null)
            mCurrentQueryTask.cancel(true);
    }

    private void initViews() {
        initTabHost();
        initListView();
        initSearchView();
        updateTabColor();
        mDialog = new Dialog(this, R.style.MyDialog);
        mDialog.setContentView(R.layout.dialog_waiting);
        mDialog.setCancelable(false);
        mSearchRelativeLayout = (RelativeLayout) findViewById(R.id.rl_search);
        mSearchLinearLayout = (LinearLayout) findViewById(R.id.ll_search);
        mSearchTextView = (TextView) findViewById(R.id.city_searching_tv);
        mSearchProgressBar = (ProgressBar) findViewById(R.id.city_searching_pb);
        mSearchTextView.setTypeface(mTypeFace);
    }

    private void updateTabColor() {
        TabWidget tabWidget = mTabHost.getTabWidget();
        for (int i = 0; i < tabWidget.getChildCount(); i++) {
            TextView tv = (TextView) tabWidget.getChildAt(i).findViewById(
                    android.R.id.title);
            tv.setTypeface(mTypeFace);
            tv.setTextSize(18);
            if (mTabHost.getCurrentTab() == i)
                tv.setTextColor(Color.parseColor("#f8e102"));
            else
                tv.setTextColor(Color.WHITE);
        }
    }

    private void initTabHost() {
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();
        TabSpec spec1 = mTabHost.newTabSpec("Tab 1");
        spec1.setContent(R.id.tab1);
        spec1.setIndicator(getString(R.string.label_offen_citys), null);
        TabSpec spec2 = mTabHost.newTabSpec("Tab 2");
        spec2.setContent(R.id.tab2);
        spec2.setIndicator(getString(R.string.label_hot_citys));
        mTabHost.addTab(spec1);
        mTabHost.addTab(spec2);

        mTabHost.setOnTabChangedListener(new OnTabChangeListener() {

            @Override
            public void onTabChanged(String tabId) {
                updateTabColor();
            }
        });
    }

    private void initListView() {
        mOffenList = (ListView) findViewById(R.id.list1);
        mHotList = (ListView) findViewById(R.id.list2);
        mQueryList = (ListView) findViewById(R.id.list_query);

        mOffenCitys = mCitysOperator.queryAll();
        if (mOffenCitys != null) {
            mOffenList.setAdapter(new OffenCityAdapter(this, mOffenCitys));
        }

        mOffenList
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                            int position, long id) {
                        OffenCity city = mOffenCitys.get(position);
                        if (mAddedCitys.contains(city.woeid)) {
                            Toast.makeText(getApplicationContext(),
                                    R.string.already_added, Toast.LENGTH_SHORT)
                                    .show();
                            return;
                        }
                        new CityTask().execute(city);
                    }

                });

        mOffenList
                .setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent,
                            View view, final int position, long id) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                SelectCityActivity.this);
                        builder.setMessage(R.string.delete_confirm);
                        builder.setNegativeButton(R.string.cancel, null);
                        builder.setPositiveButton(R.string.confirm,
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                            int which) {
                                        OffenCity city = mOffenCitys
                                                .get(position);
                                        mCitysOperator.delete(city);
                                        mOffenCitys.remove(position);
                                        ((BaseAdapter) mOffenList.getAdapter())
                                                .notifyDataSetChanged();

                                    }
                                });
                        builder.show();
                        return true;
                    }

                });

        List<String> list = Arrays.asList(getResources().getStringArray(
                R.array.citys));
        final List<OffenCity> lists = new ArrayList<OffenCity>();
        for (String str : list) {
            String[] strs = str.split(",");
            OffenCity city = new OffenCity(strs[0]);
            city.woeid = strs[1];
            lists.add(city);
        }
        mHotList.setAdapter(new OffenCityAdapter(this, lists));
        mHotList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {
                OffenCity city = lists.get(position);
                if (city == null) {
                    return;
                }
                if (mAddedCitys.contains(city.woeid)) {
                    Toast.makeText(getApplicationContext(),
                            R.string.already_added, Toast.LENGTH_SHORT).show();
                    return;
                }
                new CityTask().execute(city);
            }
        });

        mQueryList
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                            int position, long id) {
                        if (mAddedCitys.contains(mQueryCitys.get(position).woeid)) {
                            Toast.makeText(getApplicationContext(),
                                    R.string.already_added, Toast.LENGTH_SHORT)
                                    .show();
                            return;
                        }
                        QueryCity city = mQueryCitys.get(position);
                        new SaveCityTask().execute(city);
                    }
                });
    }

    private void initSearchView() {
        mSearchView = (SearchView) findViewById(R.id.city_search);
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setFocusable(false);
        mSearchView.clearFocus();
        mSearchView.setSubmitButtonEnabled(true);
        mSearchView
                .setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                    @Override
                    public boolean onQueryTextSubmit(final String query) {
                        if (query == null || query == "") {
                            return false;
                        }
                        if (mCurrentQueryTask != null)
                            mCurrentQueryTask.cancel(true);
                        mCurrentQueryTask = new QueryTask();
                        mCurrentQueryTask.execute(query);
                        return true;
                    }

                    @Override
                    public boolean onQueryTextChange(final String newText) {
                        if (newText.trim().equals("")) {
                            mSearchRelativeLayout.setVisibility(View.GONE);
                            mTabHost.setVisibility(View.VISIBLE);
                            return false;
                        }
                        if (mCurrentQueryTask != null)
                            mCurrentQueryTask.cancel(true);
                        mCurrentQueryTask = new QueryTask();
                        mCurrentQueryTask.execute(newText);
                        return true;
                    }
                });
    }

    private void saveCity(String name, String woeid) {
        if (name != null && !name.equals("")) {
            mCitysOperator.save(name, woeid);
            Intent it = new Intent();
            it.putExtra("city", name);
            setResult(RESULT_OK, it);
            onBackPressed();
        } else {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.network_error), Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private class SaveCityTask extends AsyncTask<QueryCity, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!mDialog.isShowing())
                mDialog.show();
        }

        @Override
        protected String doInBackground(QueryCity... params) {
            QueryCity city = params[0];
            if (city == null || city.woeid == null || city.name == null)
                return null;
            City2Show city2 = new City2Show();
            city2.woeid = city.woeid;
            city2.name = city.name;
            city2.timezone = city.timezone;
            city2.tempUnit = Weather_Constant.TEMP_UNIT_CENTIGRADE;
            mOperator.save(city2);
            city2.tempUnit = Weather_Constant.TEMP_UNIT_FAHRENHEIT;
            mOperator.save(city2);
            mCitysOperator.save(city.name, city.woeid);
            return city.name;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (mDialog.isShowing())
                mDialog.dismiss();
            if (result == null) {
                Toast.makeText(getApplicationContext(),
                        getString(R.string.network_error), Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            Intent it = new Intent();
            it.putExtra("city", result);
            setResult(RESULT_OK, it);
            onBackPressed();
        }

    }

    private class CityTask extends AsyncTask<OffenCity, Integer, City2Show> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!mDialog.isShowing())
                mDialog.show();
        }

        @Override
        protected City2Show doInBackground(OffenCity... params) {
            String name = params[0].name;
            String woeid = params[0].woeid;
            City2Show temp = new City2Show(name);
            temp.woeid = woeid;
            City2Show city = mWeatherFromNer.getWoeid(temp);
            return city;
        }

        @Override
        protected void onPostExecute(City2Show result) {
            super.onPostExecute(result);
            if (result == null) {
                Toast.makeText(getApplicationContext(),
                        getString(R.string.network_error), Toast.LENGTH_SHORT)
                        .show();
            } else
                saveCity(result.name, result.woeid);
            if (mDialog.isShowing())
                mDialog.dismiss();
        }

    }

    private class QueryTask extends AsyncTask<String, Integer, List<QueryCity>> {
        private String name;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mSearchRelativeLayout.setVisibility(View.VISIBLE);
            mSearchLinearLayout.setVisibility(View.VISIBLE);
            mSearchProgressBar.setVisibility(View.VISIBLE);
            mTabHost.setVisibility(View.GONE);
            mQueryList.setVisibility(View.GONE);
            mSearchTextView.setText(R.string.city_searching);
        }

        @Override
        protected List<QueryCity> doInBackground(String... params) {
            if (isCancelled())
                return null;
            name = params[0];
            List<QueryCity> citys = mWeatherFromNer.queryCitysByName(name);
            return citys;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            IwdsLog.d(this, "QueryTask:" + name + "  canceled");
        }

        @Override
        protected void onPostExecute(List<QueryCity> result) {
            super.onPostExecute(result);
            if (result == null || result.size() <= 0) {
                mSearchTextView.setText(R.string.error_city);
                mSearchProgressBar.setVisibility(View.INVISIBLE);
            } else {
                mQueryCitys = result;
                mSearchLinearLayout.setVisibility(View.GONE);
                mQueryList.setVisibility(View.VISIBLE);
                mQueryList.setAdapter(new QueryCityAdapter(
                        SelectCityActivity.this, result));
            }
        }

    }

    private class QueryCityAdapter extends BaseAdapter {
        private List<QueryCity> mCitys;
        private Context mContext;

        public QueryCityAdapter(Context context, List<QueryCity> citys) {
            this.mCitys = citys;
            this.mContext = context;
        }

        @Override
        public int getCount() {
            if (mCitys == null)
                return 0;
            return mCitys.size();
        }

        @Override
        public Object getItem(int position) {
            return mCitys.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(mContext).inflate(
                        R.layout.city_item, null);
                viewHolder.cityAdded = (TextView) convertView
                        .findViewById(R.id.city_added);
                viewHolder.cityAdded.setTypeface(mTypeFace);
                viewHolder.cityName = (TextView) convertView
                        .findViewById(R.id.city_name);
                viewHolder.cityName.setTypeface(mTypeFace);
                convertView.setTag(viewHolder);
            }
            viewHolder = (ViewHolder) convertView.getTag();
            QueryCity city = mCitys.get(position);
            String cityName = (city.name == null ? "" : city.name) + ","
                    + (city.admin1 == null ? "" : city.admin1) + ","
                    + (city.country == null ? "" : city.country);
            viewHolder.cityName.setText(cityName);
            if (mAddedCitys.contains(city.woeid)) {
                viewHolder.cityAdded.setText(R.string.city_added);
            } else
                viewHolder.cityAdded.setText("");
            return convertView;
        }

        private class ViewHolder {
            public TextView cityName;
            public TextView cityAdded;
        }

    }

    private class OffenCityAdapter extends BaseAdapter {
        private List<OffenCity> mCitys;
        private Context context;

        public OffenCityAdapter(Context context, List<OffenCity> objects) {
            mCitys = objects;
            this.context = context;
        }

        @Override
        public int getCount() {
            return mCitys.size();
        }

        @Override
        public Object getItem(int position) {
            return mCitys.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(
                        R.layout.city_item, null);
                viewHolder = new ViewHolder();
                viewHolder.cityAdded = (TextView) convertView
                        .findViewById(R.id.city_added);
                viewHolder.cityAdded.setTypeface(mTypeFace);
                viewHolder.cityName = (TextView) convertView
                        .findViewById(R.id.city_name);
                viewHolder.cityName.setTypeface(mTypeFace);
                convertView.setTag(viewHolder);
            }
            viewHolder = (ViewHolder) convertView.getTag();
            String cityName = mCitys.get(position).name;
            viewHolder.cityName.setText(cityName);
            if (mAddedCitys.contains(mCitys.get(position).woeid)) {
                viewHolder.cityAdded.setText(R.string.city_added);
            } else
                viewHolder.cityAdded.setText("");
            return convertView;
        }

        private class ViewHolder {
            public TextView cityName;
            public TextView cityAdded;
        }
    }

    @Override
    public void onCityError() {

    }

    @Override
    public void onNetworkError() {

    }

}
