/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhengchuanshu<chuanshu.zheng@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.weather;

public class Weather_Constant {
    public static final String SP_WEATHER = "weather";

    public static final long TWO_HOURS = 2 * 60 * 60 * 1000;

    public static final String TEMP_UNIT_CENTIGRADE = "c";
    public static final String TEMP_UNIT_FAHRENHEIT = "f";

    public static final String LAST_LOCATE_CITY = "last_locate_city";
    public static final String PROMPT = "prompt";
    
}
