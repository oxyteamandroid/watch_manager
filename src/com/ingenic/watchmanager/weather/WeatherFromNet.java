/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.weather;

import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;

import com.ingenic.watchmanager.R;

public class WeatherFromNet {
    private Resources mResources;
    private WeatherNetCallback mCallback;
    private Context mContext;
    private boolean isLocated;

    public WeatherFromNet(Context context, boolean isLocated) {
        mResources = context.getResources();
        mContext = context;
        this.isLocated = isLocated;
    }

    public void registCallback(WeatherNetCallback callback) {
        mCallback = callback;
    }

    public void removeCallback() {
        mCallback = null;
    }

    /**
     * 获取城市天气信息
     * 
     * @param city
     *            城市名称
     * @param tempUint
     *            温度单位，查看{@link #TEMP_FAHRENHEIT}、{@link #TEMP_CELSIUS}
     *            ，需要注意的是：使用非摄氏单位的其他字符，均认为是华氏单位
     * @return 近期天气列表
     */
    public List<WeatherInfo> getWeathersOfCity(City2Show city, String tempUnit) {
        HttpURLConnection conn = null;

        List<WeatherInfo> infos = new ArrayList<WeatherInfo>();
        try {
            URL url = new URL("http://xml.weather.yahoo.com/forecastrss?w="
                    + city.woeid + "&u=" + tempUnit);

            conn = connectURL(url);
            if (conn == null)
                return null;

            Document doc = parser(conn);
            if (doc == null)
                return null;

            NodeList list = doc.getElementsByTagName("yweather:condition");
            NamedNodeMap map = list.item(0).getAttributes();

            WeatherInfo infoToday = map2TodayInfo(map);
            int today = buildTodayTimes(map, infoToday, city);

            list = doc.getElementsByTagName("yweather:forecast");
            int count = list.getLength();
            if (count <= 0)
                return null;

            for (int i = 0; i < count; i++) {
                map = list.item(i).getAttributes();

                String dateStr = map.getNamedItem("date").getNodeValue();
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy",
                        Locale.ENGLISH);
                Date date = format.parse(dateStr);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
                calendar.clear();

                WeatherInfo info = null;
                if (today != dayOfWeek) {
                    info = new WeatherInfo();
                    if (i == 0)
                        info.dayOfWeek = mResources
                                .getString(R.string.yesterday);
                    else
                        info.dayOfWeek = mResources
                                .getStringArray(R.array.day_of_week)[dayOfWeek - 1];
                    format = new SimpleDateFormat("MM/dd", Locale.getDefault());
                    info.date = format.format(date);

                    String weather = map.getNamedItem("text").getNodeValue();
                    String weatherCode = info.weatherCode = map.getNamedItem(
                            "code").getNodeValue();
                    info.weather = getWeatherByLocal(weatherCode, weather);
                } else {
                    info = infoToday;
                    info.dayOfWeek = mResources.getString(R.string.today);
                }
                info.dayIndex = dayOfWeek - today + (dayOfWeek < today ? 7 : 0);

                buildTemps(map, info);

                info.city = city.name;
                info.woeid = city.woeid;
                info.tempUnit = tempUnit;
                infos.add(info);
            }
        } catch (Exception e) {
            e.printStackTrace();
            notifyNetworkError();
            return null;
        } finally {
            if (conn != null) {
                conn.disconnect();
                conn = null;
            }
        }
        return infos;
    }

    private WeatherInfo map2TodayInfo(NamedNodeMap map) {
        WeatherInfo info = new WeatherInfo();
        String weatherCode = info.weatherCode = map.getNamedItem("code")
                .getNodeValue();
        String weather = map.getNamedItem("text").getNodeValue();
        info.weather = getWeatherByLocal(weatherCode, weather);
        String currentTemp = map.getNamedItem("temp").getNodeValue();
        info.currentTemp = Integer.parseInt(currentTemp);
        return info;
    }

    private int buildTodayTimes(NamedNodeMap map, WeatherInfo todayInfo,
            City2Show city2Show) throws Exception {
        String dateStr = map.getNamedItem("date").getNodeValue();
        SimpleDateFormat format = new SimpleDateFormat("E, dd MMM yyyy h:mm a",
                Locale.ENGLISH);
        Date date = format.parse(dateStr);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int today = calendar.get(Calendar.DAY_OF_WEEK);
        calendar.clear();
        // mCallback为空表示Fragment已经detach掉了
        if (mCallback != null) {
            // 保存每个城市的更新时间
            long millis = System.currentTimeMillis();
            SharedPreferences sp = mContext.getSharedPreferences(
                    Weather_Constant.SP_WEATHER, Context.MODE_PRIVATE);
            City2Operator operator = new City2Operator(mContext);
            city2Show.update_time = millis;
            city2Show.tempUnit = sp.getString("temp_unit", "c");
            if (mCallback != null)
                operator.updateTime(city2Show);
        }
        SimpleDateFormat format2 = new SimpleDateFormat("MM/dd HH:mm",
                Locale.getDefault());
        dateStr = format2.format(date);
        String[] dateTime = dateStr.split(" ");
        todayInfo.date = dateTime[0];
        todayInfo.updateTime = dateTime[1];
        return today;
    }

    private void buildTemps(NamedNodeMap map, WeatherInfo info) {
        String minimumTemp = map.getNamedItem("low").getNodeValue();
        info.minimumTemp = Integer.parseInt(minimumTemp);
        String maximunTemp = map.getNamedItem("high").getNodeValue();
        info.maximumTemp = Integer.parseInt(maximunTemp);
    }

    private static HttpURLConnection connectURL(URL url) {
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(5000);
            // 设置服务器返回数据语言为当前语言
            Locale locale = Locale.getDefault();
            if (locale != null)
                conn.setRequestProperty("Accept-Language", locale.toString()
                        .replace("_", "-"));
            conn.connect();
        } catch (Exception e) {
            e.printStackTrace();
            if (conn != null) {
                conn.disconnect();
                conn = null;
            }
        }
        return conn;
    }

    private static Document parser(HttpURLConnection conn) {
        if (conn == null)
            return null;
        Document doc = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            doc = builder.parse(conn.getInputStream());
            doc.normalize();
        } catch (Exception e) {
            e.printStackTrace();
            if (conn != null) {
                conn.disconnect();
                conn = null;
            }
        }
        return doc;
    }

    public City2Show getWoeid(String city) {
        if (city == null || city.equals(""))
            return null;
        return getWoeid(new City2Show(city));
    }

    /**
     * 获取城市id
     * 
     * @param city
     *            城市名称
     * @return 城市id
     */
    public City2Show getWoeid(City2Show city) {
        if (city == null)
            return null;
        HttpURLConnection conn = null;
        City2Show city2Show = null;
        URL url = null;
        try {
            if (city.woeid != null)
                url = new URL(
                        "http://where.yahooapis.com/v1/place/"
                                + Uri.encode(city.woeid)
                                + "?appid=CPn5O1fV34FY7erCXD7OF_VFMpieKNYdLlFkb5RJgTNtLf2Oupao5FQ0Sy4CJQ--");
            else
                url = new URL(
                        "http://where.yahooapis.com/v1/places.q('"
                                + Uri.encode(city.name)
                                + "')?appid=CPn5O1fV34FY7erCXD7OF_VFMpieKNYdLlFkb5RJgTNtLf2Oupao5FQ0Sy4CJQ--");
            conn = connectURL(url);

            if (conn == null) {
                notifyNetworkError();
                return null;
            }
            Document doc = parser(conn);
            if (doc == null) {
                notifyNetworkError();
                return null;
            }

            NodeList list = doc.getElementsByTagName("woeid");
            String woeid = list.item(0).getFirstChild().getNodeValue();// 城市ID
            list = doc.getElementsByTagName("name");
            String name = list.item(0).getFirstChild().getNodeValue();// 城市名
            String timeZone = "";
            list = doc.getElementsByTagName("timezone");
            if (list != null && list.getLength() > 0)// 有的地方可能不返回時區
                timeZone = list.item(0).getFirstChild().getNodeValue();// 时区
            list = doc.getElementsByTagName("place");
            String lang = list.item(0).getAttributes().getNamedItem("xml:lang")
                    .getNodeValue(); // 语言
            if (woeid == null) {
                notifyCityError();
            } else {
                // mCallback为空表示Fragment已经detach掉了
                if (mCallback != null) {
                    // 保存每个城市的更新时间
                    City2Operator operator = new City2Operator(mContext);
                    city2Show = new City2Show();
                    city2Show.woeid = woeid;
                    city2Show.name = name;
                    city2Show.timezone = timeZone;
                    city2Show.lang = lang;
                    city2Show.tempUnit = WeatherPagerFragment.mCurrentTempUnit;
                    city2Show.isLocated = city.isLocated;
                    if (isLocated && city.woeid == null) {
                        city2Show.isLocated = true;
                    }
                    if (mCallback != null) {
                        operator.save(city2Show);
                        city2Show.tempUnit = WeatherPagerFragment.mCurrentTempUnit
                                .equals(Weather_Constant.TEMP_UNIT_CENTIGRADE) ? Weather_Constant.TEMP_UNIT_FAHRENHEIT
                                : Weather_Constant.TEMP_UNIT_CENTIGRADE;
                        operator.save(city2Show);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof NullPointerException) {
                notifyCityError();
            } else {
                notifyNetworkError();
            }
        } finally {
            if (conn != null) {
                conn.disconnect();
                conn = null;
            }
        }
        return city2Show;
    }

    private String getWeatherByLocal(String weatherCode, String weather) {
        int code = -1;
        try {
            code = Integer.parseInt(weatherCode);
        } catch (Exception e) {
            return weather;
        }

        Locale locale = Locale.getDefault();
        if (locale.equals(Locale.SIMPLIFIED_CHINESE)
                || locale.equals(Locale.TRADITIONAL_CHINESE)) {
            String[] weathers = mResources.getStringArray(R.array.weather);
            if (code >= weathers.length) {
                return mResources.getString(R.string.wrong_weather);
            } else {
                return weathers[code];
            }
        }
        return weather;
    }

    public List<QueryCity> queryCitysByName(String query) {
        HttpURLConnection conn = null;
        List<QueryCity> citys = null;
        try {
            Locale locale = Locale.getDefault();
            String languge = locale.getLanguage();
            URL url = new URL(
                    "http://query.yahooapis.com/v1/public/yql?q=select*from%20geo.places%20where%20text=\""
                            + Uri.encode(query)
                            + "*\"%20and%20lang=\""
                            + languge + "\"&format=xml");
            conn = connectURL(url);
            if (conn == null) {
                return null;
            }
            Document doc = parser(conn);
            if (doc == null) {
                return null;
            }

            NodeList nodes = doc.getElementsByTagName("place");

            if (nodes == null || nodes.getLength() <= 0)
                return null;
            citys = new ArrayList<QueryCity>();
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                QueryCity city = new QueryCity();
                NodeList childNodes = node.getChildNodes();
                for (int j = 0; j < childNodes.getLength(); j++) {
                    Node childNode = childNodes.item(j);
                    if (childNode.getNodeName().equals("woeid"))
                        city.woeid = childNode.getTextContent();
                    else if (childNode.getNodeName().equals("name"))
                        city.name = childNode.getTextContent();
                    else if (childNode.getNodeName().equals("country"))
                        city.country = childNode.getTextContent();
                    else if (childNode.getNodeName().equals("admin1"))
                        city.admin1 = childNode.getTextContent();
                    else if (childNode.getNodeName().equals("timezone"))
                        city.timezone = childNode.getTextContent();
                }
                citys.add(city);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
                conn = null;
            }
        }
        return citys;
    }

    public String getCity(double latitude, double longitude) {
        String api = "http://api.map.baidu.com/geocoder?output=xml";
        String key = "8Yf53tFyqCkAkgdKgCgvMb0Y";
        String url_str = api + "&location=" + String.valueOf(latitude) + ","
                + String.valueOf(longitude) + "&key=" + key;
        String city = null;

        HttpURLConnection conn = null;
        try {
            URL url = new URL(url_str);

            conn = connectURL(url);
            if (conn == null) {
                notifyNetworkError();
                return null;
            }

            Document doc = parser(conn);
            if (doc == null) {
                notifyNetworkError();
                return null;
            }

            doc.normalize();
            city = parseCityFromDocument(doc);
        } catch (Exception e) {
            notifyNetworkError();
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
                conn = null;
            }
        }

        return city;
    }

    private String parseCityFromDocument(Document doc) {
        String city = null;

        NodeList list = doc.getElementsByTagName("status");
        String status = list.item(0).getFirstChild().getNodeValue();

        if ("OK".equalsIgnoreCase(status)) {
            list = doc.getElementsByTagName("city");
            city = list.item(0).getFirstChild().getNodeValue();

            if (city != null) {
                city = city.substring(0, city.length() - 1);
            }
        } else {
            notifyNetworkError();
        }
        return city;
    }

    private void notifyNetworkError() {
        if (mCallback != null) {
            mCallback.onNetworkError();
        }
    }

    private void notifyCityError() {
        if (mCallback != null) {
            mCallback.onCityError();
        }
    }

    public interface WeatherNetCallback {
        void onCityError();

        void onNetworkError();
    }
}
