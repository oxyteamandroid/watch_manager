/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.remotedevice.RemoteDeviceAppListener;
import com.ingenic.iwds.remotedevice.RemoteDeviceManagerInfo;
import com.ingenic.iwds.remotedevice.RemoteDeviceStatusListener;
import com.ingenic.iwds.uniconnect.ConnectionServiceManager;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.app.AppManagerFragment;
import com.ingenic.watchmanager.app.SimpleImplListener;
import com.ingenic.watchmanager.contact.ContactFragment;
import com.ingenic.watchmanager.headset.BluetoothHeadsetManager;
import com.ingenic.watchmanager.health.HealthExerciseFragment;
import com.ingenic.watchmanager.market.MarketFragment;
import com.ingenic.watchmanager.market.RemoteDeviceUtils;
import com.ingenic.watchmanager.notification.NotificationFragment;
import com.ingenic.watchmanager.ota.OtaFragment;
import com.ingenic.watchmanager.settings.SettingsFragment;
import com.ingenic.watchmanager.theme.ThemeFragment;
import com.ingenic.watchmanager.util.ProgressUtils;
import com.ingenic.watchmanager.util.ProgressUtils.Listener;
import com.ingenic.watchmanager.util.Utils;
import com.ingenic.watchmanager.weather.WeatherPagerFragment;

public class Metro2Fragment extends WMFragment {

    private static final String FRAGMENT_TAG = "metro2";
    private static final int MSG_UNBONDED = -1;
    protected static final String TAG = Metro2Fragment.class.getSimpleName();
    private TextView mStatusView;
    private ImageView mWatchView;
    private TextView mWatchName;
    private TextView mWatchModel;
    private PopupWindow mUnbindWindow;
    private AlertDialog mUnbindConfirm;
    private ProgressDialog mUnbindDialog;
    private BluetoothHeadsetManager mBHManager;
    private ItemClickListener mItemClickListener = new ItemClickListener();

    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case MSG_UNBONDED:

                clearAllAppData();
                break;
            default:
                break;
            }
        };
    };

    private void unbindSuccess() {
        IwdsLog.d(TAG, "unbindSuccess ");

        new Thread(new Runnable() {

            @Override
            public void run() {
                WatchManagerApplication app = (WatchManagerApplication) mContext.getApplicationContext();
                app.unbond();
                if (mUnbindDialog != null && mUnbindDialog.isShowing()) {
                    mUnbindDialog.dismiss();
                }
                // if (!isClearData)
                // return;

                mHandler.post(new Runnable() {

                    @Override
                    public void run() {

                        // if (!isClearData)
                        // return;

                        // Intent intent = new Intent("ingenic.intent.action.unbind");
                        // mContext.sendBroadcast(intent);
                        // IwdsLog.d(TAG, "send broadcast ---> ingenic.intent.action.unbind");
                        startScanActivity();
                    }
                });
            }
        }).start();
    }

    private BroadcastReceiver mConnectionReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action.equals(ConnectionServiceManager.ACTION_CONNECTED_ADDRESS)) {
                DeviceDescriptor descriptor = (DeviceDescriptor) intent.getParcelableExtra("DeviceDescriptor");

                if (descriptor != null) {
                    SharedPreferences sp = getActivity().getSharedPreferences(Utils.SP_CONNECTED_DEVICE, Context.MODE_PRIVATE);
                    Editor editor = sp.edit();
                    editor.putString(Utils.KEY_DEVICE_ADDRESS, descriptor.devAddress);
                    editor.putString(Utils.KEY_MODEL, descriptor.model);
                    editor.putString(Utils.KEY_FACTURE, descriptor.manufacture);
                    editor.commit();
                }

                if (mWatchView != null) {
                    mWatchView.setImageResource(R.drawable.ic_watch_connected);
                }

                if (mStatusView != null) {
                    mStatusView.setVisibility(View.GONE);
                }

                if (mWatchName != null) {
                    mWatchName.setText(descriptor.model);
                }

                if (mWatchModel != null) {
                    mWatchModel.setText(descriptor.manufacture);
                }
            } else if (action.equals(ConnectionServiceManager.ACTION_DISCONNECTED_ADDRESS)) {
                if (BluetoothHeadsetManager.isBlueToothHeadsetConnected()) {
                    SharedPreferences sp = context.getSharedPreferences(Utils.SP_CONNECTED_DEVICE, Context.MODE_PRIVATE);
                    String deviceAddress = sp.getString(Utils.KEY_DEVICE_ADDRESS, null);
                    if (mBHManager.breakBluetoothHeadset(deviceAddress)) {
                        IwdsLog.d("Ahlin", "Disconnect Bluetooth headset disconnect");
                    }
                }

                if (mWatchView != null) {
                    mWatchView.setImageResource(R.drawable.ic_watch_disconnected);
                }

                if (mStatusView != null) {
                    mStatusView.setText(R.string.label_disconnected);
                    mStatusView.setVisibility(View.VISIBLE);
                }

                if (mWatchName != null) {
                    mWatchName.setText("");
                }

                if (mWatchModel != null) {
                    mWatchModel.setText("");
                }
            }
        }
    };
    private RemoteDeviceUtils sRemoteDeviceUtils;
    // protected ProgressUtils mProgressUtils;
    protected Context mContext;

    public void onAttach(Activity activity) {
        super.onAttach(activity);

        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectionServiceManager.ACTION_CONNECTED_ADDRESS);
        filter.addAction(ConnectionServiceManager.ACTION_DISCONNECTED_ADDRESS);
        activity.registerReceiver(mConnectionReceiver, filter);

        if (mBHManager == null) {
            mBHManager = BluetoothHeadsetManager.getDefault();
        }

        if (!mBHManager.isInitialize()) {
            mBHManager.initialize(activity);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = getActivity();
        // mProgressUtils = new ProgressUtils(getActivity(), (Listener) null);
        View rootView = inflater.inflate(R.layout.fragment_metro2, container, false);

        rootView.findViewById(R.id.watch_theme).setOnClickListener(mItemClickListener);
        rootView.findViewById(R.id.contact).setOnClickListener(mItemClickListener);
        rootView.findViewById(R.id.weather).setOnClickListener(mItemClickListener);
        rootView.findViewById(R.id.health).setOnClickListener(mItemClickListener);
        rootView.findViewById(R.id.market).setOnClickListener(mItemClickListener);
        rootView.findViewById(R.id.notification).setOnClickListener(mItemClickListener);
        rootView.findViewById(R.id.ota).setOnClickListener(mItemClickListener);
        rootView.findViewById(R.id.settings).setOnClickListener(mItemClickListener);
        rootView.findViewById(R.id.app_manager).setOnClickListener(mItemClickListener);

        mWatchName = (TextView) rootView.findViewById(R.id.watch_name);
        mWatchModel = (TextView) rootView.findViewById(R.id.watch_model);
        mWatchView = (ImageView) rootView.findViewById(R.id.icon_watch);
        mWatchView.setOnClickListener(mItemClickListener);
        mStatusView = (TextView) rootView.findViewById(R.id.watch_status);

        if (isConnected()) {
            mWatchView.setImageResource(R.drawable.ic_watch_connected);
            mStatusView.setVisibility(View.GONE);

            SharedPreferences sp = getActivity().getSharedPreferences(Utils.SP_CONNECTED_DEVICE, Context.MODE_PRIVATE);
            mWatchName.setText(sp.getString(Utils.KEY_MODEL, ""));
            mWatchModel.setText(sp.getString(Utils.KEY_FACTURE, ""));
        } else {
            mWatchView.setImageResource(R.drawable.ic_watch_disconnected);
            mStatusView.setVisibility(View.VISIBLE);
            mStatusView.setText(R.string.label_connecting);
        }

        return rootView;
    }

    @Override
    public void onDetach() {
        getActivity().unregisterReceiver(mConnectionReceiver);

        if (mBHManager != null) {
            mBHManager.cancellationBluetoothHeadset();
            mBHManager = null;
        }

        super.onDetach();
    }

    private class ItemClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            String fragment = null;
            switch (v.getId()) {
            case R.id.watch_theme:
                fragment = ThemeFragment.class.getName();
                break;
            case R.id.contact:
                if (!isConnected()) {
                    Toast.makeText(getActivity(), R.string.conectstate_tip, Toast.LENGTH_SHORT).show();
                    return;
                }

                fragment = ContactFragment.class.getName();
                break;
            case R.id.weather:
                fragment = WeatherPagerFragment.class.getName();
                break;
            case R.id.health:
                fragment = HealthExerciseFragment.class.getName();
                break;
            case R.id.market:
                fragment = MarketFragment.class.getName();
                // Toast.makeText(getActivity(), R.string.developing,
                // Toast.LENGTH_SHORT).show();
                break;
            case R.id.app_manager:
                fragment = AppManagerFragment.class.getName();
                break;
            case R.id.notification:
                fragment = NotificationFragment.class.getName();
                break;
            case R.id.ota:
                if (!isConnected()) {
                    Toast.makeText(getActivity(), R.string.conectstate_tip, Toast.LENGTH_SHORT).show();
                    return;
                }

                fragment = OtaFragment.class.getName();
                break;
            case R.id.settings:
                fragment = SettingsFragment.class.getName();
                break;
            case R.id.icon_watch:
                showUnbindWindow();
                return;
            default:
                break;
            }

            if (fragment != null) {
                startFragment(fragment);
            }
        }
    }

    private void showUnbindWindow() {
        dismissUnbindWindow();

        if (mUnbindWindow == null) {
            mUnbindWindow = new PopupWindow(getActivity());

            TextView view = new TextView(getActivity());
            view.setBackgroundResource(R.drawable.bg_button);
            view.setTextAppearance(getActivity(), android.R.style.TextAppearance_Large);
            view.setPadding(0, Utils.dp2px(getActivity(), 10), 0, Utils.dp2px(getActivity(), 10));
            view.setText(R.string.unbond);

            view.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    showUnbindConfirm();
                    dismissUnbindWindow();
                }
            });

            mUnbindWindow.setContentView(view);
            mUnbindWindow.setWindowLayoutMode(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            mUnbindWindow.setOutsideTouchable(true);
            mUnbindWindow.setFocusable(true);
        }

        mUnbindWindow.showAsDropDown(mWatchView);
    }

    private void dismissUnbindWindow() {
        if (mUnbindWindow != null && mUnbindWindow.isShowing()) {
            mUnbindWindow.dismiss();
        }
    }

    private void showUnbindConfirm() {
        if (mUnbindConfirm == null) {
            Activity context = getActivity();
            if (null == context) {
                IwdsLog.d(this, "mUnbindConfirm init fail! context is null!");
                return;
            }
            mUnbindConfirm = new AlertDialog.Builder(context).setTitle(R.string.title_unbonded).setMessage(R.string.msg_unbonded).setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    unbind();
                }
            }).setNegativeButton(android.R.string.no, null).create();
        }

        if (!mUnbindConfirm.isShowing()) {
            mUnbindConfirm.show();
        }
    }

    private void unbind() {
        if (mUnbindDialog == null) {
            String title = getString(R.string.title_connection);
            String message = getString(R.string.text_disconnecting);
            mUnbindDialog = ProgressDialog.show(getActivity(), title, message, false, false);
        } else if (!mUnbindDialog.isShowing()) {
            mUnbindDialog.show();
        }
        mHandler.sendEmptyMessage(MSG_UNBONDED);

    }

    private boolean isClearData;

    public void clearAllAppData() {
        IwdsLog.d(TAG, "clearAllAppData ");
        unbindSuccess();
        return;

        // sRemoteDeviceUtils = RemoteDeviceUtils.getInstance(getActivity());
        // sRemoteDeviceUtils.register(this, getStatusListener(), null, getAppListener());
        // isClearData = true;
        // if (!sRemoteDeviceUtils.requestClearAllAppData()) {
        // IwdsLog.d(TAG, "clearAllAppData ---> unbindSuccess " + true);
        // unbindSuccess();
        // } else {
        // // IwdsLog.d(TAG, "clearAllAppData ---> unbindSuccess " + false);
        // // unbindSuccess();
        // }
    }

    private RemoteDeviceStatusListener getStatusListener() {
        return new RemoteDeviceStatusListener() {

            @Override
            public void onRemoteDeviceReady(boolean isReady) {
                IwdsLog.d(TAG, "isReady: " + isReady);
                if (!isReady && isClearData) {
                    startScanActivity();
                }
            }

        };
    }

    private void startScanActivity() {
        if (mUnbindDialog != null && mUnbindDialog.isShowing()) {
            mUnbindDialog.dismiss();
        }

        // sRemoteDeviceUtils.unregister(this);
        // mProgressUtils.end();
        Intent it = new Intent(mContext, WatchManagerActivity.class);
        startActivity(it);
        isClearData = false;
    }

    private RemoteDeviceAppListener getAppListener() {
        return new SimpleImplListener.SimpleAppCallback() {

            @Override
            public void onResponseClearAllAppDataAndCache(int totalClearCount, int index, String packageName, int typeOfIndex, int returnCode) {

                // mProgressUtils.start();
                String lable = null;// RemoteDeviceUtils.mPkname2Label.get(packageName);
                lable = null == lable ? packageName : lable;
                IwdsLog.d(TAG, "totalClearCount,  " + totalClearCount + " index, " + index + " typeOfIndex, " + typeOfIndex + " returnCode" + returnCode);

                // boolean result = RemoteDeviceManagerInfo.REQUEST_SUCCEEDED == returnCode;
                // IwdsLog.d(TAG, "typeOfIndex : " + typeOfIndex + "清除所有数据" + (!result ? "失败" : "成功") + ": " + lable);

                if (totalClearCount == index) {
                    // Toast.makeText(mContext, mContext.getResources().getString(R.string.clear_finish), Toast.LENGTH_SHORT).show();
                    IwdsLog.d(TAG, "onResponseClearAllAppDataAndCache complete  ---> unbindSuccess! ");
                    unbindSuccess();
                }
            }
        };
    }

    private boolean isConnected() {
        WatchManagerApplication app = (WatchManagerApplication) getActivity().getApplication();
        return app.isConnected();
    }

    private void startFragment(String fragment) {
        Intent it = new Intent(getActivity(), FragmentActivity.class);
        it.putExtra(FragmentActivity.EXTRA_FRAGMENT, fragment);
        startActivity(it);
    }

    @Override
    public String getFragmentTag() {
        return FRAGMENT_TAG;
    }
}
