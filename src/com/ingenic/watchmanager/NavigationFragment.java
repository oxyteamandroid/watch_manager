/*
 * Copyright (C) 2014 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingenic.watchmanager.view.LineIterator;

public class NavigationFragment extends WMFragment {
    private static final String FRAGMENT_TAG = "mavigation";
    private static final int[] NAVIGATION_BACKGROUNDS = new int[] { R.drawable.nav_1,
            R.drawable.nav_2, R.drawable.nav_3, R.drawable.nav_4 };
    private LineIterator mIterator;
    private OnNavigationEndListener mEndListener;

    public NavigationFragment() {}

    public NavigationFragment(OnNavigationEndListener listener) {
        mEndListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_navigation, container, false);

        ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.loginViewGroup1);
        LoginAdapter adapter = new LoginAdapter(getActivity());
        viewPager.setAdapter(adapter);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                mIterator.setCurrent(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {}

            @Override
            public void onPageScrollStateChanged(int arg0) {}
        });

        viewPager.setCurrentItem(0);

        mIterator = (LineIterator) rootView.findViewById(R.id.iterator);
        mIterator.setTotalCount(adapter.getCount());
        mIterator.setCurrent(0);

        return rootView;
    }

    @Override
    public String getFragmentTag() {
        return FRAGMENT_TAG;
    }

    private class LoginAdapter extends PagerAdapter {

        private Context mContext;

        public LoginAdapter(Context context) {
            mContext = context;
        }

        @Override
        public int getCount() {
            return NAVIGATION_BACKGROUNDS.length;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == (arg1);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            int count = getCount();

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.layout_navigation_item, container, false);
            view.setBackgroundResource(NAVIGATION_BACKGROUNDS[position]);

            View start = view.findViewById(R.id.start);
            if (position == count - 1) {
                start.setVisibility(View.VISIBLE);
                start.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (mEndListener != null) {
                            mEndListener.onNavigationEnd();
                        }
                    }
                });
            }

            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = container.getChildAt(position);
            if (isViewFromObject(view, object)) {
                container.removeViewAt(position);
            }
        }
    }

    interface OnNavigationEndListener {
        void onNavigationEnd();
    }
}
