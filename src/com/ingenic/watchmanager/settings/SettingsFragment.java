/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.settings;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingenic.watchmanager.FragmentActivity;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.util.MusicUtils;

public class SettingsFragment extends PreferenceFragment implements
        OnPreferenceChangeListener {
    /**
     * 默认播放器
     */
    public static final String  GOOGLE_MUSIC= "com.google.android.music";
    public static final String[] MUSIC_PKG_LIST = { "com.sds.android.ttpod",
            "com.google.android.music", "com.miui.player", "com.htc.music",
            "com.rdio.android", "com.sonyericsson.music", "com.andrew.apollo",
            "com.real.IMP", "com.samsung.sec.android", "com.amazon.mp3",
            "com.oppo.music",
    // "com.duomi.android",
    // "com.tencent.qqmusic"
    };
    private static final String KEY_REMOTE_MUSIC = "rc_music_key_values";
    private static final String KEY_PERSONAL_INFORMATION = "personal_information";

    ArrayList<String> mEntryValuesPackageName = new ArrayList<String>();
    ArrayList<String> mEntryLabel = new ArrayList<String>();
    ArrayList<String> mCanControlMusic = new ArrayList<String>();

    private ListPreference mRcMusicControl;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container,
                false);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        initMusicAppList();
        initListPreference();
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
            Preference preference) {
        if (KEY_PERSONAL_INFORMATION.equalsIgnoreCase(preference.getKey())) {
            Intent it = new Intent(getActivity(), FragmentActivity.class);
            it.putExtra("fragment", preference.getFragment());
            it.putExtra("tag", preference.getKey());
            startActivity(it);
        }
        return true;
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference == mRcMusicControl) {

            int i = mRcMusicControl.findIndexOfValue((String) newValue);
            mRcMusicControl.setSummary(mRcMusicControl.getEntries()[i]);

            SharedPreferences sharedPreferences = getActivity()
                    .getSharedPreferences(MusicUtils.REMOTE_MUSIC_PROPTIES,
                            Context.MODE_PRIVATE);

            Editor editor = sharedPreferences.edit();

            editor.putString(MusicUtils.PACKAGE_NAME, newValue.toString());
            editor.putString(MusicUtils.APP_NAME,
                    mRcMusicControl.getEntries()[i].toString());

            editor.commit();
            mRcMusicControl.setValue((String) newValue);
        }
        return false;
    }

    private void initListPreference() {
        mRcMusicControl = (ListPreference) findPreference(KEY_REMOTE_MUSIC);
        mRcMusicControl.setOnPreferenceChangeListener(this);

        if (mEntryValuesPackageName.size() == 0 || mEntryLabel.size() == 0) {
            // getPreferenceScreen().removePreference(rc_music_control);
            mRcMusicControl.setEnabled(false);
            mRcMusicControl.setSummary(R.string.music_hint);
            return;
        }

        mRcMusicControl.setEnabled(true);
        mRcMusicControl.setEntries((String[]) mEntryLabel
                .toArray(new String[0]));
        mRcMusicControl.setEntryValues((String[]) mEntryValuesPackageName
                .toArray(new String[0]));

        SharedPreferences sharedPreferences = getActivity()
                .getSharedPreferences(MusicUtils.REMOTE_MUSIC_PROPTIES,
                        Context.MODE_PRIVATE);
        String packageName = sharedPreferences.getString(MusicUtils.PACKAGE_NAME,
                MusicUtils.NO_PLAYER);

        if (mEntryValuesPackageName.contains(packageName)) {
            mRcMusicControl.setSummary(mEntryLabel.get(mEntryValuesPackageName
                    .indexOf(packageName)));
            mRcMusicControl.setValue(packageName);
        } else if (mEntryValuesPackageName.contains(GOOGLE_MUSIC)) {

            Editor editor = sharedPreferences.edit();
            editor.putString(MusicUtils.PACKAGE_NAME, GOOGLE_MUSIC);
            editor.putString(MusicUtils.APP_NAME, mEntryLabel.get(mEntryValuesPackageName
                    .indexOf(GOOGLE_MUSIC)));
            editor.commit();

            mRcMusicControl.setSummary(mEntryLabel.get(mEntryValuesPackageName
                    .indexOf(GOOGLE_MUSIC)));
            mRcMusicControl.setValue(GOOGLE_MUSIC);

        } else {

            Editor editor = sharedPreferences.edit();
            editor.putString(MusicUtils.PACKAGE_NAME, mEntryValuesPackageName.get(0));
            editor.putString(MusicUtils.APP_NAME, mEntryLabel.get(0));
            editor.commit();

            mRcMusicControl.setSummary(mEntryLabel.get(0));
            mRcMusicControl.setValue(mEntryValuesPackageName.get(0));
        }
    }

    /**
     * 初始化支持的音乐播放器
     */
    private void initMusicAppList() {
        mCanControlMusic.clear();
        for (String musicpkg : MUSIC_PKG_LIST) {
            mCanControlMusic.add(musicpkg);
        }
        PackageManager pm = getActivity().getPackageManager();
        List<ResolveInfo> rs = pm.queryBroadcastReceivers(new Intent(
                MusicUtils.MEDIA_BUTTON), 96);
        if (rs != null && rs.size() > 0) {
            mEntryValuesPackageName.clear();
            mEntryLabel.clear();
            for (int i = 0; i < rs.size(); i++) {
                if (mCanControlMusic
                        .contains(rs.get(i).activityInfo.applicationInfo.packageName)) {
                    mEntryValuesPackageName
                            .add(rs.get(i).activityInfo.applicationInfo.packageName);
                    mEntryLabel.add(rs.get(i).loadLabel(pm).toString());
                }
            }
        } else {
            mEntryValuesPackageName.clear();
            mEntryLabel.clear();
        }
    }
}
