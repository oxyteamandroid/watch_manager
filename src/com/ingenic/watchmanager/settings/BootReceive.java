package com.ingenic.watchmanager.settings;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.util.MusicUtils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class BootReceive extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String packagen_ame = intent.getData().getSchemeSpecificPart();
        // 接收卸载广播
        if (intent.getAction().equals(Intent.ACTION_PACKAGE_REMOVED)) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(
                    MusicUtils.REMOTE_MUSIC_PROPTIES, Context.MODE_PRIVATE);
            String packageName = sharedPreferences.getString(MusicUtils.PACKAGE_NAME,
                    MusicUtils.NO_PLAYER);

            IwdsLog.i(this, "Application package name：" + packagen_ame);
            if (packageName.equals(packagen_ame)) {
                IwdsLog.i(this, "Set up a remote music player");
                MusicUtils.setDefaultMusicPlayer(context);
            }
        } else if (intent.getAction().equals(Intent.ACTION_PACKAGE_ADDED)) {
            IwdsLog.i(this, "Install application package name：" + packagen_ame);
            if (MusicUtils.mCanControlMusic != null
                    && MusicUtils.mCanControlMusic.contains(intent
                            .getDataString())) {
                MusicUtils.setDefaultMusicPlayer(context);
            }
        }
    }
}
