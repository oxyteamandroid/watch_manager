/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/WatchManager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.music;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.view.KeyEvent;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.MusicControlInfo;
import com.ingenic.iwds.datatransactor.elf.MusicControlTransactionModel;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.util.UUIDS;

/**
 *
 * @author Shi Guanghua
 *
 */
public class RemoteMusicService extends Service {

    private static MusicControlTransactionModel mMusicControlTransactionModel;

    /**
     * 手机与手表是否是通路，及是否可以正常通讯
     */
    private boolean mChannelAvaible = false;

    private static final int VOLUME_WHAT = -100;
    public static final String MUSIC_COMMAND_ACTION = "musicbroadcast.command";
    public static final String TRANSPORTSERVICE_ACTION = "cn.ingenic.indroidsync.musicsync.transportservice";
    public static final String MEDIA_VOLUME_CHANGE_ACTION = "android.media.VOLUME_CHANGED_ACTION";
    public static final String RESTART_MUSIC_SERVICE_ACTION = "com.restart.music.service.action";
    public static final String CMD = "cmd";
    private static final int START = -1;
    private static final int PAUSE = -2;
    private static final int NEXT = -3;
    private static final int PREV = -4;
    private static final int VOLUME_CHANGE = -5;
    public static final int VOLUMEUP = -13;
    public static final int VOLUMEDOWN = -14;

    /**
     * 默认来自手表端
     */
    private boolean mFromMobile = true;
    private Context mContext;
    private AudioManager mAudioManager = null;

    /**
     * 声音的最大值
     */
    private int maxVolume = 15;

    /**
     * 当前的音量值
     */
    private int currentVolume = 5;

    /**
     * 正在播放音乐的的播放器名
     */
    private String musicActiveAppName = null;
    private VolumeChangeReceiver mVolumeChangeReceiver;

    @Override
    public IBinder onBind(Intent intent) {
        IwdsLog.d(this, "RemoteMusicService onBind");
        return null;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate() {
        IwdsLog.d(this, "RemoteMusicService onCreate");
        super.onCreate();

        CharSequence label = getApplication().getApplicationInfo().loadLabel(
                getPackageManager());
        Notification notification = new Notification(getApplication()
                .getApplicationInfo().icon, label, System.currentTimeMillis());
        Intent it = new Intent(
                ServiceManagerContext.ACTION_NOTIFICATION_CLICKED);
        it.setFlags(it.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, it, 0);
        notification.setLatestEventInfo(this, label, "", pendingIntent);
        notification.flags = notification.flags
                | Notification.FLAG_ONGOING_EVENT;
        startForeground(9999, notification);
        mMusicControlTransactionModel = new MusicControlTransactionModel(this,
                mRemoteMusicTransactionCallBack, UUIDS.REMOTEMUSIC);
        mContext = RemoteMusicService.this;
        mAudioManager = (AudioManager) this
                .getSystemService(Context.AUDIO_SERVICE);
        maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        mVolumeChangeReceiver = new VolumeChangeReceiver();

        IntentFilter filter = new IntentFilter();
        filter.addAction(MEDIA_VOLUME_CHANGE_ACTION);
        this.registerReceiver(mVolumeChangeReceiver, filter);

    }

    public class VolumeChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String actionString = intent.getAction();
            if (MEDIA_VOLUME_CHANGE_ACTION.equals(actionString) && mFromMobile) {
                setWatchVolumeSeekBar();
            }
            mFromMobile = true;
        }
    }

    private Handler mVolumeHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (msg.what == VOLUME_WHAT) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        msg.arg1, 1);
            }

        }
    };

    /**
     * 远程音乐回调
     */
    private MusicControlTransactionModel.MusicControlTransactionModelCallback mRemoteMusicTransactionCallBack = new MusicControlTransactionModel.MusicControlTransactionModelCallback() {
        @Override
        public void onSendResult(DataTransactResult result) {
            int resultCode = result.getResultCode();
            switch (resultCode) {
            case DataTransactResult.RESULT_OK:
                // 获取手表端发送的命令，告诉手表端正在处理数据
                break;

            case DataTransactResult.RESULT_FAILED_IWDS_CRASH:
            case DataTransactResult.RESULT_FAILED_LINK_DISCONNECTED:
                break;
            case DataTransactResult.RESULT_FAILED_CHANNEL_UNAVAILABLE:
                // 与手机端链接出现问题，远程音乐不可点击
                break;
            default:
                break;
            }
        }

        @Override
        public void onRequestFailed() {
            IwdsLog.d(this, "music - request failed");
            // 手表端发送消息失败
        }

        @Override
        public void onRequest() {

        }

        @Override
        public void onObjectArrived(MusicControlInfo musicInfo) {
            // 手表端接受到手机端发过来的数据
            if (musicInfo != null) {
                if (!mChannelAvaible || musicInfo == null
                        || musicInfo.cmd == -99) {
                    return;
                }
                DealWithMusicCommand(musicInfo);
            }

        }

        @Override
        public void onLinkConnected(DeviceDescriptor descriptor,
                boolean isConnected) {
            IwdsLog.d(this, "Remote music - Device : " + descriptor
                    + " link connect is : " + isConnected);
        }

        @Override
        public void onChannelAvailable(boolean isAvailable) {
            IwdsLog.d(this, "Remote music - channel available is : "
                    + isAvailable);
            // 手机端和手表端的链接通路正常
            if (isAvailable) {
                // 手机端和手表端链路正常，可以操作手机端的远程音乐
                mChannelAvaible = true;
                setWatchVolumeSeekBar();
            } else {
                mChannelAvaible = false;
            }
        }
    };

    private void setWatchVolumeSeekBar() {

        MusicControlInfo musicControlInfo = new MusicControlInfo();

        if (mAudioManager == null) {
            mAudioManager = (AudioManager) mContext
                    .getSystemService(Context.AUDIO_SERVICE);
            maxVolume = mAudioManager
                    .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        }
        musicControlInfo.volumeMax = maxVolume;
        currentVolume = mAudioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC);
        musicControlInfo.volumeCurrent = currentVolume;
        musicControlInfo.cmd = VOLUME_CHANGE;

        mMusicControlTransactionModel.send(musicControlInfo);
    }

    private void DealWithMusicCommand(MusicControlInfo datas) {

        if (datas != null && datas.cmd == VOLUME_CHANGE) {

            mFromMobile = false; // 来自手表

            if (mAudioManager == null) {
                mAudioManager = (AudioManager) this
                        .getSystemService(Context.AUDIO_SERVICE);
            }

            Message msg = new Message();
            msg.arg1 = datas.volumeCurrent;
            msg.what = VOLUME_WHAT;
            mVolumeHandler.removeMessages(VOLUME_WHAT);
            mVolumeHandler.sendMessage(msg);

            return;
        }

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                "remote_music_propties", Context.MODE_PRIVATE);

        String appName = sharedPreferences.getString("packagename",
                "com.google.android.music");

        int cmd = (Integer) datas.cmd;
        IwdsLog.d(this, "44444retrive data cmd:" + cmd);
        Intent keyIntent = new Intent(Intent.ACTION_MEDIA_BUTTON, null);
        KeyEvent keyEvent;
        Intent localIntent3 = mContext.getPackageManager()
                .getLaunchIntentForPackage(appName);
        if (localIntent3 != null && !this.mAudioManager.isMusicActive()) {
            mContext.startActivity(localIntent3);
        }

        switch (cmd) {
        case START:
            // forwardKeyToReceiver(KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE);
            // broadcastMediaKeyIntents(mContext,
            // KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE);
            keyEvent = new KeyEvent(KeyEvent.ACTION_DOWN,
                    KeyEvent.KEYCODE_MEDIA_PLAY);
            keyIntent.putExtra(Intent.EXTRA_KEY_EVENT, keyEvent);
            keyIntent.setPackage(appName);
            musicActiveAppName = appName;
            mContext.sendBroadcast(keyIntent, null);

            keyEvent = new KeyEvent(KeyEvent.ACTION_UP,
                    KeyEvent.KEYCODE_MEDIA_PLAY);
            keyIntent.putExtra(Intent.EXTRA_KEY_EVENT, keyEvent);
            // mContext.sendOrderedBroadcast(keyIntent, null);
            keyIntent.setPackage(appName);
            mContext.sendBroadcast(keyIntent, null);
            break;
        case PAUSE:
            // forwardKeyToReceiver(KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE);
            // broadcastMediaKeyIntents(mContext,
            // KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE);
            keyEvent = new KeyEvent(KeyEvent.ACTION_DOWN,
                    KeyEvent.KEYCODE_MEDIA_PAUSE);
            keyIntent.putExtra(Intent.EXTRA_KEY_EVENT, keyEvent);
            if (musicActiveAppName == null)
                musicActiveAppName = appName;
            keyIntent.setPackage(musicActiveAppName);
            mContext.sendBroadcast(keyIntent, null);

            keyEvent = new KeyEvent(KeyEvent.ACTION_UP,
                    KeyEvent.KEYCODE_MEDIA_PAUSE);
            keyIntent.putExtra(Intent.EXTRA_KEY_EVENT, keyEvent);
            keyIntent.setPackage(musicActiveAppName);
            // mContext.sendOrderedBroadcast(keyIntent, null);
            mContext.sendBroadcast(keyIntent, null);
            if(!musicActiveAppName.equals(appName)){
                keyEvent = new KeyEvent(KeyEvent.ACTION_DOWN,
                        KeyEvent.KEYCODE_MEDIA_PAUSE);
                keyIntent.putExtra(Intent.EXTRA_KEY_EVENT, keyEvent);
                keyIntent.setPackage(appName);
                mContext.sendBroadcast(keyIntent, null);

                keyEvent = new KeyEvent(KeyEvent.ACTION_UP,
                        KeyEvent.KEYCODE_MEDIA_PAUSE);
                keyIntent.putExtra(Intent.EXTRA_KEY_EVENT, keyEvent);
                keyIntent.setPackage(appName);
                // mContext.sendOrderedBroadcast(keyIntent, null);
                mContext.sendBroadcast(keyIntent, null);
            }
            musicActiveAppName = null;
            break;
        case NEXT:
            // forwardKeyToReceiver(KeyEvent.KEYCODE_MEDIA_NEXT);
            broadcastMediaKeyIntents(mContext, KeyEvent.KEYCODE_MEDIA_NEXT,
                    appName);
            // keyEvent = new KeyEvent(KeyEvent.ACTION_DOWN,
            // KeyEvent.KEYCODE_MEDIA_NEXT);
            // keyIntent.putExtra(Intent.EXTRA_KEY_EVENT, keyEvent);
            // mContext.sendOrderedBroadcast(keyIntent, null);
            //
            // keyEvent = new KeyEvent(KeyEvent.ACTION_UP,
            // KeyEvent.KEYCODE_MEDIA_NEXT);
            // keyIntent.putExtra(Intent.EXTRA_KEY_EVENT, keyEvent);
            // mContext.sendOrderedBroadcast(keyIntent, null);
            break;
        case PREV:
            // forwardKeyToReceiver(KeyEvent.KEYCODE_MEDIA_PREVIOUS);
            broadcastMediaKeyIntents(mContext, KeyEvent.KEYCODE_MEDIA_PREVIOUS,
                    appName);
            // keyEvent = new KeyEvent(KeyEvent.ACTION_DOWN,
            // KeyEvent.KEYCODE_MEDIA_PREVIOUS);
            // keyIntent.putExtra(Intent.EXTRA_KEY_EVENT, keyEvent);
            // mContext.sendOrderedBroadcast(keyIntent, null);
            //
            // keyEvent = new KeyEvent(KeyEvent.ACTION_UP,
            // KeyEvent.KEYCODE_MEDIA_PREVIOUS);
            // keyIntent.putExtra(Intent.EXTRA_KEY_EVENT, keyEvent);
            // mContext.sendOrderedBroadcast(keyIntent, null);
            break;

        case VOLUMEDOWN:
            mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                    AudioManager.ADJUST_LOWER, 9);

            break;

        case VOLUMEUP:

            mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                    AudioManager.ADJUST_RAISE, 9);
            break;

        default:
            // mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
            // Math.abs(cmd), 0);

            break;
        }
    }

    public static void broadcastMediaKeyIntents(Context paramContext,
            int paramInt, String appName) {
        long l = SystemClock.uptimeMillis();
        Intent localIntent1 = new Intent("android.intent.action.MEDIA_BUTTON",
                null);
        localIntent1.putExtra("android.intent.extra.KEY_EVENT", new KeyEvent(l,
                l, KeyEvent.ACTION_DOWN, paramInt, 0));
        // paramContext.sendOrderedBroadcast(localIntent1, null);
        localIntent1.setPackage(appName);
        paramContext.sendBroadcast(localIntent1, null);

        Intent localIntent2 = new Intent("android.intent.action.MEDIA_BUTTON",
                null);
        localIntent2.putExtra("android.intent.extra.KEY_EVENT", new KeyEvent(l,
                l, KeyEvent.ACTION_UP, paramInt, 0));
        // paramContext.sendOrderedBroadcast(localIntent2, null);
        localIntent2.setPackage(appName);
        paramContext.sendBroadcast(localIntent2, null);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onStart(Intent intent, int startId) {
        IwdsLog.d(this, "RemoteMusicService onStart");
        super.onStart(intent, startId);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        IwdsLog.d(this, "RemoteMusicService onStartCommand");

        // 启动服务
        if (mMusicControlTransactionModel != null && !mChannelAvaible) {
            IwdsLog.d(this,"===test===model music===");
            mMusicControlTransactionModel.start();
        }

        if (intent != null) {
            Bundle mBundle = intent.getExtras();

            if (mBundle != null) {
                MusicControlInfo musicControlInfo = (MusicControlInfo) mBundle
                        .get(MusicBroadCast.SET_MUSIC_INFO);

                mMusicControlTransactionModel.send(musicControlInfo);
            }
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        // 停止服务
        if (mMusicControlTransactionModel != null) {
            mMusicControlTransactionModel.stop();
        }
        this.unregisterReceiver(mVolumeChangeReceiver);
        // 重启远程音乐服务
        sendBroadcast(new Intent(RESTART_MUSIC_SERVICE_ACTION));
        super.onDestroy();

    }

}