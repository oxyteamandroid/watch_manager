/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/WatchManager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.music;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.ingenic.iwds.datatransactor.elf.MusicControlInfo;

/**
 * 接受手机端音乐播放广播，然后把播放的状态同步到手表端
 * 
 * @author Shi GuangHua
 * 
 */
public class MusicBroadCast extends BroadcastReceiver {

    public static final String MUSIC_COMMAND_ACTION = "musicbroadcast.command";

    private MusicControlInfo mMusicControlInfo = null;

    public final static String SET_MUSIC_INFO = "com.ingenic.music.info";

    public String MUSIC_PLAY_STATE_CHANGE = "com.android.music.playstatechanged";
    public String MUSIC_HTC_META_CHANGE = "com.htc.music.metachanged";
    public String MUSIC_HTC_STATE_CHANGE = "com.htc.music.playstatechanged";

    private Intent mMusicIntet;

    @Override
    public void onReceive(Context context, Intent intent) {

        if (RemoteMusicService.RESTART_MUSIC_SERVICE_ACTION.equals(intent
                .getAction())) {
            context.startService(new Intent(context, RemoteMusicService.class));
            return;
        }

        mMusicIntet = new Intent(context, RemoteMusicService.class);
        mMusicControlInfo = new MusicControlInfo();
        if (intent != null) {

            String actionString = intent.getAction();

            mMusicControlInfo.cmd = intent.getBooleanExtra("playing", false) == false ? -2
                    : -1;

            if (actionString.equals(MUSIC_PLAY_STATE_CHANGE)) {
                mMusicControlInfo.cmd = intent.getBooleanExtra("playstate",
                        false) == false ? -2 : -1;
            }

            // htc mobile
            if (actionString.equals(MUSIC_HTC_STATE_CHANGE)
                    || actionString.equals(MUSIC_HTC_META_CHANGE)) {

                mMusicControlInfo.cmd = intent.getBooleanExtra("isplaying",
                        false) == false ? -2 : -1;
            }

            mMusicControlInfo.musicAlbum = intent.getStringExtra("album");
            mMusicControlInfo.musicArtist = intent.getStringExtra("artist");
            mMusicControlInfo.songName = intent.getStringExtra("track");

            // 把消息同步到手表端

            Bundle mBundle = new Bundle();
            mBundle.putParcelable(SET_MUSIC_INFO, mMusicControlInfo);
            mMusicIntet.putExtras(mBundle);

            context.startService(mMusicIntet);
        }

    }

}
