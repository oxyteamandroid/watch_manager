package com.ingenic.watchmanager.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.TextView;

import com.ingenic.watchmanager.R;

public class ThemeTextView extends TextView {

    private int mTextColor;
    private Style mStyle = Style.THEME;

    private void init(Context context, AttributeSet attrs) {

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ThemeTextView);
        int styleValue = a.getInt(R.styleable.ThemeTextView_style, 0);
        a.recycle();

        mStyle = Style.valueOf(styleValue);
    }

    public ThemeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public ThemeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ThemeTextView(Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int color = getColor();
        setTextColor(color);
        super.onDraw(canvas);
    }

    @Override
    public void setTextColor(int color) {
        if (mTextColor == color) return;

        super.setTextColor(color);
        mTextColor = color;
    }

    private int getColor() {
        switch (mStyle) {
        case BACK:
            return getResources().getColor(android.R.color.holo_blue_bright);
        case OPPBACK:
            return getResources().getColor(android.R.color.holo_blue_bright) ^ 0x00FFFFFF;
        case THEME:
        default:
            return Color.WHITE;
        }
    }

    public void setStyle(Style style) {
        this.mStyle = style;
        invalidate();
    }

    public enum Style {
        THEME(0), BACK(1), OPPBACK(2);

        private int value;

        private Style(int value) {
            this.value = value;
        }

        public int value() {
            return value;
        }

        public static Style valueOf(int value) {
            for (Style style : Style.values()) {
                if (style.value == value) return style;
            }
            return THEME;
        }
    }
}
