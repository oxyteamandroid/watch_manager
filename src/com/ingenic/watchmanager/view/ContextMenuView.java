/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ContextMenuView extends LinearLayout {
    private OnMenuItemSelectedListener mListener;
    private OnClickListener btnClick = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemSelected(v.getId());
            }
        }
    };

    public ContextMenuView(Context context) {
        this(context, null);
    }

    public ContextMenuView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ContextMenuView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setOrientation(VERTICAL);
    }

    public void setListener(OnMenuItemSelectedListener l) {
        this.mListener = l;
    }

    @SuppressWarnings("unused")
    private void add(CharSequence title) {
        add(0, title);
    }

    @SuppressWarnings("unused")
    private void add(int titleRes) {
        add(0, titleRes);
    }

    public void add(int id, CharSequence title) {
        if (title == null) {
            return;
        }
        TextView view = new TextView(getContext());
        view.setId(id);
        view.setText(title);
        view.setOnClickListener(btnClick);
        view.setPadding(5, 5, 5, 5);
        view.setTextSize(24);
        addView(view);
    }

    public void add(int id, int titleRes) {
        String title = null;
        if (titleRes != 0) {
            title = getResources().getString(titleRes);
        }
        add(id, title);
    }

    public interface OnMenuItemSelectedListener {
        void onItemSelected(int id);
    }

    public void setItemEnabled(int item, boolean enabled) {
        View child = getChildAt(item);
        if (child != null) {
            child.setEnabled(false);
        }
    }
}
