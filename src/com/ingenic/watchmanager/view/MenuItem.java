/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.view;

import android.graphics.drawable.Drawable;
import android.view.View;

public interface MenuItem {
    MenuItem setTitle(CharSequence title);

    MenuItem setTitle(int titleRes);

    CharSequence getTitle();

    MenuItem setActionView(View view);

    View getActionView();

    MenuItem setIcon(Drawable icon);

    MenuItem setIcon(int iconRes);

    Drawable getIcon();

    MenuItem setItemId(int itemId);

    int getItemId();

    MenuItem setOrder(int order);

    int getOrder();
}
