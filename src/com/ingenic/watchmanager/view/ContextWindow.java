/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.view;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.widget.PopupWindow;

import com.ingenic.watchmanager.R;

public class ContextWindow extends PopupWindow {
    private ContextMenuView mView;

    public ContextWindow(ContextMenuView view) {
        super(view);

        mView = view;
        setContentView(mView);
        setWindowLayoutMode(-1, -2);
        setOutsideTouchable(true);
        setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        setAnimationStyle(R.style.anim_pop_win);
    }

    public void setOnMenuItemSelectedListener(
            ContextMenuView.OnMenuItemSelectedListener l) {
        mView.setListener(l);
    }
}
