/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.view;

import android.annotation.TargetApi;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;

public abstract class ViewCompat {

    protected int mSdk;
    protected View mView;

    protected ViewCompat(View view, int sdk) {
        mView = view;
        mSdk = sdk;
    }

    public static ViewCompat getInstance(View view, int sdk) {
        if (sdk >= Build.VERSION_CODES.JELLY_BEAN) {
            return new SDK16ViewCompat(view);
        } else {
            return new SDK14ViewCompat(view);
        }
    }

    public abstract void postInvalidateOnAnimation();

    public abstract void postInvalidateOnAnimation(int l, int t, int r, int b);

    public abstract void setBackground(Drawable drawable);

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    static class SDK14ViewCompat extends ViewCompat {
        protected SDK14ViewCompat(View view) {
            super(view, Build.VERSION_CODES.ICE_CREAM_SANDWICH);
        }

        @Override
        public void postInvalidateOnAnimation() {
            mView.postInvalidate();
        }

        @Override
        public void postInvalidateOnAnimation(int l, int t, int r, int b) {
            mView.postInvalidate(l, t, r, b);
        }

        @SuppressWarnings("deprecation")
        @Override
        public void setBackground(Drawable drawable) {
            mView.setBackgroundDrawable(drawable);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    static class SDK16ViewCompat extends ViewCompat {

        protected SDK16ViewCompat(View view) {
            super(view, Build.VERSION_CODES.JELLY_BEAN);
        }

        @Override
        public void postInvalidateOnAnimation() {
            mView.postInvalidateOnAnimation();
        }

        @Override
        public void postInvalidateOnAnimation(int l, int t, int r, int b) {
            mView.postInvalidateOnAnimation(l, t, r, b);
        }

        @Override
        public void setBackground(Drawable drawable) {
            mView.setBackground(drawable);
        }
    }
}
