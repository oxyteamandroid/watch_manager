/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.view;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.view.ViewGroup;

import com.ingenic.watchmanager.R;

public class MenuImpl implements Menu {
    private Context mContext;
    private Resources mResources;
    private List<MenuItem> mItems = new ArrayList<MenuItem>();

    private MenuImpl(Context context) {
        mContext = context;
        mResources = context.getResources();
    }

    public static MenuImpl create(Context context) {
        return new MenuImpl(context);
    }

    @Override
    public MenuItem add(CharSequence title, Drawable icon) {
        return add(allocateItemId(), allocateOrder(), title, icon);
    }

    @Override
    public MenuItem add(int titleRes, int iconRes) {
        return add(allocateItemId(), allocateOrder(), titleRes, iconRes);
    }

    @Override
    public MenuItem add(int itemId, int order, CharSequence title, Drawable icon) {
        MenuItem item = MenuItemImpl.create(mContext).setItemId(itemId)
                .setOrder(order).setTitle(title);
        if (icon != null) {
            Drawable[] layers = new Drawable[2];
            layers[0] = mResources.getDrawable(R.drawable.btn_menu_white);
            layers[1] = icon;
            LayerDrawable drawable = new LayerDrawable(layers);
            drawable.setLayerInset(1, 10, 10, 10, 10);
            item.setIcon(drawable);
        }
        int index = getIndexByOrder(order);
        mItems.add(index, item);
        return item;
    }

    @Override
    public MenuItem add(int itemId, int order, int titleRes, int iconRes) {
        String title = null;
        if (titleRes != 0) {
            title = mResources.getString(titleRes);
        }
        if (title == null) {
            return null;
        }
        Drawable icon = null;
        if (iconRes != 0) {
            icon = mResources.getDrawable(iconRes);
        }
        return add(itemId, order, title, icon);
    }

    @Override
    public List<MenuItem> getItems() {
        return mItems;
    }

    @Override
    public int size() {
        return mItems.size();
    }

    @Override
    public MenuItem findItem(int id) {
        int count = size();
        for (int i = 0; i < count; i++) {
            MenuItem item = getItem(i);
            if (id == item.getItemId()) {
                return item;
            }
        }
        return null;
    }

    @Override
    public MenuItem getItem(int index) {
        if (index < 0 || index >= size()) {
            return null;
        }
        return mItems.get(index);
    }

    @SuppressWarnings("unused")
    private void setMenuViewGroup(ViewGroup group) {
    }

    private int getIndexByOrder(int order) {
        int count = size();
        int index = count;
        for (int i = count - 1; i >= 0; i++) {
            MenuItem that = getItem(i);
            int thatOrder = that.getOrder();
            if (order < thatOrder) {
                index = i;
            } else if (order == thatOrder) {
                throw new IllegalArgumentException("the order:" + order
                        + "has used!");
            } else {
                break;
            }
        }
        return index;
    }

    private int allocateItemId() {
        MenuItem last = getItem(size() - 1);
        int id = NONE;
        if (last != null) {
            id = last.getItemId();
        }
        return allocateItemId(id + 1);
    }

    private int allocateItemId(int id) {
        if (hasItem(id)) {
            id++;
            return allocateItemId(id);
        }
        return id;
    }

    private boolean hasItem(int itemId) {
        int count = size();
        for (int i = 0; i < count; i++) {
            MenuItem item = getItem(i);
            if (itemId != item.getItemId()) {
                continue;
            } else {
                return true;
            }
        }
        return false;
    }

    private int allocateOrder() {
        MenuItem last = getItem(size() - 1);
        int order = FIRST;
        if (last != null) {
            order = last.getOrder() + 1;
        }
        return order;
    }
}
