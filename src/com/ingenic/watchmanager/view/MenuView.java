/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingenic.watchmanager.R;

public class MenuView extends LinearLayout {
    private LinearLayout mIconLayout;
    private LinearLayout mSubLayout;
    private View mMoreView;
    private int mMenuBarHeight;
    private int mMenuIconTextSize;
    private boolean isOpenning = false;
    private boolean isShowing = true;
    private Menu mMenu;
    private OnItemSelectedListener mListener;
    private OnClickListener btnClick = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.more) {
                if (isOpenning) {
                    close();
                } else {
                    open();
                }
            } else if (mMenu != null) {
                MenuItem item = mMenu.findItem(v.getId());
                if (isOpenning) {
                    close();
                }
                if (mListener != null && item != null) {
                    mListener.onItemSelected(item);
                }
            }
        }
    };

    MenuView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mMenuIconTextSize = getResources().getDimensionPixelSize(
                R.dimen.menu_icon_text_size);
        mMenuBarHeight = getResources().getDimensionPixelSize(
                R.dimen.menu_height)
                + mMenuIconTextSize;
        inflate(context, R.layout.metro_menu_layout, this);
    }

    MenuView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    MenuView(Context context) {
        this(context, null, 0);
        onFinishInflate();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mMoreView = findViewById(R.id.more);
        mMoreView.setOnClickListener(btnClick);
        mIconLayout = (LinearLayout) findViewById(R.id.ic_menu_content);
        mSubLayout = (LinearLayout) findViewById(R.id.text_menu_content);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (mIconLayout.getChildCount() <= 0) {
            mMenuBarHeight = getMeasuredHeight()
                    - mSubLayout.getMeasuredHeight();
        }
    }

    boolean isOpenning() {
        return isOpenning;
    }

    int getMenuBarHeight() {
        return mMenuBarHeight;
    }

    void applyMenu(Menu menu) {
        if (mIconLayout.getChildCount() > 0) {
            mIconLayout.removeAllViews();
        }
        if (mSubLayout.getChildCount() > 0) {
            mSubLayout.removeAllViews();
        }
        mMenu = menu;
        int count = menu.size();
        for (int i = 0; i < count; i++) {
            MenuItem item = menu.getItem(i);
            TextView view = new TextView(getContext());
            view.setId(item.getItemId());
            view.setOnClickListener(btnClick);
            view.setText(item.getTitle());
            Drawable icon = item.getIcon();
            int padding = mMenuIconTextSize / 2;
            view.setPadding(padding, padding, padding, padding);
            if (icon != null) {
                view.setGravity(Gravity.CENTER);
                icon.setBounds(0, 0, mMenuBarHeight - mMenuIconTextSize,
                        mMenuBarHeight - mMenuIconTextSize);
                view.setCompoundDrawables(null, item.getIcon(), null, null);
                view.setTextSize(mMenuIconTextSize);
                mIconLayout.addView(view);
            } else {
                // view.setTextSize(24);
                view.setTextAppearance(getContext(),
                        android.R.style.TextAppearance_Large);
                mSubLayout.addView(view);
            }
            view.setTextColor(Color.WHITE);
        }
    }

    boolean canOpen() {
        return isShowing && getHeight() > mMenuBarHeight;
    }

    void open() {
        if (!canOpen()) {
            return;
        }
        isOpenning = true;
        int delta = getHeight() - mMenuBarHeight;
        ObjectAnimator animator = ObjectAnimator.ofFloat(this, "TranslationY",
                -delta);
        int duration = Math.max(delta, mMenuBarHeight);
        animator.setDuration(duration);
        animator.start();
    }

    void close() {
        isOpenning = false;
        int delta = getHeight() - mMenuBarHeight;
        ObjectAnimator animator = ObjectAnimator.ofFloat(this, "TranslationY",
                0);
        int duration = Math.max(delta, mMenuBarHeight);
        animator.setDuration(duration);
        animator.start();
    }

    boolean isShowing() {
        return isShowing;
    }

    void hide() {
        isShowing = false;
        ObjectAnimator animator = ObjectAnimator.ofFloat(this, "TranslationY",
                mMenuBarHeight);
        animator.setDuration(mMenuBarHeight);
        animator.start();
    }

    void show() {
        isShowing = true;
        ObjectAnimator animator = ObjectAnimator.ofFloat(this, "TranslationY",
                0);
        animator.setDuration(mMenuBarHeight);
        animator.start();
    }

    void setOnItemSelectedListener(OnItemSelectedListener l) {
        mListener = l;
    }

    public interface OnItemSelectedListener {
        void onItemSelected(MenuItem item);
    }

    void moveBy(float y) {
        float tran = getTranslationY() + y;
        if (tran > 0) {
            tran = 0;
        }
        int height = mMenuBarHeight - getHeight();
        if (tran < height) {
            tran = height;
        }
        setTranslationY(tran);
    }

    void handleUp() {
        int tran = (int) getTranslationY();
        if (tran < (mMenuBarHeight - getHeight()) / 2) {
            open();
        } else {
            close();
        }
    }
}
