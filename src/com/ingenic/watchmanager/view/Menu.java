/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.view;

import java.util.List;

import android.graphics.drawable.Drawable;

public interface Menu {
    static final int NONE = 0;
    static final int FIRST = 1;

    MenuItem add(CharSequence title, Drawable icon);

    MenuItem add(int titleRes, int iconRes);

    MenuItem add(int itemId, int order, CharSequence title, Drawable icon);

    MenuItem add(int itemId, int order, int titleRes, int iconRes);

    List<MenuItem> getItems();

    int size();

    MenuItem findItem(int id);

    MenuItem getItem(int index);
}
