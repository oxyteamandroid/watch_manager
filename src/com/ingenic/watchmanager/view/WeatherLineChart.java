/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 * Elf/AmazingWeather/ Project
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.view;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.util.Utils;
import com.ingenic.watchmanager.weather.WeatherInfo;

/**
 * 气温折线图
 */
public class WeatherLineChart extends View {

    private static final int GAP = 2;
    private static final int DEFAULT_TEXTSIZE = 24;

    private List<WeatherInfo> mForecasts = new ArrayList<WeatherInfo>();
    private int mIconSize;
    private int[] mTempRange = new int[2];
    private int mWeekTextSize = DEFAULT_TEXTSIZE;
    private int mWeatherTextSize = DEFAULT_TEXTSIZE;
    private int mTempTextSize = DEFAULT_TEXTSIZE;
    private int mDateTextSize = DEFAULT_TEXTSIZE;
    private int mCircleRadius;// 折线图节点圆心大小
    private int mTextColor; // 文字颜色
    private int mMaxTempColor;// 最高气温线段的颜色
    private int mMinTempColor;// 最低气温线段的颜色
    private int mTempLineWidth;// 气温折线图的线段宽度
    private Typeface mTypeFace;

    public WeatherLineChart(Context context) {
        this(context, null);
    }

    public WeatherLineChart(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WeatherLineChart(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        mTypeFace = Typeface.createFromAsset(context.getAssets(),
                "fonts/watch_font.ttf");

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.WeatherLineChart);
        init(a);
        a.recycle();
    }

    private void init(TypedArray typedArray) {
        this.setDrawingCacheEnabled(true);

        mIconSize = typedArray.getDimensionPixelSize(
                R.styleable.WeatherLineChart_iconSize, 48);
        mWeekTextSize = typedArray.getDimensionPixelSize(
                R.styleable.WeatherLineChart_weekTextSize, DEFAULT_TEXTSIZE);

        mTextColor = typedArray.getColor(
                R.styleable.WeatherLineChart_textColor, Color.BLACK);
        mMaxTempColor = typedArray.getColor(
                R.styleable.WeatherLineChart_maxTempColor, Color.YELLOW);
        mMinTempColor = typedArray.getColor(
                R.styleable.WeatherLineChart_minTempColor, Color.CYAN);

        mTempLineWidth = typedArray.getDimensionPixelOffset(
                R.styleable.WeatherLineChart_tempLineWidth, 2);
        mCircleRadius = mTempLineWidth * 3 / 2;

        mDateTextSize = typedArray.getDimensionPixelSize(
                R.styleable.WeatherLineChart_dateTextSize, DEFAULT_TEXTSIZE);
        mTempTextSize = typedArray.getDimensionPixelSize(
                R.styleable.WeatherLineChart_tempTextSize, DEFAULT_TEXTSIZE);
        mWeatherTextSize = typedArray.getDimensionPixelSize(
                R.styleable.WeatherLineChart_weatherTextSize, DEFAULT_TEXTSIZE);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (mForecasts != null && mForecasts.size() > 0) {
            mForecasts.clear();
        }

        mTypeFace = null;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int count = mForecasts.size();
        if (count <= 0)
            return;

        int width = getWidth();
        int singleSize = width / count;
        int top = 0;
        int topest = top;

        // 绘制气温折线图
        drawChart(canvas, top);

        top += getHeight() / 2 + mDateTextSize * 2;
        // 绘制坐标横向星期和日期Text
        for (int i = 0; i < count; i++) {
            WeatherInfo info = mForecasts.get(i);
            top = drawInfo(canvas, info, singleSize, i);
            if (top > topest)
                topest = top;
        }

        for (int i = 0; i < count; i++) {
            WeatherInfo info = mForecasts.get(i);
            drawTemp(canvas, info, singleSize, i, topest);
        }

        canvas.save();
    }

    private void drawTemp(Canvas canvas, WeatherInfo info, int size,
            int current, int top) {
        int left = size * current;

        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(mTextColor);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTypeface(mTypeFace);
        paint.setTextSize(mWeekTextSize);

        // 绘制温度
        String degress = Utils.getWeatherDegress(getContext(), info);
        String str = info.minimumTemp + "/" + info.maximumTemp + degress;
        canvas.drawText(str, left + size / 2, top + getBaseline(paint), paint);

        top += mWeatherTextSize + GAP * 2;
    }

    private int drawInfo(Canvas canvas, WeatherInfo info, int size, int current) {
        int left = size * current;
        int top = GAP + getHeight() / 2 + mDateTextSize * 2;

        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(mTextColor);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTypeface(mTypeFace);
        paint.setTextSize(mWeekTextSize);

        // 绘制星期几
        canvas.drawText(info.dayOfWeek, left + size / 2, top
                + getBaseline(paint), paint);
        top += mWeekTextSize + GAP * 2;

        // 绘制天气图片
        int code = Integer.parseInt(info.weatherCode);
        int iconId = Utils.getWeatherIconResource(getContext(), code);

        // 缩放内存
        BitmapFactory.Options option = new BitmapFactory.Options();
        option.inJustDecodeBounds = true;
        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                iconId == 0 ? R.drawable.ic_launcher : iconId, option);

        int realWidth = option.outWidth;
        int realHeight = option.outHeight;

        int scale = (realWidth > realHeight) ? realHeight / size : realWidth
                / size;
        if (scale <= 0) {
            scale = 1;
        }

        option.inSampleSize = scale;
        option.inJustDecodeBounds = false;

        icon = BitmapFactory.decodeResource(getResources(),
                iconId == 0 ? R.drawable.ic_launcher : iconId, option);
        icon = zoomImage(icon, mIconSize, mIconSize);

        canvas.drawBitmap(icon, left + (size - mIconSize) / 2, top, paint);
        top += mIconSize + GAP * 2;

        // 绘制天气
        paint.setTextSize(mWeatherTextSize);
        String temp_weather = Utils.getWeatherByLocal(getResources(),
                info.weatherCode);
        String[] weathers = temp_weather.split(" ");

        for (String weather : weathers) {
            if (paint.measureText(weather) > size) {
                int index = paint.breakText(weather, true, size, null);
                canvas.drawText(weather.substring(0, index), left + size / 2,
                        top + getBaseline(paint), paint);

                top += mWeatherTextSize;
                canvas.drawText(weather.substring(index), left + size / 2, top
                        + getBaseline(paint), paint);
            } else
                canvas.drawText(weather, left + size / 2, top
                        + getBaseline(paint), paint);
            top += mWeatherTextSize;
        }
        top += mWeatherTextSize;

        // 绘制日期
        paint.setTextSize(mDateTextSize);
        canvas.drawText(info.date, left + size / 2, getHeight() / 2 - GAP
                - mDateTextSize + getBaseline(paint), paint);
        return top;
    }

    private static float getBaseline(Paint paint) {
        FontMetrics fontMetrics = paint.getFontMetrics();
        float size = paint.getTextSize();
        // 计算文字高度
        float fontHeight = fontMetrics.bottom - fontMetrics.top;
        // 计算文字baseline
        float textBaseY = size - (size - fontHeight) / 2 - fontMetrics.bottom;
        return textBaseY;
    };

    /***
     * 图片的缩放方法
     * @param bgimage
     *            ：源图片资源
     * @param newWidth
     *            ：缩放后宽度
     * @param newHeight
     *            ：缩放后高度
     * @return
     */
    private static Bitmap zoomImage(Bitmap bgimage, double newWidth,
            double newHeight) {
        // 获取这个图片的宽和高
        float width = bgimage.getWidth();
        float height = bgimage.getHeight();

        // 创建操作图片用的matrix对象
        Matrix matrix = new Matrix();
        // 计算宽高缩放率
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        // 缩放图片动作
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap bitmap = Bitmap.createBitmap(bgimage, 0, 0, (int) width,
                (int) height, matrix, true);
        return bitmap;
    }

    private void drawChart(Canvas canvas, int top) {
        int count = mForecasts.size();
        int singleSize = getWidth() / count;
        int coordinateHeight = getHeight() / 2 - GAP * 3 - top - mDateTextSize;
        int spacePixel = (coordinateHeight - mTempTextSize * 2)
                / (mTempRange[1] - mTempRange[0]);
        int offsetTop = top + mTempTextSize;
        int[][] cs = new int[count][2];

        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setTextAlign(Paint.Align.CENTER);
        BlurMaskFilter paintBGBlur = new BlurMaskFilter(1,
                BlurMaskFilter.Blur.INNER);
        paint.setMaskFilter(paintBGBlur);
        paint.setStrokeWidth(mTempLineWidth);

        /******************* 绘制最高气温折线 **************************/
        for (int i = 0; i < count; i++) {
            WeatherInfo info = mForecasts.get(i);
            int temp = info.maximumTemp;
            int cx = singleSize * i + singleSize / 2;
            int cy = offsetTop + (mTempRange[1] - temp) * spacePixel;
            paint.setColor(mMaxTempColor);
            canvas.drawCircle(cx, cy, mCircleRadius, paint);
            cs[i][0] = cx;
            cs[i][1] = cy;
        }

        paint.setStrokeWidth(mTempLineWidth);
        for (int i = 0; i < cs.length - 1; i++) {
            int[] c = cs[i];
            int[] d = cs[i + 1];
            canvas.drawLine(c[0], c[1], d[0], d[1], paint);
        }

        /******************* 绘制最低气温折线 *************************/
        for (int j = 0; j < count; j++) {
            WeatherInfo info = mForecasts.get(j);
            int temp = info.minimumTemp;
            int cx = singleSize * j + singleSize / 2;
            int cy = offsetTop + (mTempRange[1] - temp) * spacePixel;
            paint.setColor(mMinTempColor);
            canvas.drawCircle(cx, cy, mCircleRadius, paint);
            cs[j][0] = cx;
            cs[j][1] = cy;
        }

        paint.setStrokeWidth(mTempLineWidth);
        for (int i = 0; i < cs.length - 1; i++) {
            int[] c = cs[i];
            int[] d = cs[i + 1];
            canvas.drawLine(c[0], c[1], d[0], d[1], paint);
        }

        // 绘制分割线
        paint.setColor(Color.parseColor("#5fffffff"));
        canvas.drawLine(0, getHeight() / 2 + mDateTextSize, getWidth(),
                getHeight() / 2 + mDateTextSize, paint);
    }

    public void updateWeatherForecast(List<WeatherInfo> forecasts) {
        mForecasts.clear();
        if (forecasts != null) {
            mForecasts.addAll(forecasts);
        }

        // 重新计算最高温度和最低温度
        getTempRange(mTempRange);
        invalidate();
    }

    private int[] getTempRange(int[] temps) {
        if (temps == null) {
            temps = new int[2];
        }

        if (mForecasts.size() <= 0)
            return temps;

        WeatherInfo info = mForecasts.get(0);
        temps[0] = info.minimumTemp;
        temps[1] = info.maximumTemp;

        for (int i = 1; i < mForecasts.size(); i++) {
            info = mForecasts.get(i);
            int min = info.minimumTemp;
            int max = info.maximumTemp;

            if (temps[0] > min) {
                temps[0] = min;
            }

            if (temps[1] < max) {
                temps[1] = max;
            }
        }
        return temps;
    }
}
