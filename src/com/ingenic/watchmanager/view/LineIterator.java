/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.util.Utils;

public class LineIterator extends LinearLayout {

    private int mCount;
    private int mCurrent = -1;

    public LineIterator(Context context) {
        super(context);
    }

    public LineIterator(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LineIterator(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setTotalCount(int count) {
        if (mCount == count) return;

        if (mCount > count) {
            for (int i = count; i < mCount; i++) {
                removeViewAt(i);
            }
        } else {
            int paddingH = Utils.dp2px(getContext(), 10);
            int paddingV = Utils.dp2px(getContext(), 20);

            for (int i = mCount; i < count; i++) {
                ImageView view = new ImageView(getContext());
                view.setImageResource(R.drawable.ic_dot);
                view.setPadding(paddingH, paddingV, paddingH, paddingV);
                addView(view);
            }
        }

        mCount = count;
    }

    public void setCurrent(int current) {
        if (mCurrent == current || current < 0 || current >= mCount) {
            return;
        }

        if (mCurrent != -1) {
            ImageView last = (ImageView) getChildAt(mCurrent);
            last.setImageResource(R.drawable.ic_dot);
        }

        ImageView child = (ImageView) getChildAt(current);
        child.setImageResource(R.drawable.ic_dot_sel);

        mCurrent = current;
    }
}
