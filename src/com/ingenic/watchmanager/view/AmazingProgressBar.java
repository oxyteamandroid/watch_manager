/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/iwds-ui-jar Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 */
package com.ingenic.watchmanager.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;

/**
 * AmazingProgressBar<br>
 * 圆形进度条
 * 
 * @author tZhang
 * 
 */
public class AmazingProgressBar extends View {

    /**
     * 布局宽度
     */
    private int width;

    /**
     * 布局高度
     */
    private int height;

    /**
     * 最大小球的半径
     */
    private int maxRadius = 6;

    /**
     * 用于界面重绘的Handler
     */
    private Handler mHandler;

    /**
     * 重绘界面的间隔时间
     */
    private int delayMillis = 100;

    /**
     * AmazingProgressBar中心X坐标
     */
    private int centerX;

    /**
     * AmazingProgressBar中心Y坐标
     */
    private int centerY;

    /**
     * 小球圆心所在的圆的半径
     */
    private int radius;

    /**
     * 小球位置的弧度
     */
    private double θ;

    /**
     * AmazingProgressBar中的小球环实体
     */
    private Entity entity;

    /**
     * 开启旋转标志
     */
    private boolean started;

    public AmazingProgressBar(Context context) {
        this(context, null);
    }

    public AmazingProgressBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AmazingProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        // 实例化小球环的实例多小
        entity = new Entity();

        // 更新界面
        mHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                entity.update();
                invalidate();
                mHandler.sendEmptyMessageDelayed(0, delayMillis);
                return false;
            }
        });

        // 开始旋转
        start();
    }

    /**
     * 开启旋转
     */
    public void start() {
        if (started) {
            return;
        }
        started = true;
        mHandler.sendEmptyMessage(0);
    }

    /**
     * 结束旋转
     */
    public void stop() {
        mHandler.removeMessages(0);
        started = false;
        invalidate();
    }

    public boolean isStart() {
        return started;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right,
            int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        // 获取布局控件
        width = getLayoutParams().width;
        height = getLayoutParams().height;

        // 参照尺寸
        int dimension = Math.min(width, height);

        // 确定小球尺寸
        if (dimension > 100) {
            maxRadius = 12;
        } else {
            maxRadius = 6;
        }

        // 确定小球环所在圆的半径
        radius = dimension / 2 - maxRadius;

        // 确定AmazingProgressBar的对称中心坐标
        centerX = width / 2;
        centerY = height / 2;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // 绘制小球环
        entity.draw(canvas);
        super.onDraw(canvas);
    }

    class Entity {

        /***
         * 小球圆心坐标
         */
        private PointF p;

        /**
         * 小球所在角度
         */
        private int[] angle = { 0, 30, 60, 90, 120, 150, 180, -150, -120, -90,
                -60, -30 }; // 12色环

        /**
         * 小球对应颜色
         */
        private int[] colors = { Color.BLUE, Color.CYAN, Color.GREEN,
                Color.RED, Color.YELLOW, Color.MAGENTA, Color.BLUE, Color.CYAN,
                Color.GREEN, Color.RED, Color.YELLOW, Color.MAGENTA };

        /**
         * 起始位置（最大小球所在位置）
         */
        private int startPosition;

        private int size;

        /**
         * 绘制小球的画笔
         */
        private Paint paint;

        public Entity() {
            size = angle.length;
            startPosition = size - 3;

            // 初始化画笔
            paint = new Paint();
            paint.setAntiAlias(true);
            paint.setStyle(Paint.Style.FILL);
        }

        // 更新小球的大小（通过起始位置）
        public void update() {
            if (startPosition < size - 1) {
                startPosition++;
            } else {
                startPosition = (startPosition + 1) % size;
            }
        }

        /**
         * 绘制小球环
         * 
         * @param canvas
         *            画布
         */
        public void draw(Canvas canvas) {

            canvas.save();

            int location = startPosition;

            for (float i = 0; i < maxRadius; i += (maxRadius == 6 ? 0.5f : 1)) {
                // 计算小球所在位置坐标
                p = getPoint(angle[location], radius);

                // 绘制小球
                paint.setColor(colors[location]);
                canvas.drawCircle(p.x, p.y, maxRadius - i, paint);

                if (location > 0) {
                    location--;
                } else {
                    location = size - 1;
                }

            }
            canvas.restore();
        }
    }

    /**
     * 计算小球所在位置的坐标
     * 
     * @param α
     *            角度
     * @param r
     *            小球圆心所在圆环半径
     * @return 小球所在位置的坐标
     */
    private PointF getPoint(int α, int r) {
        θ = (Math.PI * α / 180);
        float a = (float) (centerX + r * Math.cos(θ));
        float b = (float) (centerY + r * Math.sin(θ));
        return new PointF(a, b);
    }

}
