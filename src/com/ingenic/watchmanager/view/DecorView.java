/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.ingenic.watchmanager.view.MenuView.OnItemSelectedListener;

public class DecorView extends LinearLayout {
    private FrameLayout mContentLayout;
    private MenuView mMenuView;
    private boolean inMenu = false;
    private boolean isDraggingMenu = false;
    private int mTouchSlop;
    @SuppressWarnings("unused")
    private int mLastMotionX;
    private int mLastMotionY;
    private boolean mHasMenu = false;

    public DecorView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DecorView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContentLayout = new FrameLayout(context);
        addView(mContentLayout);
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public MenuView getMenuView() {
        return mMenuView;
    }

    public void setContentView(int layoutId) {
        View view = inflate(getContext(), layoutId, null);
        setContentView(view);
    }

    public void setContentView(View view) {
        setContentView(view, (LayoutParams) view.getLayoutParams());
    }

    public void setContentView(View view, ViewGroup.LayoutParams params) {
        if (mContentLayout.getChildCount() > 0) {
            mContentLayout.removeAllViews();
        }
        if (params == null) {
            params = new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT);
        }
        mContentLayout.addView(view, params);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int count = getChildCount();
        int childWidthSpec = MeasureSpec.makeMeasureSpec(width,
                MeasureSpec.EXACTLY);
        int childHeightSpec = heightMeasureSpec;

        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child == mContentLayout) {
                childHeightSpec = MeasureSpec.makeMeasureSpec(height
                        - getMenuBarHeight(), MeasureSpec.EXACTLY);
            } else if (child == mMenuView && mHasMenu) {
                childHeightSpec = MeasureSpec.makeMeasureSpec(LayoutParams.WRAP_CONTENT,
                        MeasureSpec.UNSPECIFIED);
            }
            child.measure(childWidthSpec, childHeightSpec);
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int count = getChildCount();
        int childLeft = getPaddingLeft();
        int childTop = getPaddingTop();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child == mMenuView) {
                childTop = b - getMenuBarHeight();
            }
            child.layout(childLeft, childTop,
                    childLeft + child.getMeasuredWidth(),
                    childTop + child.getMeasuredHeight());
        }

        setFocusable(true);
        requestFocus();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (!mHasMenu) {
            return false;
        }
        if (isDraggingMenu) {
            return true;
        }
        final int action = ev.getAction();
        int x = (int) ev.getX();
        int y = (int) ev.getY();
        switch (action) {
        case MotionEvent.ACTION_DOWN:
            inMenu = inMenuView(x, y);
            mLastMotionX = x;
            mLastMotionY = y;
            break;
        case MotionEvent.ACTION_MOVE:
            if (inMenu && mMenuView.canOpen()) {
                int delta = y - mLastMotionY;
                boolean isMenuOpen = mMenuView.isOpenning();
                if (delta < -mTouchSlop) {
                    isDraggingMenu = !isMenuOpen;
                } else if (delta > mTouchSlop) {
                    isDraggingMenu = isMenuOpen;
                }
            } else if (!inMenu) {
                if (mMenuView.isOpenning()) {
                    mMenuView.close();
                }
            }
            break;
        default:
            break;
        }
        return isDraggingMenu || super.onInterceptTouchEvent(ev);
    }

    private boolean inMenuView(int x, int y) {
        x -= mMenuView.getTranslationX();
        y -= mMenuView.getTranslationY();
        return x >= mMenuView.getLeft() && x < mMenuView.getRight()
                && y >= mMenuView.getTop() && y < mMenuView.getBottom();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!mHasMenu) {
            return false;
        }
        final int action = event.getAction();
        int x = (int) event.getX();
        int y = (int) event.getY();
        switch (action) {
        case MotionEvent.ACTION_DOWN:
            mLastMotionX = x;
            mLastMotionY = y;
            break;
        case MotionEvent.ACTION_MOVE:
            if (!inMenu) {
                if (mMenuView.isOpenning()) {
                    mMenuView.close();
                }
                return true;
            }
            int delta = y - mLastMotionY;
            if (inMenu && !isDraggingMenu) {
                boolean isMenuOpen = mMenuView.isOpenning();
                if (delta < -mTouchSlop) {
                    isDraggingMenu = !isMenuOpen;
                } else if (delta > mTouchSlop) {
                    isDraggingMenu = isMenuOpen;
                }
            }
            if (isDraggingMenu) {
                mMenuView.moveBy(delta);
                mLastMotionY = y;
            }
            mLastMotionX = x;
            break;
        case MotionEvent.ACTION_CANCEL:
        case MotionEvent.ACTION_UP:
            if (isDraggingMenu) {
                mMenuView.handleUp();
                isDraggingMenu = false;
            } else if (!inMenu) {
                if (mMenuView.isOpenning()) {
                    mMenuView.close();
                }
            }
            break;
        default:
            break;
        }
        return true;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
        case KeyEvent.KEYCODE_BACK:
            if (isMenuOpened()) {
                closeMenu();
                return true;
            }
            break;
        case KeyEvent.KEYCODE_MENU:
            if (isMenuOpened()) {
                closeMenu();
            } else {
                if (mHasMenu) {
                    openMenu();
                }
            }
            return true;
        default:
            break;
        }
        return super.onKeyUp(keyCode, event);
    }

    public void setOnItemSelectedListener(OnItemSelectedListener l) {
        if (mHasMenu) {
            mMenuView.setOnItemSelectedListener(l);
        }
    }

    public boolean isMenuOpened() {
        return mHasMenu && mMenuView.isOpenning();
    }

    public void closeMenu() {
        mMenuView.close();
    }

    public void openMenu() {
        mMenuView.open();
    }

    public void hideMenu() {
        if (mHasMenu) {
            mMenuView.hide();
        }
    }

    public void showMenu() {
        if (mHasMenu) {
            mMenuView.show();
        }
    }

    public void applyMenu(Menu menu) {
        if (menu != null) {
            mHasMenu = true;
            mMenuView = new MenuView(getContext());
            mMenuView.applyMenu(menu);
            addView(mMenuView);
        } else {
            mHasMenu = false;
        }
    }

    private int getMenuBarHeight() {
        if (mHasMenu) {
            return mMenuView.getMenuBarHeight();
        } else {
            return 0;
        }
    }
}
