/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.TextView;

import com.ingenic.watchmanager.util.Utils;

public class RoundTextButton extends TextView {

    private Paint mPaint;

    public RoundTextButton(Context context) {
        this(context, null);
    }

    public RoundTextButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoundTextButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        int paddingH = Utils.dp2px(context, 24);
        int paddingV = Utils.dp2px(context, 8);
        setPadding(paddingH, paddingV, paddingH, paddingV);
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        Rect rect = new Rect(5, 5, getWidth() - 5, getHeight() - 5);

        mPaint.setColor(getResources().getColor(
                android.R.color.holo_blue_bright));
        mPaint.setStyle(isPressed() ? Paint.Style.FILL : Paint.Style.STROKE);
        mPaint.setStrokeWidth(2);

        float r = Utils.dp2px(getContext(), 10f);
        canvas.drawRoundRect(new RectF(rect), r, r, mPaint);

        super.onDraw(canvas);
    }

    @Override
    public void setPressed(boolean pressed) {
        super.setPressed(pressed);
        invalidate();
    }
}
