/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;

public class MenuItemImpl implements MenuItem {
    @SuppressWarnings("unused")
    private Context mContext;
    private Resources mResources;
    private View mActionView;
    private CharSequence mTitle;
    private Drawable mIcon;
    private int mItemId = Menu.NONE;
    private int mOrder = Menu.FIRST;

    private MenuItemImpl(Context context) {
        mContext = context;
        mResources = context.getResources();
    }

    public static MenuItemImpl create(Context context) {
        return new MenuItemImpl(context);
    }

    @Override
    public MenuItem setTitle(CharSequence title) {
        mTitle = title;
        if (mActionView != null && mActionView instanceof TextView) {
            ((TextView) mActionView).setText(title);
        }
        return this;
    }

    @Override
    public MenuItem setTitle(int titleRes) {
        CharSequence title = mResources.getString(titleRes);
        return setTitle(title);
    }

    @Override
    public CharSequence getTitle() {
        return mTitle;
    }

    @Override
    public MenuItem setActionView(View view) {
        mActionView = view;
        return this;
    }

    @Override
    public View getActionView() {
        return mActionView;
    }

    @Override
    public MenuItem setIcon(Drawable icon) {
        mIcon = icon;
        if (mActionView != null && mActionView instanceof TextView) {
            ((TextView) mActionView).setCompoundDrawables(null, icon, null, null);
        }
        return this;
    }

    @Override
    public MenuItem setIcon(int iconRes) {
        Drawable icon = mResources.getDrawable(iconRes);
        return setIcon(icon);
    }

    @Override
    public Drawable getIcon() {
        return mIcon;
    }

    @Override
    public MenuItem setItemId(int itemId) {
        mItemId = itemId;
        return this;
    }

    @Override
    public int getItemId() {
        return mItemId;
    }

    @Override
    public MenuItem setOrder(int order) {
        mOrder = order;
        return this;
    }

    @Override
    public int getOrder() {
        return mOrder;
    }

}
