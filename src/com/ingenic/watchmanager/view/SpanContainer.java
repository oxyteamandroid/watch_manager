package com.ingenic.watchmanager.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;

import com.ingenic.watchmanager.metro.MetroModel;
import com.ingenic.watchmanager.metro.MetroView;

public class SpanContainer extends ViewGroup {

    static final int INVALID_POSITION = -1;
    /** 列 */
    private int mColumn;
    /** 单个控件大小 */
    private int mSpanSize;
    /** 控件之间的间距 */
    private int mSpanGap;
    private int mHorizontalPadding;
    private int mVerticalPadding;
    private int mTouchIndex = INVALID_POSITION;
    private OnItemClickListener mLiatener;
    private int mTouchSlop;
    public SpanContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mColumn = MetroModel.getMetroColumn();
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public SpanContainer(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SpanContainer(Context context) {
        this(context, null);
    }

    public void setSpanGap(int gap) {
        this.mSpanGap = gap;
        mHorizontalPadding = (int) (gap * 2.5f);
        mVerticalPadding = gap * 5;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMeasureSize = MeasureSpec.getSize(widthMeasureSpec);
        int width = widthMeasureSize - getPaddingLeft() - getPaddingRight();
        int minHeight = MeasureSpec.getSize(heightMeasureSpec);
        width -= mHorizontalPadding * 2;
        mSpanSize = (width - mSpanGap * (mColumn - 1)) / mColumn;
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            measureChild(child);

            LayoutParams lp = getLayoutParams(child);
            int bottom = lp.y + lp.height;
            if (bottom > minHeight) {
                minHeight = bottom;
            }
        }

        int height = minHeight + mVerticalPadding;
        setMeasuredDimension(widthMeasureSize, height);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() == GONE) continue;

            LayoutParams lp = getLayoutParams(child);
            int childLeft = lp.x + mHorizontalPadding;
            int childTop = lp.y + mVerticalPadding;
            child.layout(childLeft, childTop, childLeft + lp.width, childTop + lp.height);
        }
    }

    protected void measureChild(View child) {
        LayoutParams lp = getLayoutParams(child);
        lp.setup(mSpanSize, mSpanGap);
        child.measure(MeasureSpec.makeMeasureSpec(lp.width, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(lp.height, MeasureSpec.EXACTLY));
    }

    LayoutParams getLayoutParams(View c) {
        return (LayoutParams) c.getLayoutParams();
    }

    int mLastMotionX;
    int mLastMotionY;

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        final int action = ev.getAction();
        switch (action & MotionEvent.ACTION_MASK) {
        case MotionEvent.ACTION_DOWN: {
            mLastMotionX = (int) ev.getX();
            mLastMotionY = (int) ev.getY();

            mTouchIndex = findPositionByMotion(mLastMotionX, mLastMotionY);
            break;
        }
        case MotionEvent.ACTION_MOVE:
            int x = (int) ev.getX();
            int y = (int) ev.getY();
            if (Math.abs(x - mLastMotionX) > mTouchSlop || Math.abs(y - mLastMotionY) > mTouchSlop) {      
                mTouchIndex = INVALID_POSITION;
            }
            break;
        case MotionEvent.ACTION_UP:
            break;
        case MotionEvent.ACTION_CANCEL:
            mTouchIndex = INVALID_POSITION;
            break;
        default:
            break;
        }
        return mTouchIndex != INVALID_POSITION;
    }

    private int findPositionByMotion(int x, int y) {
        Rect frame;// = new Rect();
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() == VISIBLE && child instanceof MetroView) {
                // child.getHitRect(frame);

                float scale = child.getScaleX();
                frame = new Rect(child.getLeft(), child.getTop(),
                        child.getRight(), child.getBottom());
                // The child hit rect is relative to the CellLayoutChildren
                // parent, so we need to
                // offset that by this CellLayout's padding to test an (x,y)
                // point that is relative
                // to this view.
                frame.offset(getPaddingLeft(), getPaddingTop());
                frame.inset((int) (frame.width() * (1f - scale) / 2),
                        (int) (frame.height() * (1f - scale) / 2));

                if (frame.contains(x, y)) {
                    return i;
                }
            }
        }
        return INVALID_POSITION;
    }

    @SuppressWarnings("unused")
    private int findPositionByLocation(int[] location) {
        if (location == null) return INVALID_POSITION;
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            LayoutParams lp = getLayoutParams(child);

            if (lp.column <= location[0] && lp.column + lp.hSpan > location[0] && lp.row < location[1] && lp.row + lp.vSpan > location[1]) {
                return i;
            }
        }
        return INVALID_POSITION;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final int action = event.getAction();
        switch (action & MotionEvent.ACTION_MASK) {
        case MotionEvent.ACTION_DOWN:
            mLastMotionX = (int) event.getX();
            mLastMotionY = (int) event.getY();

            mTouchIndex = findPositionByMotion(mLastMotionX, mLastMotionY);
            break;
        case MotionEvent.ACTION_MOVE:
            int x = (int) event.getX();
            int y = (int) event.getY();
            if (Math.abs(x - mLastMotionX) > mTouchSlop || Math.abs(y - mLastMotionY) > mTouchSlop) {    
                mTouchIndex = INVALID_POSITION;
            }
            break;
        case MotionEvent.ACTION_UP:
            if (mTouchIndex != INVALID_POSITION) {
                performItemClick(mTouchIndex);
            }
            mTouchIndex = INVALID_POSITION;
        case MotionEvent.ACTION_CANCEL:
            mTouchIndex = INVALID_POSITION;
            break;
        default:
            break;
        }
        return true;
    }

    private void performItemClick(int position) {
        if (mLiatener != null) {
            playSoundEffect(SoundEffectConstants.CLICK);

            View child = getChildAt(position);
            mLiatener.onItemClick(this, child, position, child.getId());
        }
    }

    public void setOnItemClickListener(OnItemClickListener l) {
        mLiatener = l;
    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams {
        /** 列 */
        int column;
        /** 行 */
        int row;
        /** 横向占据 */
        int hSpan;
        /** 纵向占据 */
        int vSpan;
        /** 布局x坐标 */
        int x;
        /** 布局y坐标 */
        int y;
        /** 是否自定义 */
        boolean isCustom = false;

        public LayoutParams(int column, int row, int hSpan, int vSpan) {
            super(MATCH_PARENT, MATCH_PARENT);
            this.column = column;
            this.row = row;
            this.hSpan = hSpan;
            this.vSpan = vSpan;
        }

        public LayoutParams(LayoutParams source) {
            super(source);
            column = source.column;
            row = source.row;
            hSpan = source.hSpan;
            vSpan = source.vSpan;
        }

        /**
         * 根据尺寸设置布局
         * 
         * @param size
         *            控件大小
         * @param gap
         *            间距
         */
        private void setup(int size, int gap) {
            width = hSpan * size + gap * (hSpan - 1);
            height = vSpan * size + gap * (vSpan - 1);
            if (!isCustom) {
                x = column * (size + gap) + leftMargin;
                y = row * (size + gap) + topMargin;
            }
        }

        public void setX(int x) {
            this.x = x;
        }

        public void setY(int y) {
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getWidth() {
            return width;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public int getHeight() {
            return height;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(SpanContainer container, View view, int position, long id);
    }
}
