/*
 * Copyright (C) 2014 Ingenic Semiconductor
 * 
 * SunWenZhong(Fighter) <wenzhong.sun@ingenic.com, wanmyqawdr@126.com> LiJingWen(Kevin)
 * <jingwen.li@ingenic.com>
 * 
 * Elf/watch-manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.watchmanager;

import java.util.List;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.qrimage.CaptureActivity;
import com.ingenic.watchmanager.qrimage.CaptureActivity.OnCaptureCloseListener;
import com.ingenic.watchmanager.util.Utils;

public class WatchManagerActivity extends WMActivity implements
        NavigationFragment.OnNavigationEndListener {
    //private static final int REQUEST_SCAN_CODE = 101;
    private String mResult = "";
    private boolean mIsFirstTime = false;
    private boolean mBack = false;
    protected CaptureActivity mCaptureActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)) {
            Toast.makeText(this, getString(R.string.no_blue_tooth_device), Toast.LENGTH_LONG).show();
            this.finish();
            return;
        }
        setCaptureListener();
        IwdsLog.d(this,"===================> onCreate");
        toWhich();
    }

    private void setCaptureListener() {
        CaptureActivity
                .setOnBarActionChangeListener(new OnCaptureCloseListener() {
                    @Override
                    public void onCaptureCloseResult(String result, boolean back , CaptureActivity captureActivity) {
                        mResult = result;
                        mBack = back;
                        mCaptureActivity = captureActivity;
                        IwdsLog.d(this,"===================> onCaptureCloseResult result " + (null == mResult) + " " + "back " + mBack);
                    }
                });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        IwdsLog.d(this,"===================> onNewIntent");
        toWhich();
    }

    private void toWhich() {
        IwdsLog.d(this,"===================> toWhich");
        SharedPreferences sp = getSharedPreferences(Utils.SHARE_PREFERENCE,
                MODE_PRIVATE);
        mIsFirstTime = sp.getBoolean(Utils.SP_FIRST_USE, true);
        if (mIsFirstTime) {
            NavigationFragment fragment = new NavigationFragment(this);
            show(fragment, false, fragment.getFragmentTag());
        } else {
            BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
            if (adapter.isEnabled()) {
                scanOrMetro();
            }
        }
    }

    private void scanOrMetro() {
        IwdsLog.d(this,"===================> scanOrMetro");
        WatchManagerApplication app = WatchManagerApplication.get(this);
        List<String> addresses = app.getBondedAddresses();
        if (addresses == null || addresses.size() == 0) {
            toScan();
        } else {
            toMetro();
        }
    }

    @Override
    protected void onResume() {
        IwdsLog.d(this,"===================> onresume result " + (null == mResult) + " " + "back " + mBack);
        // TODO Auto-generated method stub
        super.onResume();
        if (mBack) {
            if (mResult != null) {
                IwdsLog.d(this, "QRcode in watch:" + mResult);
                String address = Utils.decodeAddress(mResult);
                IwdsLog.d(this, "Bluetooth address of watch:" + address);
                if (Utils.OPEN_BT_TIP.equals(address)) {
                    // TODO 手表蓝牙未打开
                    Toast.makeText(WatchManagerActivity.this,
                            "Bluetooth is disabled", Toast.LENGTH_SHORT).show();
                    IwdsLog.d(this, "result ---> result : " + "Bluetooth is disabled"+ " address: "+ address);
                    toScan();
                    return;
                }

                WatchManagerApplication app = WatchManagerApplication
                        .get(WatchManagerActivity.this);
                boolean bond = app.bondAddress(address);
                if (bond) {
                    IwdsLog.d(this, "result ---> result : success！");
                    if(null != mCaptureActivity)
                        mCaptureActivity.playBeepSoundAndVibrate();
                    toMetro();
                }else{
                    IwdsLog.d(this, "result ---> result : " + bond + " address: "+ address);
                    toScan();
                }
            } else {
                finish();
            }
        }
        mBack = false;
    }

    private void toScan() {
        IwdsLog.d(this, "to to toScan");
        Intent it = new Intent(this, CaptureActivity.class);
//        startActivityForResult(it, REQUEST_SCAN_CODE);
        startActivity(it);
    }

    private void toMetro() {
        IwdsLog.d(this, "to to Metro");
        Metro2Fragment fragment = new Metro2Fragment();
        show(fragment, false, fragment.getFragmentTag());
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        IwdsLog.d(this,"===================> onDestroy");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IwdsLog.d(this,"===================> onActivityResult");
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                if (!mIsFirstTime) {
                    scanOrMetro();
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onNavigationEnd() {
        IwdsLog.d(this,"===================> onNavigationEnd");
        SharedPreferences sp = getSharedPreferences(Utils.SHARE_PREFERENCE,
                MODE_PRIVATE);
        sp.edit().putBoolean(Utils.SP_FIRST_USE, false).commit();
        scanOrMetro();
    }
}
