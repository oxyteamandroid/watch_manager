/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  YuanBangLin(Ahlin)<banglin.yuan@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.account;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.ingenic.iwds.cloud.LoginListener;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.account.HttpAsyn.CallBack;
import com.ingenic.watchmanager.cloud.PersonalCloudModel;
import com.ingenic.watchmanager.personal.PersonalManager;
import com.ingenic.watchmanager.util.TUtils;

public class LoginFragment extends Fragment implements CallBack {
    public LoginFragment() {
    };

    /**
     * 用户名edittext
     */
    private EditText mNameText;
    /**
     * 用户名
     */
    private String mAccountName;
    /**
     * 密码edittext
     */
    private EditText mPwdText;
    /**
     * 密码
     */
    private String mPwd;
    /**
     * 注册button
     */
    private Button mRegistButton;
    /**
     * 登录Button
     */
    private Button mLoginButton;
    /**
     * 匿名登录button
     */
    private Button mAnonymousLoginButton;
    /**
     * 忘记密码button
     */
    private Button mForgetPwdButton;
    /**
     * 返回按钮
     */
    // private CircleImageView mBackImageView;
    /**
     * 异步请求实例
     */
    private HttpAsyn mHttpAsyn;
    /**
     * 个人信息云服务对象（上层封装）
     */
    private PersonalCloudModel mPersonalCloudModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mPersonalCloudModel = PersonalCloudModel.getInstance(getActivity());
        View rootView = inflater.inflate(R.layout.fragment_login, container,
                false);
        mHttpAsyn = new HttpAsyn(getActivity());
        initViews(rootView);
        return rootView;
    }

    // 初始化控件
    private void initViews(View rootView) {
        mNameText = (EditText) rootView.findViewById(R.id.name);
        mPwdText = (EditText) rootView.findViewById(R.id.pwd);
        mRegistButton = (Button) rootView.findViewById(R.id.regist_button);
        mRegistButton.setOnClickListener(new OnClickListener() {

            @SuppressLint("ShowToast")
            @Override
            public void onClick(View v) {
                // 跳注册界面
                getActivity().getIntent().putExtra("fragment",
                        RegistFragment.class.getName());
                getActivity().getIntent().putExtra("canBack", false);
                ((AccountActivity) getActivity()).initFragment(getActivity()
                        .getIntent());
            }
        });
        mLoginButton = (Button) rootView.findViewById(R.id.login_button);
        mLoginButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (verificationInput()) {
                    if (mHttpAsyn.checkNetwork()) {
                        if (mPersonalCloudModel == null) {
                            throw new NullPointerException(
                                    "PersonalCloudModel instance is null");
                        }
                        mPersonalCloudModel.login(mAccountName, mPwd,
                                new LoginListener() {

                                    @Override
                                    public void onSuccess() {
                                        PersonalManager.Personal
                                                .setAccountName(mAccountName);
                                        AccountActivity.sLoginListener
                                                .loginResult(true);
                                        if (getActivity() != null) {
                                            TUtils.showToast(getActivity(),
                                                    R.string.login_success,
                                                    Gravity.CENTER);
                                            getActivity().onBackPressed();
                                        }
                                    }

                                    @Override
                                    public void onFailure(int errCode,
                                            String errMsg) {
                                        TUtils.showToast(getActivity(), errMsg);
                                    }
                                });
                    }
                }
            }
        });
        mAnonymousLoginButton = (Button) rootView
                .findViewById(R.id.anonymous_login);
        mAnonymousLoginButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mHttpAsyn.checkNetwork()) {
                    if (mPersonalCloudModel == null) {
                        throw new NullPointerException(
                                "PersonalCloudModel instance is null");
                    }
                    mPersonalCloudModel.loginAnonymous(new LoginListener() {

                        @Override
                        public void onSuccess() {
                            PersonalManager.Personal
                                    .setAccountName(mAccountName);
                            AccountActivity.sLoginListener.loginResult(true);
                            if (getActivity() != null) {
                                TUtils.showToast(getActivity(),
                                        R.string.login_success, Gravity.CENTER);
                                getActivity().onBackPressed();
                            }
                        }

                        @Override
                        public void onFailure(int errCode, String errMsg) {
                            TUtils.showToast(getActivity(), errMsg);
                        }
                    });
                }
            }
        });
        mForgetPwdButton = (Button) rootView.findViewById(R.id.forget_pwd);
        mForgetPwdButton.setOnClickListener(new OnClickListener() {

            @SuppressLint("ShowToast")
            @Override
            public void onClick(View v) {
                // 忘记密码
                TUtils.showToast(getActivity(), "暂未开发！");
            }
        });
    }

    /**
     * 验证输入
     * @return
     */
    @SuppressLint("ShowToast")
    private boolean verificationInput() {
        mAccountName = mNameText.getText().toString().trim();
        mPwd = mPwdText.getText().toString().trim();
        if (mAccountName.isEmpty()) {
            TUtils.showToast(getActivity(), R.string.verification_name,
                    Gravity.CENTER);
        } else if (mPwd.isEmpty()) {
            TUtils.showToast(getActivity(), R.string.verification_pwd,
                    Gravity.CENTER);
        } else {
            return true;
        }
        return false;
    }

    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            String result = (String) msg.obj;
            analysisRequest(result);
        };
    };

    /**
     * 请求回调方法
     */
    @Override
    public void onRequestComplete(String result, int code) {
        Message msg = new Message();
        msg.what = 0;
        msg.obj = result;
        handler.sendMessage(msg);
    }

    /**
     *  解析请求结果
     * @param result
     */
    private void analysisRequest(String result) {
        if (result != null) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                int resultCode = jsonObject.getInt("resultCode");
                if (resultCode == 1) {
                    JSONObject json = jsonObject.getJSONObject("accountInfo");
                    if (!json.isNull("account")) {
                        PersonalManager.Personal.setAccountName(json
                                .getString("account"));
                    }
                    if (!json.isNull("nickname")) {
                        PersonalManager.Personal.setNickname(json
                                .getString("nickname"));
                    }
                    if (!json.isNull("headIcon")) {
                        PersonalManager.Personal.setHeadloadUrl(json
                                .getString("headIcon"));
                    }
                    AccountActivity.sLoginListener.loginResult(true);
                    if (getActivity() != null) {
                        TUtils.showToast(getActivity(), R.string.login_success,
                                Gravity.CENTER);
                        getActivity().onBackPressed();
                    }
                } else {
                    if (!jsonObject.isNull("reason")) {
                        TUtils.showToast(getActivity(),
                                jsonObject.getString("reason"));
                    } else {
                        TUtils.showToast(getActivity(), R.string.login_failure,
                                Gravity.CENTER);
                    }
                    AccountActivity.sLoginListener.loginResult(false);
                }
            } catch (JSONException e) {
                TUtils.showToast(getActivity(), R.string.login_abnormal,
                        Gravity.CENTER);
                e.printStackTrace();
                AccountActivity.sLoginListener.loginResult(false);
            }
        } else {
            TUtils.showToast(getActivity(), R.string.login_abnormal,
                    Gravity.CENTER);
            AccountActivity.sLoginListener.loginResult(false);
        }
    }
}
