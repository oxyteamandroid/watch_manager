/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  YuanBangLin(Ahlin)<banglin.yuan@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.account;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.account.HttpAsyn.CallBack;
import com.ingenic.watchmanager.personal.PersonalManager;
import com.ingenic.watchmanager.util.TUtils;
import com.ingenic.watchmanager.view.CircleImageView;

@SuppressLint("HandlerLeak")
public class UpdatePwdFragment extends Fragment implements CallBack {
    public UpdatePwdFragment() {
    }

    private HttpAsyn mHttpAsyn;

    // 返回View
    private CircleImageView mBackView;

    // 帐号edittext
    private EditText mOldPwdText;
    private String mOldPwd;
    // 输入密码edittext
    private EditText mUserPwdText;
    private String mUserPwd;
    // 再次输入密码edittext
    private EditText mAgainPwdText;
    private String mAgainPwd;
    // 提交button
    private Button mSubmitButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_update_pwd,
                container, false);
        mHttpAsyn = new HttpAsyn(getActivity());
        initViews(rootView);
        return rootView;
    }

    /**
     *  初始化控件
     * @param rootView
     */
    private void initViews(View rootView) {
        mBackView = (CircleImageView) rootView.findViewById(R.id.back);
        mBackView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        mOldPwdText = (EditText) rootView.findViewById(R.id.old_pwd);
        mUserPwdText = (EditText) rootView.findViewById(R.id.user_pwd);
        mAgainPwdText = (EditText) rootView.findViewById(R.id.user_pwd_);
        mSubmitButton = (Button) rootView.findViewById(R.id.update_submit);
        mSubmitButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (verificationInput()) {
                    // 请求修改密码接口
                    if (PersonalManager.isLogin())
                        mHttpAsyn.doGetAsyn("/account/editpsd/?account="
                                + PersonalManager.Personal.getAccountName()
                                + "&psd=" + MD5Utils.encrypt(mOldPwd)
                                + "&psdNew=" + MD5Utils.encrypt(mAgainPwd),
                                UpdatePwdFragment.this, HttpAsyn.PSD_URI_CODE);
                }
            }
        });
    }

    /**
     *  验证输入
     * @return
     */
    private boolean verificationInput() {
        mOldPwd = mOldPwdText.getText().toString().trim();
        if (mOldPwd == null || "".equals(mOldPwd)) {
            TUtils.showToast(getActivity(), R.string.verification_name,
                    Gravity.CENTER);
            return false;
        }
        mUserPwd = mUserPwdText.getText().toString().trim();
        mAgainPwd = mAgainPwdText.getText().toString().trim();
        if (mUserPwd == null || "".equals(mUserPwd) || mAgainPwd == null
                || "".equals(mAgainPwd)) {
            TUtils.showToast(getActivity(), R.string.verification_pwd,
                    Gravity.CENTER);
            return false;
        }
        if (!mUserPwd.equals(mAgainPwd)) {
            TUtils.showToast(getActivity(), R.string.contrast_pwd_hint,
                    Gravity.CENTER);
            return false;
        }
        return true;
    }

    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case 1:
                TUtils.showToast(getActivity(), R.string.update_success,
                        Gravity.CENTER);
                if (getActivity() != null)
                    getActivity().onBackPressed();
                break;
            case -1:
                TUtils.showToast(getActivity(), (String) msg.obj);
                break;
            default:
                break;
            }
        };
    };

    @Override
    public void onRequestComplete(String result, int code) {
        // 请求服务器返回结果回调
        if (result != null) {
            Message msg = new Message();
            try {
                JSONObject jsonObject = new JSONObject(result);
                int resultCode = jsonObject.getInt("resultCode");
                String reason = null;
                if (resultCode != 1) {
                    if (!jsonObject.isNull("reason")) {
                        reason = jsonObject.getString("reason");
                    }
                }
                msg.what = resultCode;
                msg.obj = reason;
                if (code == HttpAsyn.PSD_URI_CODE) {
                    mHandler.sendMessage(msg);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                if (getActivity() != null) {
                    msg.obj = getActivity().getResources().getString(
                            R.string.request_failed);
                    msg.what = -1;
                    mHandler.sendMessage(msg);
                }
            }
        }
    }
}
