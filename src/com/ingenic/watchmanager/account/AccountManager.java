/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  YuanBangLin(Ahlin)<banglin.yuan@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.account;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ingenic.iwds.utils.IwdsAssert;

import android.text.TextUtils;

public class AccountManager {

    /**
     * 手机号码正则
     */
    private static final String TELEPHONE_REGULARITY = "^1\\d{10}$";
    
    /**
     * email正则
     */
    private static final String EMAIL_REGULARITY = "^([a-zA-Z0-9]*[-_]?[a-zA-Z0-9]+)*@([a-zA-Z0-9]*[-_]?[a-zA-Z0-9]+)+[\\.][A-Za-z]{2,3}([\\.][A-Za-z]{2})?$";

    
    /**
     * 保存登录账户
     */
    private Account mAccount = new Account();
    
    /**
     * 单例对象
     */
    private static AccountManager accountHelp;
    
    /**
     * 私有化构造函数
     */
    private AccountManager(){}

    /**
     * 返回单例
     * @return
     */
    public static AccountManager getInstance() {
        if (accountHelp == null)
            accountHelp = new AccountManager();
        return accountHelp;
    }
    
    /**
     * 设置登录账户
     * @param account
     */
    public void setLoginAccount(Account account){
        IwdsAssert.dieIf(this, account == null, "Account is null");
        if(mAccount != null){
            mAccount.setUserName(account.getUserName());
            mAccount.setUserPassword(account.getUserPassword());
        }
    }
    
    /**
     * 获取登录账户
     * @return
     */
    public Account getLoginAccount(){
        IwdsAssert.dieIf(this, mAccount == null, "Account is null");
        return this.mAccount;
    }
    
    /**
     * 判断是否登录
     * @return
     */
    public boolean isLogin() {
        if (mAccount == null || TextUtils.isEmpty(mAccount.getUserName()))
            return false;
        return true;
    }
    
    /**
     * 验证号码是否为手机号码
     * @param number
     * @return
     */
    public static boolean isPhoneNumber(String number) {
        if(TextUtils.isEmpty(number))
            return false;
        
        Pattern pattern = Pattern.compile(TELEPHONE_REGULARITY);
        Matcher matcher = pattern.matcher(number);
        return matcher.matches();
    }

    /**
     * 验证邮箱是否合法
     * @param email
     * @return
     */
    public static boolean isEmail(String email) {
        if(TextUtils.isEmpty(email))
            return false;
        
        Pattern pattern = Pattern.compile(EMAIL_REGULARITY);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
