/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  YuanBangLin(Ahlin)<banglin.yuan@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.account;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.ingenic.iwds.cloud.AccountListener;
import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.cloud.PersonalCloudModel;
import com.ingenic.watchmanager.util.TUtils;

@SuppressLint("HandlerLeak")
public class RegistFragment extends Fragment {
    public RegistFragment() {
    }

    private HttpAsyn mHttpAsyn;

    // 用户名edittext
    private EditText mRegistNameText;
    private String mRegistName;

    private LinearLayout mVerificationLinearLayout;
    // 验证码edittext
    private EditText mVerificationText;
    private String mVerification;

    // 获取验证码button
    private Button mGetVerificationBtn;

    // 密码edittext
    private EditText mRegistPwdText;
    private String mRegistPwd;

    private EditText mRegistPwdAgainText;
    private String mRegistPwdAgain;
    // 注册button
    private Button mRegistButton;

    private PersonalCloudModel mPersonalCloudModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mPersonalCloudModel = PersonalCloudModel.getInstance(getActivity());
        View rootView = inflater.inflate(R.layout.fragment_regist, container,
                false);
        mHttpAsyn = new HttpAsyn(getActivity());
        initViews(rootView);
        return rootView;
    }

    /**
     * 初始化控件
     * @param rootView
     */
    private void initViews(View rootView) {
        mVerificationLinearLayout = (LinearLayout) rootView
                .findViewById(R.id.verification_linearlayout);
        mVerificationLinearLayout.setVisibility(View.GONE);

        mRegistNameText = (EditText) rootView
                .findViewById(R.id.regist_name_text);
        mRegistNameText.setHint(R.string.name_hint);
        mRegistNameText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                    int count) {
                mRegistName = s.toString();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                    int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (AccountManager.isPhoneNumber(mRegistName)) {
                    mVerificationLinearLayout.setVisibility(View.VISIBLE);
                } else {
                    mVerificationLinearLayout.setVisibility(View.GONE);
                }
            }
        });

        mVerificationText = (EditText) rootView
                .findViewById(R.id.verification_text);
        mGetVerificationBtn = (Button) rootView
                .findViewById(R.id.get_verification_btn);
        mGetVerificationBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                IwdsAssert.dieIf(this, mPersonalCloudModel == null,
                        "PersonalCloudModel instance is null");
                IwdsAssert.dieIf(this, !mPersonalCloudModel.getIsInit(),
                        "Cloud personal service initialization is a failure");

                mRegistName = mRegistNameText.getText().toString().trim();
                mPersonalCloudModel.requestPhoneVerifyCode(mRegistName,
                        new AccountListener() {
                            @Override
                            public void onSuccess() {
                                mGetVerificationBtn.setClickable(false);
                            }

                            @Override
                            public void onFailure(int errCode, String errMsg) {
                                mGetVerificationBtn.setClickable(false);
                                TUtils.showToast(getActivity(), errMsg);
                            }
                        });
            }
        });

        mRegistPwdText = (EditText) rootView.findViewById(R.id.regist_pwd_text);
        mRegistPwdAgainText = (EditText) rootView
                .findViewById(R.id.regist_pwd_again_text);
        mRegistButton = (Button) rootView.findViewById(R.id.regist_btn);
        mRegistButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!inputVerification())
                    return;

                if (!mHttpAsyn.checkNetwork()) {
                    TUtils.showToast(getActivity(),
                            R.string.network_connection, Gravity.CENTER);
                    return;
                }

                IwdsAssert.dieIf(this, mPersonalCloudModel == null,
                        "PersonalCloudModel instance is null");
                IwdsAssert.dieIf(this, !mPersonalCloudModel.getIsInit(),
                        "Cloud personal service initialization is a failure");

                if (AccountManager.isPhoneNumber(mRegistName)) {
                    mPersonalCloudModel.registerUserWithPhone(mRegistName,
                            mRegistPwd, mVerification, mAccountLoginListener);
                } else if (AccountManager.isEmail(mRegistName)) {
                    mPersonalCloudModel.registerUserWithEmail(mRegistName,
                            mRegistPwd, mAccountLoginListener);
                } else {
                    mPersonalCloudModel.registerUser(mRegistName, mRegistPwd,
                            mAccountLoginListener);
                }
            }
        });
    }

    private AccountListener mAccountLoginListener = new AccountListener() {

        @Override
        public void onSuccess() {
            TUtils.showToast(getActivity(), R.string.regist_success,
                    Gravity.CENTER);
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
        }

        @Override
        public void onFailure(int errCode, String errMsg) {
            TUtils.showToast(getActivity(), errMsg);
        }
    };

    /**
     * 验证输入
     * @return
     */
    private boolean inputVerification() {
        mRegistName = mRegistNameText.getText().toString().trim();
        mRegistPwd = mRegistPwdText.getText().toString().trim();
        mRegistPwdAgain = mRegistPwdAgainText.getText().toString().trim();
        if (AccountManager.isPhoneNumber(mRegistName)) {
            mVerification = mVerificationText.getText().toString().trim();
        }
        if (TextUtils.isEmpty(mRegistName) || TextUtils.isEmpty(mRegistPwd)
                || TextUtils.isEmpty(mRegistPwdAgain)) {
            TUtils.showToast(getActivity(), R.string.input_error,
                    Gravity.CENTER);
            return false;
        } else {
            if (!mRegistPwd.equals(mRegistPwdAgain)) {
                TUtils.showToast(getActivity(), R.string.contrast_pwd_hint,
                        Gravity.CENTER);
                return false;
            }
            if (AccountManager.isPhoneNumber(mRegistName)) {
                if (TextUtils.isEmpty(mVerification)) {
                    TUtils.showToast(getActivity(), R.string.input_error,
                            Gravity.CENTER);
                    return false;
                }
            }
        }
        return true;
    }
}
