/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  YuanBangLin(Ahlin)<banglin.yuan@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.account;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.cloud.PersonalCloudModel;

public class AccountActivity extends Activity {
    private PersonalCloudModel mPersonalCloudModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        mPersonalCloudModel = PersonalCloudModel.getInstance(this);
        initFragment(getIntent());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mPersonalCloudModel != null) {
            mPersonalCloudModel.startConnect();
        }
    }

    @Override
    public void onStop() {
        super.onDestroy();
        if (mPersonalCloudModel != null) {
            mPersonalCloudModel.disConnect();
        }
    }

    public void initFragment(Intent intent) {
        String fragmentName = intent.getStringExtra("fragment");
        boolean canBack = intent.getBooleanExtra("canBack", false);
        if (fragmentName != null) {
            Fragment fragment = Fragment.instantiate(this, fragmentName);
            showFragment(fragment, canBack, "account");
        }
    }

    private void showFragment(Fragment fragment, boolean canBack, String tag) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        // ft.setCustomAnimations(R.anim.fragment_bottom_in,
        // R.anim.fragment_right_out);
        ft.replace(R.id.account, fragment, tag);
        if (canBack) {
            ft.addToBackStack("back");
        }
        ft.commit();
    }

    static LoginListener sLoginListener;

    public static void login(Context context, LoginListener loginListener) {
        AccountActivity.sLoginListener = loginListener;
        Intent intent = new Intent(context, AccountActivity.class);
        intent.putExtra("fragment", LoginFragment.class.getName());
        context.startActivity(intent);
    }

    public interface LoginListener {
        public void loginResult(boolean isSuccess);
    }
}
