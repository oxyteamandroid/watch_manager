/*
 * Copyright (C) 2014 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Edit by: nongjiabao<jiabao.nong#ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.db;

import android.provider.BaseColumns;

public class WatchManagerContracts {
    static final String DATABASE_NAME = "watch_manager.db";
    static final String SQL_CREATE = "CREATE TABLE ";
    static final String SQL_DROP = "DROP TABLE IF EXISTS ";

    public static class Tables {
        public static final String TABLE_METRO = "_metro";
        public static final String TABLE_BLACK = "_black";
        public static final String WEATHER = "weather";
        public static final String CITY = "_city";// 常用城市
        public static final String NOTIFICATION = "notifications";
        public static final String CITY2 = "city";// 已添加的城市（ViewPager中显示的）
        public static final String DOWN_LIST = "downlist";// 下载应用列表
        public static final String TODAY_STEPS = "todaysteps";
        public static final String EVERYDAY_STEPS = "everydaysteps";
        public static final String HEART_RATES = "heartrates";
        public static final String EVENT = "_event";
        public static final String REMINDER = "_reminder";
    }

    public static class MetroColumns implements BaseColumns {
        public static final String _COLUMN = "_column";
        public static final String _ROW = "_row";
        public static final String _TYPE = "_type";
    }

    public static class WeatherColumns implements BaseColumns {
        public static final String CITY = "_city";
        public static final String DATE = "_date";
        public static final String DAY_INDEX = "day_of_week_int";
        public static final String DAY_OF_WEEK = "day_of_week";
        public static final String UPDATE_TIME = "update_time";
        public static final String WEATHER_CODE = "weather_code";
        public static final String WEATHER = "weather";
        public static final String TEMP_UNIT = "temp_unit";
        public static final String CURRENT_TEMP = "current_temp";
        public static final String MINIMUNTEMP = "minimum_temp";
        public static final String MAXIMUMTEMP = "maximum_temp";
        public static final String WOEID = "woeid";
    }

    public static class CityColumns implements BaseColumns {
        public static final String NAME = "_name";
        public static final String TIME = "_time";
        public static final String WOEID = "_woeid";
    }

    public static class City2Columns implements BaseColumns {
        public static final String NAME = "name";
        public static final String UPDATE_TIME = "update_time";
        public static final String LOCATED = "located";
        public static final String ISDEFAULT = "isdefault";
        public static final String UNIT = "unit";
        public static final String WOEID = "woeid";
        public static final String TIMEZONE = "timezone";
        public static final String LANG = "lang";
    }

    public static class DownListColumns implements BaseColumns {
        public static final String ID = "id";
        public static final String PROGRESS = "progress";
        public static final String NAME = "name";
        public static final String ICON = "icon";

        public static final String PKNAME = "pkName";

        public static final String DESCRIPTION = "description";
        public static final String URL = "url";
        public static final String PATH = "path";
        public static final String FINISH = "finish";
        public static final String CATEGORY = "category";
        public static final String TIME = "time";
        public static final String SIZE = "size";
        public static final String COMPLETE = "complete";
        public static final String BAK_URL = "bakUrl";
    }

    public static class TodayStepsColumns implements BaseColumns {
        public static final String ID = "id";
        public static final String DATE = "date";
        public static final String HOUR = "hour";
        public static final String STEPS = "steps";
    }

    public static class EverydayStepsColumns implements BaseColumns {
        public static final String ID = "id";
        public static final String DATE = "date";
        public static final String STEPS = "steps";
    }

    public static class HeartRatesColumns implements BaseColumns {
        public static final String RATE = "rate";
        public static final String TIME = "time";
    }

    public static class NotificationColumns implements BaseColumns {
        public static final String PACKAGE_NAME = "package_name";
        public static final String ENABLED = "enabled";
        public static final String TIME = "time";
    }

    public static class EventColumns implements BaseColumns {
        public static final String TITLE = "title";
        public static final String EVENTLOCATION = "eventLocation";
        public static final String DESCRIPTION = "description";
        public static final String DTSTART = "dtStart";
        public static final String DTEND = "dtEnd";
        public static final String EVENTTIMEZONE = "eventTimezone";
        public static final String DURATION = "duration";
        public static final String ALLDAY = "allDay";
        public static final String HASALARM = "hasAlarm";
        public static final String RRULE = "rrule";
    }

    public static class ReminderColumns implements BaseColumns {
        public static final String EVENTID = "event_id";
        public static final String MINUTES = "minutes";
        public static final String METHOD = "method";
    }
}
