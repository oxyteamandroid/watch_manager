/*
 * Copyright (C) 2014 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public abstract class Operator<T> {
    private final SQLiteHelper mHelper;
    private final String mTable;

    public Operator(Context context, String tableName) {
        mHelper = SQLiteHelper.getInstance(context);
        mTable = tableName;
    }

    public long insert(T t) {
        ContentValues values = toValues(t);
        return insert(values);
    }

    protected long insert(ContentValues values) {
        long result = -1;
        synchronized (mHelper) {
            SQLiteDatabase db = mHelper.getWritableDatabase();
            result = db.insert(mTable, null, values);
            db.close();
        }
        return result;
    }

    protected abstract ContentValues toValues(T t);

    public abstract int update(T t);

    public int update(T t, String where, String[] selections) {
        ContentValues values = toValues(t);
        return update(values, where, selections);
    }

    protected int update(ContentValues values, String where, String[] selections) {
        int result = -1;
        synchronized (mHelper) {
            SQLiteDatabase db = mHelper.getWritableDatabase();
            result = db.update(mTable, values, where, selections);
            db.close();
        }
        return result;
    }

    public T query(String[] columns, String selection, String[] selectionArgs, String groupBy,
            String having, String orderBy) {
        T t = null;
        synchronized (mHelper) {
            SQLiteDatabase db = mHelper.getReadableDatabase();
            Cursor cursor = db.query(mTable, columns, selection, selectionArgs, groupBy, having,
                    orderBy);

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    t = fromCursor(cursor);
                }
                cursor.close();
            }

            db.close();
        }
        return t;
    }

    protected abstract T fromCursor(Cursor cursor);

    public List<T> queryAll() {
        return queryAll(null);
    }

    public List<T> queryAll(String selection, String[] selectionArgs, String orderBy) {
        List<T> ts = null;
        synchronized (mHelper) {
            SQLiteDatabase db = mHelper.getReadableDatabase();
            Cursor cursor = db.query(mTable, null, selection, selectionArgs, null, null, orderBy);

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    ts = new ArrayList<T>();

                    do {
                        T t = fromCursor(cursor);
                        if (t != null) {
                            ts.add(t);
                        }
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }

            db.close();
        }
        return ts;
    }

    public List<T> queryAll(String orderBy) {
        List<T> ts = null;
        synchronized (mHelper) {
            SQLiteDatabase db = mHelper.getReadableDatabase();
            Cursor cursor = db.query(mTable, null, null, null, null, null, orderBy);

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    ts = new ArrayList<T>();

                    do {
                        T t = fromCursor(cursor);
                        if (t != null) {
                            ts.add(t);
                        }
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }

            db.close();
        }
        return ts;
    }

    public int delete(String where, String[] selections) {
        int result = -1;
        synchronized (mHelper) {
            SQLiteDatabase db = mHelper.getWritableDatabase();
            result = db.delete(mTable, where, selections);
            db.close();
        }
        return result;
    }

    public int deleteAll() {
        return delete(null, null);
    }

    public abstract int delete(T t);

    public abstract boolean hasData(T t);

    public void save(T t) {
        if (hasData(t)) {
            update(t);
        } else {
            insert(t);
        }
    }

    protected SQLiteHelper getSQLiteHelper() {
        return mHelper;
    }
}
