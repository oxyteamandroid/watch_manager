/*
 * Copyright (C) 2014 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper {
    private static SQLiteHelper sInstance;

    private SQLiteHelper(Context context) {
        super(context, WatchManagerContracts.DATABASE_NAME, null, 1);
    }

    public synchronized static SQLiteHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SQLiteHelper(context);
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createMetroTable(db);
        createBlackTable(db);
        createWeatherTable(db);
        createCityTable(db);
        createCity2Table(db);
        createNotificationTable(db);
        createDownListTable(db);
        createTodayStepsTable(db);
        createEverydayStepsTable(db);
        createHeartRatesTable(db);
        createEventTable(db);
        createReminderTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(WatchManagerContracts.SQL_DROP + WatchManagerContracts.Tables.TABLE_METRO);
        db.execSQL(WatchManagerContracts.SQL_DROP + WatchManagerContracts.Tables.TABLE_BLACK);
        db.execSQL(WatchManagerContracts.SQL_DROP + WatchManagerContracts.Tables.WEATHER);
        db.execSQL(WatchManagerContracts.SQL_DROP + WatchManagerContracts.Tables.CITY);
        db.execSQL(WatchManagerContracts.SQL_DROP + WatchManagerContracts.Tables.NOTIFICATION);
        db.execSQL(WatchManagerContracts.SQL_DROP + WatchManagerContracts.Tables.CITY2);
        db.execSQL(WatchManagerContracts.SQL_DROP + WatchManagerContracts.Tables.DOWN_LIST);
        db.execSQL(WatchManagerContracts.SQL_DROP + WatchManagerContracts.Tables.TODAY_STEPS);
        db.execSQL(WatchManagerContracts.SQL_DROP + WatchManagerContracts.Tables.EVERYDAY_STEPS);
        db.execSQL(WatchManagerContracts.SQL_DROP + WatchManagerContracts.Tables.HEART_RATES);
        db.execSQL(WatchManagerContracts.SQL_DROP + WatchManagerContracts.Tables.EVENT);
        db.execSQL(WatchManagerContracts.SQL_DROP + WatchManagerContracts.Tables.REMINDER);

        onCreate(db);
    }

    private void createMetroTable(SQLiteDatabase db) {
        db.execSQL(WatchManagerContracts.SQL_CREATE + WatchManagerContracts.Tables.TABLE_METRO
                + " (" + WatchManagerContracts.MetroColumns._ID + " INTEGER PRIMARY KEY, "
                + WatchManagerContracts.MetroColumns._COLUMN + " INTEGER, "
                + WatchManagerContracts.MetroColumns._ROW + " INTEGER, "
                + WatchManagerContracts.MetroColumns._TYPE + " INTEGER);");
    }

    private void createBlackTable(SQLiteDatabase db) {
        db.execSQL(WatchManagerContracts.SQL_CREATE + WatchManagerContracts.Tables.TABLE_BLACK
                + " (" + WatchManagerContracts.MetroColumns._ID + " INTEGER PRIMARY KEY);");
    }

    private void createWeatherTable(SQLiteDatabase db) {
        db.execSQL(WatchManagerContracts.SQL_CREATE + WatchManagerContracts.Tables.WEATHER + " ("
                + WatchManagerContracts.WeatherColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + WatchManagerContracts.WeatherColumns.CITY + " TEXT NOT NULL, "
                + WatchManagerContracts.WeatherColumns.WOEID + " TEXT NOT NULL, "
                + WatchManagerContracts.WeatherColumns.DATE + " TEXT NOT NULL, "
                + WatchManagerContracts.WeatherColumns.DAY_INDEX + " INTEGER NOT NULL, "
                + WatchManagerContracts.WeatherColumns.DAY_OF_WEEK + " TEXT NOT NULL, "
                + WatchManagerContracts.WeatherColumns.UPDATE_TIME + " TEXT, "
                + WatchManagerContracts.WeatherColumns.WEATHER_CODE + " INTEGER NOT NULL, "
                + WatchManagerContracts.WeatherColumns.WEATHER + " TEXT NOT NULL, "
                + WatchManagerContracts.WeatherColumns.TEMP_UNIT + " TEXT NOT NULL, "
                + WatchManagerContracts.WeatherColumns.CURRENT_TEMP + " INTEGER, "
                + WatchManagerContracts.WeatherColumns.MINIMUNTEMP + " INTEGER NOT NULL, "
                + WatchManagerContracts.WeatherColumns.MAXIMUMTEMP + " INTEGER NOT NULL);");
    }

    private void createCityTable(SQLiteDatabase db) {
        db.execSQL(WatchManagerContracts.SQL_CREATE + WatchManagerContracts.Tables.CITY + " ("
                + WatchManagerContracts.CityColumns.WOEID + " TEXT, "
                + WatchManagerContracts.CityColumns.NAME + " TEXT, "
                + WatchManagerContracts.CityColumns.TIME + " INTEGER);");
    }

    private void createCity2Table(SQLiteDatabase db) {
        db.execSQL(WatchManagerContracts.SQL_CREATE + WatchManagerContracts.Tables.CITY2 + " ("
                + WatchManagerContracts.City2Columns._ID + " INTEGER PRIMARY KEY,"
                + WatchManagerContracts.City2Columns.NAME + " TEXT,"
                + WatchManagerContracts.City2Columns.WOEID + " TEXT,"
                + WatchManagerContracts.City2Columns.UPDATE_TIME + " INTEGER,"
                + WatchManagerContracts.City2Columns.LOCATED + " BOOLEAN,"
                + WatchManagerContracts.City2Columns.ISDEFAULT + " BOOLEAN,"
                + WatchManagerContracts.City2Columns.TIMEZONE + " TEXT,"
                + WatchManagerContracts.City2Columns.LANG + " TEXT,"
                + WatchManagerContracts.City2Columns.UNIT + " TEXT);");
    }

    private void createNotificationTable(SQLiteDatabase db) {
        db.execSQL(WatchManagerContracts.SQL_CREATE + WatchManagerContracts.Tables.NOTIFICATION
                + " (" + WatchManagerContracts.NotificationColumns.PACKAGE_NAME + " TEXT, "
                + WatchManagerContracts.NotificationColumns.ENABLED + " INTEGER, "
                + WatchManagerContracts.NotificationColumns.TIME + " INTEGER);");
    }

    private void createDownListTable(SQLiteDatabase db) {
        db.execSQL(WatchManagerContracts.SQL_CREATE + WatchManagerContracts.Tables.DOWN_LIST + " ("
                + WatchManagerContracts.DownListColumns.ID + " INTEGER PRIMARY KEY, "
                + WatchManagerContracts.DownListColumns.PROGRESS + " INTEGER, "
                + WatchManagerContracts.DownListColumns.NAME + " TEXT, "
                + WatchManagerContracts.DownListColumns.ICON + " TEXT, "
                + WatchManagerContracts.DownListColumns.PKNAME + " TEXT, "
                + WatchManagerContracts.DownListColumns.DESCRIPTION + " TEXT, "
                + WatchManagerContracts.DownListColumns.URL + " TEXT, "
                + WatchManagerContracts.DownListColumns.PATH + " TEXT, "
                + WatchManagerContracts.DownListColumns.CATEGORY + " TEXT, "
                + WatchManagerContracts.DownListColumns.TIME + " INTEGER, "
                + WatchManagerContracts.DownListColumns.SIZE + " INTEGER, "
                + WatchManagerContracts.DownListColumns.COMPLETE + " INTEGER, "
                + WatchManagerContracts.DownListColumns.BAK_URL + " TEXT, "
                + WatchManagerContracts.DownListColumns.FINISH + " TEXT);");
    }

    private void createTodayStepsTable(SQLiteDatabase db) {
        db.execSQL(WatchManagerContracts.SQL_CREATE + WatchManagerContracts.Tables.TODAY_STEPS
                + " (" + WatchManagerContracts.TodayStepsColumns.ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + WatchManagerContracts.TodayStepsColumns.DATE + " TEXT  NOT NULL ,"
                + WatchManagerContracts.TodayStepsColumns.HOUR + " INTEGER  NOT NULL ,"
                + WatchManagerContracts.TodayStepsColumns.STEPS + " INTEGER);");
    }

    private void createEverydayStepsTable(SQLiteDatabase db) {
        db.execSQL(WatchManagerContracts.SQL_CREATE + WatchManagerContracts.Tables.EVERYDAY_STEPS
                + " (" + WatchManagerContracts.EverydayStepsColumns.ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + WatchManagerContracts.EverydayStepsColumns.DATE + " TEXT  NOT NULL ,"
                + WatchManagerContracts.EverydayStepsColumns.STEPS + " INTEGER);");
    }

    private void createHeartRatesTable(SQLiteDatabase db) {
        db.execSQL(WatchManagerContracts.SQL_CREATE + WatchManagerContracts.Tables.HEART_RATES
                + " (" + WatchManagerContracts.HeartRatesColumns._ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + WatchManagerContracts.HeartRatesColumns.RATE + " INTEGER, "
                + WatchManagerContracts.HeartRatesColumns.TIME + " INTEGER);");
    }

    private void createEventTable(SQLiteDatabase db) {
        db.execSQL(WatchManagerContracts.SQL_CREATE + WatchManagerContracts.Tables.EVENT + " ("
                + WatchManagerContracts.EventColumns._ID + " INTEGER, "
                + WatchManagerContracts.EventColumns.TITLE + " TEXT, "
                + WatchManagerContracts.EventColumns.EVENTLOCATION + " TEXT, "
                + WatchManagerContracts.EventColumns.DESCRIPTION + " TEXT, "
                + WatchManagerContracts.EventColumns.DTSTART + " INTEGER, "
                + WatchManagerContracts.EventColumns.DTEND + " INTEGER, "
                + WatchManagerContracts.EventColumns.EVENTTIMEZONE + " TEXT, "
                + WatchManagerContracts.EventColumns.DURATION + " TEXT, "
                + WatchManagerContracts.EventColumns.ALLDAY + " INTEGER, "
                + WatchManagerContracts.EventColumns.HASALARM + " INTEGER, "
                + WatchManagerContracts.EventColumns.RRULE + " TEXT);");
    }

    private void createReminderTable(SQLiteDatabase db) {
        db.execSQL(WatchManagerContracts.SQL_CREATE + WatchManagerContracts.Tables.REMINDER + " ("
                + WatchManagerContracts.ReminderColumns._ID + " INTEGER, "
                + WatchManagerContracts.ReminderColumns.EVENTID + " INTEGER, "
                + WatchManagerContracts.ReminderColumns.MINUTES + " INTEGER, "
                + WatchManagerContracts.ReminderColumns.METHOD + " INTEGER);");
    }
}
