/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/WatchManager/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.notification;

import android.app.Notification;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.ingenic.iwds.utils.IwdsLog;

/**
 * 对notification扩展消息处理的类<br>
 * 处理API16以下版本
 *
 * @author tZhang
 */

class ExtraCompat {
    public static final String SEPARATOR_CHAR = "$$";
    public static final String SPLIT_CHAR = "\\$\\$";
    protected Context mContext;

    public ExtraCompat(Context context) {
        mContext = context;
    }

    public String getExtraData(Notification notification) {
        RemoteViews views = notification.contentView;
        if (views == null)
            return "";

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try {
            ViewGroup localView = (ViewGroup) inflater.inflate(
                    views.getLayoutId(), null);
            views.reapply(mContext, localView);
            return dumpViewGroup(localView);
        } catch (Exception e) {
            return "";
        }
    }

    protected String dumpViewGroup(ViewGroup group) {
        String text = "";
        int count = group.getChildCount();

        for (int i = 0; i < count; i++) {
            View v = group.getChildAt(i);
            if (v instanceof Button
                    || v.getClass().toString()
                            .contains("android.widget.DateTimeView")) {
                IwdsLog.e(this, "Ignore Buttons or DateTimeViews.");
                continue;
            }

            if (v instanceof TextView) {
                TextView tv = (TextView) v;
                CharSequence tvText = tv.getText();

                if (tvText != null) {
                    String tmp = tvText.toString();
                    if (tmp.equalsIgnoreCase("")) {
                        IwdsLog.e(this, "Ignore unknow text:" + tmp);
                        continue;
                    }

                    text += tmp + SEPARATOR_CHAR;
                }
            } else if (v instanceof ViewGroup) {
                text += dumpViewGroup((ViewGroup) v);
            }
        }

        return text;
    }

    protected String replace(String content, String packageName) {
        if (TextUtils.isEmpty(content)) {
            return "";
        }

        String[] array = content.split(SPLIT_CHAR);

        String text = "";
        int len = array.length;
        if (packageName.equals("com.whatsapp")) {
            if ("WhatsApp".equalsIgnoreCase(array[0]) && len > 3) {
                // 多个好友发送多条消息
                // WhatsApp
                // friend 1:msg
                // friend 2:msg
                // …
                // 2条新消息
                text += array[len - 3];
            } else {
                if (len == 3) {
                    // 一个好友发送第一条消息
                    // friend
                    // msg
                    // 1条新消息
                    text += array[0] + ":" + array[1];
                } else {
                    // 一个好友连续发送多条消息
                    // friend
                    // msg 1
                    // msg 2
                    // …
                    // n条新消息
                    text += array[0] + ":" + array[len - 3];
                }
            }
        } else {
            // 普通消息
            // title:content
            for (int i = 0; i < len; i++) {
                text += array[i];
                if (i == 0) {
                    text += ":";
                }
            }
        }

        return text;
    }
}
