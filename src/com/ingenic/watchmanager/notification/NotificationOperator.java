/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.notification;

import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.ingenic.watchmanager.db.Operator;
import com.ingenic.watchmanager.db.WatchManagerContracts;

public class NotificationOperator extends Operator<NoticeAppItem> {

    private static NotificationOperator sInstance;

    private NotificationOperator(Context context) {
        super(context, WatchManagerContracts.Tables.NOTIFICATION);
    }

    public synchronized static NotificationOperator getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new NotificationOperator(context);
        }
        return sInstance;
    }

    @Override
    protected ContentValues toValues(NoticeAppItem item) {
        ContentValues values = new ContentValues();
        values.put(WatchManagerContracts.NotificationColumns.PACKAGE_NAME, item.packageName);
        values.put(WatchManagerContracts.NotificationColumns.ENABLED, item.enabled);
        values.put(WatchManagerContracts.NotificationColumns.TIME, item.count);
        return values;
    }

    @Override
    public int update(NoticeAppItem item) {
        return update(item, WatchManagerContracts.NotificationColumns.PACKAGE_NAME + " = ? ",
                new String[] { item.packageName });
    }

    @Override
    protected NoticeAppItem fromCursor(Cursor cursor) {
        String packageName = cursor.getString(cursor
                .getColumnIndex(WatchManagerContracts.NotificationColumns.PACKAGE_NAME));

        if (packageName != null) {
            NoticeAppItem item = new NoticeAppItem(packageName);
            item.enabled = cursor.getInt(cursor
                    .getColumnIndex(WatchManagerContracts.NotificationColumns.ENABLED)) == 1;
            item.count = cursor.getInt(cursor
                    .getColumnIndex(WatchManagerContracts.NotificationColumns.TIME));
            return item;
        }

        return null;
    }

    @Override
    public int delete(NoticeAppItem t) {
        return delete(WatchManagerContracts.NotificationColumns.PACKAGE_NAME + " = ? ",
                new String[] { t.packageName });
    }

    @Override
    public boolean hasData(NoticeAppItem t) {
        NoticeAppItem item = query(null, WatchManagerContracts.NotificationColumns.PACKAGE_NAME
                + " = ? ", new String[] { t.packageName }, null, null, null);
        return item != null;
    }

    @Override
    public List<NoticeAppItem> queryAll() {
        return queryAll(WatchManagerContracts.NotificationColumns.TIME + " DESC");
    }

    public NoticeAppItem queryNotification(String packageName) {
        NoticeAppItem item = query(null, WatchManagerContracts.NotificationColumns.PACKAGE_NAME
                + " = ? ", new String[] { packageName }, null, null, null);
        return item;
    }

    public void save(String packageName) {
        NoticeAppItem item = queryNotification(packageName);

        if (item == null) {
            item = new NoticeAppItem(packageName);
            item.enabled = true;
        }
        item.count++;

        save(item);
    }
}
