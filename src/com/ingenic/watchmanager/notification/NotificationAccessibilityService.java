/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.notification;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Notification;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Parcelable;
import android.view.accessibility.AccessibilityEvent;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.NotificationInfo;
import com.ingenic.iwds.datatransactor.elf.NotificationTransactionModel;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.util.UUIDS;

/**
 * 4.3一下版本使用的通知服务
 *
 * @author LiJinWen
 */
public class NotificationAccessibilityService extends AccessibilityService
        implements
        NotificationTransactionModel.NotificationTransactionModelCallback {

    private static NotificationTransactionModel mModel;

    @Override
    protected void onServiceConnected() {
        if(!rightVersion()) return;

        IwdsLog.d(this, "AccessibilityService is connected.");

        if (mModel == null) {
            mModel = new NotificationTransactionModel(this, this,
                    UUIDS.NOTIFICATION);
        }

        if (!mModel.isStarted()) {
            mModel.start();
        }

        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_VISUAL;
        info.eventTypes = AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED;
        info.notificationTimeout = 100;
        setServiceInfo(info);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if(!rightVersion()) return;

        CharSequence eventPackage = event.getPackageName();
        IwdsLog.d(this,
                "The package of event which AccessibilityService has received："
                        + eventPackage);

        String eventPackageName = "";
        if (eventPackage != null) {
            eventPackageName = eventPackage.toString();
        }

        if (eventPackageName == ""
                || eventPackageName.equalsIgnoreCase(getPackageName())
                || eventPackageName.startsWith("android")
                || eventPackageName.contains("com.android")
                || eventPackageName.contains("com.google.android")) {
            IwdsLog.w(this,
                    "This event is send by Android(or unknow) APP!Ignore...");
            return;
        }

        NotificationOperator operator = NotificationOperator.getInstance(this);
        NoticeAppItem item = operator.queryNotification(eventPackageName);

        boolean enabled = item == null || item.enabled;
        if (!enabled) {
            IwdsLog.w(this, "The package:" + eventPackageName
                    + " is disabled by User!Ignore...");
            return;
        }

        Parcelable parcelable = event.getParcelableData();
        if (!(parcelable instanceof Notification)) {
            IwdsLog.w(this, "The event is no a Notification!Ignore...");
            return;
        }

        Notification notification = (Notification) parcelable;
        NotificationInfo info = new NotificationInfo();
        info.packageName = eventPackageName;

        PackageManager pm = getPackageManager();
        try {
            info.title = info.appName = pm.getPackageInfo(eventPackageName, 0).applicationInfo
                    .loadLabel(pm).toString();
        } catch (Exception e) {
            info.title = info.appName = eventPackageName;
        }
        IwdsLog.d(this, "Title of notification:" + info.title);

        // get the notification text
        boolean ignoreExtra = eventPackageName.contains("com.tencent.qq")
                || eventPackageName.contains("com.tencent.mobileqq")
                || eventPackageName.equalsIgnoreCase("com.tencent.mm");

        if (ignoreExtra) {
            info.content = notification.tickerText.toString();
            IwdsLog.d(this, "Content of notification:" + info.content);
        } else {
            ExtraCompat compat;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                compat = new SDK16ExtraCompat(this);
            } else {
                compat = new ExtraCompat(this);
            }

            info.content = compat.replace(compat.getExtraData(notification),
                    eventPackageName);

            if("".equals(info.content) && (null != notification.tickerText)){
                info.content = notification.tickerText.toString();
            }

            IwdsLog.d(this, "Content of notification(after get extra):"
                    + info.content);
        }

        if (info.content == null || info.content.equalsIgnoreCase("")
                || info.content.equals(NotificationFragment.NOT_ALLOWED_MSG)) {
            IwdsLog.w(this, "Content of notification is null!Ignore...");
            return;
        }

        info.updateTime = System.currentTimeMillis();
        IwdsLog.d(this, "Update time of notification:" + info.updateTime);

        operator.save(eventPackageName);
        mModel.send(info);
    }

    @Override
    public void onInterrupt() {
        IwdsLog.d(this, "Interrupt on AccessibilityService.");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        IwdsLog.d(this, "Unbind AccessibilityService.");
        if (mModel != null) {
            mModel.stop();
        }
        return super.onUnbind(intent);
    }

    @Override
    public void onRequest() {
    }

    @Override
    public void onRequestFailed() {
    }

    @Override
    public void onObjectArrived(NotificationInfo object) {
    }

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        if (!isConnected) {
            IwdsLog.w(this, "Model of notification is disconnected!");
        }
    }

    @Override
    public void onChannelAvailable(boolean isAvailable) {
        if (!isAvailable) {
            IwdsLog.w(this, "Model of notification is unAvailabled!");
        }
    }

    @Override
    public void onSendResult(DataTransactResult result) {
        int resultCode = result.getResultCode();
        if (resultCode != DataTransactResult.RESULT_OK) {
            IwdsLog.w(this, "Send a notification failed:" + resultCode);
        }
    }

    /**
     * 检查系统版本,防止在4.3以上版本启动该服务，影响通知读取权限的那个通知服务
     */
    private boolean rightVersion() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            IwdsLog.w(this, "Invalid operation!");
            return false;
        }

        return true;
    }
}
