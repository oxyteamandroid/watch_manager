/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/WatchManager/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.notification;

import android.app.Notification;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RemoteViews;

/**
 * 对notification扩展消息处理的类<br>
 * 处理API16及以上版本
 *
 * @author tZhang
 */

class SDK16ExtraCompat extends ExtraCompat {
    public SDK16ExtraCompat(Context context) {
        super(context);
    }

    @Override
    public String getExtraData(Notification notification) {
        RemoteViews views = null;
        try {
            views = notification.bigContentView;
        } catch (Exception e) {
            return super.getExtraData(notification);
        }

        if (views == null) {
            return super.getExtraData(notification);
        }

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try {
            ViewGroup localView = (ViewGroup) inflater.inflate(
                    views.getLayoutId(), null);
            views.reapply(mContext, localView);
            return dumpViewGroup(localView);
        } catch (Exception e) {
            return "";
        }
    }
}
