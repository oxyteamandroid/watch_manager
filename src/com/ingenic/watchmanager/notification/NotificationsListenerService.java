/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/watchmanager Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.notification;

import a_vcard.android.text.TextUtils;
import android.app.Notification;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.NotificationInfo;
import com.ingenic.iwds.datatransactor.elf.NotificationTransactionModel;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.util.UUIDS;

/**
 * 4.3及以上版本使用的通知服务
 *
 * @author tZhang
 */
public class NotificationsListenerService extends NotificationListenerService
        implements
        NotificationTransactionModel.NotificationTransactionModelCallback {

    private static NotificationTransactionModel mModel;

    @Override
    public void onCreate() {
        super.onCreate();
        IwdsLog.d(this, "NotificationListenerService is created.");

        if (mModel == null) {
            mModel = new NotificationTransactionModel(this, this,
                    UUIDS.NOTIFICATION);
        }

        if (!mModel.isStarted()) {
            mModel.start();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        IwdsLog.d(this, "onBind NotificationListenerService.");
        return super.onBind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        IwdsLog.d(this, "Unbind NotificationListenerService.");
        if (mModel != null) {
            mModel.stop();
        }
        return super.onUnbind(intent);
    }

    @Override
    public void onRequest() {
    }

    @Override
    public void onRequestFailed() {
    }

    @Override
    public void onObjectArrived(NotificationInfo object) {
    }

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        if (!isConnected) {
            IwdsLog.w(this, "Model of notification is disconnected!");
        }
    }

    @Override
    public void onChannelAvailable(boolean isAvailable) {
        if (!isAvailable) {
            IwdsLog.w(this, "Model of notification is unAvailabled!");
        }
    }

    @Override
    public void onSendResult(DataTransactResult result) {
        int resultCode = result.getResultCode();
        if (resultCode != DataTransactResult.RESULT_OK) {
            IwdsLog.w(this, "Send a notification failed:" + resultCode);
        }
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        IwdsLog.d(this,
                "The package of event which AccessibilityService has received："
                        + packageName);

        if (TextUtils.isEmpty(packageName)
                || packageName.equalsIgnoreCase(getPackageName())
                || packageName.startsWith("android")
                || packageName.contains("com.android")
                || packageName.contains("com.google.android")) {
            IwdsLog.w(this,
                    "This event is send by Android(or unknow) APP!Ignore...");
            return;
        }

        NotificationOperator operator = NotificationOperator.getInstance(this);
        NoticeAppItem item = operator.queryNotification(packageName);

        boolean enabled = item == null || item.enabled;
        if (!enabled) {
            IwdsLog.w(this, "The package:" + packageName
                    + " is disabled by User!Ignore...");
            return;
        }

        Notification notification = sbn.getNotification();
        NotificationInfo info = new NotificationInfo();
        info.packageName = packageName;

        PackageManager pm = getPackageManager();
        try {
            info.title = info.appName = pm.getPackageInfo(packageName, 0).applicationInfo
                    .loadLabel(pm).toString();
        } catch (Exception e) {
            info.title = info.appName = packageName;
        }
        IwdsLog.d(this, "Title of notification:" + info.title);

        // get the notification text
        boolean ignoreExtra = packageName.contains("com.tencent.qq")
                || packageName.contains("com.tencent.mobileqq")
                || packageName.equalsIgnoreCase("com.tencent.mm");

        if (ignoreExtra && (null != notification.tickerText)) {
            info.content = notification.tickerText.toString();
            IwdsLog.d(this, "Content of notification:" + info.content);
        } else {
            ExtraCompat compat;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                compat = new SDK16ExtraCompat(this);
            } else {
                compat = new ExtraCompat(this);
            }

            info.content = compat.replace(compat.getExtraData(notification),
                    packageName);

            if("".equals(info.content) && (null != notification.tickerText)){
                info.content = notification.tickerText.toString();
            }

            IwdsLog.d(this, "Content of notification(after get extra):"
                    + info.content);
        }

        if (info.content == null || info.content.equalsIgnoreCase("")
                || info.content.equals(NotificationFragment.NOT_ALLOWED_MSG)) {
            IwdsLog.w(this, "Content of notification is null!Ignore...");
            return;
        }

        info.updateTime = sbn.getPostTime();
        IwdsLog.d(this, "Update time of notification:" + info.updateTime);

        operator.save(packageName);
        mModel.send(info);
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
    }
}
