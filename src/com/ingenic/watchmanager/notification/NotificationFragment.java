/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.notification;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import android.app.Activity;
import android.app.ListFragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.ingenic.watchmanager.R;

public class NotificationFragment extends ListFragment {
    public static final String NOT_ALLOWED_MSG = "“QQ”正在运行:点击了解详情或停止应用。";
    private static final String ACCESSIBILITY_SERVICE = "com.ingenic.watchmanager/com.ingenic.watchmanager.notification.NotificationAccessibilityService";

    private static final String ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";
    private static final String ACTION_NOTIFICATION_LISTENER_SETTINGS = "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS";

    private List<NoticeAppItem> mPackageInfos = new ArrayList<NoticeAppItem>();
    private ApplicationAdapter mAdapter;
    private TextView mAccessibilityText;
    private boolean mServiceEnabled = false;
    private NotificationOperator mOperator;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdapter = new ApplicationAdapter(getActivity(),
                R.layout.list_item_notifications, mPackageInfos);
        setListAdapter(mAdapter);
        loadApplications();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mOperator = NotificationOperator.getInstance(activity);
    }

    private void loadApplications() {
        mAdapter.clear();
        List<NoticeAppItem> items = mOperator.queryAll();
        if (items == null) {
            mAdapter.notifyDataSetChanged();
            return;
        }

        PackageManager pm = getActivity().getPackageManager();
        List<PackageInfo> infos = pm.getInstalledPackages(0);
        if (infos != null) {
            int count = infos.size();
            for (int i = count - 1; i >= 0; i--) {
                PackageInfo info = infos.get(i);
                String packageName = info.packageName;

                NoticeAppItem item = getNoticeAppItem(items, packageName);
                if (item != null) {
                    item.label = info.applicationInfo.loadLabel(pm);
                    item.icon = info.applicationInfo.loadIcon(pm);
                    mAdapter.add(item);
                }
            }
        }

        mAdapter.sort(new Comparator<NoticeAppItem>() {

            @Override
            public int compare(NoticeAppItem lhs, NoticeAppItem rhs) {
                return rhs.count - lhs.count;
            }
        });

        mAdapter.notifyDataSetChanged();
    }

    private NoticeAppItem getNoticeAppItem(List<NoticeAppItem> items,
            String packageName) {
        for (NoticeAppItem noticeAppItem : items) {
            if (noticeAppItem.packageName.equalsIgnoreCase(packageName)) {
                return noticeAppItem;
            }
        }
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_notifications,
                container, false);

        mAccessibilityText = (TextView) rootView
                .findViewById(R.id.accessibility_enabler);
        rootView.findViewById(R.id.config).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                            openNotificationAccess();
                        } else {
                            startActivity(new Intent(
                                    Settings.ACTION_ACCESSIBILITY_SETTINGS));
                        }
                    }
                });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            // 在4.3及以上版本用设置中的通知权限这一项来打开通知服务
            mServiceEnabled = isNotificationListenerServiceEnabled();
        } else {
            // 在4.3以下版本用设置中的辅助功能手表管理这一项来打开通知服务
            mServiceEnabled = isAccessibilityServiceEnabled();
        }

        mAccessibilityText
                .setText(mServiceEnabled ? R.string.accessibility_enabled
                        : R.string.accessibility_disabled);
        mAdapter.notifyDataSetChanged();
    }

    private boolean isNotificationListenerServiceEnabled() {
        String pkgName = getActivity().getPackageName();
        final String flat = Settings.Secure.getString(getActivity()
                .getContentResolver(), ENABLED_NOTIFICATION_LISTENERS);
        if (!TextUtils.isEmpty(flat)) {
            final String[] names = flat.split(":");
            for (int i = 0; i < names.length; i++) {
                final ComponentName cn = ComponentName
                        .unflattenFromString(names[i]);
                if (cn != null) {
                    if (TextUtils.equals(pkgName, cn.getPackageName())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean isAccessibilityServiceEnabled() {
        int accessibilityEnabled = 0;
        try {
            accessibilityEnabled = Settings.Secure.getInt(getActivity()
                    .getContentResolver(),
                    android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
        } catch (Exception e) {
            e.printStackTrace();
        }

        TextUtils.SimpleStringSplitter stringColonSplitter = new TextUtils.SimpleStringSplitter(
                ':');
        if (accessibilityEnabled == 1) {
            String settingValue = Settings.Secure.getString(getActivity()
                    .getContentResolver(),
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                TextUtils.SimpleStringSplitter splitter = stringColonSplitter;
                splitter.setString(settingValue);

                while (splitter.hasNext()) {
                    String accessabilityService = splitter.next();
                    if (accessabilityService.equalsIgnoreCase(ACCESSIBILITY_SERVICE)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private void openNotificationAccess() {
        startActivity(new Intent(ACTION_NOTIFICATION_LISTENER_SETTINGS));
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        NoticeAppItem item = (NoticeAppItem) l.getItemAtPosition(position);
        item.enabled = !item.enabled;
        mAdapter.notifyDataSetChanged();
    }

    private class ApplicationAdapter extends ArrayAdapter<NoticeAppItem> {

        private int mResourceId;
        private Context mContext;
        private List<NoticeAppItem> mObjects;

        public ApplicationAdapter(Context context, int resource,
                List<NoticeAppItem> objects) {
            super(context, resource, objects);
            mContext = context;
            mResourceId = resource;
            mObjects = objects;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(mResourceId, null);

                holder = new ViewHolder();
                holder.icon = (ImageView) convertView.findViewById(R.id.icon);
                holder.label = (TextView) convertView.findViewById(R.id.label);
                holder._switch = (Switch) convertView
                        .findViewById(R.id._switch);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            final NoticeAppItem info = mObjects.get(position);
            holder.icon.setImageDrawable(info.icon);
            holder.label.setText(info.label);

            holder._switch
                    .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(CompoundButton buttonView,
                                boolean isChecked) {
                            if (mServiceEnabled) {
                                info.enabled = isChecked;
                                mOperator.save(info);
                            }
                        }
                    });

            if (mServiceEnabled) {
                holder._switch.setEnabled(true);
                holder._switch.setChecked(info.enabled);
            } else {
                holder._switch.setEnabled(false);
                holder._switch.setChecked(false);
            }

            return convertView;
        }
    }

    private static class ViewHolder {
        ImageView icon;
        TextView label;
        Switch _switch;
    }
}
