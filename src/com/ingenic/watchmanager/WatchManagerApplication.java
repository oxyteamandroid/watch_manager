/*
 * Copyright (C) 2014 Ingenic Semiconductor
 * 
 * SunWenZhong(Fighter) <wenzhong.sun@ingenic.com, wanmyqawdr@126.com>
 * 
 * Elf/watch-manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager;

import java.util.ArrayList;
import java.util.List;

import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.IwdsApplication;
import com.ingenic.iwds.uniconnect.link.Adapter;
import com.ingenic.iwds.uniconnect.link.AdapterManager;
import com.ingenic.iwds.uniconnect.link.Link;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.calendar.CalendarSyncService;
import com.ingenic.watchmanager.cloud.PersonalCloudModel;
import com.ingenic.watchmanager.contact.CalllogService;
import com.ingenic.watchmanager.contact.ContactSyncService;
import com.ingenic.watchmanager.contact.PhoneService;
import com.ingenic.watchmanager.contact.SmsService;
import com.ingenic.watchmanager.device.DeviceModel;
import com.ingenic.watchmanager.health.HealthExerciseService;
import com.ingenic.watchmanager.loss.FindMobilePhoneModel;
import com.ingenic.watchmanager.market.AppDownloadUtils;
import com.ingenic.watchmanager.metro.MetroModel;
import com.ingenic.watchmanager.music.RemoteMusicService;
import com.ingenic.watchmanager.ota.OtaModel;
import com.ingenic.watchmanager.ota.UpdateUtils;
import com.ingenic.watchmanager.remotecamera.PhoneCameraService;
import com.ingenic.watchmanager.remotecamera.RemoteCameraTransactorModel;
import com.ingenic.watchmanager.settings.BootReceive;
import com.ingenic.watchmanager.speech.SpeechShowOnPhoneModel;
import com.ingenic.watchmanager.theme.ThemeFileSendModel;
import com.ingenic.watchmanager.timeadditional.TimeAdditionalModel;
import com.ingenic.watchmanager.util.MusicUtils;
import com.ingenic.watchmanager.weather.WeatherInfoTransatorModel;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

public class WatchManagerApplication extends IwdsApplication {
    private final static String PACKAGE_ADDED = "android.intent.action.PACKAGE_ADDED";
    private final static String PACKAGE_REMOVED = "android.intent.action.PACKAGE_REMOVED";

    private AdapterManager mAdapterManager;
    private Adapter mAdapter;
    private List<Link> mLinks = new ArrayList<Link>();

    private BootReceive mBootReceive;

    @Override
    public void onCreate() {
        super.onCreate();
        if (!getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_BLUETOOTH)) {
            return;
        } else {
            String packageName = getPackageName();
            String procress = getCurProcessName();
            if (!packageName.equalsIgnoreCase(procress)) {
                IwdsLog.w(this, "Process:" + procress
                        + " is not main process.Ignore...");
                return;
            }
            AppDownloadUtils.init(this);
            ThemeFileSendModel.initSerivceAndModel(this);

            initialize(DeviceDescriptor.DEVICE_CLASS_MOBILE,
                    DeviceDescriptor.MOBILE_DEVICE_SUBCLASS_SMARTPHONE);

            MetroModel.setMetroColumn(MetroModel.COLUMN_SIX);

            mAdapterManager = AdapterManager
                    .getInstance(getApplicationContext());
            mAdapter = mAdapterManager
                    .getAdapter(Adapter.TAG_ANDROID_BT_DATA_CHANNEL);
            mAdapter.enable();

            List<String> addresses = getBondedAddresses();
            if (addresses != null) {
                DeviceDescriptor descriptor = new DeviceDescriptor(
                        mAdapter.getLocalAddress(), mAdapter.getLinkTag(),
                        DeviceDescriptor.DEVICE_CLASS_MOBILE,
                        DeviceDescriptor.MOBILE_DEVICE_SUBCLASS_SMARTPHONE);

                for (String address : addresses) {
                    Link link = mAdapter.createLink(descriptor);
                    link.bondAddress(address);
                    mLinks.add(link);
                }
            }

            startService(new Intent(this, PhoneService.class));
            startService(new Intent(this, SmsService.class));
            IwdsLog.d(SmsService.TAG, "start " + SmsService.TAG);
            startService(new Intent(this, CalllogService.class));

            DeviceModel deviceModel = DeviceModel.getInstance(this);
            deviceModel.startTransaction();

            startService(new Intent(this, CalendarSyncService.class));

            WeatherInfoTransatorModel weatherModel = WeatherInfoTransatorModel
                    .getInstance(this);
            weatherModel.startTransaction();

            TimeAdditionalModel timeAdditionalModel = TimeAdditionalModel
                    .getInstance(this);
            timeAdditionalModel.startTransaction();

            OtaModel otamodel = OtaModel.getInstance(this);
            otamodel.startTransaction();

            FindMobilePhoneModel findMobileModel = FindMobilePhoneModel
                    .getInstance(this);
            findMobileModel.startDataTransactor();
            SpeechShowOnPhoneModel speechShowOnPhoneModel = SpeechShowOnPhoneModel
                    .getInstance(this);
            speechShowOnPhoneModel.startDataTransactor();
            RemoteCameraTransactorModel.getInstance(this);
            // 启动远程相机的服务
            startService(new Intent(this, PhoneCameraService.class));
            // 启动远程音乐服务
            startService(new Intent(this, RemoteMusicService.class));
            // 启动通讯录同步服务。
            startService(new Intent(this, ContactSyncService.class));
            // 启动健康运动的同步服务
            startService(new Intent(this, HealthExerciseService.class));

            // 启动个人信息云服务
            PersonalCloudModel.getInstance(this);
            initImageLoader(this);

            registerReceiver();
            MusicUtils.setDefaultMusicPlayer(this);
        }
    }

    private void registerReceiver() {
        mBootReceive = new BootReceive();
        IntentFilter IF = new IntentFilter();
        IF.addAction(PACKAGE_ADDED);
        IF.addAction(PACKAGE_REMOVED);
        IF.addDataScheme("package");
        registerReceiver(mBootReceive, IF);
    }

    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you
        // may tune some of them,
        // or you can create default configuration by
        // ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(
                context);

        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }

    private String getCurProcessName() {
        int pid = android.os.Process.myPid();
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        for (ActivityManager.RunningAppProcessInfo appProcess : activityManager
                .getRunningAppProcesses()) {
            if (appProcess.pid == pid)
                return appProcess.processName;
        }
        return null;
    }

    public static WatchManagerApplication get(Context context) {
        return (WatchManagerApplication) (context.getApplicationContext());
    }

    public Adapter getAdapter() {
        return mAdapter;
    }

    public List<Link> getLinks() {
        return mLinks;
    }

    public Link getLink(String address) {
        for (Link link : mLinks) {
            if (address.equals(link.getBondedAddress()))
                return link;
        }
        return null;
    }

    public ArrayList<String> getBondedAddresses() {
        if (mAdapter == null)
            return null;
        return mAdapter.getBondedAddressStorage();
    }

    boolean bondAddress(String address) {
        /* 绑定之前清空本地OTA信息 */
        if(!BluetoothAdapter.checkBluetoothAddress(address)){
            return false;
        }
        UpdateUtils.clearUpdateInfo(getBaseContext());
        Link link = mAdapter.createLink(new DeviceDescriptor(mAdapter
                .getLocalAddress(), mAdapter.getLinkTag(),
                DeviceDescriptor.DEVICE_CLASS_MOBILE,
                DeviceDescriptor.MOBILE_DEVICE_SUBCLASS_SMARTPHONE));
        boolean bonded = link.bondAddress(address);
        if (bonded) {
            mLinks.add(link);
            /* 启动Calllog服务 */
            Intent itcalllog = new Intent(this, CalllogService.class);
            itcalllog.putExtra("commandId", CalllogService.COM_START);
            getApplicationContext().startService(itcalllog);
        }
        return bonded;
    }

    public void unbond() {
        for (final Link link : mLinks) {
            if (link.isBonded()) {
                link.unbond();
                mAdapter.destroyLink(link);
            }
        }

        mLinks.clear();

        /* 解绑之后清空本地OTA信息 */
        UpdateUtils.clearUpdateInfo(getApplicationContext());
        UpdateUtils.setUpdateState(getApplicationContext(),
                UpdateUtils.STATE_DEFAULT);
    }

    public boolean isConnected() {
        for (Link link : mLinks) {
            if (link.getState() == Link.LinkState.STATE_CONNECTED)
                return true;
        }
        return false;
    }

    @Override
    protected void finalize() throws Throwable {
        unregisterReceiver(mBootReceive);
        super.finalize();
    }

}
