/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/iwds-ui-jar Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import a_vcard.android.text.Spanned;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ingenic.watchmanager.health.EverydayStepsInfo;
import com.ingenic.watchmanager.health.EverydayStepsOperator;

public class StepDetailsActivity extends Activity implements OnClickListener {

    /**
     * 顶部返回按钮
     */
    private ImageView mIconBack;

    /**
     * 标题
     */
    @SuppressWarnings("unused")
    private TextView mTitle;

    /**
     * 计步详情列表
     */
    private ListView mStepList;

    /**
     * 计步详情列表适配器
     */
    private StepListAdapter mAdapter;

    /**
     * listview --> emptyview
     */
    private View mEmptyView;

    private EverydayStepsOperator mStepsOperator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_details);

        // 初始化计步数据操作对象
        mStepsOperator = new EverydayStepsOperator(this);

        initView();

        loadDatas();
    }

    private void loadDatas() {

        mAdapter = new StepListAdapter(this);
        mStepList.setAdapter(mAdapter);

        new AsyncTask<Void, Void, List<EverydayStepsInfo>>() {

            @Override
            protected List<EverydayStepsInfo> doInBackground(Void... params) {

                List<EverydayStepsInfo> infos = mStepsOperator.queryAll();
                List<EverydayStepsInfo> result = new ArrayList<EverydayStepsInfo>();
                for (EverydayStepsInfo info : infos) {
                    if (info.steps != 0) {
                        result.add(info);
                    }
                }

                return result;
            }

            @Override
            protected void onPostExecute(List<EverydayStepsInfo> result) {
                super.onPostExecute(result);
                mAdapter.setList(result);
                mAdapter.notifyDataSetChanged();
            }

        }.execute();

    }

    /**
     * 初始化
     */
    private void initView() {

        // 返回
        mIconBack = (ImageView) findViewById(R.id.step_icon_back);
        mIconBack.setOnClickListener(this);

        // 标题
        mTitle = (TextView) findViewById(R.id.step_details_title);

        // ListView
        mStepList = (ListView) findViewById(R.id.health_step_details_lv);

        // empty view
        mEmptyView = findViewById(R.id.step_details_empty_view);

        mStepList.setEmptyView(mEmptyView);
    }

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
    }

    @Override
    public void onBackPressed() {
        try {
            super.onBackPressed();
            overridePendingTransition(R.anim.slide_right_in,
                    R.anim.slide_right_out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.step_icon_back:
            onBackPressed();
            break;

        default:
            break;
        }
    }

    /**
     * 计步详情ListView适配器
     * 
     * @author tzhang
     */
    private class StepListAdapter extends BaseAdapter {

        private Context mContext;

        private List<EverydayStepsInfo> mList;

        private Calendar mCalendar = Calendar.getInstance();

        public StepListAdapter(Context context) {
            this.mContext = context;
        }

        public void setList(List<EverydayStepsInfo> list) {
            this.mList = list;
        }

        @Override
        public int getCount() {
            if (mList != null) {
                return mList.size();
            }

            return 0;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(mContext).inflate(
                        R.layout.health_step_details_lv_item, null);
                holder.date = (TextView) convertView.findViewById(R.id.date);
                holder.step = (TextView) convertView.findViewById(R.id.step);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            mCalendar.setTimeInMillis(Long.parseLong(mList.get(position).date));

            String date = mCalendar.get(Calendar.YEAR) + "\n"
                    + (mCalendar.get(Calendar.MONTH) + 1) + "/"
                    + mCalendar.get(Calendar.DAY_OF_MONTH);

            holder.date.setText(setSubscriptDate(date));

            holder.step.setText(mList.get(position).steps + " steps");

            return convertView;
        }

        class ViewHolder {
            TextView date;
            TextView step;
        }

        /**
         * 设置日期格式
         * 
         * @param str
         * @return
         */
        private SpannableString setSubscriptDate(String date) {
            SpannableString msp = new SpannableString(date);
            msp.setSpan(new AbsoluteSizeSpan(14, true), 0, 4,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            return msp;
        }

    }

}
