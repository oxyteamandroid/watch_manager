/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 *  Elf/IDWS Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.timeadditional;

import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import android.net.Uri;

import com.ingenic.iwds.datatransactor.elf.TimeAdditionalInfo;

public class TimeAdditionalFromNet {
    private TimeAdditionalNetCallback mCallback;

    public void registCallback(TimeAdditionalNetCallback callback) {
        mCallback = callback;
    }

    public TimeAdditionalInfo getTimeAdditionalOfCity(String city) {

        String woeid = getWoeid(city);
        if (woeid == null) {
            return null;
        }

        HttpURLConnection conn = null;
        try {
            URL url = new URL(
                    "https://query.yahooapis.com/v1/public/yql?q=select%20astronomy%20from%20weather.forecast%20where%20woeid%3D"
                            + woeid + "&format=xml&diagnostics=true&callback=");

            conn = connectURL(url);
            if (conn == null)
                return null;

            Document doc = parser(conn);
            if (doc == null)
                return null;

            NodeList list = doc.getElementsByTagName("yweather:astronomy");
            NamedNodeMap map = list.item(0).getAttributes();
            String sunrise = map.getNamedItem("sunrise").getNodeValue();
            String sunset = map.getNamedItem("sunset").getNodeValue();

            if (sunrise != null && sunset != null) {
                TimeAdditionalInfo info = new TimeAdditionalInfo(sunrise,
                        sunset);
                return info;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private static HttpURLConnection connectURL(URL url) {
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);
            conn.connect();
        } catch (Exception e) {
            e.printStackTrace();
            if (conn != null) {
                conn.disconnect();
                conn = null;
            }
        }
        return conn;
    }

    private static Document parser(HttpURLConnection conn) {
        if (conn == null)
            return null;
        Document doc = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            doc = builder.parse(conn.getInputStream());
            doc.normalize();
        } catch (Exception e) {
            e.printStackTrace();
            if (conn != null) {
                conn.disconnect();
                conn = null;
            }
        }
        return doc;
    }

    /**
     * 获取城市id
     * 
     * @param city
     *            城市名称
     * @return 城市id
     */
    private String getWoeid(String city) {
        HttpURLConnection conn = null;
        String woeid = null;
        try {
            URL url = new URL(
                    "http://where.yahooapis.com/v1/places.q('"
                            + Uri.encode(city)
                            + "')?appid=CPn5O1fV34FY7erCXD7OF_VFMpieKNYdLlFkb5RJgTNtLf2Oupao5FQ0Sy4CJQ--");
            conn = connectURL(url);

            if (conn == null) {
                notifyNetworkError();
                return null;
            }
            Document doc = parser(conn);
            if (doc == null) {
                notifyNetworkError();
                return null;
            }

            NodeList list = doc.getElementsByTagName("woeid");
            woeid = list.item(0).getFirstChild().getNodeValue();
            if (woeid == null) {
                notifyCityError();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof NullPointerException) {
                notifyCityError();
            } else {
                notifyNetworkError();
            }
        } finally {
            if (conn != null) {
                conn.disconnect();
                conn = null;
            }
        }
        return woeid;
    }

    public String getCity(double latitude, double longitude) {
        String api = "http://api.map.baidu.com/geocoder?output=xml";
        String key = "8Yf53tFyqCkAkgdKgCgvMb0Y";
        String url_str = api + "&location=" + String.valueOf(latitude) + ","
                + String.valueOf(longitude) + "&key=" + key;
        String city = null;

        HttpURLConnection conn = null;
        try {
            URL url = new URL(url_str);

            conn = connectURL(url);
            if (conn == null) {
                notifyNetworkError();
                return null;
            }

            Document doc = parser(conn);
            if (doc == null) {
                notifyNetworkError();
                return null;
            }

            doc.normalize();
            city = parseCityFromDocument(doc);
        } catch (Exception e) {
            notifyNetworkError();
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
                conn = null;
            }
        }

        return city;
    }

    private String parseCityFromDocument(Document doc) {
        String city = null;

        NodeList list = doc.getElementsByTagName("status");
        String status = list.item(0).getFirstChild().getNodeValue();

        if ("OK".equalsIgnoreCase(status)) {
            list = doc.getElementsByTagName("city");
            city = list.item(0).getFirstChild().getNodeValue();

            if (city != null) {
                city = city.substring(0, city.length() - 1);
            }
        } else {
            notifyNetworkError();
        }
        return city;
    }

    private void notifyNetworkError() {
        if (mCallback != null) {
            mCallback.onNetworkError();
        }
    }

    private void notifyCityError() {
        if (mCallback != null) {
            mCallback.onCityError();
        }
    }

    public interface TimeAdditionalNetCallback {
        void onCityError();

        void onNetworkError();
    }
}
