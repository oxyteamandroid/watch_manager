/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 *  Elf/IDWS Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.timeadditional;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.TimeAdditionalInfo;
import com.ingenic.iwds.datatransactor.elf.TimeAdditionalTransactionModel;
import com.ingenic.watchmanager.util.UUIDS;

public class TimeAdditionalModel
        implements
        TimeAdditionalTransactionModel.TimeAdditionalInfoTransactionModelCallback,
        TimeAdditionalFromNet.TimeAdditionalNetCallback, BDLocationListener {
    private static TimeAdditionalModel sInstance;
    private TimeAdditionalFromNet mFromNet;
    private TimeAdditionalTransactionModel mTransactionModel;
    private LocationClient mClient;
    private volatile boolean mRequesting = false;
    private HandlerThread mBackThread;
    private Handler mNetworkHandler;
    private Runnable mCityRunnable;
    private Runnable mTimeAdditionalRunnable;

    private TimeAdditionalModel(Context context) {
        mFromNet = new TimeAdditionalFromNet();
        mFromNet.registCallback(this);

        if (mTransactionModel == null) {
            mTransactionModel = new TimeAdditionalTransactionModel(context, this,
                    UUIDS.TIMEADDITIONAL);
        }

        // 百度位置服务
        mClient = new LocationClient(context, getLocationOption());
        mClient.registerLocationListener(this);

        if (mBackThread == null) {
            mBackThread = new HandlerThread("network");
            mBackThread.start();
        }

        mNetworkHandler = new Handler(mBackThread.getLooper());
    }

    /** 百度位置配置 */
    private LocationClientOption getLocationOption() {
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationMode.Hight_Accuracy);
        option.setCoorType("gcj02");
        option.setScanSpan(2000);
        option.setIsNeedAddress(true);
        return option;
    }

    public void startTransaction() {
        mTransactionModel.start();
    }

    public void stopTransaction() {
        mTransactionModel.stop();
    }

    @Override
    protected void finalize() throws Throwable {

        cancelGetCity();
        cancelGetTimeAdditional();

        if (mBackThread != null) {
            mBackThread.interrupt();
            mBackThread = null;
        }
        super.finalize();
    }

    public synchronized static TimeAdditionalModel getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new TimeAdditionalModel(context);
        }
        return sInstance;
    }

    @Override
    public void onRequest() {
        mRequesting = true;

        // 开启定位
        if (!mClient.isStarted()) {
            mClient.start();
        }
    }

    @Override
    public void onRequestFailed() {

    }

    @Override
    public void onObjectArrived(TimeAdditionalInfo object) {

    }

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        if (!isConnected) {
            mRequesting = false;
        }
    }

    @Override
    public void onChannelAvailable(boolean isAvailable) {
        if (!isAvailable) {
            mRequesting = false;
        }

    }

    @Override
    public void onSendResult(DataTransactResult result) {
        mRequesting = false;

        int resultCode = result.getResultCode();
        if (resultCode == DataTransactResult.RESULT_OK) {
        } else {
            // Failed
        }
    }

    @Override
    public void onCityError() {
        if (mTransactionModel != null)
            mTransactionModel.notifyRequestFailed();
    }

    @Override
    public void onNetworkError() {
        if (mTransactionModel != null)
            mTransactionModel.notifyRequestFailed();
    }

    @Override
    public void onReceiveLocation(BDLocation location) {
        if (location != null) {
            mClient.stop();

            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            mCityRunnable = new CityRunnable(latitude, longitude);
            mNetworkHandler.post(mCityRunnable);
        }
    }

    private class CityRunnable implements Runnable {
        private double mLatitude;
        private double mLongitude;

        public CityRunnable(double latitude, double longitude) {
            mLatitude = latitude;
            mLongitude = longitude;
        }

        @Override
        public void run() {
            String city = mFromNet.getCity(mLatitude, mLongitude);
            if (city == null) {
                return;
            }

            getTimeAdditionalFromNet(city);
        }

    }

    public void getTimeAdditionalFromNet(String city) {
        mTimeAdditionalRunnable = new TimeAdditionalRunnable(city);
        mNetworkHandler.post(mTimeAdditionalRunnable);
    }

    private class TimeAdditionalRunnable implements Runnable {
        private String mCity;

        public TimeAdditionalRunnable(String city) {
            mCity = city;
        }

        @Override
        public void run() {
            TimeAdditionalInfo info = mFromNet.getTimeAdditionalOfCity(mCity);

            if (info != null) {
                if (mRequesting) {
                    mTransactionModel.send(info);
                }
            } else if (mRequesting) {
                mTransactionModel.notifyRequestFailed();
            }
        }
    }

    private void cancelGetCity() {
        if (mCityRunnable != null) {
            mNetworkHandler.removeCallbacks(mCityRunnable);
            mCityRunnable = null;
        }
    }

    private void cancelGetTimeAdditional() {
        if (mTimeAdditionalRunnable != null) {
            mNetworkHandler.removeCallbacks(mTimeAdditionalRunnable);
            mTimeAdditionalRunnable = null;
        }
    }

}
