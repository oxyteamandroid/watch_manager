package com.ingenic.watchmanager.device;

import java.util.Locale;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.LocaleTransactionModel;
import com.ingenic.iwds.datatransactor.elf.SyncTimeInfo;
import com.ingenic.iwds.datatransactor.elf.SyncTimeTransactionModel;
import com.ingenic.iwds.datatransactor.elf.SyncTimeTransactionModel.SyncTimeTransactionModelCallback;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.Model;
import com.ingenic.watchmanager.db.Operator;
import com.ingenic.watchmanager.util.UUIDS;

public class DeviceModel extends Model<DeviceInfo> {
    private static DeviceModel sInstance;
    private static LocaleTransactionModel mLocaleModel;
    private static SyncTimeTransactionModel mSyncTimeModel;
    private boolean isConnected = false;
    private boolean isSynctime_Availale = false;
    private boolean isLocale_Available = false;
    private DeviceModelBroadCast mBroadCast;
    private Context mContext;
    private static final int RETRYTIME = 3;
    private static int retry_time = 0;
    private static int retry_local = 0;
    private static final int SEND_LOCALE = 1;
    private static final int SEND_LOCALE_RET = 2;
    private static final int SEND_SYNCTIME = 3;
    private static final int SEND_SYNCTIME_RET = 4;

    private class DeviceModelBroadCast extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String str = intent.getAction();
            IwdsLog.d(DeviceModel.this, "action:" + str);
            if (Intent.ACTION_LOCALE_CHANGED.equals(str)
                    || Intent.ACTION_CONFIGURATION_CHANGED.equals(str)) {
                // Synchronous locale to watch
                if (!myHandler.hasMessages(SEND_LOCALE))
                    myHandler.obtainMessage(SEND_LOCALE).sendToTarget();

            } else if (Intent.ACTION_TIMEZONE_CHANGED.equals(str)
                    || Intent.ACTION_TIME_CHANGED.equals(str)
                    || Intent.ACTION_DATE_CHANGED.equals(str)) {
                // Synchronous calender to watch
                if (!myHandler.hasMessages(SEND_SYNCTIME))
                    myHandler.obtainMessage(SEND_SYNCTIME).sendToTarget();
            }
        }
    }
    // Synchronous calender & locale to watch handler
    private Handler myHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case SEND_LOCALE:
                if (isLocale_Available) {
                    IwdsLog.i(DeviceModel.this, "+++++++Synchronous locale to watch");
                    Locale locale = Locale.getDefault();
                    mLocaleModel.send(locale);
                }

                break;
            case SEND_LOCALE_RET:
                if (msg.arg1 == DataTransactResult.RESULT_OK) {
                    retry_local = 0;
                    IwdsLog.d(DeviceModel.this, "+++++++Synchronous Language success .");
                } else {
                    IwdsLog.d(DeviceModel.this, "+++++++Synchronous Language fail .");
                    if (retry_local ++ < RETRYTIME ){
                        if (!this.hasMessages(SEND_LOCALE)){
                            IwdsLog.i(DeviceModel.this, "+++++++Synchronous locale to watch again :" + retry_local);
                            this.obtainMessage(SEND_LOCALE).sendToTarget();
                        }
                    }
                }
                break;
           case SEND_SYNCTIME:
               if (isSynctime_Availale){
                   SyncTimeInfo syncinfo= new SyncTimeInfo();
                   mSyncTimeModel.send(syncinfo);
                   IwdsLog.d(DeviceModel.this, "+++++++Synchronous time to watch");
                   IwdsLog.d(DeviceModel.this, syncinfo.toString());
                   syncinfo = null;
               }
               break;
           case SEND_SYNCTIME_RET:
                if (msg.arg1 == DataTransactResult.RESULT_OK) {
                    retry_time = 0;
                    IwdsLog.d(DeviceModel.this, "+++++++Synchronous time success .");
                } else {
                    IwdsLog.d(DeviceModel.this, "+++++++Synchronous time fail .");
                    if (retry_time ++ < RETRYTIME ) {
                        if (!this.hasMessages(SEND_SYNCTIME)){
                            IwdsLog.i(DeviceModel.this, "+++++++Synchronous time to watch again :" + retry_time);
                            this.obtainMessage(SEND_SYNCTIME).sendToTarget();
                        }
                    }
                }
                break;
            default:
                break;
            }
            super.handleMessage(msg);
        }

    };

    private LocaleTransactionModel.LocaleTransactionModelCallback mLocaleCallback = new LocaleTransactionModel.LocaleTransactionModelCallback() {

        @Override
        public void onSendResult(DataTransactResult result) {
            // TODO Auto-generated method stub
            if (!myHandler.hasMessages(SEND_LOCALE_RET))
                myHandler.obtainMessage(SEND_LOCALE_RET, result.getResultCode(), 0).sendToTarget();
        }

        @Override
        public void onRequestFailed() {
            // TODO Auto-generated method stub

        }

        @Override
        public void onRequest() {
            // TODO Auto-generated method stub
            if (!myHandler.hasMessages(SEND_LOCALE))
                myHandler.obtainMessage(SEND_LOCALE).sendToTarget();
        }

        @Override
        public void onObjectArrived(Locale object) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onChannelAvailable(boolean isAvailable) {
            // TODO Auto-generated method stub
            isLocale_Available = isAvailable;
            if (!myHandler.hasMessages(SEND_LOCALE))
                myHandler.obtainMessage(SEND_LOCALE).sendToTarget();
        }
    };

    private SyncTimeTransactionModelCallback mSynctimeCallback = new SyncTimeTransactionModelCallback() {

        @Override
        public void onSendResult(DataTransactResult result) {
            // TODO Auto-generated method stub
            if (!myHandler.hasMessages(SEND_SYNCTIME_RET))
                myHandler.obtainMessage(SEND_SYNCTIME_RET, result.getResultCode(), 0).sendToTarget();
        }

        @Override
        public void onRequestFailed() {
            // TODO Auto-generated method stub

        }

        @Override
        public void onRequest() {
            // TODO Auto-generated method stub
            if (!myHandler.hasMessages(SEND_SYNCTIME))
                myHandler.obtainMessage(SEND_SYNCTIME).sendToTarget();
        }

        @Override
        public void onObjectArrived(SyncTimeInfo object) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onLinkConnected(DeviceDescriptor descriptor,
                boolean isConnected) {
            // TODO Auto-generated method stub
            for (Callback<?> callback : mCallbacks) {
                if (callback instanceof DeviceModelCallback) {
                    ((DeviceModelCallback) callback).onLinkConnected(isConnected);
                }
            }
            DeviceModel.this.isConnected = isConnected;
        }

        @Override
        public void onChannelAvailable(boolean isAvailable) {
            // TODO Auto-generated method stub
            isSynctime_Availale = isAvailable;
            if (!myHandler.hasMessages(SEND_SYNCTIME))
                myHandler.obtainMessage(SEND_SYNCTIME).sendToTarget();
        }
    };

    private DeviceModel(Context context) {
        super(context);
        mContext = context;
        if (mLocaleModel == null) {
            mLocaleModel = new LocaleTransactionModel(mContext, mLocaleCallback, UUIDS.LOCALE);
        }
        if (mSyncTimeModel == null) {
            mSyncTimeModel = new SyncTimeTransactionModel(mContext, mSynctimeCallback,
                    UUIDS.SYNCTIME);
        }
    }

    public synchronized static DeviceModel getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DeviceModel(context);
        }
        return sInstance;
    }

    public void startTransaction() {
        mLocaleModel.start();
        mSyncTimeModel.start();
        mBroadCast = new DeviceModelBroadCast();
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(Intent.ACTION_LOCALE_CHANGED);
        mIntentFilter.addAction(Intent.ACTION_CONFIGURATION_CHANGED);
        mIntentFilter.addAction(Intent.ACTION_DATE_CHANGED);
        mIntentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        mIntentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        mContext.registerReceiver(mBroadCast, mIntentFilter);
    }

    public void stopTransaction() {
        mLocaleModel.stop();
        mSyncTimeModel.stop();
        mContext.unregisterReceiver(mBroadCast);
    }

    @Override
    public Operator<DeviceInfo> getOperator(Context context) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void registCallback(
            com.ingenic.watchmanager.Model.Callback<DeviceInfo> callback) {
        super.registCallback(callback);
        if (callback instanceof DeviceModelCallback) {
            ((DeviceModelCallback) callback).onLinkConnected(isConnected);
        }
    }

    public interface DeviceModelCallback extends Callback<DeviceInfo> {
        void onLinkConnected(boolean isConnected);
    }
}
