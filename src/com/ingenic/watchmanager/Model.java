package com.ingenic.watchmanager;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Handler;

import com.ingenic.watchmanager.db.Operator;

public abstract class Model<T> {
    /**
     * 数据获取成功
     */
    protected static final int MSG_DATA_SUCCESS = 1;

    /**
     * 数据获取失败
     */
    protected static final int MSG_DATA_FAILED = -1;

    protected Operator<T> mOperator;
    protected List<Callback<T>> mCallbacks = new ArrayList<Callback<T>>();

    protected Handler mPrivateHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case MSG_DATA_SUCCESS:
                @SuppressWarnings("unchecked")
                List<T> ts = (List<T>) msg.obj;
                for (Model.Callback<T> callback : mCallbacks) {
                    callback.onLocalDataSuccess(ts, msg.arg1);
                }
                break;
            case MSG_DATA_FAILED:
                for (Model.Callback<T> callback : mCallbacks) {
                    callback.onLocalDataFailed(msg.arg1);
                }
                break;
            default:
                break;
            }
        };
    };

    public Model(Context context) {
        mOperator = getOperator(context);
    }

    public abstract Operator<T> getOperator(Context context);

    protected synchronized void writeToDatabase(T t) {
        mOperator.save(t);
    }

    public void registCallback(Callback<T> callback) {
        if (!mCallbacks.contains(callback)) {
            mCallbacks.add(callback);
        }
    }

    public void unRegistCallback(Callback<T> callback) {
        if (mCallbacks.contains(callback)) {
            mCallbacks.remove(callback);
        }
    }

    public interface Callback<T> {
        void onLocalDataSuccess(List<T> ts, int where);

        void onLocalDataFailed(int reasonCode);
    }
}
