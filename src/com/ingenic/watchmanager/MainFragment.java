/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.uniconnect.ConnectionServiceManager;
import com.ingenic.watchmanager.util.Utils;

public class MainFragment extends Fragment implements AdapterView.OnItemClickListener {
    private static final int MSG_UNBONDED = -1;

    /**
     * Icon在xml中的标签名称。
     */
    private static final String TAG_ICON = "icon";

    /**
     * Icon标签中的ID属性：android：id。
     */
    private static final String ATTR_ID = "id";

    /**
     * Icon标签中的Title属性：android：title。
     */
    private static final String ATTR_TITLE = "title";

    /**
     * Icon标签中的Icon属性：android：icon。
     */
    private static final String ATTR_ICON = TAG_ICON;

    /**
     * Icon标签中的Fragment属性：android：framgnet。
     */
    private static final String ATTR_FRAGMENT = "fragment";

    private IconAdapter mIconAdapter;
    private IconAdapter mMenuAdapter;
    private AlertDialog mUnbondConfirm;
    private ProgressDialog mUnbondDialog;
    private TextView mTitleView;

    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case MSG_UNBONDED:
                if (mUnbondDialog != null && mUnbondDialog.isShowing()) {
                    mUnbondDialog.dismiss();
                }

                Intent it = new Intent(getActivity(), WatchManagerActivity.class);
                startActivity(it);
                break;
            default:
                break;
            }
        };
    };

    private BroadcastReceiver mConnectionReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action.equals(ConnectionServiceManager.ACTION_CONNECTED_ADDRESS)) {
                DeviceDescriptor descriptor = (DeviceDescriptor) intent
                        .getParcelableExtra("DeviceDescriptor");

                if (descriptor != null) {
                    SharedPreferences sp = getActivity().getSharedPreferences(
                            Utils.SP_CONNECTED_DEVICE, Context.MODE_PRIVATE);
                    Editor editor = sp.edit();
                    editor.putString(Utils.KEY_DEVICE_ADDRESS, descriptor.devAddress);
                    editor.commit();
                }

                if (mTitleView != null) {
                    mTitleView.setText(R.string.label_connected);
                }
            } else if (action.equals(ConnectionServiceManager.ACTION_DISCONNECTED_ADDRESS)) {
                if (mTitleView != null) {
                    mTitleView.setText(R.string.label_disconnected);
                }
            }
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectionServiceManager.ACTION_CONNECTED_ADDRESS);
        filter.addAction(ConnectionServiceManager.ACTION_DISCONNECTED_ADDRESS);
        activity.registerReceiver(mConnectionReceiver, filter);

        if (mIconAdapter == null) {
            mIconAdapter = new IconAdapter(activity, R.layout.layout_main_item);
        }
        attachDataToAdapter(mIconAdapter, R.xml.icons);

        if (mMenuAdapter == null) {
            mMenuAdapter = new IconAdapter(activity, R.layout.layout_menu_item);
        }
        attachDataToAdapter(mMenuAdapter, R.xml.menus);
    }

    private void attachDataToAdapter(IconAdapter adapter, int xml) {
        if (adapter == null) return;

        adapter.clear();
        adapter.addAll(parseIcons(xml));
        adapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_main, container, false);

        mTitleView = (TextView) rootView.findViewById(android.R.id.title);
        mTitleView.setText(isConnected() ? R.string.label_connected : R.string.label_connecting);

        GridView grid = (GridView) rootView.findViewById(R.id.gridView1);
        grid.setOnItemClickListener(this);
        grid.setAdapter(mIconAdapter);

        ListView list = (ListView) rootView.findViewById(R.id.content);
        list.setOnItemClickListener(this);
        list.setAdapter(mMenuAdapter);

        return rootView;
    }

    @Override
    public void onDetach() {
        getActivity().unregisterReceiver(mConnectionReceiver);
        super.onDetach();
    }

    private boolean isConnected() {
        WatchManagerApplication app = (WatchManagerApplication) getActivity().getApplication();
        return app.isConnected();
    }

    private List<Icon> parseIcons(int xml) {
        XmlResourceParser parser = getResources().getXml(xml);

        List<Icon> icons = new ArrayList<Icon>();
        try {
            int eventType = parser.getEventType();
            Icon icon = null;
            while (eventType != XmlResourceParser.END_DOCUMENT) {
                switch (eventType) {
                case XmlResourceParser.START_TAG:
                    if (TAG_ICON.equals(parser.getName())) {
                        icon = parserIcon(parser);
                    }
                    break;
                case XmlResourceParser.END_TAG:
                    if (TAG_ICON.equals(parser.getName()) && icon != null) {
                        icons.add(icon);
                    }
                    break;
                default:
                    break;
                }

                eventType = parser.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return icons;
    }

    private Icon parserIcon(XmlResourceParser parser) {
        int attrCount = parser.getAttributeCount();

        int id = 0;
        String title = null;
        int icon = 0;
        String fragment = null;

        for (int i = 0; i < attrCount; i++) {
            String name = parser.getAttributeName(i);
            if (ATTR_ID.equals(name)) {
                id = parser.getAttributeResourceValue(i, 0);

                if (id == 0) {
                    id = parser.getAttributeIntValue(i, 0);
                }
            } else if (ATTR_TITLE.equals(name)) {
                int titleRes = parser.getAttributeResourceValue(i, 0);

                if (titleRes != 0) {
                    title = getString(titleRes);
                } else {
                    title = parser.getAttributeValue(i);
                }
            } else if (ATTR_ICON.equals(name)) {
                icon = parser.getAttributeResourceValue(i, 0);
            } else if (ATTR_FRAGMENT.equals(name)) {
                fragment = parser.getAttributeValue(i);
            }
        }

        if (id == 0) return null;

        Icon ic = new Icon();
        ic.id = id;
        ic.title = title;
        ic.icon = icon;
        ic.fragment = fragment;

        return ic;
    }

    private static class IconAdapter extends ArrayAdapter<Icon> {

        private Context mContext;
        private int mResource;

        public IconAdapter(Context context, int resource) {
            super(context, resource);

            mContext = context;
            mResource = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(mResource, parent, false);

                holder = new ViewHolder();
                holder.icon = (ImageView) convertView.findViewById(android.R.id.icon);
                holder.title = (TextView) convertView.findViewById(android.R.id.title);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Icon icon = getItem(position);
            holder.icon.setImageResource(icon.icon);
            holder.title.setText(icon.title);

            return convertView;
        }

        @Override
        public long getItemId(int position) {
            Icon icon = getItem(position);
            return icon.id;
        }

        private static class ViewHolder {
            ImageView icon;
            TextView title;
        }
    }

    private static class Icon {
        int id;
        int icon;
        String title;
        String fragment;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Icon icon = (Icon) parent.getItemAtPosition(position);

        if (icon.id == R.id.ota) {
            if (!isConnected()) {
                Toast.makeText(getActivity(), R.string.conectstate_tip, Toast.LENGTH_SHORT).show();
                return;
            }
        }

        if (icon.fragment != null) {
            startFragment(icon.fragment);
        } else {
            if (icon.id == R.id.unbond) {
                showUnbondConfirm();
            }
        }
    }

    private void showUnbondConfirm() {
        if (mUnbondConfirm == null) {
            mUnbondConfirm = new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.title_unbonded).setMessage(R.string.msg_unbonded)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            unbond();
                        }
                    }).setNegativeButton(android.R.string.no, null).create();
        }

        if (!mUnbondConfirm.isShowing()) {
            mUnbondConfirm.show();
        }
    }

    private void unbond() {
        if (mUnbondDialog == null) {
            String title = getString(R.string.title_connection);
            String message = getString(R.string.text_disconnecting);
            mUnbondDialog = ProgressDialog.show(getActivity(), title, message, false, false);
        } else if (!mUnbondDialog.isShowing()) {
            mUnbondDialog.show();
        }

        new Thread(new Runnable() {

            @Override
            public void run() {
                WatchManagerApplication app = (WatchManagerApplication) getActivity()
                        .getApplication();
                app.unbond();
                mHandler.sendEmptyMessage(MSG_UNBONDED);
            }
        }).start();
    }

    private void startFragment(String fragment) {
        Intent it = new Intent(getActivity(), FragmentActivity.class);
        it.putExtra(FragmentActivity.EXTRA_FRAGMENT, fragment);
        startActivity(it);
    }
}
