/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.downloader;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import android.annotation.SuppressLint;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.downloader.Downloader.FinishListener;

@SuppressLint("UseSparseArrays")
public class DownloaderQueue implements FinishListener {

    private static final String TAG = "DownloaderQueue";

    private LinkedBlockingQueue<Downloader> mTaskQueue = new LinkedBlockingQueue<Downloader>();
    private Set<Downloader> mCurrentDownloadingSet = new HashSet<Downloader>();

    private final int mMaxLaunchCount;
    public AtomicInteger mCurrentLaunchCount = new AtomicInteger(0);

    public DownloaderQueue() {
        this(1);
    }

    public DownloaderQueue(int count) {
        mMaxLaunchCount = Math.max(1, count);
    }

    public boolean enqueue(Downloader down) {
        if (null == down)
            return false;
        down.setFinishListener(this);
        boolean result = mTaskQueue.offer(down);
        if (result) {
            mCurrentDownloadingSet.add(down);
            start();
        }
        return result;
    }

    public void start() {
        synchronized (DownloaderQueue.class) {
            if (mCurrentLaunchCount.get() >= mMaxLaunchCount)
                return;
            mCurrentLaunchCount.incrementAndGet();
        }
        Downloader down = mTaskQueue.poll();
        if (null == down) {
            mCurrentLaunchCount.decrementAndGet();
            return;
        }
        if (down.isPause()) {
            mCurrentLaunchCount.decrementAndGet();
            mCurrentDownloadingSet.remove(down);
            start();
            return;
        }
        down.start();
        IwdsLog.d(TAG, "start downloader :");
    }

    @Override
    public void taskFinish(Downloader down) {
        IwdsLog.d(TAG, "start downloader ---> release:");
        mCurrentDownloadingSet.remove(down);
        mCurrentLaunchCount.decrementAndGet();
        start();
    }

    public void pauseAll() {
        for (Downloader item : mCurrentDownloadingSet) {
            item.pause();
        }
    }

}