/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.downloader;

import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;

import com.ingenic.iwds.utils.IwdsLog;

public class DownloadThread extends Thread {

    public static interface Listener {
        public void updateCompleteSize(int size);
    }

    private SliceInfo mSlice;
    private Listener mListener;
    public Exception mException;
    private HttpURLConnection mConnection;
    private InputStream mInputStream;
    private RandomAccessFile mRandomAccessFile;
    private Downloader mDownloader;

    public DownloadThread(Downloader downloader, SliceInfo slice, Listener listener) {
        mSlice = slice;
        mListener = listener;
        mDownloader = downloader;
    }

    @Override
    public void run() {
        try {
            IwdsLog.d(Downloader.TAG, "downloaderSubThread start!!!");
            if (mDownloader.isPause()) {
                return;
            }

            URL url = new URL(mSlice.url);
            mConnection = (HttpURLConnection) url.openConnection();

            mConnection.setConnectTimeout(5 * 1000);
            mConnection.setRequestMethod("GET");
            Downloader.setConnectionOtherParameter(mConnection);
            mConnection.setRequestProperty("Range", "bytes=" + (mSlice.start + mSlice.completeSize) + "-" + mSlice.end);

            int responseCode = mConnection.getResponseCode();
            if (206 == responseCode) {

                mInputStream = mConnection.getInputStream();
                mRandomAccessFile = new RandomAccessFile(mSlice.filePath, "rwd");
                mRandomAccessFile.seek(mSlice.start + mSlice.completeSize);
                byte[] b = new byte[1024];
                int len = 0;
                while ((len = mInputStream.read(b)) != -1) {
                    if (mDownloader.isPause()) {
                        break;
                    }
                    mRandomAccessFile.write(b, 0, len);
                    mSlice.completeSizeAdd(len);
                    if (null != mListener) {
                        mListener.updateCompleteSize(len);
                    }
                    if (mDownloader.isPause()) {
                        break;
                    }
                }
            } else {
                throw new IOException("responseCode Exception: " + responseCode);
            }

        } catch (Exception e) {

            mException = e;
            e.printStackTrace();

        } finally {
            IwdsLog.d(Downloader.TAG, "downloaderSubThread Finish!!!");
            try {
                if (null != mConnection) {
                    mConnection.disconnect();
                }
                if (null != mInputStream) {
                    mInputStream.close();
                }
                if (null != mRandomAccessFile) {
                    mRandomAccessFile.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
