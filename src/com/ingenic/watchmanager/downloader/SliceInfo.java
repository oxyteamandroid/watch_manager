/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.downloader;

public class SliceInfo {

    public String url;

    public String filePath;

    public long start;
    public long end;
    public long block;
    public long completeSize;

    public SliceInfo(String url, String filePath, long start, long end, long blockTemp, int completeSize) {
        this.url = url;
        this.filePath = filePath;
        this.start = start;
        this.end = end;
        this.block = blockTemp;
        this.completeSize = completeSize;
    }

    public void completeSizeAdd(int size) {
        completeSize += size;
    }

}
