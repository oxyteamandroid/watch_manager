/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.downloader;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DownloadInfoDB {
    private final SQLiteHelper mHelper;

    public DownloadInfoDB(Context context) {
        mHelper = SQLiteHelper.getInstance(context);
    }

    private SQLiteDatabase getDB() {
        return mHelper.getWritableDatabase();
    }

    public long insertSlice(SQLiteDatabase db, SliceInfo slice) {

        ContentValues cv = new ContentValues();

        cv.put("url", slice.url);
        cv.put("block", slice.block);
        cv.put("start", slice.start);
        cv.put("end", slice.end);
        cv.put("complete", slice.completeSize);

        long result = -1;
        result = db.insert("slice", null, cv);
        return result;
    }

    public boolean saveBreakPointInfo(String url, List<SliceInfo> list) {

        if (null == list || list.size() == 0)
            return false;

        synchronized (mHelper) {
            SQLiteDatabase db = getDB();
            db.beginTransaction();
            try {
                db.delete("slice", "url=?", new String[] { url });
                for (SliceInfo s : list) {
                    insertSlice(db, s);
                }
                db.setTransactionSuccessful();
                return true;
            } catch (Exception e) {
                return false;
            } finally {
                db.endTransaction();
                db.close();
            }
        }
    }

    public List<SliceInfo> getBreakPointInfo(String url, String filePath) {
        synchronized (mHelper) {
            SQLiteDatabase db = getDB();
            Cursor cursorSlice = null;
            cursorSlice = db.query("slice", null, "url=?", new String[] { url }, null, null, null);

            List<SliceInfo> list = null;
            while (cursorSlice.moveToNext()) {
                list = null != list ? list : new ArrayList<SliceInfo>();
                int start = cursorSlice.getInt(cursorSlice.getColumnIndex("start"));
                int end = cursorSlice.getInt(cursorSlice.getColumnIndex("end"));
                int block = cursorSlice.getInt(cursorSlice.getColumnIndex("block"));
                int complete = cursorSlice.getInt(cursorSlice.getColumnIndex("complete"));

                SliceInfo s = new SliceInfo(url, filePath, start, end, block, complete);
                list.add(s);
            }
            cursorSlice.close();
            db.close();
            return list;
        }

    }

    public void remove(String url) {
        synchronized (mHelper) {
            SQLiteDatabase db = getDB();
            db.delete("slice", "url=?", new String[] { url });
            db.close();
        }
    }

    private static class SQLiteHelper extends SQLiteOpenHelper {
        private static SQLiteHelper sHelper;

        private final static String DOWNLOAD_DATABASE_NAME = "downloader.db";

        private SQLiteHelper(Context context) {
            super(context, DOWNLOAD_DATABASE_NAME, null, 16);
        }

        public static SQLiteHelper getInstance(Context context) {
            if (sHelper == null) {
                synchronized (SQLiteHelper.class) {
                    if (sHelper == null) {
                        sHelper = new SQLiteHelper(context);
                    }
                }
            }
            return sHelper;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL(" create table if not exists" + " slice ("

            + "    url text primary key ,"

            + "    start integer not null,"

            + "    end integer not null,"

            + "    block integer not null,"

            + "    complete integer not null"

            + " )");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            db.execSQL(" DROP TABLE IF EXISTS" + " slice");

            onCreate(db);
        }
    }

}