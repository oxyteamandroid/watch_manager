/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.downloader;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.os.Handler;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.downloader.DownloadThread.Listener;

public class Downloader implements Listener, Runnable {

    static final String TAG = Downloader.class.getSimpleName();

    private static final int TIME_OUT = 5 * 1000;

    public static final int NEW = 0;
    public static final int PAUSE = 1;
    public static final int REMOVE = 2;

    private static Handler sHandle;
    private static DownloadInfoDB sDownloadInfoDb;

    private static Map<String, DownloadInfo> mMap = new HashMap<String, DownloadInfo>();

    private int mThreadCount;
    private volatile int mStatus;

    private DownloadInfo mInfo;
    private List<DownloadThread> mDownThreadList;

    private DownloadListener mListener;
    private DownloaderQueue mTaskQueue;

    private Thread mCurrentThread;
    private Exception mException;

    private int mLastProgress;
    private boolean mIsSuccess;

    public static void init(Context context) {
        if (null == sDownloadInfoDb) {
            synchronized (Downloader.class) {
                if (null == sDownloadInfoDb) {
                    sHandle = new Handler(context.getMainLooper());
                    sDownloadInfoDb = new DownloadInfoDB(context.getApplicationContext());
                }
            }
        }
    }

    private Downloader(String filePath, String fileUrl, int threadCount, DownloadListener listener) {
        mInfo = new DownloadInfo();
        mInfo.path = filePath;
        mInfo.url = fileUrl;
        this.mThreadCount = threadCount;
        this.mListener = listener == null ? sEmptyListener : listener;
        this.mDownThreadList = new ArrayList<DownloadThread>();
        this.mStatus = NEW;
        this.mCurrentThread = new Thread(this);
        DownloadInfo info = mMap.get(fileUrl);
        if (null != info)
            mInfo = info;
    }

    public void setUrlAndPath(String fileUrl, String path) {
        mInfo.url = fileUrl;
        mInfo.path = path;
        DownloadInfo info = mMap.get(fileUrl);
        if (null != info)
            mInfo = info;
    }

    public static Downloader getOrNewDownloader(String filePath, String fileUrl, int threadCount, DownloadListener listener) {
        Downloader downloader = new Downloader(filePath, fileUrl, threadCount, listener);
        return downloader;
    }

    public void start() {
        mCurrentThread.start();
    }

    @Override
    public void run() {

        try {
            IwdsLog.d(TAG, "start run : " + mInfo.url);

            if (!isPause()) {
                sHandle.post(downloadPreCallback);
            } else {
                return;
            }

            ParentDirCreate();

            List<SliceInfo> sliceList = null != mInfo.sliceList ? mInfo.sliceList : sDownloadInfoDb.getBreakPointInfo(mInfo.url, mInfo.path);

            if (null != sliceList && checkFileExists(mInfo.path)) {
                int fileSize = 0;
                for (SliceInfo s : sliceList) {
                    fileSize += s.block;
                }

                int completeSize = 0;
                for (SliceInfo s : sliceList) {
                    completeSize += s.completeSize;
                }

                mInfo.size = fileSize;
                mInfo.completeSize = completeSize;
                mInfo.sliceList = sliceList;

            } else {
                sDownloadInfoDb.remove(mInfo.url);
                mInfo.sliceList = null;
                mInfo.completeSize = 0;
                mInfo.progress = 0;
                mInfo.size = 0;

                mInfo.size = getFileSize();
                mInfo.sliceList = getNewSliceList(mInfo, mThreadCount);
            }
            mMap.put(mInfo.url, mInfo);
            IwdsLog.d(TAG, "mdownloadinfo.complete: " + mInfo.completeSize);

            for (SliceInfo s : mInfo.sliceList) {

                IwdsLog.d(TAG, mInfo.sliceList.size() + " slice ---> complete: " + s.completeSize + " size: " + s.block);
                if (s.completeSize == s.block) {
                    continue;
                }
                DownloadThread down = new DownloadThread(this, s, this);
                mDownThreadList.add(down);
            }
            if (isPause()) {
                return;
            }
            for (DownloadThread thread : mDownThreadList) {
                thread.start();
            }

            waitSubThread();

            getSubThreadExceptionAndThrow();

            mIsSuccess = mInfo.completeSize == mInfo.size;
            IwdsLog.d(TAG, "total complete: " + mInfo.completeSize + " total size: " + mInfo.size);

        } catch (final Exception e) {
            IwdsLog.d(TAG, e.toString());
            mException = e;
            if (!isPause()) {
                sHandle.post(downloadErrorCallback);
            }
            IwdsLog.d(TAG, "downloader exception !!!" + e.getMessage());
        } finally {
            IwdsLog.d(TAG, "downloader finally !!!");
            if (!isPause()) {
                sHandle.post(downloadSuccessCallback);
            }
            if (!isRemove() && !mIsSuccess) {
                sDownloadInfoDb.saveBreakPointInfo(mInfo.url, mInfo.sliceList);
            }
            mMap.remove(mInfo.url);
            if (isRemove() || mIsSuccess) {
                sDownloadInfoDb.remove(mInfo.url);
            }

            if (null != mTaskQueue) {
                sHandle.post(new Runnable() {
                    @Override
                    public void run() {
                        mTaskQueue.taskFinish(Downloader.this);
                        IwdsLog.d(TAG, "downloader taskFinish !!!");
                    }
                });
            }
        }
    }

    @Override
    public void updateCompleteSize(int size) {
        if (!isPause())
            updateComplete(size);
    }

    public synchronized void updateComplete(int size) {

        mInfo.addCompleteSize(size);

        int progress = mInfo.progress;

        if (mLastProgress == progress) {
            return;
        }
        mLastProgress = progress;
        if (!isPause()) {
            sHandle.post(updateProgressCallback);
        }
    }

    public void pause() {
        IwdsLog.d(TAG, "Downloader pause!");
        mStatus = PAUSE;
        mListener.onStop(true);
        IwdsLog.d(TAG, "isPause: " + isPause());
    }

    public void remove() {
        mStatus = REMOVE;
        mListener.onStop(false);
    }

    public static void remove(String url) {
        sDownloadInfoDb.remove(url);
    }

    private boolean checkFileExists(String filePath) {
        return new File(filePath).exists();
    }

    private void ParentDirCreate() {
        File fi = new File(mInfo.path);
        File parent = fi.getParentFile();
        if (!parent.exists())
            parent.mkdirs();
    }

    private void waitSubThread() {
        for (DownloadThread thread : mDownThreadList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private List<SliceInfo> getNewSliceList(DownloadInfo downInfo, int threadCount) throws IOException {

        String fileUrl = downInfo.url;
        String filePath = downInfo.path;
        long fileSize = downInfo.size;

        if (fileSize <= 0) {
            throw new IOException("file size exception：" + fileSize);
        }

        RandomAccessFile file = new RandomAccessFile(filePath, "rwd");
        file.setLength(fileSize);
        file.close();

        long remainder = fileSize % threadCount;

        long result = fileSize / threadCount;
        long block = remainder == 0 ? result : result + 1;

        List<SliceInfo> list = new ArrayList<SliceInfo>();

        for (int i = 0; i < threadCount; i++) {

            long blockTemp = block - ((i == (threadCount - 1)) ? (threadCount * block - fileSize) : 0);

            long start = i * block;
            long end = start + blockTemp - 1;

            SliceInfo sls = new SliceInfo(fileUrl, filePath, start, end, blockTemp, 0);
            list.add(sls);
        }
        return list;
    }

    private int getFileSize() throws IOException {

        int responseCode = 301;
        HttpURLConnection conn = null;
        int timeCount = 3;
        while (301 == responseCode && timeCount > 0) {
            timeCount--;
            if (null != conn)
                conn.disconnect();
            URL url = new URL(mInfo.url);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            setConnectionOtherParameter(conn);

            responseCode = conn.getResponseCode();
            mInfo.url = conn.getURL().toString();
        }
        int fileSize = conn.getContentLength();

        conn.disconnect();
        if (fileSize <= 0) {
            throw new IOException("file size exception: " + fileSize);
        }
        return fileSize;
    }

    public static void setConnectionOtherParameter(HttpURLConnection conn) {
        conn.setRequestProperty("Accept", "image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/x-shockwave-flash, application/xaml+xml, application/vnd.ms-xpsdocument, application/x-ms-xbap, application/x-ms-application, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*");
        conn.setRequestProperty("Accept-Language", "zh-CN");
        // conn.setRequestProperty("Referer", downloadUrl);
        conn.setRequestProperty("Charset", "UTF-8");
        conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)");
        conn.setRequestProperty("Connection", "Keep-Alive");
        conn.setConnectTimeout(TIME_OUT);
        conn.setReadTimeout(TIME_OUT);
    }

    public boolean isPause() {
        return PAUSE == mStatus || REMOVE == mStatus;
    }

    public boolean isRemove() {
        return REMOVE == mStatus;
    }

    private void getSubThreadExceptionAndThrow() throws Exception {
        String exMessage = "";

        for (DownloadThread th : mDownThreadList) {
            if (null != th.mException)
                exMessage += th.mException.toString() + "\r\n" + "\r\n";
        }

        if (exMessage.length() > 0) {
            throw new Exception(exMessage);
        }
    }

    private static DownloadListener sEmptyListener = new DownloadListener() {

        @Override
        public void onUpdateDownloadProgress(int progress, long completeSize, long fileSize) {
        }

        @Override
        public void onStop(boolean isPauseOrRemove) {
        }

        @Override
        public void onPreDownload() {
        }

        @Override
        public void onDownloadFinish(boolean isSuccess) {
        }

        @Override
        public void onDownloadError(Exception e) {
        }
    };

    public void setFinishListener(DownloaderQueue downloaderQueue) {
        mTaskQueue = downloaderQueue;
    }

    public String getId() {
        return mInfo.url;
    }

    public static interface DownloadListener {

        public void onUpdateDownloadProgress(int progress, long completeSize, long fileSize);

        public void onDownloadFinish(boolean isSuccess);

        public void onDownloadError(Exception e);

        public void onPreDownload();

        public void onStop(boolean isPauseOrRemove);
    }

    public static interface FinishListener {
        public void taskFinish(Downloader down);
    }

    private Runnable downloadPreCallback = new Runnable() {

        @Override
        public void run() {
            if (!isPause())
                mListener.onPreDownload();
        }
    };
    private Runnable downloadSuccessCallback = new Runnable() {

        @Override
        public void run() {
            if (!isPause())
                mListener.onDownloadFinish(mIsSuccess);
        }
    };

    private Runnable downloadErrorCallback = new Runnable() {
        @Override
        public void run() {
            if (!isPause())
                mListener.onDownloadError(mException);
        }
    };

    private Runnable updateProgressCallback = new Runnable() {
        @Override
        public void run() {
            if (!isPause())
                mListener.onUpdateDownloadProgress(mInfo.progress, mInfo.completeSize, mInfo.size);
        }
    };

}