/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.market;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.WMFragment;
import com.ingenic.watchmanager.util.DensityUtil;
import com.ingenic.watchmanager.util.ImageRequest;
import com.ingenic.watchmanager.util.ProgressUtils;
import com.ingenic.watchmanager.util.Request;
import com.ingenic.watchmanager.util.Request.ErrorListener;
import com.ingenic.watchmanager.util.Request.Listener;
import com.ingenic.watchmanager.util.StringRequest;
import com.ingenic.watchmanager.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

public class AppDetailFragment extends WMFragment implements ProgressUtils.Listener {

    private static final int GRADE_FAIL = -1;
    private static final int GRADE_SUCCESS = 1;
    private static final String FRAGMENT_TAG = "app";
    protected static final String TAG = AppDetailFragment.class.getSimpleName();
    private AppInfo mInfo;
    private TextView mNameView;
    private ImageView mIconView;
    private TextView mUseCountView;
    private TextView mSizeView;
    private TextView mDescriptionView;
    private LinearLayout mImageLayout;
    private ListView mRecommendAppList;
    private ProgressUtils mProgressUtils;

    private AppAdapter mAdapter;
    private LinearLayout mRecommendAppIcon;
    private TextView mDownloadStatusView;
    private Context mContext;
    private TextView mDeveloper;
    private TextView mLanguage;
    private TextView mCompact;
    private TextView mCreateTime;
    private TextView mVersion;

    private Dialog mDialog;

    public AppDetailFragment() {
    }

    public AppDetailFragment(AppInfo info) {
        mInfo = info;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_app_detail, container, false);

        initView(rootView);

        setup(mInfo);

        TextView gradeButton = (TextView) rootView.findViewById(R.id.grade);
        gradeButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                showDialog();
            }
        });

        mDownloadStatusView = (TextView) rootView.findViewById(R.id.tv_download);
        mContext = getActivity();
        mDownloadStatusView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                AppDetailFragment.setDownTextViewClickEvent(mInfo);
            }
        });

        refreshDownloadTV();

        initRecommend();

        mProgressUtils = new ProgressUtils(getActivity(), rootView, this);
        mProgressUtils.start();

        IwdsLog.d(this, "mInfo.packageName: " + mInfo.packageName);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        AppDownloadUtils.registerRefreshRunnable(this, new Runnable() {

            @Override
            public void run() {
                refreshDownloadTV();
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        AppDownloadUtils.unregisterRefreshRunnable(this);
    }

    private void refreshDownloadTV() {
        if (DownloadInfoRepository.getCurrentDownloadInfoMap().containsKey(mInfo.id)) {
            DownAppListInfo downInfo = DownloadInfoRepository.getCurrentDownloadInfoMap().get(mInfo.id);
            mDownloadStatusView.setText(downInfo.getStatusText());
            if (downInfo.finish.equals(DownAppListInfo.STATUS_COMPLETE)) {
                mDownloadStatusView.setText(mContext.getString(R.string.complete));
            }
        }
    }

    private void initView(View rootView) {
        mNameView = (TextView) rootView.findViewById(R.id.app_name);
        mIconView = (ImageView) rootView.findViewById(R.id.app_icon);
        mUseCountView = (TextView) rootView.findViewById(R.id.downCount);
        mSizeView = (TextView) rootView.findViewById(R.id.size);

        mDescriptionView = (TextView) rootView.findViewById(R.id.app_description);
        mImageLayout = (LinearLayout) rootView.findViewById(R.id.image_layout);
        mRecommendAppIcon = (LinearLayout) rootView.findViewById(R.id.recommendAppIcon);
        mRecommendAppList = (ListView) rootView.findViewById(R.id.recommendAppList);

        mDeveloper = (TextView) rootView.findViewById(R.id.developer);

        mLanguage = (TextView) rootView.findViewById(R.id.language);
        mCompact = (TextView) rootView.findViewById(R.id.compact);
        mCreateTime = (TextView) rootView.findViewById(R.id.create_time);
        mVersion = (TextView) rootView.findViewById(R.id.version);

        IwdsLog.d(TAG, "mDeveloper: " + mDeveloper + mLanguage + mCompact + mCreateTime + mVersion);
    }

    private void initRecommend() {
        String url = AppGetter.getRecommendApp(mInfo.id, 3);
        StringRequest request = new StringRequest(this, url, new Listener<String>() {

            @Override
            public void onResponse(String response) {
                List<AppInfo> infos = new ArrayList<AppInfo>();
                String result = response;

                try {
                    JSONObject obj = new JSONObject(result);
                    JSONArray array = obj.getJSONArray("list");

                    int count = array.length();
                    for (int i = 0; i < count; i++) {
                        JSONObject json = array.getJSONObject(i);
                        AppInfo info = simpleAppInfoFromJson(json);
                        if (info != null) {
                            infos.add(info);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }

                Activity activity = getActivity();
                if (null == activity)
                    return;
                mAdapter = new AppAdapter(activity, R.layout.app_list_item);
                mRecommendAppList.setAdapter(mAdapter);
                mAdapter.addAll(infos);
                mAdapter.notifyDataSetChanged();

                for (AppInfo info : infos) {

                    final AppInfo infoTemp = info;
                    final TextView tv = new TextView(getActivity());

                    tv.setText(info.name);

                    LinearLayout.LayoutParams para = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    tv.setLayoutParams(para);
                    int paddingDp = DensityUtil.dip2px(mContext, 10);
                    tv.setPadding(paddingDp, paddingDp, paddingDp, paddingDp);

                    mRecommendAppIcon.addView(tv);

                    tv.setTextAppearance(mContext, android.R.style.TextAppearance_Large);

                    tv.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), AppActivity.class);

                            intent.putExtra(AppActivity.EXTRA_APP, infoTemp);
                            getActivity().startActivity(intent);
                        }
                    });

                    final String urlStr = info.iconUrl;

                    IwdsLog.d(TAG, "urlStr: " + urlStr);

                    new ImageRequest(AppDetailFragment.this, urlStr, new Listener<Bitmap>() {

                        @Override
                        public void onResponse(Bitmap response) {

                            IwdsLog.d(TAG, "image isDetache: " + isDetached());

                            BitmapDrawable top = new BitmapDrawable(getResources(), response);

                            top.setBounds(0, 0, response.getWidth(), response.getHeight());
                            tv.setCompoundDrawables(null, top, null, null);
                            IwdsLog.d(TAG, "request recommend icon success! --> " + urlStr);

                        }
                    }, new ErrorListener() {

                        @Override
                        public void onErrorResponse(Exception ex) {
                            IwdsLog.d(TAG, "request recommend icon error! --> " + urlStr);
                        }
                    }).start();
                }
            }
        }, null);

        request.start();
    }

    @Override
    public void onDestroyView() {
        IwdsLog.d(TAG, "onDestoryView!");
        Request.cancelAll(this);
        super.onDestroyView();
    }

    protected AppInfo simpleAppInfoFromJson(JSONObject json) {
        try {
            AppInfo info = new AppInfo();
            info.name = json.getString("name");
            info.id = json.getLong("id");
            info.packageName = json.getString("package");
            info.iconUrl = json.getString("icon");
            info.description = json.getString("description");
            info.version = json.getString("version");
            info.size = json.getInt("size");
            info.devloper = json.getString("dev");
            info.uploadTime = json.getLong("addtime");
            info.downloadCount = json.getInt("down");
            info.score = json.getString("average");
            info.scoreCount = json.getInt("number");
            return info;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void showDialog() {
        mDialog = new MarketCustomDialog(mContext, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int rating) {
                onRatingChanged(rating);
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    public void onRatingChanged(int score) {

        String url = AppGetter.getGradeAppUrl(mInfo.id, score, "guest");

        StringRequest request = new StringRequest(this, url, new Listener<String>() {

            @Override
            public void onResponse(String response) {
                JSONObject obj;
                int code = 0;
                try {
                    obj = new JSONObject(response);
                    code = obj.getInt("resultCode");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                switch (code) {
                case GRADE_SUCCESS:
                    Toast.makeText(getActivity(), getActivity().getString(R.string.grade_success), Toast.LENGTH_SHORT).show();
                    break;
                case GRADE_FAIL:
                    Toast.makeText(getActivity(), getActivity().getString(R.string.grade_failure), Toast.LENGTH_SHORT).show();
                    break;
                default:
                    // Toast.makeText(getActivity(), "network wrong!", Toast.LENGTH_SHORT).show();
                    break;
                }
            }
        }, new ErrorListener() {

            @Override
            public void onErrorResponse(Exception ex) {
                Toast.makeText(getActivity(), getActivity().getString(R.string.grade_failure), Toast.LENGTH_SHORT).show();
            }
        });

        request.start();
    }

    private void setup(AppInfo info) {

        mNameView.setText(info.name);
        int useCount = info.downloadCount;
        if (useCount == 0) {
            mUseCountView.setText(R.string.no_downloads);
        } else {
            mUseCountView.setText(getString(R.string.downloads, Utils.formatCount(useCount)));
        }

        mSizeView.setText(Utils.formatSize(info.size));
        mDescriptionView.setText(info.description);

        ImageLoader.getInstance().displayImage(info.iconUrl, mIconView);

        if (info.imagesUrl != null) {
            addImages(info.imagesUrl);
        }

        mDeveloper.setText(getString(R.string.developer, info.devloper));
        mVersion.setText(getString(R.string.version, info.version));
        mCreateTime.setText(getString(R.string.uploadtime, new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(new Date(info.uploadTime))));

        mCompact.setText(getString(R.string.version, " "));
        mLanguage.setText(getString(R.string.language, Locale.getDefault().getDisplayLanguage()));

        IwdsLog.d(TAG, "info.devloper 2:" + info.devloper);
    }

    private void addImages(String... images) {
        int count = images.length;
        mImageLayout.removeAllViews();
        int viewCount = mImageLayout.getChildCount();

        if (count > viewCount) {
            for (int i = viewCount; i < count; i++) {
                ImageView view = new ImageView(getActivity());
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(DensityUtil.dip2px(mContext, 120), DensityUtil.dip2px(mContext, 210));
                lp.leftMargin = lp.rightMargin = lp.topMargin = lp.bottomMargin = 20;
                view.setImageDrawable(new ColorDrawable(getResources().getColor(R.color.default_pic_color)));// 0xFF999999));

                mImageLayout.addView(view, lp);
            }
        } else if (viewCount > count) {
            for (int i = viewCount - 1; i >= count; i--) {
                mImageLayout.removeViewAt(i);
            }
        }

        for (int i = 0; i < count; i++) {
            final ImageView view = (ImageView) mImageLayout.getChildAt(i);
            ImageLoader.getInstance().displayImage(images[i], view);
        }
    }

    @Override
    public String getFragmentTag() {
        return FRAGMENT_TAG;
    }

    public static void setDownTextViewClickEvent(AppInfo info) {
        long start = System.currentTimeMillis();
        IwdsLog.d(TAG, "new click timestamp: " + System.currentTimeMillis());
        DownAppListInfo down = DownloadInfoRepository.getCurrentDownloadInfoMap().get(info.id);
        if (null == down) {
            AppDownloadUtils.getDownloadFileUrlAndDownload(info);
        } else {
            down.triggerEvent();
        }
        IwdsLog.d(TAG, "new click timestamp end: " + (System.currentTimeMillis() - start));
    }

    @Override
    public void request() {
        String url = AppGetter.fillAppDetailUrl(mInfo);
        IwdsLog.d(TAG, "app detail url:" + url);
        StringRequest request = new StringRequest(this, url, new Listener<String>() {

            @Override
            public void onResponse(String response) {

                AppGetter.getAppDetail(mInfo, response);
                setup(mInfo);
                mProgressUtils.end();
            }
        }, new ErrorListener() {

            @Override
            public void onErrorResponse(Exception ex) {
                mProgressUtils.fail();
            }
        });
        request.start();
    }
}
