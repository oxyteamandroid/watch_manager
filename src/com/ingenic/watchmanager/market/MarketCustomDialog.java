/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.market;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ingenic.watchmanager.R;

public class MarketCustomDialog extends Dialog {

    private String mContent;
    private android.view.View.OnClickListener mListener;
    private OnClickListener mDialogListener;
    private String mName;

    public MarketCustomDialog(Context context, String content, View.OnClickListener listener) {
        super(context);
        mContent = content;
        mListener = listener;
    }

    public MarketCustomDialog(Context context, OnClickListener onClickListener) {
        super(context);
        mDialogListener = onClickListener;
    }

    public void setName(String name) {

        mName = name;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custom);

        TextView content = (TextView) findViewById(R.id.content);
        if (mName != null) {
            TextView name = (TextView) findViewById(R.id.name);
            name.setText(mName);
            name.setVisibility(View.VISIBLE);
        }

        final RatingBar ratingBar = (RatingBar) findViewById(R.id.rating);
        ratingBar.setStepSize(1.0f);
        ratingBar.setNumStars(5);
        if (mContent != null) {
            content.setText(mContent);
            ratingBar.setVisibility(View.GONE);
        } else {
            content.setVisibility(View.GONE);
        }

        findViewById(R.id.confirm).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mListener != null)
                    mListener.onClick(v);
                if (mDialogListener != null)
                    mDialogListener.onClick(MarketCustomDialog.this, (int) ratingBar.getRating());
            }
        });

        findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                MarketCustomDialog.this.dismiss();
            }
        });
    }
}
