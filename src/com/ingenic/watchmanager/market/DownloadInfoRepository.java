/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.market;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;

import com.ingenic.iwds.utils.IwdsLog;

@SuppressLint("UseSparseArrays")
public class DownloadInfoRepository {

    private static final String TAG = DownloadInfoRepository.class.getSimpleName();

    private static DownAppListInfoOperator sDownloadInfoOperator;

    public static List<DownAppListInfo> sCurrentDownloadList;
    public static List<DownAppListInfo> sCurrentDownloadingList;
    public static List<DownAppListInfo> sCurrentDownloadFinishList;

    public static Map<Long, DownAppListInfo> sCurrentDownloadInfoMap;
    public static Map<String, DownAppListInfo> sCurrentPackageNameMap;

    public static void init(Context context) {
        sDownloadInfoOperator = new DownAppListInfoOperator(context);
    }

    public static List<DownAppListInfo> getDownloadingList() {

        if (null != sCurrentDownloadingList)
            return sCurrentDownloadingList;
        sCurrentDownloadingList = new ArrayList<DownAppListInfo>();

        for (DownAppListInfo downApp : getCurrentDownloadList()) {
            if (!downApp.isComplete()) {
                sCurrentDownloadingList.add(downApp);
            }
        }
        return sCurrentDownloadingList;
    }

    public static List<DownAppListInfo> getDownloadFinishList() {
        if (null != sCurrentDownloadFinishList)
            return sCurrentDownloadFinishList;
        sCurrentDownloadFinishList = new ArrayList<DownAppListInfo>();

        for (DownAppListInfo downApp : getCurrentDownloadList()) {
            if (downApp.isComplete()) {
                sCurrentDownloadFinishList.add(downApp);
            }
        }
        return sCurrentDownloadFinishList;
    }

    public static List<DownAppListInfo> getCurrentDownloadList() {

        if (null != sCurrentDownloadList)
            return sCurrentDownloadList;

        sCurrentDownloadList = sDownloadInfoOperator.queryAll();
        sCurrentDownloadList = sCurrentDownloadList == null ? new ArrayList<DownAppListInfo>() : sCurrentDownloadList;
        IwdsLog.d(TAG, "queryAll : " + sCurrentDownloadList.size());
        return sCurrentDownloadList;
    }

    public static Map<String, DownAppListInfo> getCurrentPackageNameMap() {
        if (null != sCurrentPackageNameMap)
            return sCurrentPackageNameMap;
        sCurrentPackageNameMap = new LinkedHashMap<String, DownAppListInfo>();

        for (DownAppListInfo downApp : getCurrentDownloadList()) {
            sCurrentPackageNameMap.put(downApp.pkName, downApp);
        }
        return sCurrentPackageNameMap;
    }

    public static Map<Long, DownAppListInfo> getCurrentDownloadInfoMap() {

        if (null != sCurrentDownloadInfoMap)
            return sCurrentDownloadInfoMap;
        sCurrentDownloadInfoMap = new HashMap<Long, DownAppListInfo>();

        for (DownAppListInfo downApp : getCurrentDownloadList()) {
            sCurrentDownloadInfoMap.put(downApp.id, downApp);
        }
        return sCurrentDownloadInfoMap;
    }

    public static void clear() {

        for (DownAppListInfo it : sCurrentDownloadList) {
            IwdsLog.d(TAG, "id: " + it.name);
        }

        sCurrentDownloadingList = null;
        sCurrentDownloadFinishList = null;

        sCurrentDownloadInfoMap = null;
        sCurrentPackageNameMap = null;
    }
}