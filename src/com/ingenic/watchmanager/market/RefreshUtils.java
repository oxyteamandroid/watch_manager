/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.market;

import android.os.Handler;
import android.os.Looper;

/**
 * 间隔刷新工具类
 * 
 * @author yanbo.jiang
 * 
 */
public class RefreshUtils {
    private static final int DEFAULT_CD = 1000;
    private long cd;
    private Handler mHandler;
    private Listener mListener;
    private boolean isRefresh;
    private Runnable mRunnable = new Runnable() {

        @Override
        public void run() {
            if (null != mListener)
                mListener.refresh();
            isRefresh = false;
        }
    };

    public RefreshUtils(long time, Listener listener) {
        cd = time;
        mListener = listener;
        mHandler = new Handler(Looper.getMainLooper());
    }

    public RefreshUtils(Listener listener) {
        this(DEFAULT_CD, listener);
    }

    public void refresh(boolean immediate) {
        if (immediate)
            mHandler.post(mRunnable);
        if (isRefresh)
            return;
        isRefresh = true;
        mHandler.postDelayed(mRunnable, cd);
    }

    public static interface Listener {
        public void refresh();
    }
}