/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  elf/watch-apps/WatchManager project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.market;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONException;
import org.json.JSONObject;

import a_vcard.android.text.TextUtils;
import android.content.Context;
import android.os.Environment;
import android.widget.BaseAdapter;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.downloader.Downloader;
import com.ingenic.watchmanager.downloader.Downloader.DownloadListener;
import com.ingenic.watchmanager.downloader.DownloaderQueue;
import com.ingenic.watchmanager.util.Request.ErrorListener;
import com.ingenic.watchmanager.util.StringRequest;
import com.ingenic.watchmanager.util.Utils;

public class AppDownloadUtils {

    private static final String TAG = AppDownloadUtils.class.getSimpleName();

    public static final String ACCOUNT = "guest";
    public static final String PASSWORD = "123456";

    public static final String ROOT_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static final String DOWNLOAD_PATH = ROOT_PATH + "/watchmanager/market/";

    private static Context mContext;

    public static final Map<Object, Runnable> sRefreshListenerMap = new LinkedHashMap<Object, Runnable>();
    private static DownAppListInfoOperator sDownloadInfoOperator;

    private static DownloaderQueue sDownloadQueue;

    public static void init(Context context) {
        mContext = context.getApplicationContext();
        sDownloadQueue = new DownloaderQueue();
        sDownloadInfoOperator = new DownAppListInfoOperator(mContext);
        DownloadInfoRepository.init(mContext);
        Downloader.init(context);
        RemoteDeviceUtils.init(context);
    }

    public static Downloader getDownloadFileUrlAndDownload(DownAppListInfo down) {

        final Downloader downloader = downloadFile(down);
        if (null != down.url) {
            sDownloadQueue.enqueue(downloader);
            down.download = downloader;
            return downloader;
        }

        final DownAppListInfo downInfo = down;
        StringRequest request = new StringRequest(mContext, downInfo.bakUrl, new StringRequest.Listener<String>() {

            @Override
            public void onResponse(String response) {

                JSONObject json;
                String addr = null;
                try {
                    json = new JSONObject(response);
                    addr = json.getString("addr");
                } catch (JSONException e) {
                    e.printStackTrace();
                    downInfo.finish = DownAppListInfo.STATUS_GET_ADDR_ERROR;
                    sendStatusChangeBroadcast();
                    return;
                }
                if (null == addr) {
                    downInfo.finish = DownAppListInfo.STATUS_GET_ADDR_ERROR;
                    sendStatusChangeBroadcast();
                    return;
                }
                downInfo.url = addr;
                downInfo.path = DOWNLOAD_PATH + Utils.getNameByUrl(addr);

                if (downInfo.finish != DownAppListInfo.STATUS_PAUSE) {
                    downInfo.finish = DownAppListInfo.STATUS_WAIT;
                }

                sDownloadInfoOperator.save(downInfo);
                IwdsLog.d(TAG, "queryAll: save donwInfo!");
                List<DownAppListInfo> tem = sDownloadInfoOperator.queryAll();
                if (tem != null) {
                    IwdsLog.d(TAG, "queryAll: " + tem.size());
                } else {
                    IwdsLog.d(TAG, "queryAll: " + 0);
                }
                DownloadInfoRepository.clear();
                sendStatusChangeBroadcast();

                downloader.setUrlAndPath(addr, downInfo.path);
                sDownloadQueue.enqueue(downloader);
                IwdsLog.d(TAG, "file url: " + downInfo.url);
            }

        }, new ErrorListener() {

            @Override
            public void onErrorResponse(Exception ex) {
                downInfo.finish = DownAppListInfo.STATUS_GET_ADDR_ERROR;
                sendStatusChangeBroadcast();
            }
        });

        request.start();
        return downloader;
    }

    public static Downloader getDownloadFileUrlAndDownload(AppInfo info) {
        IwdsLog.d(TAG, "getDownloadFileUrlAndDownload");

        DownAppListInfo down = DownloadInfoRepository.getCurrentDownloadInfoMap().get(info.id);
        if (null == down) {
            down = new DownAppListInfo();
            down.id = info.id;
            down.progress = 0;
            down.name = info.name;
            down.description = info.description;
            // down.url = addr;
            // down.path = DOWNLOAD_PATH + Utils.getNameByUrl(addr);
            down.finish = DownAppListInfo.STATUS_GET_ADDR;
            down.iconUrl = info.iconUrl;
            down.pkName = info.packageName;
            down.category = info.typeName;
            down.time = System.currentTimeMillis();
            down.size = info.size;

            String account = ACCOUNT;
            String pwd = PASSWORD;
            String url = AppGetter.getDownloadAppUrl(info.id, account, pwd);
            down.bakUrl = url;

            sDownloadInfoOperator.save(down);
            DownloadInfoRepository.sCurrentDownloadList.add(down);
            DownloadInfoRepository.clear();
            sendStatusChangeBroadcast();
        }
        down.download = getDownloadFileUrlAndDownload(down);
        return down.download;
    }

    public static void changeDownloadInfoStatus(String pkName, String f) {
        DownAppListInfo info = DownloadInfoRepository.getCurrentPackageNameMap().get(pkName);
        if (null == info) {
            IwdsLog.d(TAG, "info is null , send broadcast fail!");
            return;
        }
        IwdsLog.d(TAG, "pkName: " + pkName + " not null ---> original status: " + info.finish);
        info.finish = f;
        sDownloadInfoOperator.save(info);
        sendStatusChangeBroadcast();
        IwdsLog.d(TAG, "id: " + pkName + " not null ---> status: " + info.finish);
    }

    private static RefreshUtils refreshUtils = new RefreshUtils(new RefreshUtils.Listener() {

        @Override
        public void refresh() {
            for (Entry<Object, Runnable> it : sRefreshListenerMap.entrySet()) {
                it.getValue().run();
            }
        }
    });

    public static void sendStatusChangeBroadcast(boolean immediate) {
        refreshUtils.refresh(immediate);
        IwdsLog.d(TAG, "refresh !!!");
    }

    public static void sendStatusChangeBroadcast() {
        refreshUtils.refresh(false);
    }

    public static void refreshAdapterListener(final BaseAdapter adapter) {
        sRefreshListenerMap.put(adapter, new Runnable() {

            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    public static void unrefreshAdapterListener(final BaseAdapter adapter) {
        sRefreshListenerMap.remove(adapter);
    }

    public static void registerRefreshRunnable(Object key, Runnable run) {
        sRefreshListenerMap.put(key, run);
    }

    public static void unregisterRefreshRunnable(Object key) {
        sRefreshListenerMap.remove(key);
    }

    public static Downloader downloadFile(final DownAppListInfo info) {
        info.finish = DownAppListInfo.STATUS_WAIT;

        Downloader downloader = Downloader.getOrNewDownloader(info.path, info.url, 1, new DownloadListener() {

            @Override
            public void onUpdateDownloadProgress(int progress, long byteCount, long fileSize) {
                info.progress = progress;
                info.completeSize = byteCount;
                sendStatusChangeBroadcast();
                IwdsLog.v(TAG, "download id: " + info.name + " ---> progress: " + progress + " --> complete: " + byteCount + " --> size: " + fileSize);
            }

            @Override
            public void onPreDownload() {
                info.finish = DownAppListInfo.STATUS_DOWNLOADING;
                sDownloadInfoOperator.save(info);
                sendStatusChangeBroadcast(true);
                IwdsLog.v(TAG, "statue change ---> id: " + info.id);
            }

            @Override
            public void onDownloadFinish(boolean isSuccess) {
                IwdsLog.d(TAG, "statue finish ---> id: " + isSuccess);
                if (isSuccess) {
                    info.finish = DownAppListInfo.STATUS_COMPLETE;
                    info.time = System.currentTimeMillis();
                }
                sDownloadInfoOperator.save(info);
                DownloadInfoRepository.clear();
                sendStatusChangeBroadcast(true);
            }

            @Override
            public void onDownloadError(Exception e) {
                e.printStackTrace();
                IwdsLog.d(TAG, "statue error ---> id: " + info.id);
                info.finish = DownAppListInfo.STATUS_ERROR;
                // sDownloadInfoOperator.save(daInfo);
                sendStatusChangeBroadcast();
            }

            @Override
            public void onStop(boolean isPauseOrRemove) {
                if (isPauseOrRemove) {
                    IwdsLog.d(TAG, "statue pause ---> id: " + info.id);
                    info.finish = DownAppListInfo.STATUS_PAUSE;
                } else {
                    IwdsLog.d(TAG, "statue remove ---> id: " + info.id);
                    removeFile(info.path);
                }
                sDownloadInfoOperator.save(info);
                sendStatusChangeBroadcast(true);
            }
        });
        // sDownloadQueue.enqueue(downloader);
        sendStatusChangeBroadcast();
        return downloader;
    }

    public static void removeAll() {
        List<DownAppListInfo> list = DownloadInfoRepository.getDownloadingList();
        for (DownAppListInfo it : list) {
            Downloader.remove(it.url);
            removeFile(it.path);
            sDownloadInfoOperator.delete(it);
            IwdsLog.d(TAG, "it.id: " + it.id + " " + it.path);
        }
        IwdsLog.d(TAG, "remove downInfo size: " + list.size());
        DownloadInfoRepository.sCurrentDownloadList.removeAll(list);

        DownloadInfoRepository.clear();
        sendStatusChangeBroadcast(true);
    }

    public static void resumeAll() {
        List<DownAppListInfo> list = DownloadInfoRepository.getDownloadingList();
        for (DownAppListInfo it : list) {
            if (it.download == null || it.isPause()) {
                getDownloadFileUrlAndDownload(it);
                IwdsLog.d(TAG, "resume name: " + it.name);
            }
            IwdsLog.d(TAG, "resume name --- no: " + it.name + "  " + it.isPause() + " " + it.finish);
        }
    }

    private static void removeFile(String filePath) {
        if (!TextUtils.isEmpty(filePath)) {
            File file = new File(filePath);
            if (file.exists())
                file.delete();
        }
    }

    public static void removeFile(DownAppListInfo info) {
        IwdsLog.d(TAG, "resumeDownload name: " + info.name);
        Downloader.remove(info.url);
        removeFile(info.path);
        sDownloadInfoOperator.delete(info.id);
        DownloadInfoRepository.sCurrentDownloadList.remove(info);
        IwdsLog.d(TAG, "Downloader removeFileDownload! id: " + info.id + " path: " + info.path);
        DownloadInfoRepository.clear();
        sendStatusChangeBroadcast(true);
    }
}
