/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.market;

import android.content.Intent;
import android.os.Bundle;

import com.ingenic.watchmanager.WMActivity;

public class AppActivity extends WMActivity {
    static final String EXTRA_APP = "app";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        init(intent);
    }

    private void init(Intent intent) {
        AppInfo info = (AppInfo) intent.getSerializableExtra(EXTRA_APP);
        if (info == null) {
            finish();
            return;
        }

        AppDetailFragment fragment = new AppDetailFragment(info);
        show(fragment, false, fragment.getFragmentTag());
    }
}
