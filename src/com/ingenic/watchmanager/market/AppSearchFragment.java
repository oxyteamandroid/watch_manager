/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.market;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.util.Request;
import com.ingenic.watchmanager.util.Request.ErrorListener;
import com.ingenic.watchmanager.util.Request.Listener;
import com.ingenic.watchmanager.util.StringRequest;
import com.ingenic.watchmanager.util.StringRequest.Range;

public class AppSearchFragment extends ListFragment {
    private EditText mSearchView;
    private Button mSearchButton;
    private AppAdapter mAdapter;
    private AutoCompleteTextView mAutoSearchView;
    private Range mRange;
    protected String mCurrentChar = "";
    protected ArrayAdapter<String> mEmptyAadapter;

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mAdapter = new AppAdapter(activity, R.layout.app_list_item);
        mEmptyAadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, new ArrayList<String>());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListAdapter(mAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_app_search, container, false);
        mSearchButton = (Button) rootView.findViewById(R.id.button1);
        mSearchButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CharSequence cs = mSearchView.getText();
                if (cs == null || cs.length() == 0 || cs == "") {
                    getActivity().onBackPressed();
                } else {
                    search(cs.toString());
                }
            }
        });
        mSearchView = (EditText) rootView.findViewById(R.id.autoCompleteTextView1);
        initSearchView();
        mSearchView.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView arg0, int actionId, KeyEvent arg2) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    mSearchButton.performClick();
                    return true;
                }
                return false;
            }
        });
        mAutoSearchView = (AutoCompleteTextView) mSearchView;
        mAutoSearchView.setThreshold(1);
        initSearchView();
        initRecommendAPP(rootView);

        mRange = new StringRequest.Range();
        return rootView;
    }

    private void initSearchView() {
        mSearchView.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s == null || s.length() == 0 || s == "") {
                    mSearchButton.setText(android.R.string.cancel);
                } else {
                    mSearchButton.setText(android.R.string.search_go);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String result = s.toString().trim();
                if (result.length() > 0 && result.substring(0, 1) != mCurrentChar) {
                    mCurrentChar = s.toString().trim().substring(0, 1);
                    searchSuggest();
                }
            }
        });
    }

    private void searchSuggest() {

        String searchStr = mSearchView.getText().toString().trim();
        searchStr = mCurrentChar;
        if (searchStr.length() > 0) {
            String searchKey = searchStr;
            final String searchKeywork = searchStr;

            StringRequest requst = new StringRequest(getActivity(), AppGetter.getSearchSuggestUrl(searchKey), new Request.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (searchKeywork.equals(mSearchView.getText().toString().trim())) {
                        List<String> suggestList = Arrays.asList(response.split(","));
                        ArrayAdapter<String> mAadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, suggestList);

                        if (response.length() > 0 && !searchKeywork.equals(response.trim())) {
                            mAutoSearchView.setAdapter(mAadapter);
                            mAutoSearchView.onFilterComplete(1);
                        }
                    }
                }
            }, new ErrorListener() {

                @Override
                public void onErrorResponse(Exception ex) {
                    mCurrentChar = "";
                }
            });
            requst.start();
        }
    }

    private void initRecommendAPP(View rootView) {

        final GridView recommendGV = (GridView) rootView.findViewById(R.id.recommendGV);

        String url = AppGetter.getRecommendSearchUrl();
        StringRequest request = new StringRequest(getActivity(), url, new Request.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response.trim().length() < 1)
                    return;
                String[] keywork = response.split(",");
                int i = 0;
                for (String s : keywork) {
                    keywork[i++] = s.trim();
                }
                initRecommendApp(recommendGV, Arrays.asList(keywork));
            }
        }, new Request.ErrorListener() {

            @Override
            public void onErrorResponse(Exception ex) {
                ex.printStackTrace();
                initRecommendApp(recommendGV, null);
            }
        });
        request.start();
        recommendGV.setOnItemClickListener(new OnItemClickListener() {

            @SuppressWarnings("unchecked")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String searchStr = ((TextView) view).getText().toString().trim();
                ArrayAdapter<String> adapter = (ArrayAdapter<String>) mAutoSearchView.getAdapter();
                mAutoSearchView.setAdapter(mEmptyAadapter);
                mAutoSearchView.setText(searchStr);
                mAutoSearchView.setSelection(searchStr.length());
                mAutoSearchView.setAdapter(adapter);
                mSearchButton.performClick();
            }
        });
    }

    private void initRecommendApp(GridView recommendGV, List<String> listStr) {

        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        if (null != listStr) {
            for (int i = 0; i < listStr.size(); i++) {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("Text", listStr.get(i));
                list.add(m);
            }
        }
        String[] from = { "Text" };
        int[] to = { android.R.id.text1 };

        SimpleAdapter adapter = new SimpleAdapter(getActivity(), list, android.R.layout.simple_list_item_1, from, to) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView tv = (TextView) view.findViewById(android.R.id.text1);
                tv.setGravity(Gravity.CENTER);
                return view;
            }
        };

        recommendGV.setAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(getActivity(), AppActivity.class);
        intent.putExtra(AppActivity.EXTRA_APP, mAdapter.getItem(position));
        getActivity().startActivity(intent);
    }

    private void search(String keyword) {
        int start = mRange.getStart();
        int end = mRange.getEnd();
        String url = AppGetter.getSearchAppUrl(StringRequest.UUID, keyword, start, end);
        StringRequest request = new StringRequest(getActivity(), url, new Listener<String>() {

            @Override
            public void onResponse(String response) {

                List<AppInfo> infos = AppGetter.getSearchApp(response);
                mAdapter.clear();
                if (infos != null) {
                    mAdapter.addAll(infos);
                }
                mAdapter.notifyDataSetChanged();
                mRange.success();
            }
        }, new ErrorListener() {

            @Override
            public void onErrorResponse(Exception ex) {
            }
        });
        request.start();
    }
}
