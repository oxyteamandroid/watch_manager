/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.market;

import java.util.List;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;

public class DownloadingFragment extends Fragment {
    protected static final String TAG = DownloadingFragment.class.getSimpleName();
    private TextView mRemoveAll;
    private TextView mResumeAll;

    private ListView mDownList;
    private DownAdapter mAdapter;
    private List<DownAppListInfo> mDownAppListInfo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_donwload_list, container, false);
        mDownList = (ListView) rootView.findViewById(R.id.downloadList);
        mRemoveAll = (TextView) rootView.findViewById(R.id.removeAll);
        mResumeAll = (TextView) rootView.findViewById(R.id.resumeAll);

        mAdapter = new DownAdapter(getActivity(), R.layout.fragment_download_list_item);
        mDownList.setAdapter(mAdapter);
        mResumeAll.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                AppDownloadUtils.resumeAll();
            }
        });
        mRemoveAll.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                AppDownloadUtils.removeAll();
            }
        });

        return rootView;
    }

    public void refreshData() {

        mDownAppListInfo = DownloadInfoRepository.getDownloadingList();
        IwdsLog.v(TAG, "download count: " + mDownAppListInfo.size());
        mAdapter.clear();
        mAdapter.addAll(mDownAppListInfo);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();

        AppDownloadUtils.registerRefreshRunnable(this, new Runnable() {

            @Override
            public void run() {
                refreshData();
                IwdsLog.v(TAG, "refreshData --->  status or progress change");
            }
        });
        refreshData();
        IwdsLog.d(TAG, "onstart!");
    }

    @Override
    public void onStop() {
        super.onStop();
        AppDownloadUtils.unregisterRefreshRunnable(this);
        IwdsLog.d(TAG, "onstop!");
    }
}