/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.market;

import java.util.List;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.util.ProgressUtils;
import com.ingenic.watchmanager.util.Request.ErrorListener;
import com.ingenic.watchmanager.util.Request.Listener;
import com.ingenic.watchmanager.util.StringRequest;
import com.ingenic.watchmanager.util.StringRequest.Range;

public class AppListFragment extends ListFragment implements ProgressUtils.Listener {

    protected static final String TAG = AppListFragment.class.getSimpleName();
    private AppAdapter mAdapter;
    private int mType;
    private String mState;
    private ProgressUtils mProgressUtils;
    private Range mRange;
    private StringRequest mStringRequest;
    protected boolean mIsRequest;

    public AppListFragment() {
    }

    public AppListFragment(int type, String state) {
        mType = type;
        mState = state;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_app_list, container, false);

        mAdapter = new AppAdapter(getActivity(), R.layout.app_list_item);
        setListAdapter(mAdapter);

        mRange = new Range();

        mProgressUtils = new ProgressUtils(getActivity(), rootView, this);
        mProgressUtils.start();

        ListView lv = (ListView) rootView.findViewById(android.R.id.list);
        scrollRefresh(lv);

        return rootView;
    }

    private void scrollRefresh(ListView lv) {
        lv.setOnScrollListener(new OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (OnScrollListener.SCROLL_STATE_IDLE == scrollState) {
                    if (null == mStringRequest && mIsRequest) {
                        request();
                        mIsRequest = false;
                    }
                }
                IwdsLog.d(TAG, SCROLL_STATE_IDLE + " onScrollStateChanged : " + scrollState);
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                IwdsLog.v(TAG, " int firstVisibleItem, int visibleItemCount, int totalItemCount : " + firstVisibleItem + "," + visibleItemCount + "," + totalItemCount);

                if (totalItemCount < (firstVisibleItem + visibleItemCount + 4)) {
                    mIsRequest = true;
                } else {
                    mIsRequest = false;
                }
            }
        });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(getActivity(), AppActivity.class);
        intent.putExtra(AppActivity.EXTRA_APP, mAdapter.getItem(position));
        getActivity().startActivity(intent);
        // mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {

        super.onStart();
        AppDownloadUtils.refreshAdapterListener(mAdapter);
        mAdapter.notifyDataSetChanged();
        IwdsLog.d(TAG, "onstart!");
    }

    @Override
    public void onStop() {
        IwdsLog.d(TAG, "onstop!");
        super.onStop();
        AppDownloadUtils.unrefreshAdapterListener(mAdapter);
    }

    @Override
    public void request() {
        int start = mRange.getStart();
        int end = mRange.getEnd();

        String url = null;
        if (mState != null) {
            url = AppGetter.getAppListUrl(StringRequest.UUID, mState, start, end);
        } else if (mType != -1) {
            url = AppGetter.getAppsWithTypeUrl(StringRequest.UUID, mType, start, end);
        }

        mStringRequest = new StringRequest(getActivity(), url, new Listener<String>() {

            @Override
            public void onResponse(String response) {
                List<AppInfo> infos = AppGetter.getAppList(response);
                mAdapter.addAll(infos);
                mAdapter.notifyDataSetChanged();
                mProgressUtils.end();
                mRange.success();
                mStringRequest = null;
            }
        }, new ErrorListener() {

            @Override
            public void onErrorResponse(Exception ex) {
                mProgressUtils.fail();
                mStringRequest = null;
            }
        });
        mStringRequest.start();
    }
}