/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.market;

import java.util.LinkedHashMap;
import java.util.Map;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.downloader.Downloader;

public class DownAppListInfo {

    private static final String TAG = DownAppListInfo.class.getSimpleName();

    public static final String STATUS_GET_ADDR = "G";
    public static final String STATUS_WAIT = "W";
    public static final String STATUS_DOWNLOADING = "N";

    public static final String STATUS_COMPLETE = "Y";
    public static final String STATUS_INSTALLED = "I";
    public static final String STATUS_FAIL = "F";

    public static final String STATUS_GET_ADDR_ERROR = "R";
    public static final String STATUS_ERROR = "E";
    public static final String STATUS_PAUSE = "P";

    public static Map<String, Integer> sStatusDic;
    public static Map<String, Integer> sEventDic;

    public long id;
    public int progress;

    public String bakUrl;

    public String name;
    public String description;

    public String url;
    public String path;
    public String finish;

    public String iconUrl;
    public String pkName;

    public long size;
    public long completeSize;

    public long time;
    public String category;

    public Downloader download;

    public Map<String, Integer> getDic() {
        if (null == sStatusDic) {
            sStatusDic = new LinkedHashMap<String, Integer>();

            sStatusDic.put(STATUS_GET_ADDR, R.string.get_down_addr);
            sStatusDic.put(STATUS_WAIT, R.string.wait_for_download);
            sStatusDic.put(STATUS_DOWNLOADING, R.string.downloading);

            sStatusDic.put(STATUS_GET_ADDR_ERROR, R.string.get_down_error);
            sStatusDic.put(STATUS_ERROR, R.string.download_error);
            sStatusDic.put(STATUS_PAUSE, R.string.pause);

            sStatusDic.put(STATUS_COMPLETE, R.string.install);
            sStatusDic.put(STATUS_INSTALLED, R.string.installed);
            sStatusDic.put(STATUS_FAIL, R.string.install_fail2);
        }
        return sStatusDic;
    }

    public Map<String, Integer> getEventDic() {
        if (null == sEventDic) {
            sEventDic = new LinkedHashMap<String, Integer>();

            sEventDic.put(STATUS_GET_ADDR, R.string.pause);
            sEventDic.put(STATUS_WAIT, R.string.pause);
            sEventDic.put(STATUS_DOWNLOADING, R.string.pause);

            sEventDic.put(STATUS_GET_ADDR_ERROR, R.string.continue_);
            sEventDic.put(STATUS_ERROR, R.string.continue_);
            sEventDic.put(STATUS_PAUSE, R.string.continue_);

            sEventDic.put(STATUS_COMPLETE, R.string.install);
            sEventDic.put(STATUS_INSTALLED, R.string.installed);
            sEventDic.put(STATUS_FAIL, R.string.install_fail2);
        }
        return sEventDic;
    }

    public int getStatusText() {
        return getDic().get(finish);
    }

    public int getEventText() {
        return getEventDic().get(finish);
    }

    public void triggerEvent() {

        if (isComplete()) {
            IwdsLog.d(TAG, "Downloader triggerEvent complete!");
            return;
        }

        if (!isDownloading()) {
            finish = STATUS_WAIT;
            if (null != download) {
                download.pause();
            }
            AppDownloadUtils.getDownloadFileUrlAndDownload(this);
            IwdsLog.d(TAG, "Downloader resumeDownloadFile!");
        } else {
            if (null != download) {
                download.pause();
                IwdsLog.d(TAG, "Downloader pauseDownloadFile! ___>  url: " + url);
                return;
            }
            finish = STATUS_PAUSE;
            AppDownloadUtils.sendStatusChangeBroadcast(true);
            IwdsLog.d(TAG, "Downloader pauseDownloadFile! url: " + url);
        }
    }

    public DownAppListInfo() {
    }

    private boolean isDownloading() {
        return STATUS_DOWNLOADING.equals(finish) || STATUS_WAIT.equals(finish) || STATUS_GET_ADDR.equals(finish);
    }

    public boolean isPause() {
        return STATUS_GET_ADDR_ERROR.equals(finish) || STATUS_ERROR.equals(finish) || STATUS_PAUSE.equals(finish);
    }

    public boolean isComplete() {
        return STATUS_COMPLETE.equals(finish) || STATUS_INSTALLED.equals(finish) || STATUS_FAIL.equals(finish);
    }
}