/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.market;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.iwds.remotedevice.RemoteDeviceManagerInfo;
import com.ingenic.iwds.remotedevice.RemoteDeviceServiceManager;
import com.ingenic.iwds.remotedevice.RemoteDeviceStatusListener;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.app.AppManagerFragment;
import com.ingenic.watchmanager.app.SimpleImplListener;
import com.ingenic.watchmanager.market.RemoteDeviceUtils.Callback;
import com.ingenic.watchmanager.market.RemoteDeviceUtils.Register;
import com.ingenic.watchmanager.theme.CustomDialogFragment;

public class DownloadFinishFragment extends Fragment {

    private static final String TAG = AppManagerFragment.class.getSimpleName();
    private static final int MAX_PROGRESS = 100;

    private static DownAdapter sAdapter;
    private static ProgressDialog mAppSendDialog;

    private static CustomDialogFragment mSendDialog;
    private static CustomDialogFragment mChangeDialog;

    private static RemoteDeviceServiceManager sService;
    private static boolean sIsReady;

    private static boolean sIsInstalling;

    private List<DownAppListInfo> mCurrentDownloadFinish;
    private ListView mDownList;

    private static boolean sIsChange;
    private static boolean isPause;
    private static boolean mIsFinish;

    private static Context mContext;
    private static RemoteDeviceUtils sRemoteDeviceUtils;
    private static Register sRegister;
    private static FragmentManager sFragmentManager;
    private static Map<String, String> pkMap = new LinkedHashMap<String, String>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_donwload_list, container, false);
        mDownList = (ListView) rootView.findViewById(R.id.downloadList);
        mContext = getActivity();
        mIsFinish = false;
        sFragmentManager = getFragmentManager();
        View btnGroup = rootView.findViewById(R.id.btn_group);
        btnGroup.setVisibility(View.GONE);

        sAdapter = new DownAdapter(mContext, R.layout.fragment_download_list_item) {
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                View result = super.getView(position, convertView, parent);
                TextView status = (TextView) result.findViewById(R.id.status);
                IwdsLog.d(TAG, "position: " + position + " setonclick()");
                status.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        DownAppListInfo item = getItem(position);
                        IwdsLog.d(TAG, "install: " + item.finish + " " + DownAppListInfo.STATUS_COMPLETE);
                        if (item.isComplete()) {
                            IwdsLog.d(TAG, "install: " + item.finish);
                            installAPP(item);
                        }
                    }
                });
                return result;
            }
        };
        mDownList.setAdapter(sAdapter);

        initRemoteDeviceDrive();
        IwdsLog.d(TAG, "initRemoteDeviceDrive ");
        return rootView;
    }

    private void installAPP(DownAppListInfo item) {

        String apkFilePath = item.path;
        pkMap.put(item.pkName, item.name);
        IwdsLog.d(TAG, "install app ---> apkPath: " + apkFilePath);
        if (sService != null) {
            if (!checkRemoteDevice())
                return;
            showSendDialogue();
            sService.requestInstallApp(apkFilePath, false);
            sIsInstalling = true;
            IwdsLog.d(TAG, "install app ---> call: " + item.name);
        } else {
            IwdsLog.d(TAG, "sService is null");
        }
    }

    public boolean checkRemoteDevice() {
        IwdsLog.d(TAG, "checkRemoteDevice ---> isReady: " + sIsReady);
        if (!sIsReady) {
            Toast.makeText(mContext, R.string.check_watch_link, Toast.LENGTH_SHORT).show();
        }
        return sIsReady;
    }

    private static void initRemoteDeviceDrive() {
        if (null != sRemoteDeviceUtils)
            return;

        sRemoteDeviceUtils = RemoteDeviceUtils.getInstance(mContext);
        sRemoteDeviceUtils.start(new Callback() {

            @Override
            public void onCall(RemoteDeviceServiceManager service, Register register, boolean isReady, boolean isCalledReady) {
                IwdsLog.d(TAG, "RemoteDeviceUtils onCall!  ---> isReady: " + isReady + " isCalledReady: " + isCalledReady);
                sService = service;
                sRegister = register;
                if (isCalledReady) {
                    sIsReady = isReady;
                    IwdsLog.d(TAG, "download finish --> isReady --> : " + isReady);
                }
                sRegister.register(DownloadFinishFragment.class, new RemoteDeviceStatusListener() {

                    @Override
                    public void onRemoteDeviceReady(boolean isReady) {
                        sIsReady = isReady;
                        IwdsLog.d(TAG, "download finish --> isReady: " + isReady);
                        if (!isReady) {
                            hideSendDialog();
                            hideChangeDialog();
                        }
                    }
                }, null, new SimpleImplListener.SimpleAppCallback() {

                    @Override
                    public void onDoneInstallApp(String packageName, int returnCode) {
                        IwdsLog.d(TAG, "install app ---> packageName: " + packageName + "  returnCode: " + returnCode);
                        sIsInstalling = false;
                        hideSendDialog();
                        hideChangeDialog();
                        String name = pkMap.get(packageName);
                        name = null == name ? packageName : name;
                        sAdapter.mIsEnable = true;

                        if (RemoteDeviceManagerInfo.INSTALL_SUCCEEDED == returnCode) {
                            Toast.makeText(mContext, name + " : " + mContext.getString(R.string.install_success), Toast.LENGTH_SHORT).show();
                            AppDownloadUtils.changeDownloadInfoStatus(packageName, DownAppListInfo.STATUS_INSTALLED);
                            IwdsLog.d(TAG, name + " : " + mContext.getString(R.string.install_success));

                        } else {
                            AppDownloadUtils.changeDownloadInfoStatus(packageName, DownAppListInfo.STATUS_FAIL);

                            if (RemoteDeviceManagerInfo.REQUEST_FAILED_PREVIOUS_DOING == returnCode) {
                                Toast.makeText(mContext, R.string.Background_install, Toast.LENGTH_SHORT).show(); // "后台正在安装,请稍后在试!"
                                sAdapter.mIsEnable = false;
                            } else {
                                Toast.makeText(mContext, name + " : " + mContext.getString(R.string.install_fail), Toast.LENGTH_SHORT).show();
                            }
                            IwdsLog.d(TAG, name + " : " + mContext.getString(R.string.install_fail));
                        }
                        sAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onSendFileProgressForInstall(String packageName, int progress) {
                        IwdsLog.d(TAG, "onSendFileProgressForInstallpackageName  --> : " + packageName + "  progress: " + progress);
                        hideChangeDialog();
                        if (null == mSendDialog) {
                            showSendDialogue();
                        }
                        if (null != mAppSendDialog)
                            mAppSendDialog.setProgress(progress);
                        if (MAX_PROGRESS == progress) {
                            hideSendDialog();
                            if (!mIsFinish) {
                                showInsatllDialog();
                            }
                        }
                    }
                });
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        isPause = false;
        if (sIsChange || sIsInstalling) {
            showInsatllDialog();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        isPause = true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isPause = false;
        mIsFinish = true;
        mAppSendDialog = null;
        mSendDialog = null;
        mChangeDialog = null;
        IwdsLog.d(TAG, "onDestoryView!");
    }

    @Override
    public void onStart() {
        super.onStart();
        AppDownloadUtils.registerRefreshRunnable(this, new Runnable() {

            @Override
            public void run() {
                refreshData();
                IwdsLog.v(TAG, "refreshData --->  status or progress change");
            }
        });
        refreshData();
        IwdsLog.d(TAG, "onstart!");
    }

    private void refreshData() {
        mCurrentDownloadFinish = DownloadInfoRepository.getDownloadFinishList();
        sAdapter.clear();
        sAdapter.addAll(mCurrentDownloadFinish);
        sAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStop() {
        super.onStop();
        AppDownloadUtils.unregisterRefreshRunnable(this);
        IwdsLog.d(TAG, "onstop!");
    }

    private static void showInsatllDialog() {
        hideChangeDialog();
        IwdsLog.d(TAG, "change dialog show --> !");
        sIsChange = true;
        if (isPause) {
            return;
        }

        mChangeDialog = new CustomDialogFragment();
        ProgressDialog changeDialog = new ProgressDialog(mContext);
        changeDialog.setMessage(mContext.getString(R.string.installing));
        changeDialog.setIndeterminate(true);
        mChangeDialog.setDialog(changeDialog);
        mChangeDialog.setCancelable(false);

        mChangeDialog.show(sFragmentManager, "changeDialog");
        IwdsLog.d(TAG, "change show !!!");
    }

    private static void hideChangeDialog() {
        if (mChangeDialog != null)
            mChangeDialog.dismissAllowingStateLoss();
        sIsChange = false;
        mChangeDialog = null;
        IwdsLog.d(TAG, "change dialog hide!");
    }

    private static void showSendDialogue() {

        if (mIsFinish)
            return;

        IwdsLog.d(TAG, "send dialog show --> !");
        hideSendDialog();

        mSendDialog = new CustomDialogFragment();
        mAppSendDialog = new ProgressDialog(mContext);
        mAppSendDialog.setMax(MAX_PROGRESS);
        mAppSendDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mAppSendDialog.setTitle(mContext.getString(R.string.sending_file));
        mAppSendDialog.incrementProgressBy(-mAppSendDialog.getProgress());
        mSendDialog.setDialog(mAppSendDialog);
        mSendDialog.setCancelable(false);

        mSendDialog.show(sFragmentManager, "sendDialog");
    }

    private static void hideSendDialog() {
        IwdsLog.d(TAG, "send dialog hide!");
        if (mSendDialog == null)
            return;
        mSendDialog.dismissAllowingStateLoss();
        mSendDialog = null;
        IwdsLog.d(TAG, "send dialog hide!");
    }
}
