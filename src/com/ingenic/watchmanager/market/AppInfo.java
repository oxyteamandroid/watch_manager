/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.market;

import java.io.Serializable;

public class AppInfo implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    long id;
    String name;
    String packageName;
    String description;
    String iconUrl;
    int size;
    long uploadTime;
    String devloper;
    String[] imagesUrl;
    String score;
    int scoreCount;
    int downloadCount;
    String typeName;
    int versionCode;
    String version;

    public static class Type {
        private int id;
        private String name;

        public Type(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }
    }
}
