/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.market;

import java.util.List;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.util.DensityUtil;
import com.ingenic.watchmanager.util.ProgressUtils;
import com.ingenic.watchmanager.util.Request.ErrorListener;
import com.ingenic.watchmanager.util.Request.Listener;
import com.ingenic.watchmanager.util.StringRequest;

public class CategoryFragment extends ListFragment implements ProgressUtils.Listener {

    private CategoryAdapter mAdapter;
    private ProgressUtils mProgressUtils;

    @Override
    public void request() {
        String url = AppGetter.getTypesUrl();
        StringRequest request = new StringRequest(getActivity(), url, new Listener<String>() {

            @Override
            public void onResponse(String response) {
                List<AppInfo.Type> types = AppGetter.getTypes(response);
                mAdapter.clear();
                mAdapter.addAll(types);
                mAdapter.notifyDataSetChanged();
                mProgressUtils.end();
            }
        }, new ErrorListener() {

            @Override
            public void onErrorResponse(Exception ex) {
                mProgressUtils.fail();
            }
        });

        request.start();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (mAdapter == null) {
            mAdapter = new CategoryAdapter(activity, R.layout.list_item_category);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rooView = super.onCreateView(inflater, container, savedInstanceState);
        rooView.setBackgroundColor(getResources().getColor(android.R.color.black));// 0xFF000000);
        mProgressUtils = new ProgressUtils(getActivity(), rooView, this);
        mProgressUtils.start();

        return rooView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListAdapter(mAdapter);

        ListView listView = getListView();
        listView.setSelector(new ColorDrawable(getResources().getColor(android.R.color.white)));// 0xFFFFFFFF));
        Drawable divider = new ColorDrawable(getResources().getColor(android.R.color.black));// 0xFF000000);
        listView.setDivider(divider);
        listView.setDividerHeight(DensityUtil.dip2px(getActivity(), 5));
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(getActivity(), CategoryActivity.class);
        AppInfo.Type type = mAdapter.getItem(position);
        intent.putExtra(CategoryActivity.EXTRA_TYPE, type.getId());
        intent.putExtra(CategoryActivity.EXTRA_NAME, type.getName());
        startActivity(intent);
    }

    private static class CategoryAdapter extends ArrayAdapter<AppInfo.Type> {

        private Context mContext;
        private int mResource;

        public CategoryAdapter(Context context, int resource) {
            super(context, resource);

            mContext = context;
            mResource = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(mResource, parent, false);
                TextView textView1 = (TextView) convertView.findViewById(R.id.label);
                convertView.setTag(textView1);
            }
            TextView textView = (TextView) convertView.getTag();
            textView.setText(getItem(position).getName());
            return convertView;
        }
    }
}
