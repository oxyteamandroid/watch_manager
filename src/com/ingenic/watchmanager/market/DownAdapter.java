/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.market;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DownAdapter extends ArrayAdapter<DownAppListInfo> {

    public static final String TAG = DownAdapter.class.getSimpleName();

    private int mResource;
    private LayoutInflater mInflater;

    public boolean mIsEnable = true;

    protected MarketCustomDialog mDialog;

    public DownAdapter(Context context, int resource) {
        super(context, resource);
        mResource = resource;
        mInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public long getItemId(int position) {
        DownAppListInfo info = getItem(position);
        return info.id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(mResource, parent, false);
            holder = buildHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final DownAppListInfo info = getItem(position);
        setDownAppListInfo(holder, info);
        if (info.isComplete()) {
            holder.downloadStatus.setVisibility(View.GONE);
            holder.rate.setVisibility(View.GONE);
            holder.progress.setVisibility(View.GONE);
        } else {
            holder.time.setVisibility(View.GONE);
            holder.category.setVisibility(View.GONE);
        }

        holder.rate.setText(Utils.formatSize((int) info.completeSize) + "/" + Utils.formatSize((int) info.size));

        IwdsLog.v(TAG, "info.iconUrl: " + info.iconUrl);
        // ImageRequest.loadingImage(getContext(), holder.icon, info.iconUrl);
        ImageLoader.getInstance().displayImage(info.iconUrl, holder.icon, Utils.DEFAULT_OPTIONS);

        holder.status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                info.triggerEvent();
                holder.status.setText(info.getEventText());
            }
        });

        holder.status.setText(info.getEventText());

        if (DownAppListInfo.STATUS_COMPLETE.equals(info.finish)) {
            holder.status.setText(R.string.install);
        }

        holder.progress.setProgress(info.progress);

        holder.remove.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String content = getContext().getResources().getString(R.string.delete_task_file);
                if (info.isComplete()) {
                    content = getContext().getResources().getString(R.string.delete_file);
                }
                mDialog = new MarketCustomDialog(getContext(), content, new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        AppDownloadUtils.removeFile(info);
                        mDialog.dismiss();
                    }
                });
                mDialog.show();
            }
        });
        if (mIsEnable) {
            holder.status.setBackgroundResource(R.drawable.selector_app_manager_round);
            holder.status.setTextColor(getContext().getResources().getColor(android.R.color.white));
            holder.status.setClickable(true);
        } else {
            holder.status.setClickable(false);
            holder.status.setTextColor(getContext().getResources().getColor(R.color.app_manager_operate_btn));
            holder.status.setBackgroundResource(R.drawable.selector_tv_round_border);
        }

        return convertView;
    }

    private ViewHolder buildHolder(View view) {
        ViewHolder holder = new ViewHolder();

        holder.icon = (ImageView) view.findViewById(R.id.icon);
        holder.name = (TextView) view.findViewById(R.id.name);

        holder.downloadStatus = (TextView) view.findViewById(R.id.download_status);
        holder.rate = (TextView) view.findViewById(R.id.rate);
        holder.progress = (ProgressBar) view.findViewById(R.id.progress);

        holder.category = (TextView) view.findViewById(R.id.category);
        holder.time = (TextView) view.findViewById(R.id.time);

        holder.status = (TextView) view.findViewById(R.id.status);
        holder.remove = (ImageView) view.findViewById(R.id.remove);
        return holder;
    }

    private void setDownAppListInfo(ViewHolder holder, DownAppListInfo info) {
        // holder.icon
        holder.name.setText(info.name);

        holder.downloadStatus.setText(info.getStatusText());
        holder.progress.setProgress(info.progress);
        holder.rate.setText("1M/20M");

        holder.category.setText(info.pkName);
        holder.time.setText(new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(new Date(info.time)));

        holder.category.setText(info.category);

        holder.status.setText(getContext().getString(R.string.download_file));
    }

    private static class ViewHolder {

        ImageView icon;
        TextView name;

        TextView downloadStatus;
        ProgressBar progress;
        TextView rate;

        TextView category;
        TextView time;

        TextView status;
        ImageView remove;
    }
}