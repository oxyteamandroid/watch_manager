/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.market;

import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.ingenic.watchmanager.db.Operator;
import com.ingenic.watchmanager.db.WatchManagerContracts;

public class DownAppListInfoOperator extends Operator<DownAppListInfo> {

    public DownAppListInfoOperator(Context context) {
        super(context, WatchManagerContracts.Tables.DOWN_LIST);
    }

    @Override
    protected ContentValues toValues(DownAppListInfo t) {
        ContentValues values = new ContentValues();
        values.put(WatchManagerContracts.DownListColumns.ID, t.id);
        values.put(WatchManagerContracts.DownListColumns.PROGRESS, t.progress);
        values.put(WatchManagerContracts.DownListColumns.NAME, t.name);
        values.put(WatchManagerContracts.DownListColumns.ICON, t.iconUrl);
        values.put(WatchManagerContracts.DownListColumns.PKNAME, t.pkName);
        values.put(WatchManagerContracts.DownListColumns.DESCRIPTION, t.description);
        values.put(WatchManagerContracts.DownListColumns.URL, t.url);
        values.put(WatchManagerContracts.DownListColumns.PATH, t.path);
        values.put(WatchManagerContracts.DownListColumns.FINISH, t.finish);
        values.put(WatchManagerContracts.DownListColumns.CATEGORY, t.category);
        values.put(WatchManagerContracts.DownListColumns.TIME, t.time);
        values.put(WatchManagerContracts.DownListColumns.SIZE, t.size);
        values.put(WatchManagerContracts.DownListColumns.COMPLETE, t.completeSize);
        values.put(WatchManagerContracts.DownListColumns.BAK_URL, t.bakUrl);

        return values;
    }

    @Override
    public int update(DownAppListInfo t) {
        ContentValues values = new ContentValues();
        values.put(WatchManagerContracts.DownListColumns.PROGRESS, t.progress);
        values.put(WatchManagerContracts.DownListColumns.NAME, t.name);
        values.put(WatchManagerContracts.DownListColumns.DESCRIPTION, t.description);
        values.put(WatchManagerContracts.DownListColumns.URL, t.url);
        values.put(WatchManagerContracts.DownListColumns.PATH, t.path);
        values.put(WatchManagerContracts.DownListColumns.FINISH, t.finish);

        return update(values, WatchManagerContracts.DownListColumns.ID + " = ? ", new String[] { "" + t.id });
    }

    @Override
    protected DownAppListInfo fromCursor(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndex(WatchManagerContracts.DownListColumns.ID));
        int progress = cursor.getInt(cursor.getColumnIndex(WatchManagerContracts.DownListColumns.PROGRESS));
        String name = cursor.getString(cursor.getColumnIndex(WatchManagerContracts.DownListColumns.NAME));
        String description = cursor.getString(cursor.getColumnIndex(WatchManagerContracts.DownListColumns.DESCRIPTION));

        String iconUrl = cursor.getString(cursor.getColumnIndex(WatchManagerContracts.DownListColumns.ICON));
        String pkName = cursor.getString(cursor.getColumnIndex(WatchManagerContracts.DownListColumns.PKNAME));

        String url = cursor.getString(cursor.getColumnIndex(WatchManagerContracts.DownListColumns.URL));
        String path = cursor.getString(cursor.getColumnIndex(WatchManagerContracts.DownListColumns.PATH));
        String finish = cursor.getString(cursor.getColumnIndex(WatchManagerContracts.DownListColumns.FINISH));

        String category = cursor.getString(cursor.getColumnIndex(WatchManagerContracts.DownListColumns.CATEGORY));
        long time = cursor.getLong(cursor.getColumnIndex(WatchManagerContracts.DownListColumns.TIME));
        long size = cursor.getLong(cursor.getColumnIndex(WatchManagerContracts.DownListColumns.SIZE));
        long completeSize = cursor.getLong(cursor.getColumnIndex(WatchManagerContracts.DownListColumns.COMPLETE));
        String bakUrl = cursor.getString(cursor.getColumnIndex(WatchManagerContracts.DownListColumns.BAK_URL));

        DownAppListInfo downlist = new DownAppListInfo();
        downlist.id = id;
        downlist.progress = progress;
        downlist.name = name;
        downlist.description = description;
        downlist.url = url;
        downlist.path = path;
        downlist.finish = finish;
        downlist.iconUrl = iconUrl;
        downlist.pkName = pkName;
        downlist.category = category;
        downlist.time = time;
        downlist.size = size;
        downlist.completeSize = completeSize;
        downlist.bakUrl = bakUrl;
        return downlist;
    }

    @Override
    public int delete(DownAppListInfo t) {
        if (null == t)
            return -1;
        return delete(t.id);
    }

    public int delete(long id) {
        return delete(WatchManagerContracts.DownListColumns.ID + " = ? ", new String[] { "" + id });
    }

    @Override
    public boolean hasData(DownAppListInfo t) {
        DownAppListInfo c = query(null, WatchManagerContracts.DownListColumns.ID + " = ? ", new String[] { "" + t.id }, null, null, null);
        return c != null;
    }

    @Override
    public List<DownAppListInfo> queryAll() {
        return queryAll(null);
    }

    public List<DownAppListInfo> queryDownloadingAll() {
        return queryAll(WatchManagerContracts.DownListColumns.FINISH + " = ? ", new String[] { DownAppListInfo.STATUS_DOWNLOADING }, null);
    }

    public List<DownAppListInfo> queryDownloadedAll() {
        return queryAll(WatchManagerContracts.DownListColumns.FINISH + " = ? ", new String[] { DownAppListInfo.STATUS_COMPLETE }, null);
    }

    public void saveList(List<DownAppListInfo> mCurrentDownAppList) {
        if (null != mCurrentDownAppList) {
            for (DownAppListInfo da : mCurrentDownAppList) {
                save(da);
            }
        }
    }
}
