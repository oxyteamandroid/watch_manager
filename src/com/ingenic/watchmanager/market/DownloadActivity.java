/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.market;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;

public class DownloadActivity extends Activity {
    protected static final String TAG = DownloadActivity.class.getSimpleName();
    private TextView mDownloading;
    private TextView mDownloadFinish;
    private int mTextNoSelected;
    private int mTextSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_donwload_manage);

        FragmentManager fm = getFragmentManager();

        final List<Fragment> list = new ArrayList<Fragment>();
        list.add(new DownloadingFragment());
        list.add(new DownloadFinishFragment());
        mTextNoSelected = getResources().getColor(R.color.text_noselected);
        mTextSelected = getResources().getColor(R.color.text_selected);
        final ViewPager vp = (ViewPager) findViewById(R.id.container);

        vp.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                changeSelectStatus(arg0 == 0);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        FragmentPagerAdapter adapter = new FragmentPagerAdapter(fm) {

            @Override
            public int getCount() {
                return list.size();
            }

            @Override
            public Fragment getItem(int arg0) {
                return list.get(arg0);
            }
        };

        vp.setAdapter(adapter);

        mDownloading = (TextView) findViewById(R.id.downloading);
        mDownloadFinish = (TextView) findViewById(R.id.downloadFinish);

        mDownloading.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                vp.setCurrentItem(0);
                changeSelectStatus(true);
            }

        });

        mDownloadFinish.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                vp.setCurrentItem(1);
                changeSelectStatus(false);
            }
        });

        AppDownloadUtils.registerRefreshRunnable(this, new Runnable() {

            @Override
            public void run() {
                refresh();
                IwdsLog.v(TAG, "refreshData --->  status or progress change");
            }

        });

        refresh();
        changeSelectStatus(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppDownloadUtils.unregisterRefreshRunnable(this);
    }

    private void refresh() {

        mDownloading.setText(getString(R.string.process, "" + DownloadInfoRepository.getDownloadingList().size()));
        mDownloadFinish.setText(getString(R.string.finish, "" + DownloadInfoRepository.getDownloadFinishList().size()));
    }

    private void changeSelectStatus(boolean status) {
        if (status) {
            mDownloading.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            mDownloadFinish.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            mDownloadFinish.setTextColor(mTextNoSelected);
            mDownloading.setTextColor(mTextSelected);
        } else {
            mDownloadFinish.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            mDownloading.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            mDownloading.setTextColor(mTextNoSelected);
            mDownloadFinish.setTextColor(mTextSelected);
        }
    }
}
