/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.market;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ingenic.watchmanager.market.AppInfo.Type;
import com.ingenic.watchmanager.util.StringRequest;
import com.ingenic.watchmanager.util.Utils;

public class AppGetter {

    private static AppGetter mInstance;

    private AppGetter() {
    }

    public synchronized static AppGetter getIstance() {
        if (mInstance == null) {
            mInstance = new AppGetter();
        }
        return mInstance;
    }

    public static String getAppListUrl(String uuid, String state, int start, int end) {
        String method = "/v1/app/category/?uuid=" + uuid + "&locale=" + Utils.getDefaultLocaleCode() + "&start=" + start + "&end=" + end + "&state=" + state;
        return StringRequest.MARKET_IP + method;
    }

    public static List<AppInfo> getAppList(String result) {
        List<AppInfo> infos = new ArrayList<AppInfo>();
        try {
            JSONObject obj = new JSONObject(result);
            JSONArray array = obj.getJSONArray("list");
            int count = array.length();

            for (int i = 0; i < count; i++) {
                JSONObject json = array.getJSONObject(i);
                AppInfo info = simpleAppInfoFromJson(json);
                if (info != null) {
                    infos.add(info);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return infos;
    }

    public static String getAppsWithTypeUrl(String uuid, int type, int start, int end) {
        String method = "/v1/app/applist/?uuid=" + uuid + "&cart_id=" + type + "&locale=" + Utils.getDefaultLocaleCode() + "&start=" + start + "&end=" + end;
        return StringRequest.MARKET_IP + method;
    }

    public static AppInfo simpleAppInfoFromJson(JSONObject json) {
        try {
            AppInfo info = new AppInfo();
            info.name = json.getString("name");
            info.id = json.getLong("id");
            info.packageName = json.getString("package");
            info.iconUrl = json.getString("icon");
            info.description = json.getString("description");
            info.version = json.getString("version");
            info.versionCode = json.getInt("versionCode");
            info.size = json.getInt("size");
            info.devloper = json.getString("dev");
            info.uploadTime = json.getLong("addtime");
            info.typeName = json.getString("cart_name");
            info.downloadCount = json.getInt("down");
            info.devloper = json.getString("dev");
            info.score = json.getString("average");
            info.scoreCount = json.getInt("number");
            return info;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTypesUrl() {
        String method = "/v1/app/types/?locale=" + Utils.getDefaultLocaleCode();
        return StringRequest.MARKET_IP + method;
    }

    public static List<Type> getTypes(String result) {
        List<AppInfo.Type> types = new ArrayList<AppInfo.Type>();
        try {
            JSONObject obj = new JSONObject(result);
            JSONArray array = obj.getJSONArray("list");
            int count = array.length();

            for (int i = 0; i < count; i++) {
                JSONObject json = array.getJSONObject(i);
                int id = json.getInt("id");
                String name = json.getString("cart_name");
                AppInfo.Type type = new AppInfo.Type(id, name);
                types.add(type);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return types;
    }

    public static String fillAppDetailUrl(AppInfo info) {
        if (info == null)
            throw new IllegalArgumentException("info is null!");
        String method = "/v1/app/detail/?id=" + info.id + "&locale=" + Utils.getDefaultLocaleCode();
        return StringRequest.MARKET_IP + method;
    }

    public static AppInfo getAppDetail(AppInfo info, String result) {
        try {
            JSONObject json = new JSONObject(result);
            fillDetailInfo(info, json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return info;
    }

    private static void fillDetailInfo(AppInfo info, JSONObject json) {
        if (info == null || json == null)
            return;

        try {
            info.name = json.getString("name");
            info.imagesUrl = json.getString("images").split(",");
            info.description = json.getString("description");
            info.version = json.getString("version");
            info.versionCode = json.getInt("versionCode");
            info.uploadTime = json.getLong("addtime");
            info.devloper = json.getString("dev");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getGradeAppUrl(long id, int score, String account) {
        if (1 > score || 5 < score)
            return "";

        String url = "/v1/app/grade/?id=" + id + "&score=" + score + "&account=" + account;
        return StringRequest.MARKET_IP + url;
    }

    public static String getDownloadAppUrl(long id, String account, String psd) {
        psd = Utils.getMd5(psd);

        String url = "/v1/app/down/?id=" + id + "&account=" + account + "&psd=" + psd;
        return StringRequest.MARKET_IP + url;
    }

    public static String getRecommendApp(long id, int count) {
        String url = "/v1/app/guest/?id=" + id + "&locale=" + Utils.getDefaultLocaleCode() + "&count=" + count;
        return StringRequest.MARKET_IP + url;
    }

    public static String getRecommendSearchUrl() {
        String url = "/v1/app/recommend/?";
        return StringRequest.MARKET_IP + url;
    }

    public static String getSearchSuggestUrl(String keyword) {
        String url = "/v1/app/suggest/?keyword=" + keyword;
        return StringRequest.MARKET_IP + url;
    }

    public static String getSearchAppUrl(String uuid, String keyword, int start, int end) {
        String method = "/v1/app/search/?uuid=" + uuid + "&keyword=" + keyword + "&locale=" + Utils.getDefaultLocaleCode() + "&start=" + start + "&end=" + end;
        return StringRequest.MARKET_IP + method;
    }

    public static List<AppInfo> getSearchApp(String result) {
        List<AppInfo> infos = new ArrayList<AppInfo>();
        try {
            JSONObject obj = new JSONObject(result);
            JSONArray array = obj.getJSONArray("list");
            int count = array.length();

            for (int i = 0; i < count; i++) {
                JSONObject json = array.getJSONObject(i);
                AppInfo info = simpleAppInfoFromJson(json);
                if (info != null) {
                    infos.add(info);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return infos;
    }
}
