/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.market;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

public class AppAdapter extends ArrayAdapter<AppInfo> {

    private static final int TOP_THIRD = 3;
    public static final int FLAG_RANK = 1;
    public static final int FLAG_NOT_RANK = 2;
    private static final String TAG = AppAdapter.class.getSimpleName();
    private Context mContext;
    private int mResource;
    private LayoutInflater mInflater;
    private int mType;

    public AppAdapter(Context context, int type, int resource) {
        super(context, resource);
        mType = type;
        mContext = context;
        mResource = resource;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public AppAdapter(Context context, int resource) {
        this(context, FLAG_NOT_RANK, resource);
    }

    @Override
    public long getItemId(int position) {
        AppInfo info = getItem(position);
        return info.id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(mResource, parent, false);
            holder = buildHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.sequece.setVisibility(View.GONE);
        if (mType == FLAG_RANK) {
            holder.sequece.setVisibility(View.VISIBLE);
        }
        holder.sequece.setText("" + (position + 1));
        if (position < TOP_THIRD) {
            holder.sequece.setTextColor(getContext().getResources().getColor(R.color.rank_top_three_sequence));// 0xFFFFD800);
        } else {
            holder.sequece.setTextColor(mContext.getResources().getColor(android.R.color.white));
        }
        final AppInfo info = getItem(position);
        setAppInfo(holder, info);

        return convertView;
    }

    private ViewHolder buildHolder(View view) {
        ViewHolder holder = new ViewHolder();

        holder.icon = (ImageView) view.findViewById(R.id.icon);
        holder.name = (TextView) view.findViewById(R.id.name);

        holder.score = (RatingBar) view.findViewById(R.id.ratingBar);
        holder.size = (TextView) view.findViewById(R.id.size);
        holder.description = (TextView) view.findViewById(R.id.download_status);

        holder.down = (TextView) view.findViewById(R.id.down);
        holder.downCount = (TextView) view.findViewById(R.id.downCount);
        holder.description = (TextView) view.findViewById(R.id.download_status);

        holder.downloadVisible = view.findViewById(R.id.downloadVisible);
        holder.downloadVisibleNot = view.findViewById(R.id.downloadVisibleNot);

        holder.downStatus = (TextView) view.findViewById(R.id.down_status);
        holder.fileSize = (TextView) view.findViewById(R.id.file_size);
        holder.downProgress = (ProgressBar) view.findViewById(R.id.down_progress);

        holder.sequece = (TextView) view.findViewById(R.id.sequence);

        return holder;
    }

    private void setAppInfo(ViewHolder holder, final AppInfo info) {

        ImageLoader.getInstance().displayImage(info.iconUrl, holder.icon, Utils.DEFAULT_OPTIONS);

        holder.name.setText(info.name);
        holder.size.setText(Utils.formatSize(info.size));
        holder.description.setText(info.description);
        int useCount = info.downloadCount;
        if (useCount == 0) {
            holder.downCount.setText(R.string.no_downloads);
        } else {
            holder.downCount.setText(mContext.getString(R.string.downloads, Utils.formatCount(useCount)));
        }
        holder.down.setText(R.string.download);
        int count = info.scoreCount;
        holder.score.setRating(count);

        final TextView downView = holder.down;

        downView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                AppDetailFragment.setDownTextViewClickEvent(info);
            }
        });

        downView.setText(R.string.download_file);
        if (DownloadInfoRepository.getCurrentDownloadInfoMap().containsKey(info.id)) {
            DownAppListInfo downInfo = DownloadInfoRepository.getCurrentDownloadInfoMap().get(info.id);
            downView.setText(downInfo.getEventText());

            if (downInfo.isComplete()) {
                holder.downloadVisible.setVisibility(View.GONE);
                holder.downloadVisibleNot.setVisibility(View.VISIBLE);
            } else {
                holder.downStatus.setText(downInfo.getStatusText());
                // holder.fileSize.setText(Utils.formatSize(info.size));
                holder.fileSize.setText(Utils.formatSize((int) downInfo.completeSize) + "/" + Utils.formatSize((int) info.size));

                holder.downProgress.setProgress(downInfo.progress);
                holder.downloadVisible.setVisibility(View.VISIBLE);
                holder.downloadVisibleNot.setVisibility(View.GONE);
            }

            IwdsLog.v(TAG, "app adapter ---> finish: " + downInfo.finish);
            if (downInfo.finish.equals(DownAppListInfo.STATUS_COMPLETE)) {
                downView.setText(mContext.getString(R.string.complete));
                IwdsLog.v(TAG, "app adapter ---> finish: complete!");
            }
        } else {
            holder.downloadVisible.setVisibility(View.GONE);
            holder.downloadVisibleNot.setVisibility(View.VISIBLE);
        }
    }

    private static class ViewHolder {
        public TextView sequece;
        public ProgressBar downProgress;
        public TextView fileSize;
        public TextView downStatus;
        public View downloadVisibleNot;
        public View downloadVisible;

        public ImageView icon;
        public TextView name;
        public RatingBar score;

        public TextView size;
        public TextView downCount;

        public TextView down;
        public TextView description;
    }
}