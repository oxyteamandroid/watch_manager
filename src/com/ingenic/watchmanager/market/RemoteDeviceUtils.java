/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.market;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.content.Context;
import android.content.pm.PackageStats;
import android.graphics.Bitmap;
import android.widget.Toast;

import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceClient.ConnectionCallbacks;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.remotedevice.RemoteApplicationInfo;
import com.ingenic.iwds.remotedevice.RemoteDeviceAppListener;
import com.ingenic.iwds.remotedevice.RemoteDeviceManagerInfo;
import com.ingenic.iwds.remotedevice.RemoteDeviceProcessListener;
import com.ingenic.iwds.remotedevice.RemoteDeviceServiceManager;
import com.ingenic.iwds.remotedevice.RemoteDeviceStatusListener;
import com.ingenic.iwds.remotedevice.RemoteProcessInfo;
import com.ingenic.iwds.remotedevice.RemoteStorageInfo;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.app.SimpleImplListener;

public class RemoteDeviceUtils implements ConnectionCallbacks {
    private static final String TAG = RemoteDeviceUtils.class.getSimpleName();

    public static Map<String, Long> mAppSize = new LinkedHashMap<String, Long>();
    public static Map<String, RemoteApplicationInfo> mAppList = new LinkedHashMap<String, RemoteApplicationInfo>();

    public static Map<String, String> mPkname2Label = new LinkedHashMap<String, String>();
    public static Map<String, Bitmap> mPkname2Icon = new LinkedHashMap<String, Bitmap>();

    private static final Callback empty = new Callback() {
        @Override
        public void onCall(RemoteDeviceServiceManager service, Register register, boolean isReady, boolean isCalledReady) {
        }
    };

    private static RemoteDeviceUtils sInstance;

    private Context mContext;
    private ServiceClient mClient;

    private Set<Callback> mCallSet;
    private Map<Object, RemoteDeviceStatusListener> mRemoteDeviceStatusListenerSet;
    private Map<Object, RemoteDeviceProcessListener> mRemoteDeviceProcessListenerSet;
    private Map<Object, RemoteDeviceAppListener> mRemoteDeviceAppListenerSet;

    private RemoteDeviceServiceManager mService;
    private Register mRegister;
    private boolean mIsReady;

    private boolean mIsCallReady;

    public boolean checkRemoteDevice() {
        if (!mIsReady) {
            Toast.makeText(mContext, R.string.check_watch_link, Toast.LENGTH_SHORT).show();
        }
        return mIsReady;
    }

    private RemoteDeviceStatusListener mStatusListener = new RemoteDeviceStatusListener() {

        @Override
        public void onRemoteDeviceReady(boolean isReady) {
            mIsCallReady = true;
            mIsReady = isReady;
            IwdsLog.d(TAG, "onRemoteDeviceReady --> isReady: " + isReady);
            for (RemoteDeviceStatusListener item : mRemoteDeviceStatusListenerSet.values()) {
                item.onRemoteDeviceReady(isReady);
            }
            if (!isReady)
                clear();
        }
    };

    private RemoteDeviceProcessListener mProcessListener = new RemoteDeviceProcessListener() {

        @Override
        public void onResponseSystemMemoryInfo(long availMemSize, long totalMemSize) {
            for (RemoteDeviceProcessListener item : mRemoteDeviceProcessListenerSet.values()) {
                item.onResponseSystemMemoryInfo(availMemSize, totalMemSize);
            }
        }

        @Override
        public void onResponseRunningAppProcessInfo(List<RemoteProcessInfo> processInfoList) {

            List<RemoteProcessInfo> list = new ArrayList<RemoteProcessInfo>();
            for (RemoteProcessInfo item : processInfoList) {
                if (isFilter(item.processName))
                    continue;
                list.add(item);
            }

            for (RemoteDeviceProcessListener item : mRemoteDeviceProcessListenerSet.values()) {
                item.onResponseRunningAppProcessInfo(list);
            }
        }

        @Override
        public void onDoneKillProcess(String packageName) {
            for (RemoteDeviceProcessListener item : mRemoteDeviceProcessListenerSet.values()) {
                item.onDoneKillProcess(packageName);
            }
        }
    };

    private static final Set<String> filter = new HashSet<String>();
    static {
        filter.add("com.ingenic.theme");
        filter.add("com.ingenic.iwds.device");
        filter.add("com.ingenic.provider.notification");

        filter.add("com.ingenic.provider.calendar");
        filter.add("system");
    }

    private boolean isFilter(String packageName) {
        String trimStr = packageName.trim();
        return trimStr.startsWith("com.android.") || trimStr.startsWith("android") || filter.contains(packageName);
    }

    private void clear() {
        mAppList.clear();
        mPkname2Label.clear();
        mPkname2Icon.clear();
    }

    private RemoteDeviceAppListener mAppListener = new RemoteDeviceAppListener() {

        @Override
        public void onRemoteAppInfoListAvailable(List<RemoteApplicationInfo> appList) {
            List<RemoteApplicationInfo> list = new ArrayList<RemoteApplicationInfo>();
            for (RemoteApplicationInfo item : appList) {
                mAppList.put(item.packageName, item);

                if (isFilter(item.packageName))
                    continue;
                list.add(item);
                if (!mAppSize.containsKey(item.packageName)) {
                    mService.requestPkgSizeInfo(item.packageName);
                }
                mPkname2Label.put(item.packageName, item.label.toString());
                mPkname2Icon.put(item.packageName, item.iconBitmap);
            }

            for (RemoteDeviceAppListener item : mRemoteDeviceAppListenerSet.values()) {
                item.onRemoteAppInfoListAvailable(list);
            }
        }

        @Override
        public void onRemoteStorageInfoAvailable(RemoteStorageInfo storageInfo) {
            for (RemoteDeviceAppListener item : mRemoteDeviceAppListenerSet.values()) {
                item.onRemoteStorageInfoAvailable(storageInfo);
            }
        }

        @Override
        public void onSendFileProgressForInstall(String packageName, int progress) {
            for (RemoteDeviceAppListener item : mRemoteDeviceAppListenerSet.values()) {
                item.onSendFileProgressForInstall(packageName, progress);
            }
        }

        @Override
        public void onDoneInstallApp(String packageName, int returnCode) {
            if (RemoteDeviceManagerInfo.INSTALL_SUCCEEDED == returnCode)
                clear();
            for (RemoteDeviceAppListener item : mRemoteDeviceAppListenerSet.values()) {
                item.onDoneInstallApp(packageName, returnCode);
            }
        }

        @Override
        public void onDoneDeleteApp(String packageName, int returnCode) {
            if (RemoteDeviceManagerInfo.DELETE_SUCCEEDED == returnCode)
                clear();
            for (RemoteDeviceAppListener item : mRemoteDeviceAppListenerSet.values()) {
                item.onDoneDeleteApp(packageName, returnCode);
            }
        }

        @Override
        public void onResponsePkgSizeInfo(PackageStats stats, int returnCode) {

            if (RemoteDeviceManagerInfo.REQUEST_SUCCEEDED == returnCode) {
                mAppSize.put(stats.packageName, stats.cacheSize + stats.codeSize + stats.dataSize);
            }
            for (RemoteDeviceAppListener item : mRemoteDeviceAppListenerSet.values()) {
                item.onResponsePkgSizeInfo(stats, returnCode);
            }
        }

        @Override
        public void onResponseClearAppDataOrCache(String packageName, int requestType, int returnCode) {
            for (RemoteDeviceAppListener item : mRemoteDeviceAppListenerSet.values()) {
                item.onResponseClearAppDataOrCache(packageName, requestType, returnCode);
            }
        }

        @Override
        public void onResponseClearAllAppDataAndCache(int totalClearCount, int index, String packageName, int typeOfIndex, int returnCode) {
            for (RemoteDeviceAppListener item : mRemoteDeviceAppListenerSet.values()) {
                item.onResponseClearAllAppDataAndCache(totalClearCount, index, packageName, typeOfIndex, returnCode);
            }
        }

    };

    private RemoteDeviceUtils(Context context) {
        mContext = context.getApplicationContext();
        mCallSet = new HashSet<Callback>();
        mRegister = new Register();
        mRemoteDeviceStatusListenerSet = new HashMap<Object, RemoteDeviceStatusListener>();
        mRemoteDeviceProcessListenerSet = new HashMap<Object, RemoteDeviceProcessListener>();
        mRemoteDeviceAppListenerSet = new HashMap<Object, RemoteDeviceAppListener>();
    }

    public static RemoteDeviceUtils getInstance(Context context) {
        if (null == sInstance) {
            synchronized (RemoteDeviceUtils.class) {
                if (null == sInstance) {
                    sInstance = new RemoteDeviceUtils(context);
                    IwdsLog.d(TAG, "mClient new!");
                }
            }
        }
        return sInstance;
    }

    public static void init(Context context) {
        getInstance(context).start(null);
    }

    public void start(Callback call) {

        IwdsLog.d(TAG, "mClient start");
        call = null == call ? empty : call;

        if (null == mClient) {
            mClient = new ServiceClient(mContext, ServiceManagerContext.SERVICE_REMOTE_DEVICE, this);
            mCallSet.add(call);
            mClient.connect();
            IwdsLog.d(TAG, "mClient start connect");
        } else {

            if (null == mService) {
                mCallSet.add(call);
            } else {
                if (mIsCallReady) {
                    call.onCall(mService, mRegister, mIsReady, true);
                }
            }
        }
    }

    @SuppressWarnings("unused")
    private void end() {

        IwdsLog.d(TAG, "mClient end");
        // if (0 == mCount) {
        // mClient.disconnect();
        // sInstance = null;
        // mService = null;
        // IwdsLog.d(TAG, "mClient disconnect");
        // }
    }

    public static interface Callback {
        public void onCall(RemoteDeviceServiceManager service, Register register, boolean isReady, boolean isCalledReady);
    }

    public class Register {
        public void register(Object key, RemoteDeviceStatusListener status, RemoteDeviceProcessListener process, RemoteDeviceAppListener app) {
            RemoteDeviceUtils.this.register(key, status, process, app);
        }

        public void unregister(Object key) {
            RemoteDeviceUtils.this.unregister(key);
        }
    }

    public void register(Object key, RemoteDeviceStatusListener status, RemoteDeviceProcessListener process, RemoteDeviceAppListener app) {
        IwdsLog.d(TAG, "register statue,process,app");
        status = null == status ? new SimpleImplListener.SimpleStatusCallback() : status;
        process = null == process ? new SimpleImplListener.SimpleProcessCallback() : process;
        app = null == app ? new SimpleImplListener.SimpleAppCallback() : app;
        mRemoteDeviceStatusListenerSet.put(key, status);
        mRemoteDeviceProcessListenerSet.put(key, process);
        mRemoteDeviceAppListenerSet.put(key, app);
    }

    public void unregister(Object key) {
        IwdsLog.d(TAG, "unregister statue,process,app");
        mRemoteDeviceStatusListenerSet.remove(key);
        mRemoteDeviceProcessListenerSet.remove(key);
        mRemoteDeviceAppListenerSet.remove(key);
    }

    @Override
    public void onConnected(ServiceClient serviceClient) {
        IwdsLog.d(TAG, "RemoteDeviceUtils onConnected :");
        mService = (RemoteDeviceServiceManager) serviceClient.getServiceManagerContext();
        mService.registerStatusListener(mStatusListener);
        mService.registerAppListener(mAppListener);
        mService.registerProcessListener(mProcessListener);

        for (Callback item : mCallSet) {
            item.onCall(mService, mRegister, mIsReady, false);
        }
        mCallSet.clear();
    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
        IwdsLog.d(TAG, "RemoteDeviceUtils onDisconnected :" + unexpected);

    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient, ConnectFailedReason reason) {
        IwdsLog.d(TAG, "RemoteDeviceUtils onConnectFailed: " + reason.getReasonCode());
    }

    public boolean requestClearAllAppData() {
        IwdsLog.d(TAG, "RemoteDeviceUtils requestClearAllAppData: " + mIsReady);
        if (mIsReady) {
            mService.requestClearAllAppDataAndCache();
        }
        return mIsReady;
    }

}
