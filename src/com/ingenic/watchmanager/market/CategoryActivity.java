/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.market;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ingenic.watchmanager.FragmentActivity;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.WMActivity;

//import com.ingenic.watchmanager.personal.PersonalFragment;

public class CategoryActivity extends WMActivity {
    static final String EXTRA_TYPE = "type";
    static final String EXTRA_NAME = "name";

    private View mAccount;
    private View mDownlaodIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_list_category);
        iniView();
        initEvent();

        init(getIntent());
    }

    private void iniView() {
        mAccount = findViewById(R.id.account);
        mDownlaodIcon = findViewById(R.id.download);
    }

    private void initEvent() {
        mAccount.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Intent it = new Intent(CategoryActivity.this, FragmentActivity.class);
                // it.putExtra(FragmentActivity.EXTRA_FRAGMENT, PersonalFragment.class.getName());
                // it.putExtra(FragmentActivity.EXTRA_TAG, "person");
                // startActivity(it);
            }
        });

        mDownlaodIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                CategoryActivity.this.startActivity(new Intent(CategoryActivity.this, DownloadActivity.class));
            }
        });
    }

    private void init(Intent intent) {
        int type = intent.getIntExtra(EXTRA_TYPE, -1);
        String name = intent.getStringExtra(EXTRA_NAME);
        TextView view = (TextView) findViewById(R.id.title);
        view.setText(name);
        show(new AppListFragment(type, null), false, "app_list");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        init(intent);
    }
}
