/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.market;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Build.VERSION;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ingenic.watchmanager.FragmentActivity;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.app.AppManagerFragment;
//import com.ingenic.watchmanager.personal.PersonalFragment;
import com.ingenic.watchmanager.util.ViewPagerUtils;
import com.ingenic.watchmanager.view.UnderlinePageIndicator;

public class MarketFragment extends Fragment {

    private ViewPager mViewPager;
    private ViewGroup mBtnGroup;
    private ImageView mDownlaodIcon;
    private ImageView mAccount;
    private View mSearch;
    private UnderlinePageIndicator mPageIndicator;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_market, container, false);

        initView(rootView);
        initEvent(rootView);
        initViewpager();
        bindViewPager();

        return rootView;
    }

    private void bindViewPager() {
        ViewPagerUtils.bind(mViewPager, mBtnGroup, null, null);
        mPageIndicator.setViewPager(mViewPager);
    }

    @SuppressLint("NewApi")
    private void initViewpager() {

        Fragment NewAppList = new NewAppListFragment();
        Fragment Rank = new RankAppListFragment();
        Fragment Category = new CategoryFragment();
        Fragment Manager = new AppManagerFragment();

        final List<Fragment> list = new ArrayList<Fragment>();
        list.add(NewAppList);
        list.add(Rank);
        list.add(Category);
        list.add(Manager);

        FragmentManager fm = getFragmentManagerByVersion();
        FragmentPagerAdapter adapter = new FragmentPagerAdapter(fm) {

            @Override
            public int getCount() {
                return list.size();
            }

            @Override
            public Fragment getItem(int arg0) {
                return list.get(arg0);
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
            }
        };
        mViewPager.setAdapter(adapter);

    }

    private void initEvent(View rootView) {
        // mAccount.setOnClickListener(new OnClickListener() {
        //
        // @Override
        // public void onClick(View v) {
        // Intent it = new Intent(getActivity(), FragmentActivity.class);
        // it.putExtra(FragmentActivity.EXTRA_FRAGMENT, PersonalFragment.class.getName());
        // it.putExtra(FragmentActivity.EXTRA_TAG, "person");
        // startActivity(it);
        // }
        // });

        mSearch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent it = new Intent(getActivity(), FragmentActivity.class);
                it.putExtra(FragmentActivity.EXTRA_FRAGMENT, AppSearchFragment.class.getName());
                it.putExtra(FragmentActivity.EXTRA_TAG, "search");
                startActivity(it);
            }
        });

        mDownlaodIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                getActivity().startActivity(new Intent(getActivity(), DownloadActivity.class));
            }
        });
    }

    private void initView(View rootView) {
        mViewPager = (ViewPager) rootView.findViewById(R.id.app_viewpager);
        mBtnGroup = (ViewGroup) rootView.findViewById(R.id.btn_group);
        mDownlaodIcon = (ImageView) rootView.findViewById(R.id.download);
        mAccount = (ImageView) rootView.findViewById(R.id.account);
        mSearch = rootView.findViewById(R.id.app_search);
        mPageIndicator = (UnderlinePageIndicator) rootView.findViewById(R.id.health_exercise_page_indicator);
    }

    @SuppressLint("NewApi")
    public FragmentManager getFragmentManagerByVersion() {
        if (VERSION.SDK_INT >= 17)
            return getChildFragmentManager();
        return getFragmentManager();
    }

}
