/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.metro;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.XmlResourceParser;

import com.ingenic.watchmanager.R;

public class MetroInfoParser {
    private static final List<MetroInfo> mInfos = new ArrayList<MetroInfo>();
    private static final String TAG_NAME = "info";
    private static final String ATTR_ID = "id";
    private static final String ATTR_TITLE = "title";
    private static final String ATTR_ICON = "icon";
    private static final String ATTR_TYPE = "type";
    private static final String ATTR_FRAGMENT = "fragment";

    public static List<MetroInfo> initSupportInfos(Context context)
            throws Exception {
        mInfos.clear();
        MetroInfo info = null;
        XmlResourceParser parser = context.getResources().getXml(
                R.xml.support_metro);

        int eventType = parser.getEventType();
        while (eventType != XmlResourceParser.END_DOCUMENT) {
            switch (eventType) {
            case XmlResourceParser.START_TAG:
                if (TAG_NAME.equals(parser.getName())) {
                    info = parserInfo(context, parser);
                }
                break;
            case XmlResourceParser.END_TAG:
                if (TAG_NAME.equals(parser.getName()) && info != null) {
                    mInfos.add(info);
                }
                break;
            default:
                break;
            }
            eventType = parser.next();
        }
        return mInfos;
    }

    public static List<MetroInfo> getSupportInfos() {
        return mInfos;
    }

    private static MetroInfo parserInfo(Context context, XmlResourceParser parser) {
        int attrCount = parser.getAttributeCount();
        int id = 0;
        String title = null;
        int icon = 0;
        String fragment = null;
        for (int i = 0; i < attrCount; i++) {
            String name = parser.getAttributeName(i);
            if (ATTR_ID.equals(name)) {
                id = parser.getAttributeResourceValue(i, 0);
                if (id == 0) {
                    id = parser.getAttributeIntValue(i, 0);
                }
            } else if (ATTR_TITLE.equals(name)) {
                int titleRes = parser.getAttributeResourceValue(i, 0);
                if (titleRes != 0) {
                    title = context.getString(titleRes);
                } else {                    
                    title = parser.getAttributeValue(i);
                }
            } else if (ATTR_ICON.equals(name)) {
                icon = parser.getAttributeResourceValue(i, 0);
            } else if (ATTR_FRAGMENT.equals(name)) {
                fragment = parser.getAttributeValue(i);
            }
        }
        if (id == 0) {
            return null;
        }
        MetroInfo info = new MetroInfo(id);
        info.title = title;
        info.setIcons(icon);
        info.fragment = fragment;
        return info;
    }

    public static List<MetroInfo> getFirstLoadInfos(Context context)
            throws Exception {
        if (mInfos == null) {
            return null;
        }
        List<MetroInfo> infos = new ArrayList<MetroInfo>();
        MetroInfo info = null;
        XmlResourceParser parser = context.getResources().getXml(
                R.xml.first_load);

        int eventType = parser.getEventType();
        while (eventType != XmlResourceParser.END_DOCUMENT) {
            switch (eventType) {
            case XmlResourceParser.START_TAG:
                if (TAG_NAME.equals(parser.getName())) {
                    info = meargeInfo(parser);
                }
                break;
            case XmlResourceParser.END_TAG:
                if (TAG_NAME.equals(parser.getName()) && info != null) {
                    infos.add(info);
                }
                break;
            default:
                break;
            }
            eventType = parser.next();
        }
        return infos;
    }

    private static MetroInfo meargeInfo(XmlResourceParser parser) {
        int attrCount = parser.getAttributeCount();
        int id = 0;
        int type = 0;
        for (int i = 0; i < attrCount; i++) {
            String name = parser.getAttributeName(i);
            if (ATTR_ID.equals(name)) {
                id = parser.getAttributeResourceValue(i, 0);
                if (id == 0) {
                    id = parser.getAttributeIntValue(i, 0);
                }
            } else if (ATTR_TYPE.equals(name)) {
                type = parser.getAttributeIntValue(i, 0);
            }
        }
        if (id == 0) {
            return null;
        }
        for (MetroInfo info : mInfos) {
            if (info.getId() == id) {
                info.setType(type);
                return info;
            }
        }
        return null;
    }

    public static MetroInfo getInfo(int id) {
        for (MetroInfo info : mInfos) {
            if (info.getId() == id) {
                return info;
            }
        }
        return null;
    }
}
