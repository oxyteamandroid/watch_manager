/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.metro;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.uniconnect.ConnectionServiceManager;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.FragmentActivity;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.WMFragment;
import com.ingenic.watchmanager.WatchManagerActivity;
import com.ingenic.watchmanager.WatchManagerApplication;
import com.ingenic.watchmanager.personal.PersonalFragment;
import com.ingenic.watchmanager.settings.SettingsFragment;
import com.ingenic.watchmanager.util.Utils;
import com.ingenic.watchmanager.view.Menu;
import com.ingenic.watchmanager.view.SpanContainer;

public class MetroFragment extends WMFragment implements
        SpanContainer.OnItemClickListener {
    private static final int MSG_UNBONDED = 0;
    private static final int MENU_ID_SETTINGS = Menu.FIRST;
    private static final int MENU_ID_ME = MENU_ID_SETTINGS + 1;
    @SuppressWarnings("unused")
    private static final int MENU_ID_UNBOND = MENU_ID_ME + 1;
    private SpanContainer mContainer;
    private MetroProxy mProxy;
    @SuppressWarnings("unused")
    private ImageView mIconView;
    private TextView mTitleView;
    private MetroReceiveBroadCast mReceiver;
    private AlertDialog mUnbondConfirm;
    private ProgressDialog mUnbondDialog;
    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case MSG_UNBONDED:
                if (mUnbondDialog != null && mUnbondDialog.isShowing()) {
                    mUnbondDialog.dismiss();
                }
                Intent it = new Intent(getActivity(),
                        WatchManagerActivity.class);
                startActivity(it);
                break;

            default:
                break;
            }
        };
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            MetroInfoParser.initSupportInfos(activity);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mProxy == null) {
            mProxy = new MetroProxy(activity);
        }

        mReceiver = new MetroReceiveBroadCast();
        IntentFilter intentFilter = new IntentFilter(Utils.HAS_NEW_OTA_ACTION);
        intentFilter
                .addAction(ConnectionServiceManager.ACTION_CONNECTED_ADDRESS);
        intentFilter
                .addAction(ConnectionServiceManager.ACTION_DISCONNECTED_ADDRESS);
        activity.registerReceiver(mReceiver, intentFilter);
    }

    @Override
    public void onDetach() {
        Activity activity = getActivity();
        if (activity != null) {
            activity.unregisterReceiver(mReceiver);
        }
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_metro, container,
                false);

        mIconView = (ImageView) rootView.findViewById(R.id.icon);
        mTitleView = (TextView) rootView.findViewById(R.id.title);
        mTitleView.setText(isConnected() ? R.string.label_connected
                : R.string.label_disconnected);

        mContainer = (SpanContainer) rootView
                .findViewById(R.id.metro_container);
        mContainer.setSpanGap(10);
        mContainer.setOnItemClickListener(this);
        mProxy.setSpanContainer(mContainer);
        addInfos();
        return rootView;
    }

    private boolean isConnected() {
        WatchManagerApplication app = (WatchManagerApplication) getActivity()
                .getApplication();
        return app.isConnected();
    }

    private void addInfos() {
        @SuppressWarnings("unused")
        MetroOperator operator = MetroOperator.getInstance(getActivity());
        // List<MetroInfo> infos = operator.queryAll();
        List<MetroInfo> infos = null;
        boolean first = false;
        // if (infos == null || infos.size() == 0) {
        List<MetroInfo> tmps = firstLoad();
        if (tmps != null) {
            if (infos == null) {
                infos = new ArrayList<MetroInfo>();
            }
            infos.addAll(tmps);
        }
        first = true;
        // }
        for (MetroInfo info : infos) {
            addMetro(info, first, false);
        }
    }

    private List<MetroInfo> firstLoad() {
        try {
            return MetroInfoParser.getFirstLoadInfos(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addMetro(MetroInfo info, boolean first, boolean scroll) {
        // measureInfo(info);
        // MetroView view = new MetroView(this, info);
        // view.applyFromMetroInfo(info);
        // view.setLayoutParams(new SpanContainer.LayoutParams(info.column,
        // info.row, info.getHorizontalSpan(), info.getVerticalSpan()));
        // mContainer.addView(view);
        // mWorkspace.addMetro(info, measure, scroll);
        mProxy.addMetro(info, scroll);
        // if (first) {
        // MetroOperator operator = MetroOperator.getInstance(getActivity());
        // operator.save(info);
        // }
        // info.isAdded = true;
    }

    @SuppressWarnings("unused")
    private void showSettings() {
        // Fragment fragment =
        // getFragmentManager().findFragmentByTag("settings");
        // if (fragment == null) {
        // fragment = new SettingsFragment();
        // }
        // showFragment(fragment, "settings");
        Intent it = new Intent(getActivity(), FragmentActivity.class);
        it.putExtra("fragment", SettingsFragment.class.getName());
        it.putExtra("tag", "settings");
        startActivity(it);
    }

//    private void showFragment(Fragment fragment, String tag) {
//        if (fragment == null)
//            return;
//
//        FragmentTransaction ft = getFragmentManager().beginTransaction();
//        ft.setCustomAnimations(R.anim.fragment_bottom_in,
//                R.anim.fragment_right_out, R.anim.fragment_bottom_in,
//                R.anim.fragment_right_out);
//        ft.add(R.id.container, fragment, tag);
//        ft.addToBackStack(null);
//        ft.commit();
//    }

    @SuppressWarnings("unused")
    private void showAccount() {
        // Fragment fragment =
        // getFragmentManager().findFragmentByTag("account");
        // if (fragment == null) {
        // fragment = new PersonalFragment();
        // }
        //
        // showFragment(fragment, "account");
        Intent it = new Intent(getActivity(), FragmentActivity.class);
        it.putExtra("fragment", PersonalFragment.class.getName());
        it.putExtra("tag", "account");
        startActivity(it);
    }

    @SuppressWarnings("unused")
    private void showUnbondConfirm() {
        if (mUnbondConfirm == null) {
            mUnbondConfirm = new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.title_unbonded)
                    .setPositiveButton(android.R.string.yes,
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                        int which) {
                                    unbond();
                                }
                            }).setNegativeButton(android.R.string.no, null)
                    .create();
        }
        if (!mUnbondConfirm.isShowing()) {
            mUnbondConfirm.show();
        }
    }

    private void unbond() {
        if (mUnbondDialog == null) {
            String title = getString(R.string.title_connection);
            String message = getString(R.string.text_disconnecting);
            mUnbondDialog = ProgressDialog.show(getActivity(), title, message,
                    false, false);
        } else if (!mUnbondDialog.isShowing()) {
            mUnbondDialog.show();
        }

        new Thread(new Runnable() {

            @Override
            public void run() {
                WatchManagerApplication app = (WatchManagerApplication) getActivity()
                        .getApplication();
                app.unbond();
                mHandler.sendEmptyMessage(MSG_UNBONDED);
            }
        }).start();
    }

    private class MetroReceiveBroadCast extends BroadcastReceiver {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            final String action = intent.getAction();
            if (action.equals(Utils.HAS_NEW_OTA_ACTION)) {
                boolean setflag = intent.getBooleanExtra("setoppsiteview",
                        false);
                int size = mProxy.getMetroProxySize();
                IwdsLog.d(this, " +++++" + intent.toString()
                        + "  mProxy size: " + size);
                for (int i = 0; i < size; i++) {
                    View view = mContainer.getChildAt(i);
                    if (view instanceof MetroView) {
                        MetroInfo info = ((MetroView) view).getTag();
                        if (info.getId() == R.id.ota) {
                            IwdsLog.d(this, " +++++setOppsiteView view is : "
                                    + info.title + " setoppsiteview :"
                                    + setflag);
                            if (!setflag)
                                ((MetroView) view).setOppsiteView(null);
                            else {
                                ((MetroView) view).setReverseTimeout(5000);
                                ((MetroView) view).setOppsiteView(mProxy
                                        .buildOtaOppsite());
                            }
                        }
                    }
                }
            } else if (action
                    .equals(ConnectionServiceManager.ACTION_CONNECTED_ADDRESS)) {
                DeviceDescriptor descriptor = (DeviceDescriptor) intent
                        .getParcelableExtra("DeviceDescriptor");
                if (descriptor != null) {
                    SharedPreferences sp = MetroFragment.this.getActivity()
                            .getSharedPreferences("device_address",
                                    Context.MODE_PRIVATE);
                    Editor editor = sp.edit();
                    editor.putString("deviceAddress", descriptor.devAddress);
                    editor.commit();
                }
                if (mTitleView != null) {
                    mTitleView.setText(R.string.label_connected);
                }
            } else if (action
                    .equals(ConnectionServiceManager.ACTION_DISCONNECTED_ADDRESS)) {
                if (mTitleView != null) {
                    mTitleView.setText(R.string.label_disconnected);
                }
            }
        }
    }

    @Override
    public void onItemClick(SpanContainer container, View view, int position,
            long id) {
        if (!(view instanceof MetroView)) {
            return;
        }
        MetroInfo info = ((MetroView) view).getTag();
        boolean hasFragment = info.fragment != null;
        int mid = info.getId();
        if (mid == R.id.ota) {
            if (!isConnected()) {
                Toast.makeText(getActivity(), R.string.conectstate_tip,
                        Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if (hasFragment) {
            Intent it = new Intent(getActivity(), FragmentActivity.class);
            it.putExtra("fragment", info.fragment);
            startActivity(it);
            // Fragment fragment = Fragment.instantiate(getActivity(),
            // info.fragment);
            // showFragment(fragment, "info");
        } else {
            Toast.makeText(getActivity(), info.title, Toast.LENGTH_SHORT)
                    .show();
        }
    }

    @Override
    public String getFragmentTag() {
        return "metro";
    }
}
