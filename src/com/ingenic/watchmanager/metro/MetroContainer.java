package com.ingenic.watchmanager.metro;

import android.content.Context;
import android.util.AttributeSet;

import com.ingenic.watchmanager.view.SpanContainer;

public class MetroContainer extends SpanContainer {

    public MetroContainer(Context context) {
        super(context);
    }

    public MetroContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MetroContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @SuppressWarnings("unused")
    private void setMetroProxy() {

    }
}
