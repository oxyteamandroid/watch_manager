/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.metro;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.ingenic.watchmanager.db.Operator;
import com.ingenic.watchmanager.db.WatchManagerContracts;

public class MetroOperator extends Operator<MetroInfo> {
    private static MetroOperator mInstance;

    private MetroOperator(Context context) {
        super(context, WatchManagerContracts.Tables.TABLE_METRO);
    }

    public static MetroOperator getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MetroOperator(context);
        }
        return mInstance;
    }

//    private synchronized void insert(MetroInfo info) {
//        SQLiteDatabase db = mHelper.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(DataBaseUtils.COLUMN_ID, info.getId());
//        values.put(DataBaseUtils.COLUMN_COLUMN, info.getColumn());
//        values.put(DataBaseUtils.COLUMN_ROW, info.getRow());
//        values.put(DataBaseUtils.COLUMN_TYPE, info.getType());
//        db.insert(DataBaseUtils.TABLE_METRO, null, values);
//        db.close();
//    }
//
//    private synchronized void update(MetroInfo info) {
//        SQLiteDatabase db = mHelper.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(DataBaseUtils.COLUMN_COLUMN, info.getColumn());
//        values.put(DataBaseUtils.COLUMN_ROW, info.getRow());
//        values.put(DataBaseUtils.COLUMN_TYPE, info.getType());
//        db.update(DataBaseUtils.TABLE_METRO, values, DataBaseUtils.COLUMN_ID
//                + " = ? ", new String[] { "" + info.getId() });
//        db.close();
//    }
//
//    public synchronized void update(int id, int column, int row, String title,
//            int type) {
//        SQLiteDatabase db = mHelper.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        if (column >= 0) {
//            values.put(DataBaseUtils.COLUMN_COLUMN, column);
//        }
//        if (row >= 0) {
//            values.put(DataBaseUtils.COLUMN_ROW, row);
//        }
//        if (type >= 0) {
//            values.put(DataBaseUtils.COLUMN_TYPE, type);
//        }
//        db.update(DataBaseUtils.TABLE_METRO, values, DataBaseUtils.COLUMN_ID
//                + " = ? ", new String[] { "" + id });
//        db.close();
//    }
//
//    public synchronized void save(MetroInfo info) {
//        if (hasInfo(info)) {
//            update(info);
//        } else {
//            insert(info);
//        }
//    }
//
//    public synchronized boolean hasInfo(MetroInfo info) {
//        SQLiteDatabase db = mHelper.getReadableDatabase();
//        Cursor cursor = db.query(DataBaseUtils.TABLE_METRO,
//                new String[] { DataBaseUtils.COLUMN_ID },
//                DataBaseUtils.COLUMN_ID + " = ? ",
//                new String[] { "" + info.getId() }, null, null, null);
//        if (cursor == null) {
//            db.close();
//            return false;
//        }
//        boolean result = cursor.getCount() != 0;
//        cursor.close();
//        db.close();
//        return result;
//    }
//
//    public synchronized MetroInfo query(int id) {
//        SQLiteDatabase db = mHelper.getReadableDatabase();
//        Cursor cursor = db.query(DataBaseUtils.TABLE_METRO, new String[] {
//                DataBaseUtils.COLUMN_COLUMN, DataBaseUtils.COLUMN_ROW,
//                DataBaseUtils.COLUMN_TYPE }, DataBaseUtils.COLUMN_ID + " = ? ",
//                new String[] { "" + id }, null, null, null);
//        if (cursor == null) {
//            db.close();
//            return null;
//        }
//        if (cursor.getCount() == 0) {
//            cursor.close();
//            db.close();
//            return null;
//        }
//        MetroInfo info = null;
//        if (cursor.moveToFirst()) {
//            info = MetroInfoParser.getInfo(id);
//            if (info == null) {
//                return null;
//            }
//            info.column = cursor.getInt(cursor
//                    .getColumnIndex(DataBaseUtils.COLUMN_COLUMN));
//            info.row = cursor.getInt(cursor
//                    .getColumnIndex(DataBaseUtils.COLUMN_ROW));
//            info.setType(cursor.getInt(cursor
//                    .getColumnIndex(DataBaseUtils.COLUMN_TYPE)));
//        }
//        cursor.close();
//        db.close();
//        return info;
//    }
//
//    public synchronized List<MetroInfo> queryAll() {
//        SQLiteDatabase db = mHelper.getReadableDatabase();
//        Cursor cursor = db.query(DataBaseUtils.TABLE_METRO, new String[] {
//                DataBaseUtils.COLUMN_ID, DataBaseUtils.COLUMN_COLUMN,
//                DataBaseUtils.COLUMN_ROW, DataBaseUtils.COLUMN_TYPE }, null,
//                null, null, null, null);
//        if (cursor == null) {
//            db.close();
//            return null;
//        }
//        if (cursor.getCount() == 0) {
//            cursor.close();
//            db.close();
//            return null;
//        }
//        List<MetroInfo> infos = null;
//        if (cursor.moveToFirst()) {
//            infos = new ArrayList<MetroInfo>();
//            do {
//                int id = cursor.getInt(cursor
//                        .getColumnIndex(DataBaseUtils.COLUMN_ID));
//                MetroInfo info = MetroInfoParser.getInfo(id);
//                if (info == null) {
//                    continue;
//                }
//                info.column = cursor.getInt(cursor
//                        .getColumnIndex(DataBaseUtils.COLUMN_COLUMN));
//                info.row = cursor.getInt(cursor
//                        .getColumnIndex(DataBaseUtils.COLUMN_ROW));
//                info.setType(cursor.getInt(cursor
//                        .getColumnIndex(DataBaseUtils.COLUMN_TYPE)));
//                infos.add(info);
//            } while (cursor.moveToNext());
//        }
//        cursor.close();
//        db.close();
//        return infos;
//    }
//
//    public synchronized void delete(MetroInfo info) {
//        SQLiteDatabase db = mHelper.getReadableDatabase();
//        db.delete(DataBaseUtils.TABLE_METRO, DataBaseUtils.COLUMN_ID + " = ? ",
//                new String[] { "" + info.getId() });
//        db.close();
//    }

    @Override
    protected ContentValues toValues(MetroInfo info) {
        ContentValues values = new ContentValues();
        values.put(WatchManagerContracts.MetroColumns._ID, info.getId());
        values.put(WatchManagerContracts.MetroColumns._COLUMN, info.getColumn());
        values.put(WatchManagerContracts.MetroColumns._ROW, info.getRow());
        values.put(WatchManagerContracts.MetroColumns._TYPE, info.getType());
        return values;
    }

    @Override
    protected MetroInfo fromCursor(Cursor cursor) {
        int id = cursor.getInt(cursor.getColumnIndex(WatchManagerContracts.MetroColumns._ID));
        MetroInfo info = MetroInfoParser.getInfo(id);
        if (info == null) {
            return null;
        }
        info.column = cursor.getInt(cursor
                .getColumnIndex(WatchManagerContracts.MetroColumns._COLUMN));
        info.row = cursor.getInt(cursor
                .getColumnIndex(WatchManagerContracts.MetroColumns._ROW));
        info.setType(cursor.getInt(cursor
                .getColumnIndex(WatchManagerContracts.MetroColumns._TYPE)));
        return info;
    }

    @Override
    public int delete(MetroInfo info) {
        return delete(WatchManagerContracts.MetroColumns._ID + " = ? ",
                new String[] { String.valueOf(info.getId()) });
    }

    @Override
    public int update(MetroInfo info) {
        return update(info, WatchManagerContracts.MetroColumns._ID + " = ? ",
                new String[] { String.valueOf(info.getId()) });
    }

    @Override
    public boolean hasData(MetroInfo info) {
        MetroInfo inf = query(null, info.getId() + " = ? ", new String[] {String.valueOf(info.getId())}, null, null, null);
        return inf != null;
    }
}
