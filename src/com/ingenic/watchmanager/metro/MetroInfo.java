/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.metro;

import java.util.ArrayList;
import java.util.List;

public class MetroInfo {
    static final int HSPAN_SMALL = 1;
    static final int HSPAN_MIDDLE = 2;
    static final int HSPAN_LARGE = 4;
    static final int VSPAN_SMALL = HSPAN_SMALL;
    static final int VSPAN_MIDDLE = HSPAN_MIDDLE;
    static final int VSPAN_LARGE = VSPAN_MIDDLE;
    public int column = -1;
    public int row = -1;
    int reverseTimeout = 10;
    private int icon;
    public String title;
    private int mId;
    private MetroType type;
    public boolean isAdded = false;
    String fragment;
    private List<OnMetroTypeChangeListener> mListeners = new ArrayList<MetroInfo.OnMetroTypeChangeListener>();

    public MetroInfo(int id) {
        this(id, MetroType.MIDDLE);
    }

    MetroInfo(int id, MetroType type) {
        mId = id;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public int getId() {
        return mId;
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    public void setType(int typeValue) {
        MetroType type = MetroType.getMetroType(typeValue);
        if (this.type != type) {
            MetroType old = this.type;
            this.type = type;
            for (OnMetroTypeChangeListener l : mListeners) {
                l.onMetroTypeChange(old, type);
            }
        }
    }

    public int getType() {
        return type.value;
    }

    int getHorizontalSpan() {
        return type.hSpan;
    }

    int getVerticalSpan() {
        return type.vSpan;
    }

    public void setIcons(int icon) {
        this.icon = icon;
    }

    public int getIcon() {
        return icon;
    }

    void addListener(OnMetroTypeChangeListener l) {
        if (!mListeners.contains(l)) {
            synchronized (mListeners) {
                mListeners.add(l);
            }
        }
    }

    void removeListener(OnMetroTypeChangeListener l) {
        if (mListeners.contains(l)) {
            synchronized (mListeners) {
                mListeners.remove(l);
            }
        }
    }

    interface OnMetroTypeChangeListener {
        void onMetroTypeChange(MetroType oldType, MetroType newType);
    }

    enum MetroType {
        SMALL(0, HSPAN_SMALL, VSPAN_SMALL), MIDDLE(1, HSPAN_MIDDLE,
                VSPAN_MIDDLE), LARGE(2, HSPAN_LARGE, VSPAN_LARGE);
        private int value;
        private int hSpan;
        private int vSpan;

        MetroType(int value, int hSpan, int vSpan) {
            this.value = value;
            this.hSpan = hSpan;
            this.vSpan = vSpan;
        }

        public int value() {
            return value;
        }

        static MetroType getMetroType(int value) {
            for (MetroType type : MetroType.values()) {
                if (type.value == value) {
                    return type;
                }
            }
            return MIDDLE;
        }
    }
}
