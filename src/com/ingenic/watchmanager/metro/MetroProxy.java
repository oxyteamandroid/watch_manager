package com.ingenic.watchmanager.metro;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingenic.iwds.datatransactor.elf.WeatherInfoArray;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.ota.UpdateInfo;
import com.ingenic.watchmanager.ota.UpdateUtils;
import com.ingenic.watchmanager.util.Utils;
import com.ingenic.watchmanager.view.SpanContainer;
import com.ingenic.watchmanager.weather.WeatherOperator;

public class MetroProxy {
    private Context mContext;
    private SpanContainer mContainer;
    private List<MetroInfo> mInfos = new ArrayList<MetroInfo>();
    /** 控件布局矩阵 */
    private Vector<boolean[]> mOccupied = new Vector<boolean[]>();
    private int mColumn;

    public MetroProxy(Context context) {
        mContext = context;
        mColumn = MetroModel.getMetroColumn();
    }

    public int getMetroProxySize() {
        int size = 0;
        synchronized (mInfos) {
            size = mInfos.size();
        }
        return size;
    }

    void setSpanContainer(SpanContainer container) {
        mContainer = container;
    }

    void addMetro(MetroInfo info, boolean scroll) {
        if (mInfos.contains(info)) {
            throw new IllegalArgumentException("This info is added!");
        }

        if (info.column == -1 || info.row == -1) {
            measureInfo(info);
        }

        synchronized (mInfos) {            
            mInfos.add(info);
        }
        markAsOccupied(info);
        info.isAdded = true;

        MetroView view = buildViewByInfo(info);
        view.setLayoutParams(new SpanContainer.LayoutParams(info.column, info.row, info.getHorizontalSpan(), info.getVerticalSpan()));
        mContainer.addView(view);
    }

    private MetroView buildViewByInfo(MetroInfo info) {
        MetroView view = new MetroView(mContext, info);
        view.setLayoutParams(new SpanContainer.LayoutParams(info.column, info.row, info.getHorizontalSpan(), info.getVerticalSpan()));

        switch (info.getId()) {
        case R.id.weather:
            view.setOppsiteView(buildWeatherOppsite());
            break;
        case R.id.ota:
            view.setOppsiteView(buildOtaOppsite());
            break;
        default:
            break;
        }
        return view;
    }

    private View buildWeatherOppsite() {
        WeatherOperator operator = new WeatherOperator(mContext);
        WeatherInfoArray.WeatherInfo info = operator.queryToday();
        if (info == null) {
            return null;
        }

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View oppsite = inflater.inflate(R.layout.metro_oppsite_weather, null);

        TextView city = (TextView) oppsite.findViewById(R.id.city);
        TextView weather = (TextView) oppsite.findViewById(R.id.weather);
        TextView temps = (TextView) oppsite.findViewById(R.id.temps);
        TextView date = (TextView) oppsite.findViewById(R.id.date);
        ImageView icon = (ImageView) oppsite.findViewById(R.id.weather_icon);
        TextView current = (TextView) oppsite.findViewById(R.id.current_temp);

        city.setText(info.city);
        weather.setText(info.weather);
        int weatherRes = Utils.getWeatherIconResource(mContext, Integer.parseInt(info.weatherCode));
        icon.setImageResource(weatherRes);
        String degress = Utils.getWeatherDegress(mContext, info);
        temps.setText(info.minimumTemp + "~" + info.maximumTemp + degress);
        date.setText(info.date);
        current.setText(info.currentTemp + degress);

        return oppsite;
    }

    public View buildOtaOppsite() {
        //get updateinfo from SharedPreferences,the updateinfo be saved by OtaService
        UpdateInfo info = UpdateUtils.getUpdateInfo(mContext);
        boolean state   = UpdateUtils.getRecoveryState(mContext);
        if (info == null) {
            return null;
        }

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View oppsite = inflater.inflate(R.layout.metro_oppsite_ota, null);
        TextView toverion = (TextView) oppsite.findViewById(R.id.to_version);
        TextView updatenote = (TextView) oppsite.findViewById(R.id.oppsite_updatenote);

        if (state) {
            updatenote.setText(R.string.recovery_tips);
        } else {
            updatenote.setText(R.string.update_note);
        }
        toverion.setText(info.version_to);
        return oppsite;
    }
    private void measureInfo(MetroInfo info) {
        int column = 0;
        int row = Math.max(0, mOccupied.size() - 2);
        int hSpan = info.getHorizontalSpan();
        int vSpan = info.getVerticalSpan();
        boolean measured = false;
        for (; row < mOccupied.size(); row++) {
            for (; column <= mColumn - hSpan; column++) {
                if (measureInfo(column, row, hSpan, vSpan)) {
                    info.column = column;
                    info.row = row;
                    measured = true;
                    break;
                }
            }
            if (measured)
                break;
            else
                column = 0;
        }
        if (!measured) {
            info.column = column;
            info.row = row;
        }
    }

    private boolean measureInfo(int column, int row, int hSpan, int vSpan) {
        for (int i = row; i < row + vSpan; i++) {
            if (i >= mOccupied.size())
                continue;
            boolean[] occupied = mOccupied.get(i);
            for (int j = column; j < column + hSpan; j++) {
                if (occupied[j])
                    return false;
            }
        }
        return true;
    }

    private void markAsOccupied(MetroInfo info) {
        markAsOccupied(info, mOccupied, true);
    }

    private void markAsOccupied(MetroInfo info, Vector<boolean[]> occupied, boolean fix) {
        markForMetro(info.column, info.row, info.getHorizontalSpan(), info.getVerticalSpan(), occupied, true, fix);
    }

    @SuppressWarnings("unused")
    private void markAsUnoccupied(MetroInfo info) {
        markAsUnoccupied(info, mOccupied, true);
    }
    
    /**
     * 清除控件布局的矩阵
     * 
     * @param view 清除的控件
     * @param occupied 目标矩阵
     */
    private void markAsUnoccupied(MetroInfo info, Vector<boolean[]> occupied,
            boolean fix) {
        markForMetro(info.column, info.row, info.getHorizontalSpan(),
                info.getVerticalSpan(), occupied, false, fix);
    }

    /**
     * 记录或清除矩阵信息
     * 
     * @param column 列
     * @param row 行
     * @param spanX 横向占据
     * @param spanY 纵向占据
     * @param occupied 目标矩阵
     * @param value true为记录，false为清除
     */
    private void markForMetro(int column, int row, int spanX, int spanY,
            Vector<boolean[]> occupied, boolean value, boolean fix) {
        int right = column + spanX;
        if (column < 0 || row < 0 || right > mColumn)
            throw new IllegalArgumentException();

        int count = occupied.size();
        int bottom = row + spanY;

        if (bottom > count) {
            for (int i = count; i < bottom; i++) {
                occupied.add(new boolean[mColumn]);
            }
        }

        for (int y = row; y < bottom; y++) {
            for (int x = column; x < right; x++) {
                occupied.get(y)[x] = value;
            }
        }

        if (fix) {
            inner: for (int i = count - 1; i >= 0; i--) {
                boolean[] occupy = occupied.get(i);
                int length = occupy.length;
                for (int j = 0; j < length; j++) {
                    if (occupy[j]) {
                        break inner;
                    }
                }
                occupied.remove(i);
            }
        }
    }
}
