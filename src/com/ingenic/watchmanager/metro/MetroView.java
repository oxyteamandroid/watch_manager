/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.metro;

import java.util.Random;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.view.View;
import android.view.ViewParent;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.metro.MetroInfo.MetroType;
import com.ingenic.watchmanager.metro.MetroInfo.OnMetroTypeChangeListener;
import com.ingenic.watchmanager.util.Utils;

public class MetroView extends ViewFlipper implements OnMetroTypeChangeListener {
    static final int MINIMUM_TIMEOUT = 5000;
    private int mReverseTimeout;
    private ObjectAnimator mFloatAnimator;
    private boolean mFloatAnimating = false;
    private Handler mHandler;
    private ReverseRunnable mReverseRunnable = new ReverseRunnable();
    private boolean mReverseEnable = false;
    private ImageView mIconView;
    private TextView mTitleView;
    private View mOppsite;

    public MetroView(Context context, MetroInfo info) {
        super(context);
        View obverse = inflate(context, R.layout.metro_item, null);
        mIconView = (ImageView) obverse.findViewById(R.id.icon);
        mTitleView = (TextView) obverse.findViewById(R.id.title);
        setBackgroundColor(getResources().getColor(android.R.color.holo_blue_bright));
        addView(obverse);

        Utils.initTextColor(this, Color.WHITE);

        mReverseTimeout = MINIMUM_TIMEOUT;
        mHandler = new Handler();

        setup(info);
    }

    public int getReverseTimeout() {
        return mReverseTimeout;
    }

    public void setOppsiteView(View oppsite) {
        if (mOppsite != null) {
            removeView(mOppsite);
            mOppsite = null;
        }

        if (oppsite == null) {
            setReverseEnable(false);
        } else {
            setReverseEnable(true);
            addView(oppsite);

            Utils.initTextColor(oppsite, Color.WHITE);
        }
        mOppsite = oppsite;
    }

    /**
     * 设置反转动画时间间隔，最小间隔不能小于5妙，如果小于，强制设置为5妙
     * 
     * @param timeout
     *            时间间隔
     */
    public void setReverseTimeout(int timeout) {
        if (timeout < MINIMUM_TIMEOUT) {
            timeout = MINIMUM_TIMEOUT;
        }
        this.mReverseTimeout = timeout;
    }

    private void setup(MetroInfo info) {
        setReverseTimeout(info.reverseTimeout * 1000);
        info.addListener(this);
        setTag(info);
//        setReverseEnable(true);
        mIconView.setImageResource(info.getIcon());
        mTitleView.setText(info.title);
    }

    private void onReserve() {
        showNext();
    }

    public void applyFromMetroInfo(MetroInfo info) {
        setReverseTimeout(info.reverseTimeout * 1000);
        info.addListener(this);
        setTag(info);
        setReverseEnable(true);
        mIconView.setImageResource(info.getIcon());
        mTitleView.setText(info.title);
    }

    void changeSize() {
        MetroInfo info = getTag();
        int value = info.getType();
        value -= 1;
        if (value < 0) {
            value += 3;
        }
        info.setType(value);
    }

    public MetroInfo getTag() {
        return (MetroInfo) super.getTag();
    }

    @Override
    public void onMetroTypeChange(MetroType oldType, MetroType newType) {
        if (newType == MetroInfo.MetroType.SMALL) {
            setReverseEnable(false);
            mTitleView.setVisibility(GONE);
        } else {
            setReverseEnable(true);
            mTitleView.setVisibility(VISIBLE);
        }
    }

    void floatAnimation(final float translation) {
        Random random = new Random(System.currentTimeMillis());
        PropertyValuesHolder xHolder = PropertyValuesHolder.ofFloat(
                "translationX", random.nextFloat() * translation * 2
                        - translation);
        PropertyValuesHolder yHolder = PropertyValuesHolder.ofFloat(
                "translationY", random.nextFloat() * translation * 2
                        - translation);
        mFloatAnimator = ObjectAnimator.ofPropertyValuesHolder(this, xHolder,
                yHolder);
        mFloatAnimator.setDuration(1000 + random.nextInt(1001));
        mFloatAnimator.start();
        mFloatAnimating = true;
        mFloatAnimator
                .addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        ViewParent parent = getParent();
                        if (parent != null && parent instanceof View) {
                            ((View) parent).invalidate();
                        }
                    }
                });
        mFloatAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                animation.removeAllListeners();
                animation = null;
                if (mFloatAnimating) {
                    floatAnimation(translation);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                animation = null;
            }
        });
    }

    void stopFloatAnimation() {
        if (mFloatAnimating) {
            mFloatAnimating = false;
            mFloatAnimator.end();
        }
        setTranslationX(0);
        setTranslationY(0);
    }

    public boolean isReverseEnable() {
        return mReverseEnable;
    }

    /** 设置能否反转，如果磁贴类型为小，则强制设置为不可反转。 */
    public void setReverseEnable(boolean enable) {
        MetroInfo info = getTag();
        if (info.getType() == MetroInfo.MetroType.SMALL.value()) {
            mReverseEnable = false;
        } else {
            mReverseEnable = enable;
        }
        mHandler.removeCallbacks(mReverseRunnable);
        if (mReverseEnable) {
            startReverse();
        }
    }

    private ObjectAnimator rotationAnimation(float rotation, int duration,
            Interpolator i) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(this, "RotationX",
                rotation);
        animator.setDuration(duration);
        animator.setInterpolator(i);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                ViewParent parent = getParent();
//                while (parent != null && !(parent instanceof Workspace)) {
//                    parent = parent.getParent();
//                }
                if (parent != null) {
                    ((View) parent).invalidate();
                }
            }
        });
        return animator;
    }

    private void startReverse() {
        mHandler.postDelayed(mReverseRunnable, mReverseTimeout);
    }

    private void reverse() {
        ObjectAnimator animator = rotationAnimation(30, 70,
                new DecelerateInterpolator());
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                // TODO Auto-generated method stub
                clearAnimation();
                ObjectAnimator animator = rotationAnimation(-90, 140,
                        new AccelerateInterpolator());
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        clearAnimation();
                        setRotationX(90);

                        onReserve();

                        ObjectAnimator animator = rotationAnimation(-30, 140,
                                new DecelerateInterpolator());
                        animator.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                clearAnimation();
                                ObjectAnimator animator = rotationAnimation(0,
                                        70, new DecelerateInterpolator());
                                animator.start();
                            }
                        });
                        animator.start();
                    }
                });
                animator.start();
            }
        });
        animator.start();
    }

    private class ReverseRunnable implements Runnable {

        @Override
        public void run() {
            if (!mReverseEnable) {
                return;
            }
            reverse();
            mHandler.postDelayed(this, mReverseTimeout);
        }
    }
}
