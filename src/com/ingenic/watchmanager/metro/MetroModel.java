/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.metro;

public class MetroModel {
    public static final int COLUMN_FOUR = 4;
    public static final int COLUMN_SIX = 6;
    private static int METRO_COLUMN = COLUMN_FOUR;

    public static void setMetroColumn(int column) {
        if (METRO_COLUMN == column) {
            return;
        }
        if (column == COLUMN_SIX) {
            METRO_COLUMN = column;
        } else {
            METRO_COLUMN = COLUMN_FOUR;
        }
    }

    public static int getMetroColumn() {
        return METRO_COLUMN;
    }
}
