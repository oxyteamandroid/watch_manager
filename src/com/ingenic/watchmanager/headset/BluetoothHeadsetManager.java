/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * YuanBanglin(Ahlin)<banglin.yuan@ingenic.com>
 *
 * Elf/IDWS Project
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.headset;

import java.lang.reflect.Method;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothProfile.ServiceListener;
import android.content.Context;

public class BluetoothHeadsetManager {

    private static BluetoothHeadsetManager mBluetoothHeadsetManager;
    private BluetoothHeadset mBH;
    private boolean isInitialize = false;

    private BluetoothHeadsetManager() {

    }

    /**
     * 初始化
     * 
     * @param device
     */
    public void initialize(Context context) {
        if (mBluetoothHeadsetManager == null) {
            throw new NullPointerException(
                    "Initialization must be instantiated before!");
        }
        if (mBH == null) {
            if (context == null) {
                throw new NullPointerException("Context is null!");
            }
            BluetoothAdapter.getDefaultAdapter().getProfileProxy(context,
                    serviceListener, BluetoothProfile.HEADSET);
        }
    }

    /**
     * 初始化是否成功
     * 
     * @return
     */
    public boolean isInitialize() {
        return isInitialize;
    }

    /**
     * 注销蓝牙耳机服务
     */
    public void cancellationBluetoothHeadset() {
        if (mBH != null) {
            BluetoothAdapter.getDefaultAdapter().closeProfileProxy(
                    BluetoothProfile.HEADSET, mBH);
        }
        mBH = null;
        isInitialize = false;
    }

    ServiceListener serviceListener = new ServiceListener() {

        @Override
        public void onServiceDisconnected(int profile) {
            mBH = null;
            isInitialize = false;
        }

        @Override
        public void onServiceConnected(int profile, BluetoothProfile proxy) {
            mBH = (BluetoothHeadset) proxy;
            isInitialize = true;
        }
    };

    /**
     * 实例化
     * 
     * @return
     */
    public static BluetoothHeadsetManager getDefault() {
        if (mBluetoothHeadsetManager == null) {
            mBluetoothHeadsetManager = new BluetoothHeadsetManager();
        }
        return mBluetoothHeadsetManager;
    }

    /**
     * 判断蓝牙耳机是否连接
     * 
     * @return
     */
    public static boolean isBlueToothHeadsetConnected() {
        boolean retval = false;
        try {
            retval = BluetoothAdapter.getDefaultAdapter()
                    .getProfileConnectionState(BluetoothHeadset.HEADSET) == BluetoothHeadset.STATE_CONNECTED;
        } catch (Exception exc) {
            // nothing to do
        }
        return retval;
    }

    /**
     * 连接蓝牙耳机
     * 
     * @param bh
     * @param device
     * @return
     */
    public boolean connectBluetoothHeadset(String deviceAddress) {
        boolean bo = false;
        if (deviceAddress == null) {
            throw new NullPointerException("Bluetooth Device address is null");
        }
        BluetoothDevice device = BluetoothAdapter.getDefaultAdapter()
                .getRemoteDevice(deviceAddress);
        if (device == null) {
            throw new NullPointerException("Device is null");
        }
        Class<?> demo = null;
        try {
            if (mBH != null) {
                demo = mBH.getClass();
            } else {
                throw new NullPointerException("Bluetooth Headset is null");
            }
            if (demo != null) {
                Method m = demo.getMethod("connect", BluetoothDevice.class);
                bo = (Boolean) m.invoke(mBH, device);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bo;
    }

    /**
     * 断开蓝牙耳机连接
     * 
     * @param bh
     * @param device
     * @return
     */
    public boolean breakBluetoothHeadset(String deviceAddress) {
        boolean bo = false;
        if (deviceAddress == null) {
            throw new NullPointerException("Bluetooth Device address is null");
        }
        BluetoothDevice device = BluetoothAdapter.getDefaultAdapter()
                .getRemoteDevice(deviceAddress);
        if (device == null) {
            throw new NullPointerException("Get a Bluetooth device failure");
        }
        Class<?> demo = null;
        try {
            if (mBH != null) {
                demo = mBH.getClass();
            }
            if (demo != null) {
                Method m = demo.getMethod("disconnect", BluetoothDevice.class);
                bo = (Boolean) m.invoke(mBH, device);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bo;
    }
}
