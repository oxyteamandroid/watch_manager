/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager;

import android.content.Context;
import android.preference.SwitchPreference;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.util.Utils;

/**
 * 自定义的SwitchPreference。调整了SwitchPreference的UI。
 */
public class IngenicSwitchPreference extends SwitchPreference {

    public IngenicSwitchPreference(Context context) {
        super(context);
    }

    public IngenicSwitchPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public IngenicSwitchPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        configSwitch(view);
        configIcon(view);
    }

    /**
     * 调整Switch控件的UI
     */
    private boolean configSwitch(View root) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            IwdsLog.w(this, "SDK < 16.Do not config Switch!");
            return false;
        }

        if (root instanceof Switch) {
            Switch _switch = (Switch) root;
            _switch.setTrackResource(R.drawable.ic_track);
            _switch.setThumbResource(R.drawable.ic_thumb);
            _switch.setTextOff("");
            _switch.setTextOn("");
            _switch.setSwitchMinWidth(Utils.dp2px(getContext(), 48));

            return true;
        }

        if (root instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) root;
            int childCount = vg.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = vg.getChildAt(i);
                if (configSwitch(child)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * 调整Icon的UI
     */
    private void configIcon(View root) {
        ImageView view = (ImageView) root.findViewById(android.R.id.icon);
        if (view != null) {
            ViewGroup.LayoutParams lp = view.getLayoutParams();
            int size = Utils.dp2px(getContext(), 48);

            if (lp != null) {
                lp.width = size;
                lp.height = size;
            } else {
                lp = new ViewGroup.LayoutParams(size, size);
            }

            view.setLayoutParams(lp);
        }
    }
}
