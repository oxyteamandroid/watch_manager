/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.theme;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.util.ImageRequest;
import com.ingenic.watchmanager.util.Request.ErrorListener;
import com.ingenic.watchmanager.util.Request.Listener;

public class GridViewAdapter extends BaseAdapter {

    protected List<ThemePreViewInfo> themeList;
    protected LayoutInflater mInflater = null;
    protected Context mContext;

    private int mScreenWidth;

    public GridViewAdapter(Context context, int screenWidth, int screenHeight) {
        super();
        mInflater = LayoutInflater.from(context);
        mScreenWidth = screenWidth;
        mContext = context;
    }

    public List<ThemePreViewInfo> getThemeList() {
        return themeList;
    }

    public void setThemeList(List<ThemePreViewInfo> themeList) {
        this.themeList = themeList;
    }

    @Override
    public int getCount() {
        return themeList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class Holder {
        TextView tv;
        TextView tv_;
        ImageView img;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Holder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.theme_gridview_item, parent, false);
            holder = new Holder();
            holder.tv = (TextView) convertView.findViewById(R.id.gridview_text);
            holder.img = (ImageView) convertView.findViewById(R.id.gridview_img);
            holder.tv_ = (TextView) convertView.findViewById(R.id.gridview_hint);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.tv.setText(themeList.get(position).getName());

        LinearLayout.LayoutParams mRelayoutParams = new LinearLayout.LayoutParams((int) ((mScreenWidth / 2) * 0.9), (int) ((mScreenWidth / 2) * 0.9));
        mRelayoutParams.topMargin = 5;
        mRelayoutParams.gravity = Gravity.CENTER;
        holder.img.setLayoutParams(mRelayoutParams);

        holder.tv_.setVisibility(View.VISIBLE);
        holder.tv_.setText(R.string.change_cheme);
        holder.tv_.setVisibility(View.GONE);

        final int tempPosition = position;
        ImageRequest request = new ImageRequest(mContext, themeList.get(position).getIcon(), new Listener<Bitmap>() {

            @Override
            public void onResponse(Bitmap response) {
                holder.img.setImageBitmap(response);
                themeList.get(tempPosition).pic = response;
            }
        }, new ErrorListener() {

            @Override
            public void onErrorResponse(Exception ex) {
                holder.img.setImageResource(R.drawable.ic_launcher);
                themeList.get(tempPosition).pic = ((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.ic_launcher)).getBitmap();
                ex.printStackTrace();
            }
        }, true);
        request.start();

        return convertView;
    }
}
