/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.theme;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

public class CustomDialogFragment extends DialogFragment {

    private Dialog mDialog;

    public CustomDialogFragment() {
    }

    public void setDialog(Dialog dialog) {
        mDialog = dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return mDialog;
    }
}