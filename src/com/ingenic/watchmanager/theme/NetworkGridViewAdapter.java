/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.theme;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.theme.ThemeDbOperation.DOWNLOAD_STATUS;

public class NetworkGridViewAdapter extends GridViewAdapter {

    private int mThemeDownloadedStatusColor; // 已下载
    private int mThemeDownloadStatusColor; // 未下载
    private int mThemeDownloadingStatusColor; // 下载中
    private int mThemeDownloadErrorStatusColor; // 下载错误

    private String mDownloading;
    private String mDownload_Error;

    public NetworkGridViewAdapter(Context context, int screenWidth, int screenHeight) {
        super(context, screenWidth, screenHeight);
        mThemeDownloadedStatusColor = mContext.getResources().getColor(R.color.theme_downloadcolor);
        mThemeDownloadStatusColor = mContext.getResources().getColor(R.color.theme_loadcolor);
        mThemeDownloadingStatusColor = mContext.getResources().getColor(R.color.color_green_1);
        mThemeDownloadErrorStatusColor = mContext.getResources().getColor(R.color.color_red_1);

        mDownloading = mContext.getString(R.string.downloading);
        mDownload_Error = mContext.getString(R.string.download_error);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = super.getView(position, convertView, parent);
        final Holder holder = (Holder) convertView.getTag();

        holder.tv.setText(themeList.get(position).getName());
        if (themeList.get(position).getDownloadStatus() == DOWNLOAD_STATUS.TO_WATCH.ordinal()) {

            holder.tv_.setTextColor(mThemeDownloadedStatusColor);
            holder.tv_.setText(R.string.to_download);

        } else if (themeList.get(position).getDownloadStatus() == DOWNLOAD_STATUS.DOWNLOADING.ordinal()) {

            holder.tv_.setText(mDownloading);
            holder.tv_.setTextColor(mThemeDownloadingStatusColor);

        } else if (themeList.get(position).getDownloadStatus() == DOWNLOAD_STATUS.NETWORK_ERROR.ordinal() || themeList.get(position).getDownloadStatus() == DOWNLOAD_STATUS.NO_DOWNLOAD_GET_ADDR_FAILSE.ordinal()) {

            holder.tv_.setText(mDownload_Error);
            holder.tv_.setTextColor(mThemeDownloadErrorStatusColor);

        } else {
            holder.tv_.setText(R.string.downloadtheme);
            holder.tv_.setTextColor(mThemeDownloadStatusColor);
        }
        holder.tv_.setVisibility(View.VISIBLE);
        return convertView;
    }
}
