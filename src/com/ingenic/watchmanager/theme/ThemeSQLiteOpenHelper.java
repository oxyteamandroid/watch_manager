/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.theme;

/**
 * 该文件用于维护和建造数据库
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ThemeSQLiteOpenHelper extends SQLiteOpenHelper {

    // 版本号
    public final static int version = 16;
    // 数据库名
    public static final String DATABASE_NAME = "theme";

    // 分别为id字段、添加时间、主题大小、是否下载完成、语言、作者、描述、图片、主题名
    public static final String THEME_ID = "id";
    public static final String THEME_ADDTIME = "addtime";
    public static final String THEME_SIZE = "size";
    public static final String THEME_DOWNLOAD = "download";
    public static final String THEME_DOWNLOAD_SIZE = "downloadsize";
    public static final String THEME_DOWNLOAD_PERCENT = "theme_download_persent";
    public static final String THEME_MD5 = "md5";
    public static final String THEME_IMAGES = "images";

    public static final String THEME_LOCALE = "locale";
    public static final String THEME_AUTHOR = "author";
    public static final String THEME_DESCRIPTION = "description";
    public static final String THEME_NAME = "name";
    public static final String THEME_ICON = "icon";

    // 记录一个主题对应的唯一一条记录
    public static final String TABLE_CONSTANT = "theme_constant";

    // 创建主题不变的表
    private final String create_table_theme_constant = "CREATE TABLE IF NOT EXISTS "
            + TABLE_CONSTANT
            + " ("
            + THEME_ID                  + " TEXT NOT NULL, "
            + THEME_ADDTIME             + " TEXT, "
            + THEME_DOWNLOAD            + " INTEGER NOT NULL, "
            + THEME_IMAGES              + " TEXT, "
            + THEME_DOWNLOAD_SIZE       + " TEXT, "
            + THEME_DOWNLOAD_PERCENT    + " TEXT, "
            + THEME_MD5                 + " TEXT, "
            + THEME_SIZE                + " TEXT, "

            + THEME_LOCALE              + " INTEGER NOT NULL, "
            + THEME_AUTHOR              + " TEXT, "
            + THEME_NAME                + " TEXT, "
            + THEME_DESCRIPTION         + " TEXT, "
            + THEME_ICON                + " TEXT, "

            + "PRIMARY KEY (" + THEME_ID + ", " + THEME_LOCALE + "))";

    private static ThemeSQLiteOpenHelper mInstance = null;

    public synchronized static ThemeSQLiteOpenHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ThemeSQLiteOpenHelper(context);
        }
        return mInstance;
    }

    // 构造函数
    public ThemeSQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(create_table_theme_constant);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_CONSTANT + ";");

        onCreate(db);
    }
}
