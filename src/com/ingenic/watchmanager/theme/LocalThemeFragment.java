/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.theme;

/**
 * 该类是主题主界面
 */
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.util.Utils;

public class LocalThemeFragment extends Fragment implements OnItemClickListener {

    public static final String ID = "id";

    public static final String IS_LOCAL = "isLocal";

    private List<ThemePreViewInfo> locadThemeLists = new ArrayList<ThemePreViewInfo>();

    private GridView mGridView = null;
    private GridViewAdapter mAdapter = null;

    // 本地提示控件
    private TextView mLocadHintView;

    private ThemeDbOperation mThemeDbOperator;
    private ThemeDataChangeReceiver mThemeDataChangeReceiver;

    private int mScreenWidth;
    private int mScreenHeight;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_theme, container, false);
        mThemeDbOperator = new ThemeDbOperation(getActivity().getApplicationContext());
        mThemeDataChangeReceiver = new ThemeDataChangeReceiver();
        IntentFilter filterTheme = new IntentFilter();
        filterTheme.addAction(ThemePreviewActivity.THEME_DATA_CHANGE);
        this.getActivity().registerReceiver(mThemeDataChangeReceiver, filterTheme);

        getScreenWidthHeight();
        // 本地GridView操作
        mGridView = (GridView) rootView.findViewById(R.id.theme_gridView);
        mLocadHintView = (TextView) rootView.findViewById(R.id.localProgressLinear);

        mAdapter = new GridViewAdapter(getActivity(), mScreenWidth, mScreenHeight);

        mAdapter.setThemeList(locadThemeLists);

        mGridView.setAdapter(mAdapter);

        refreshDataTheme();

        return rootView;
    }

    private void refreshDataTheme() {
        locadThemeLists.clear();
        localThemeDbOperation();
    }

    private void localThemeDbOperation() {

        List<ThemePreViewInfo> TPVILists = mThemeDbOperator.queryThemeConstantId();

        if (TPVILists == null) {
            locadThemeLists.clear();
            mLocadHintView.setVisibility(View.VISIBLE);
            // 更新界面
            mAdapter.notifyDataSetChanged();
            mGridView.getEmptyView();
            return;
        }

        List<ThemePreViewInfo> tempPreviewInfoList = new ArrayList<ThemePreViewInfo>();
        for (ThemePreViewInfo tpvi : TPVILists) {
            File file = new File(ThemePreviewActivity.getFilePath(tpvi.id));
            if (file == null || !file.exists()) {
                mThemeDbOperator.deleteThemeConstantRecord(tpvi);
                continue;
            } else if (null != tpvi.getMd5file() && !tpvi.getMd5file().equals(Utils.getFileMD5(file))) {
                mThemeDbOperator.deleteThemeConstantRecord(tpvi);
                // file.delete();
                continue;
            }
            // 存在则查询相关信息
            ThemePreViewInfo tempPreViewInfo = mThemeDbOperator.queryThemeConstantPreView(tpvi.getId() + "");
            if (tempPreViewInfo != null) {
                // 添加进列表中
                tempPreviewInfoList.add(tempPreViewInfo);
            }
        }
        // 本地GridView更新
        locadThemeLists.clear();
        if (tempPreviewInfoList != null && tempPreviewInfoList.size() > 0) {
            locadThemeLists.addAll(tempPreviewInfoList);
            mLocadHintView.setVisibility(View.GONE);
        } else {
            mLocadHintView.setVisibility(View.VISIBLE);
        }
        // 更新界面
        mAdapter.notifyDataSetChanged();
    }

    public class ThemeDataChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (ThemePreviewActivity.THEME_DATA_CHANGE.equals(action)) {
                refreshDataTheme();
            }
        }

    }

    @Override
    public void onDestroy() {
        this.getActivity().unregisterReceiver(mThemeDataChangeReceiver);
        super.onDestroy();
    }

    private void getScreenWidthHeight() {

        DisplayMetrics metric = new DisplayMetrics();
        this.getActivity().getWindowManager().getDefaultDisplay().getMetrics(metric);
        mScreenWidth = metric.widthPixels; // 屏幕宽度（像素）
        mScreenHeight = metric.heightPixels; // 屏幕高度（像素）

    }

    @Override
    public void onResume() {
        super.onResume();
        mGridView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mGridView.setOnItemClickListener(null);
        Intent intent = new Intent(getActivity(), ThemePreviewActivity.class);
        long tempId = locadThemeLists.get(position).getId();
        intent.putExtra(ID, tempId);
        intent.putExtra(IS_LOCAL, true);
        startActivity(intent);
    }
}
