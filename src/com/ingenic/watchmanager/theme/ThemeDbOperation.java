/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.theme;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ingenic.watchmanager.util.Utils;

public class ThemeDbOperation {

    public static enum DOWNLOAD_STATUS {
        NO_DOWNLOAD, NO_DOWNLOAD_GET_ADDR, NO_DOWNLOAD_GET_ADDR_FAILSE, DOWNLOADING, PAUSE, NETWORK_ERROR, TO_WATCH // ,
                                                                                                                    // USING
    };

    @SuppressLint("UseSparseArrays")
    public static Map<Integer, DOWNLOAD_STATUS> dic = new HashMap<Integer, DOWNLOAD_STATUS>();
    static {
        for (DOWNLOAD_STATUS s : DOWNLOAD_STATUS.values()) {
            dic.put(s.ordinal(), s);
        }
    }

    private ThemeSQLiteOpenHelper mSqLiteOpenHelper;
    private SQLiteDatabase mSqLiteDatabase;
    private Cursor mCursor;

    public ThemeDbOperation(Context context) {
        mSqLiteOpenHelper = ThemeSQLiteOpenHelper.getInstance(context);
        mSqLiteDatabase = mSqLiteOpenHelper.getWritableDatabase();
    }

    public void insertThemeConstant(ThemePreViewInfo value) {

        if (exists(value)) return;

        ContentValues content = new ContentValues();

        content.put(ThemeSQLiteOpenHelper.THEME_ID, value.getId());
        content.put(ThemeSQLiteOpenHelper.THEME_ADDTIME, value.getAddtime());

        content.put(ThemeSQLiteOpenHelper.THEME_SIZE, value.getSize());
        content.put(ThemeSQLiteOpenHelper.THEME_DOWNLOAD, DOWNLOAD_STATUS.NO_DOWNLOAD.ordinal());
        content.put(ThemeSQLiteOpenHelper.THEME_DOWNLOAD_SIZE, 0);

        content.put(ThemeSQLiteOpenHelper.THEME_DOWNLOAD_PERCENT, value.getProgress());
        content.put(ThemeSQLiteOpenHelper.THEME_MD5, value.getMd5file());
        content.put(ThemeSQLiteOpenHelper.THEME_IMAGES, value.getImagesStr());

        content.put(ThemeSQLiteOpenHelper.THEME_LOCALE, value.getLocale());
        content.put(ThemeSQLiteOpenHelper.THEME_AUTHOR, value.getDev());
        content.put(ThemeSQLiteOpenHelper.THEME_DESCRIPTION, value.getDescription());

        content.put(ThemeSQLiteOpenHelper.THEME_ICON, value.getIcon());
        content.put(ThemeSQLiteOpenHelper.THEME_NAME, value.getName());

        synchronized (mSqLiteDatabase) {
            mSqLiteDatabase.insert(ThemeSQLiteOpenHelper.TABLE_CONSTANT, null, content);
        }
    }

    public void deleteThemeConstantRecord(ThemePreViewInfo value) {

        synchronized (mSqLiteDatabase) {
            mSqLiteDatabase.delete(ThemeSQLiteOpenHelper.TABLE_CONSTANT, ThemeSQLiteOpenHelper.THEME_ID + " = ? and "
                    + ThemeSQLiteOpenHelper.THEME_LOCALE + " = ? ",
                    new String[] { value.getId() + "", value.getLocale() + "" });
        }
    }

    public void deleteThemeConstantRecord(long id) {

        synchronized (mSqLiteDatabase) {
            mSqLiteDatabase.delete(ThemeSQLiteOpenHelper.TABLE_CONSTANT, ThemeSQLiteOpenHelper.THEME_ID + " = ?",
                    new String[] { id + "" });
        }
    }

    public boolean exists(ThemePreViewInfo value) {

        String sql = "select 1 from " + ThemeSQLiteOpenHelper.TABLE_CONSTANT + " where "
                + ThemeSQLiteOpenHelper.THEME_ID + " = ? and " + ThemeSQLiteOpenHelper.THEME_LOCALE + " = ? ";
        synchronized (mSqLiteDatabase) {
            mCursor = mSqLiteDatabase.rawQuery(sql, new String[] { value.getId() + "", value.getLocale() + "" });
        }
        boolean result = mCursor.moveToFirst();
        mCursor.close();
        return result;
    }

    public void updateThemeDownloadStatus(ThemePreViewInfo info) {

        ContentValues values = new ContentValues();
        values.put(ThemeSQLiteOpenHelper.THEME_DOWNLOAD, info.getDownloadStatus());

        values.put(ThemeSQLiteOpenHelper.THEME_DOWNLOAD_SIZE, info.getDownloadSize());
        values.put(ThemeSQLiteOpenHelper.THEME_DOWNLOAD_PERCENT, info.getProgress());
        synchronized (mSqLiteDatabase) {
            mSqLiteDatabase.update(ThemeSQLiteOpenHelper.TABLE_CONSTANT, values, ThemeSQLiteOpenHelper.THEME_ID
                    + "  = ? and " + ThemeSQLiteOpenHelper.THEME_LOCALE + " = ? ", new String[] { info.getId() + "",
                    info.getLocale() + "" });
        }
    }

    public void updateThemeConstantRecord(ThemePreViewInfo info) {

        ContentValues values = new ContentValues();
        if (info.getAddtime() > 0L) values.put(ThemeSQLiteOpenHelper.THEME_ADDTIME, info.getAddtime());
        if (info.getSize() > 0) values.put(ThemeSQLiteOpenHelper.THEME_SIZE, info.getSize());

        if (info.getMd5file() != null && !info.getMd5file().trim().equals(""))
            values.put(ThemeSQLiteOpenHelper.THEME_MD5, info.getMd5file());
        if (info.getImages() != null) values.put(ThemeSQLiteOpenHelper.THEME_IMAGES, info.getImagesStr());

        if (info.getDev() != null) values.put(ThemeSQLiteOpenHelper.THEME_AUTHOR, info.getDev());
        if (info.getDescription() != null) values.put(ThemeSQLiteOpenHelper.THEME_DESCRIPTION, info.getDescription());
        if (info.getName() != null) values.put(ThemeSQLiteOpenHelper.THEME_NAME, info.getName());
        if (info.getIcon() != null) values.put(ThemeSQLiteOpenHelper.THEME_ICON, info.getIcon());
        synchronized (mSqLiteDatabase) {
            mSqLiteDatabase.update(ThemeSQLiteOpenHelper.TABLE_CONSTANT, values, ThemeSQLiteOpenHelper.THEME_ID
                    + "  = ? and " + ThemeSQLiteOpenHelper.THEME_LOCALE + " = ? ", new String[] { info.getId() + "",
                    info.getLocale() + "" });
        }
    }

    private ThemePreViewInfo fillInfo(Cursor cursor1) {

        ThemePreViewInfo localPreviewInfo = new ThemePreViewInfo();
        Cursor cursor2 = cursor1;

        localPreviewInfo.setId(cursor2.getLong(cursor2.getColumnIndex(ThemeSQLiteOpenHelper.THEME_ID)));
        localPreviewInfo.setAddtime(cursor2.getLong(cursor2.getColumnIndex(ThemeSQLiteOpenHelper.THEME_ADDTIME)));

        localPreviewInfo.setSize(cursor2.getLong(cursor2.getColumnIndex(ThemeSQLiteOpenHelper.THEME_SIZE)));
        localPreviewInfo
                .setDownloadStatus(cursor2.getInt(cursor2.getColumnIndex(ThemeSQLiteOpenHelper.THEME_DOWNLOAD)));

        localPreviewInfo.setDownloadSize(cursor2.getLong(cursor2
                .getColumnIndex(ThemeSQLiteOpenHelper.THEME_DOWNLOAD_SIZE)));

        localPreviewInfo.setMd5file(cursor2.getString(cursor2.getColumnIndex(ThemeSQLiteOpenHelper.THEME_MD5)));
        String images = cursor2.getString(cursor2.getColumnIndex(ThemeSQLiteOpenHelper.THEME_IMAGES));
        if (images != null) {
            String[] imagess = images.split(",");
            List<String> list = new ArrayList<String>();
            for (int i = 0; i < imagess.length; i++) {
                if (null != imagess[i] && !imagess[i].trim().equals("")) list.add(imagess[i].trim());
            }
            localPreviewInfo.setImages(list);
        }

        localPreviewInfo.setLocale(cursor2.getInt(cursor2.getColumnIndex(ThemeSQLiteOpenHelper.THEME_LOCALE)));
        localPreviewInfo.setDev(cursor2.getString(cursor2.getColumnIndex(ThemeSQLiteOpenHelper.THEME_AUTHOR)));
        localPreviewInfo.setDescription(cursor2.getString(cursor2
                .getColumnIndex(ThemeSQLiteOpenHelper.THEME_DESCRIPTION)));

        localPreviewInfo.setIcon(cursor2.getString(cursor2.getColumnIndex(ThemeSQLiteOpenHelper.THEME_ICON)));
        localPreviewInfo.setName(cursor2.getString(cursor2.getColumnIndex(ThemeSQLiteOpenHelper.THEME_NAME)));

        localPreviewInfo.setProgress(cursor2.getInt(cursor2.getColumnIndex(ThemeSQLiteOpenHelper.THEME_DOWNLOAD_PERCENT)));

        return localPreviewInfo;
    }

    public List<ThemePreViewInfo> queryThemeConstantId() {

        List<ThemePreViewInfo> tempList = null;

        mCursor = mSqLiteDatabase.rawQuery("select * from " + ThemeSQLiteOpenHelper.TABLE_CONSTANT + " where "
                + ThemeSQLiteOpenHelper.THEME_DOWNLOAD + " = " + DOWNLOAD_STATUS.TO_WATCH.ordinal(), null);

        if (mCursor.moveToFirst()) {
            tempList = new ArrayList<ThemePreViewInfo>();
            while (!mCursor.isAfterLast()) {
                ThemePreViewInfo tpvi = fillInfo(mCursor);
                tempList.add(tpvi);
                mCursor.moveToNext();
            }
        }
        mCursor.close();
        return tempList;
    }

    public List<ThemePreViewInfo> queryThemeStatus() {

        List<ThemePreViewInfo> tempList = null;

        mCursor = mSqLiteDatabase.rawQuery("select * from " + ThemeSQLiteOpenHelper.TABLE_CONSTANT + " where "
                + ThemeSQLiteOpenHelper.THEME_DOWNLOAD + " <> " + DOWNLOAD_STATUS.NO_DOWNLOAD.ordinal(), null);

        if (mCursor.moveToFirst()) {
            tempList = new ArrayList<ThemePreViewInfo>();
            while (!mCursor.isAfterLast()) {
                ThemePreViewInfo tpvi = fillInfo(mCursor);
                tempList.add(tpvi);
                mCursor.moveToNext();
            }
        }
        mCursor.close();
        return tempList;
    }

    public ThemePreViewInfo queryThemeConstantPreView(String id) {

        ThemePreViewInfo themePreviewInfo = null;

        mCursor = mSqLiteDatabase.rawQuery("select * from " + ThemeSQLiteOpenHelper.TABLE_CONSTANT + " where "
                + ThemeSQLiteOpenHelper.THEME_ID + " = " + id + " and " + ThemeSQLiteOpenHelper.THEME_LOCALE + " = "
                + Utils.getLocaleCode(Locale.getDefault()), null);

        if (!mCursor.moveToFirst()) {
            mCursor = mSqLiteDatabase.rawQuery("select * from " + ThemeSQLiteOpenHelper.TABLE_CONSTANT + " where "
                    + ThemeSQLiteOpenHelper.THEME_ID + " = " + id + " and " + ThemeSQLiteOpenHelper.THEME_LOCALE
                    + " = " + Utils.getLocaleCode(Locale.ENGLISH), null);
        }

        if (!mCursor.moveToFirst()) {
            mCursor = mSqLiteDatabase.rawQuery("select * from " + ThemeSQLiteOpenHelper.TABLE_CONSTANT + " where "
                    + ThemeSQLiteOpenHelper.THEME_ID + " = " + id, null);
        }

        if (mCursor.moveToFirst()) {
            themePreviewInfo = fillInfo(mCursor);
        }
        mCursor.close();
        return themePreviewInfo;
    }

    public ThemePreViewInfo queryThemeConstantPreView(long id, int locale) {

        mCursor = mSqLiteDatabase.rawQuery("select * from " + ThemeSQLiteOpenHelper.TABLE_CONSTANT + " where "
                + ThemeSQLiteOpenHelper.THEME_ID + " = " + id + " and " + ThemeSQLiteOpenHelper.THEME_LOCALE + " = "
                + locale, null);
        if (mCursor.moveToFirst()) {
            return fillInfo(mCursor);
        }
        mCursor.close();
        return null;
    }

}