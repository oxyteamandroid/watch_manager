/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.theme;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;

/**
 * 该类是主题基本信息的封装
 * 
 * @author yanbo.jiang
 */
public class ThemePreViewInfo {
    /**
     * 主题ID
     */
    public long id;

    /**
     * 主题名字
     */
    public String name;
    /**
     * 主题封面
     */
    public String icon;

    /**
     * 主题作者
     */
    public String dev;
    /**
     * 主题添加时间
     */
    public long addtime;
    /**
     * 主题描述
     */
    public String description;

    /**
     * 主题预览图（多张）
     */
    public List<String> images = new ArrayList<String>();

    /**
     * 主题文件大小]
     */
    public long size;
    /**
     * 主题已下载文件大小
     */
    public long downloadSize;
    /**
     * 主题文件状态
     */
    public int downloadStatus;

    /**
     * 主题文件下载地址
     */
    public String addr;
    /**
     * 主题MD5值
     */
    public String md5file;
    /**
     * 主题语言
     */
    public int locale;
    /**
     * 下载主题时，notification用的icon
     */
    public Bitmap pic;

    /**
     * 下载主题进度
     */
    public int progress;

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("id:");
        stringBuilder.append(id + "\n");
        stringBuilder.append("name:");
        stringBuilder.append(name + "\n");
        stringBuilder.append("icon:");
        stringBuilder.append(icon + "\n");
        stringBuilder.append("dev:");
        stringBuilder.append(dev + "\n");
        stringBuilder.append("addtime:");
        stringBuilder.append(addtime + "\n");
        stringBuilder.append("description:");
        stringBuilder.append(description + "\n");
        stringBuilder.append("images:");
        stringBuilder.append(images + "\n");
        stringBuilder.append("pic:");
        stringBuilder.append("downloadSize:");
        stringBuilder.append(downloadSize + "\n");
        stringBuilder.append("fileStatus:");
        stringBuilder.append(downloadStatus + "\n");
        stringBuilder.append("addr:");
        stringBuilder.append(addr + "\n");
        stringBuilder.append("md5file:");
        stringBuilder.append(md5file + "\n");
        stringBuilder.append("locale:");
        stringBuilder.append(locale + "\n");
        return stringBuilder.toString();
    }

    public long getDownloadSize() {
        return downloadSize;
    }

    public void setDownloadSize(long downloadSize) {
        this.downloadSize = downloadSize;
    }

    public int getDownloadStatus() {
        return downloadStatus;
    }

    public void setDownloadStatus(int downloadStatus) {
        this.downloadStatus = downloadStatus;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getMd5file() {
        return md5file;
    }

    public void setMd5file(String md5file) {
        this.md5file = md5file;
    }

    public int getLocale() {
        return locale;
    }

    public void setLocale(int locale) {
        this.locale = locale;
    }

    public ThemePreViewInfo() {
        super();
    }

    public ThemePreViewInfo(Long id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public List<String> getImages() {
        return images;
    }

    public String getImagesStr() {
        String temp = "";
        if (null != getImages()) {
            for (String s : getImages()) {
                temp += s + ",";
            }
        }
        return temp;
    }

    public void setImages(List<String> images) {
        this.images.clear();
        this.images.addAll(images);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDev() {
        return dev;
    }

    public void setDev(String dev) {
        this.dev = dev;
    }

    public long getAddtime() {
        return addtime;
    }

    public void setAddtime(long addtime) {
        this.addtime = addtime;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }
}
