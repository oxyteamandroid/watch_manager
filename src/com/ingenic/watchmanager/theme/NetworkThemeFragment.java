/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.theme;

/**
 * 该类是主题主界面
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.theme.ThemeDbOperation.DOWNLOAD_STATUS;
import com.ingenic.watchmanager.util.NetworkOperator;
import com.ingenic.watchmanager.util.ProgressUtils;
import com.ingenic.watchmanager.util.Request;
import com.ingenic.watchmanager.util.StringRequest;
import com.ingenic.watchmanager.util.TUtils;
import com.ingenic.watchmanager.util.Utils;

public class NetworkThemeFragment extends Fragment implements ProgressUtils.Listener {

    private static final String TAG = NetworkThemeFragment.class.getSimpleName();
    private NetworkOperator mNetworkOprator;
    private ThemeDbOperation mThemeDbOperation;

    private List<ThemePreViewInfo> netThemeLists = new ArrayList<ThemePreViewInfo>();

    private final String THEME_UUID = "46AEE255-CB73-BF17-B60E-208B2C7E";
    private final String PREFIX = "/v1/theme/themelist/?uuid=";
    private final String SUFFIX = "&locale=";
    private final String IP_FIRST = PREFIX + THEME_UUID + SUFFIX;

    private GridView mNetworkGridView = null;
    private NetworkGridViewAdapter mNetworkAdapter = null;

    private int mScreenWidth;
    private int mScreenHeight;

    public int mLocaleCode;

    private boolean mIsEnable;

    private ProgressUtils mProgressUtils;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_theme_network, container, false);
        mNetworkOprator = new NetworkOperator(getActivity().getApplicationContext());
        getScreenWidthHeight();
        mThemeDbOperation = new ThemeDbOperation(getActivity());
        mLocaleCode = Utils.getLocaleCode(Locale.getDefault());

        mNetworkGridView = (GridView) rootView.findViewById(R.id.networkGrid);
        mNetworkAdapter = new NetworkGridViewAdapter(mNetworkGridView.getContext(), mScreenWidth, mScreenHeight);
        mNetworkAdapter.setThemeList(netThemeLists);
        mNetworkGridView.setAdapter(mNetworkAdapter);

        mNetworkGridView.setOnItemClickListener(mNetworkClickListener);

        mProgressUtils = new ProgressUtils(getActivity(), rootView, this);

        refresh();

        return rootView;
    }

    @SuppressLint("UseSparseArrays")
    private Map<Long, ThemePreViewInfo> mRepository = new HashMap<Long, ThemePreViewInfo>();

    @Override
    public void onStart() {
        super.onStart();
        IwdsLog.d(this, "onStart");
        IntentFilter filter = new IntentFilter(ThemePreviewActivity.THEME_DATA_CHANGE);
        getActivity().registerReceiver(mReceiver, filter);
        refresh();
    }

    @Override
    public void onPause() {
        super.onPause();
        IwdsLog.d(this, "onPause");
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            IwdsLog.d(NetworkThemeFragment.this, "receive broadcast: " + intent.getAction());
            refresh();
        }
    };

    @Override
    public void onStop() {
        super.onStop();
        IwdsLog.d(this, "onStop");
        getActivity().unregisterReceiver(mReceiver);
    }

    private void refresh() {
        List<ThemePreViewInfo> TPVILists = mThemeDbOperation.queryThemeStatus();
        mRepository.clear();
        if (null != TPVILists) {
            for (ThemePreViewInfo info : TPVILists) {
                mRepository.put(info.getId(), info);
            }
        }
        for (ThemePreViewInfo info : netThemeLists) {
            if (mRepository.containsKey(info.getId()))
                info.downloadStatus = mRepository.get(info.getId()).getDownloadStatus();
            else
                info.downloadStatus = DOWNLOAD_STATUS.NO_DOWNLOAD.ordinal();
        }
        mNetworkAdapter.notifyDataSetChanged();
    }

    private void getScreenWidthHeight() {
        DisplayMetrics metric = new DisplayMetrics();
        this.getActivity().getWindowManager().getDefaultDisplay().getMetrics(metric);
        mScreenWidth = metric.widthPixels; // 屏幕宽度（像素）
        mScreenHeight = metric.heightPixels; // 屏幕高度（像素）
    }

    private List<ThemePreViewInfo> analysisOfJSON(String json, Locale locale) {
        List<ThemePreViewInfo> lists = new ArrayList<ThemePreViewInfo>();
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = jsonObject.getJSONArray("list");
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                ThemePreViewInfo info = new ThemePreViewInfo();
                info.setId(object.getInt("id"));
                info.setIcon(object.getString("icon"));
                info.setAddtime(object.getLong("addtime"));
                info.setName(object.getString("name"));
                info.setDev(object.getString("dev"));
                info.setDescription(object.getString("description"));
                info.setLocale(mLocaleCode);
                lists.add(info);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            lists = null;
        }
        return lists;
    }

    @Override
    public void request() {
        if(!mIsEnable){
            return;
        }
        mProgressUtils.start();
        if( !mProgressUtils.checkNetwork()){
            mProgressUtils.fail();
            return;
        }

        String url = StringRequest.MARKET_IP + IP_FIRST + mLocaleCode;
        IwdsLog.d(TAG, "net fragment url = " + url);
        StringRequest request = new StringRequest(getActivity(), url, new Request.Listener<String>() {

            @Override
            public void onResponse(String response) {
                List<ThemePreViewInfo> themeInfo = analysisOfJSON(response, Locale.getDefault());
                if (null != themeInfo && themeInfo.size() > 0) {
                    netThemeLists.clear();
                    if (themeInfo != null && netThemeLists.size() <= 0) {
                        for (ThemePreViewInfo tempInfo : themeInfo) {

                            ThemePreViewInfo temp = mThemeDbOperation.queryThemeConstantPreView(tempInfo.getId(),
                                    mLocaleCode);
                            if (null != temp) {
                                netThemeLists.add(temp);
                            } else {
                                netThemeLists.add(tempInfo);
                                tempInfo.setLocale(mLocaleCode);
                                mThemeDbOperation.insertThemeConstant(tempInfo);
                            }
                        }
                    }
                    mNetworkAdapter.notifyDataSetChanged();
                    mProgressUtils.end();
                } else {
                    TUtils.showToast(getActivity(), R.string.network_error);
                    mProgressUtils.fail();
                }
            }
        }, new Request.ErrorListener() {

            @Override
            public void onErrorResponse(Exception ex) {
                mProgressUtils.fail();
                ex.printStackTrace();
            }
        });
        request.start();
    }

    private OnItemClickListener mNetworkClickListener = new OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            if (!mNetworkOprator.checkNetwork()) {
                Toast.makeText(getActivity(), R.string.nt_isopen, Toast.LENGTH_SHORT).show();
                return;
            }
            mNetworkGridView.setOnItemClickListener(null);

            Intent intent = new Intent(getActivity(), ThemePreviewActivity.class);
            intent.putExtra("id", netThemeLists.get(position).getId());
            intent.putExtra("pic", compressNotificationLargeIcon(netThemeLists.get(position).pic));
            startActivity(intent);
        }
    };

    private Bitmap compressNotificationLargeIcon(Bitmap mNotificationIcon) {
        if (null == mNotificationIcon) return null;
        mNotificationIcon = Utils.compressBitmap(mNotificationIcon, 3);
        mNotificationIcon = Utils.resizeBitmap(mNotificationIcon, 0.5f, 0.5f);
        return mNotificationIcon;
    }

    @Override
    public void onResume() {
        super.onResume();
        IwdsLog.d(this, "onResume");
        mNetworkGridView.setOnItemClickListener(mNetworkClickListener);
    }

    public void setRequest() {
        if (mIsEnable)
            return;
        IwdsLog.d(TAG, "setRequest true");
        mIsEnable = true;
        request();
    }
}
