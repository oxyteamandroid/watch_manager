/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.theme;

/**
 * 该类是主题主界面
 */
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.os.Build.VERSION;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ingenic.watchmanager.R;

public class ThemeFragment extends android.app.Fragment {

	private List<Fragment> mViews;
	private TextView localTvw, networkTvw;
	private ViewPager mViewPager;
	private ThemePageIndicator mThemePageIndicator;
	private FragmentPagerAdapter mAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_theme_main, container, false);
		mViewPager = (ViewPager) rootView.findViewById(R.id.id_viewpager);
		mThemePageIndicator = (ThemePageIndicator) rootView.findViewById(R.id.theme_indicator);
		localTvw = (TextView) rootView.findViewById(R.id.localTvw);
		networkTvw = (TextView) rootView.findViewById(R.id.networkTvw);
		localTvw.setOnClickListener(mTabListener);
		networkTvw.setOnClickListener(mTabListener);
		initView();
		mViewPager.setAdapter(mAdapter);
		mThemePageIndicator.setViewPager(mViewPager);
		mThemePageIndicator.setFades(false);

		mThemePageIndicator.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				// 设置初始状态
				localTvw.setTextColor(getActivity().getResources().getColor(R.color.normalTvw));
				networkTvw.setTextColor(getActivity().getResources().getColor(R.color.normalTvw));
				switch (position) {
				case 0:
					// 本地字体颜色改变
					localTvw.setTextColor(getActivity().getResources().getColor(R.color.pressTvw));
					break;
				case 1:
					// 网络字体颜色改变
					networkTvw.setTextColor(getActivity().getResources().getColor(R.color.pressTvw));
					mNetFragment.setRequest();
					break;
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		return rootView;
	}

	/**
	 * 点击TextView切换界面,local 与 network
	 */
	private OnClickListener mTabListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.localTvw:
				mViewPager.setCurrentItem(0);
				break;
			case R.id.networkTvw:
				mViewPager.setCurrentItem(1);
				break;
			default:
				break;
			}
		}
	};

	private LocalThemeFragment mLocalFragment;
	private NetworkThemeFragment mNetFragment;

	private void initView() {
		mViews = new ArrayList<Fragment>();

		mLocalFragment = new LocalThemeFragment();
		mNetFragment = new NetworkThemeFragment();

		mViews.add(mLocalFragment);
		mViews.add(mNetFragment);
		FragmentManager fm = getFragmentManagerByVersion();
		mAdapter = new FragmentPagerAdapter(fm) {

			@Override
			public android.app.Fragment getItem(int arg0) {
				return mViews.get(arg0);
			}

			@Override
			public int getCount() {
				return mViews.size();
			}
		};
	}

	@SuppressLint("NewApi")
	public FragmentManager getFragmentManagerByVersion() {
		if (VERSION.SDK_INT >= 17)
			return getChildFragmentManager();
		return getFragmentManager();
	}

}
