/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.theme;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactorCallback;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.account.MD5Utils;
import com.ingenic.watchmanager.downloader.Downloader;
import com.ingenic.watchmanager.downloader.Downloader.DownloadListener;
import com.ingenic.watchmanager.theme.ThemeDbOperation.DOWNLOAD_STATUS;
import com.ingenic.watchmanager.util.ImageRequest;
import com.ingenic.watchmanager.util.ProgressUtils;
import com.ingenic.watchmanager.util.Request.ErrorListener;
import com.ingenic.watchmanager.util.Request.Listener;
import com.ingenic.watchmanager.util.RequestQueue;
import com.ingenic.watchmanager.util.SimpleFileTransactionModelCallbackImpl;
import com.ingenic.watchmanager.util.StringRequest;
import com.ingenic.watchmanager.util.Utils;

@SuppressLint("UseSparseArrays")
public class ThemePreviewActivity extends Activity implements ProgressUtils.Listener, DownloadListener {

    public static final int MAX_PROGRESS_100 = 100;
    public static final String THEME_DATA_CHANGE = "com.ingenic.theme.data.change";

    private static final Map<Long, ThemePreViewInfo> REPOSITORY = new HashMap<Long, ThemePreViewInfo>();
    private static final String TAG = ThemePreviewActivity.class.getSimpleName();

    private static final String PREFIX = "/v1/theme/detail?id=";
    private static final String SUFFIX = "&locale=";

    private static final String FILE_PREFIX = "/v1/theme/down/?id=";
    private static final String FILE_SUFFIX = "&account=guest&psd=";

    private static final String FIRST_PATH = "watchmanager";
    private static final String SECOND_PATH = "theme";

    private TextView themeName, themeInfo, sizeAuthor;

    private ProgressBar mProgressBar;
    private LinearLayout mImageLayout;

    private int mLocale;
    private ProgressUtils mRequestProgressView;
    private boolean mIsLocal;
    private AlertDialog mRemoveDialog;
    private Bitmap mNotificationIcon;

    private long mLastSend = 0L;
    private String mDownloadSuccess;

    private ThemePreViewInfo mThemeInfo;
    private ThemeDbOperation mThemeDbOperator;

    private long mId;

    private TextView mDownLoadButton;
    /**
     * 请求队列
     */
    private RequestQueue mRequestQueue = new RequestQueue(1);
    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;

    private boolean mIsChannelAvailable;
    private ProgressDialog mThemeSendDialog;

    private StringRequest mRequest;

    private String mFilePath;
    /**
     * notification id
     */
    private int mNoticeId;
    private CustomDialogFragment mSendDialog;
    private CustomDialogFragment mChangeDialog;
    private Downloader mDownloader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.theme_preview);

        mThemeDbOperator = new ThemeDbOperation(this);
        mLocale = Utils.getLocaleCode(Locale.getDefault());

        ThemeFileSendModel.register(getInstanceModelListener(), getInstanceDataListener());

        Intent intent = getIntent();
        mId = intent.getLongExtra(LocalThemeFragment.ID, 0L);
        mNotificationIcon = intent.getParcelableExtra("pic");

        mNoticeId = (int) mId;
        mIsLocal = intent.getBooleanExtra(LocalThemeFragment.IS_LOCAL, false);

        mFilePath = getFilePath(mId);

        initViews();

        mThemeInfo = mThemeDbOperator.queryThemeConstantPreView("" + mId);

        if (!mIsLocal) { // 显示网络theme详情
            mRequestProgressView = new ProgressUtils(this);
            mRequestProgressView.start();
            findViewById(R.id.frameProgress).setVisibility(View.INVISIBLE);
        } else { // 显示本地theme详情
            commonOperate();
            setLocalButton();
        }
        IwdsLog.d(TAG, "theme preview oncreate!");
    }

    /**
     * 设置本地按钮 删除和应用到手表(显示与事件之类)
     */
    private void setLocalButton() {
        findViewById(R.id.frameProgress).setVisibility(View.GONE);
        findViewById(R.id.localButtonLayout).setVisibility(View.VISIBLE);
        View remove = findViewById(R.id.remove);
        View push = findViewById(R.id.push);
        remove.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mRemoveDialog = new AlertDialog.Builder(ThemePreviewActivity.this).setTitle(R.string.to_delete).setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteInfoAndFile();
                    }
                }).setNegativeButton(android.R.string.no, null).create();
                mRemoveDialog.show();
            }
        });
        push.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                pushToWatch();
            }
        });
    }

    private DataTransactorCallback getInstanceDataListener() {
        return new DataTransactorCallback() {

            @Override
            public void onSendResult(DataTransactResult result) {
            }

            @Override
            public void onSendFileProgress(int progress) {
            }

            @Override
            public void onRecvFileProgress(int progress) {
            }

            @Override
            public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
            }

            @Override
            public void onDataArrived(Object object) {
                hideChangeDialog();
                refreshUI();
            }

            @Override
            public void onChannelAvailable(boolean isAvailable) {
                IwdsLog.d(ThemePreviewActivity.this, "Data --> onChannelAvailable: isAvailable --> " + isAvailable);
                if (!isAvailable) {
                    hideChangeDialog();
                }
            }

            @Override
            public void onSendFileInterrupted(int index) {

            }

            @Override
            public void onRecvFileInterrupted(int index) {

            }
        };
    }

    @Override
    public void request() {

        String url = StringRequest.MARKET_IP + PREFIX + mId + SUFFIX + mLocale;
        IwdsLog.d(TAG, "thmeme detail url: " + url);
        StringRequest request = new StringRequest(this, url, new Listener<String>() {

            @Override
            public void onResponse(String response) {

                ThemePreViewInfo tempInfo = getThemePreViewInfo(response);

                if (mThemeInfo != null) {
                    tempInfo.setDownloadStatus(mThemeInfo.getDownloadStatus());
                    tempInfo.setDownloadSize(mThemeInfo.getDownloadSize());
                    tempInfo.setProgress(mThemeInfo.getProgress());
                }

                mThemeInfo = tempInfo;

                mThemeInfo.setLocale(mLocale);
                mThemeDbOperator.updateThemeConstantRecord(mThemeInfo);
                mRequestProgressView.end();
                findViewById(R.id.frameProgress).setVisibility(View.VISIBLE);

                IwdsLog.d(this, "request.end ---> Download Status: " + ThemeDbOperation.dic.get(mThemeInfo.getDownloadStatus()) + " == " + mThemeInfo.getDownloadStatus());

                commonOperate();
            }

        }, new ErrorListener() {

            @Override
            public void onErrorResponse(Exception ex) {
                mRequestProgressView.fail();
            }

        });
        request.start();
    }

    private SimpleFileTransactionModelCallbackImpl getInstanceModelListener() {
        return new SimpleFileTransactionModelCallbackImpl() {
            @Override
            public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
            }

            @Override
            public void onSendResult(DataTransactResult result) {

                if (result.getResultCode() == DataTransactResult.RESULT_OK) {
                    IwdsLog.d(ThemePreviewActivity.this, "send data success！");
                    mDownLoadButton.setEnabled(false);
                } else {
                    Toast.makeText(ThemePreviewActivity.this, getString(R.string.transmit_failse), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onChannelAvailable(boolean isAvailable) {
                IwdsLog.d(ThemePreviewActivity.this, "File --> onChannelAvailable: isAvailable --> " + isAvailable);
                mIsChannelAvailable = isAvailable;
                if (!isAvailable) {
                    hideSendDialog();
                }
            }

            @Override
            public void onSendFileProgress(int progress) {
                if (null == mThemeSendDialog)
                    createSendDialog();

                mThemeSendDialog.setProgress(progress);
                IwdsLog.d(ThemePreviewActivity.this, "themeSendDialog->progress: " + progress);
                if (progress >= MAX_PROGRESS_100) {
                    mDownLoadButton.setEnabled(false);
                    saveThemePreViewInfo(mThemeInfo);

                    hideSendDialog();
                    createChangThemeDialog();
                }
            }
        };
    }

    private void loadImages(List<String> images) {
        for (String s : images) {

            final String url = s;
            if (null == url || url.length() == 0) {
                return;
            }

            final ImageView child = new ImageView(this);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            // child.setImageResource(R.drawable.heart_icon);
            lp.leftMargin = lp.rightMargin = lp.topMargin = lp.bottomMargin = 20;
            mImageLayout.addView(child, lp);
            IwdsLog.d(this, "image url: " + url);
            ImageRequest request = new ImageRequest(this, url, new Listener<Bitmap>() {

                @Override
                public void onResponse(Bitmap response) {
                    child.setImageBitmap(response);
                }
            }, new ErrorListener() {

                @Override
                public void onErrorResponse(Exception ex) {
                    ex.printStackTrace();
                }
            }, true);
            mRequestQueue.add(request);
        }
    }

    private void initViews() {
        themeName = (TextView) findViewById(R.id.theme_name_tvw);
        themeInfo = (TextView) findViewById(R.id.theme_info_tvw);
        mImageLayout = (LinearLayout) findViewById(R.id.image_layout);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        sizeAuthor = (TextView) findViewById(R.id.local_size_author);
        Typeface themeTypeFace = Typeface.createFromAsset(this.getAssets(), "fonts/watch_font.ttf");
        themeName.setTypeface(themeTypeFace);
        mDownLoadButton = (TextView) findViewById(R.id.downLoadButton);
    }

    /**
     * float保留2位小数
     * 
     * @param f
     * @return
     */
    private float retain(float f) {
        BigDecimal b = new BigDecimal(f);
        return b.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
    }

    private void setViews() {
        themeName.setText(mThemeInfo.getName());
        themeInfo.setText(mThemeInfo.getDescription());
        sizeAuthor.setText(retain((float) mThemeInfo.getSize() / 1024 / 1024) + "M" + " | " + mThemeInfo.getDev());

        mDownLoadButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                IwdsLog.v(ThemePreviewActivity.this, "click download button --> down status: " + mThemeInfo.downloadStatus + ":" + ThemeDbOperation.dic.get(mThemeInfo.downloadStatus));

                switch (ThemeDbOperation.dic.get(mThemeInfo.downloadStatus)) {
                case DOWNLOADING:
                case NO_DOWNLOAD_GET_ADDR:
                    pauseDownloadTheme();
                    break;
                case PAUSE:
                case NO_DOWNLOAD:
                case NETWORK_ERROR:
                case NO_DOWNLOAD_GET_ADDR_FAILSE:
                    if (null == mThemeInfo.addr) {
                        downloadTheme();
                    } else {
                        startDownloadFile();
                    }
                    break;
                case TO_WATCH:
                    pushToWatch();
                    break;
                default:
                    break;
                }
                refreshUI();
            }
        });
        // LongClickClearData();
    }

    // debug 使用
    @SuppressWarnings("unused")
    private void LongClickClearData() {
        mDownLoadButton.setOnLongClickListener(new OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(ThemePreviewActivity.this, "delete file！", Toast.LENGTH_SHORT).show();
                deleteInfoAndFile();
                return true;
            }
        });
    }

    private void pushToWatch() {
        if (null != mNotifyManager)
            mNotifyManager.cancel(mNoticeId);
        transferFile(mFilePath);
    }

    private void showToast(String content) {
        Toast.makeText(this, content, Toast.LENGTH_SHORT).show();
        IwdsLog.d(this, content);
    }

    private void transferFile(String path) {

        if (null == path) {
            showToast(getString(R.string.path_is_null));
            return;
        }
        File mFile = new File(path);
        if (!mFile.exists()) {
            mDownLoadButton.setClickable(true);
            showToast(getString(R.string.file_noexist));
            return;
        }
        if (!mIsChannelAvailable) {
            mDownLoadButton.setClickable(true);
            showToast(getString(R.string.channel_unavailable));
            return;
        }

        sendFileAndShowDialog(path);

    }

    public boolean checkLink() {
        if (!mIsChannelAvailable) {
            Toast.makeText(this, this.getString(R.string.check_watch_link), Toast.LENGTH_SHORT).show();
        }
        return mIsChannelAvailable;
    }

    private void sendFileAndShowDialog(String path) {

        if (!checkLink())
            return;

        createSendDialog();

        try {
            ThemeFileSendModel.requestSendFile(path);
            IwdsLog.d(TAG, "request change theme!");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            hideSendDialog();
        }
    }

    private void createChangThemeDialog() {
        hideChangeDialog();
        IwdsLog.d(TAG, "create change dialog!");
        mChangeDialog = new CustomDialogFragment();
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.themeSwitch));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        mChangeDialog.setDialog(progressDialog);
        mChangeDialog.setCancelable(false);
        mChangeDialog.show(getFragmentManager(), "changeDialog");
    }

    private void hideChangeDialog() {
        if (null != mChangeDialog) {
            mChangeDialog.dismissAllowingStateLoss();
            IwdsLog.d(TAG, "hide change dialog");
        }
    }

    private void hideSendDialog() {
        if (null != mSendDialog) {
            mSendDialog.dismissAllowingStateLoss();
            mSendDialog = null;
            IwdsLog.d(TAG, "hide send dialog");
        }
    }

    private void createSendDialog() {
        hideSendDialog();
        mSendDialog = new CustomDialogFragment();
        mThemeSendDialog = new ProgressDialog(this);
        /* mThemeSendDialog.setIcon(R.drawable.sending); */
        mThemeSendDialog.setMax(MAX_PROGRESS_100);
        mThemeSendDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mThemeSendDialog.setTitle("  ");
        mThemeSendDialog.setCancelable(false);
        mThemeSendDialog.incrementProgressBy(-mThemeSendDialog.getProgress());
        mSendDialog.setDialog(mThemeSendDialog);
        mSendDialog.setCancelable(false);
        mSendDialog.show(getFragmentManager(), "sendDialog");
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshUI();
        if (ThemeFileSendModel.sIsShowChangeDialog) {
            createChangThemeDialog();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ThemeFileSendModel.unregister();
        IwdsLog.d(TAG, "ondestroy and unregister!6");
    }

    private void refreshUI() {

        if (null == mThemeInfo)
            return;
        IwdsLog.d(this, "refreshUI ---> downlaodSize: " + mThemeInfo.getDownloadSize() + " fileSize: " + mThemeInfo.getSize() + ":" + ThemeDbOperation.dic.get(mThemeInfo.getDownloadStatus()) + " getProgress() = " + mThemeInfo.getProgress());

        String text = "";
        int tempStatus = mThemeInfo.getDownloadStatus();
        DOWNLOAD_STATUS downlaodStatus = ThemeDbOperation.dic.get(tempStatus);
        switch (downlaodStatus) {
        case NO_DOWNLOAD:
            text = getString(R.string.download);
            break;

        case NETWORK_ERROR:
        case NO_DOWNLOAD_GET_ADDR_FAILSE:
            text = getString(R.string.download_error);
            break;

        case PAUSE:
            text = getString(R.string.continue_);
            break;

        case TO_WATCH:
            text = getString(R.string.appliy_theme);
            break;

        case NO_DOWNLOAD_GET_ADDR:
        case DOWNLOADING:
            text = mThemeInfo.getProgress() + "%";
            break;
        default:
            IwdsLog.v(this, "refreshUI --> default --> download status: " + downlaodStatus);
            return;
        }

        IwdsLog.v(this, "refreshUI --> download text: " + downlaodStatus + " : " + text);

        IwdsLog.d(this, "getProgress: " + mThemeInfo.getProgress());
        mProgressBar.setProgress(mThemeInfo.getProgress());
        mDownLoadButton.setText(text);
    }

    private void pauseDownloadTheme() {
        if (null != mRequest) {
            mRequest.cancel();
        }
        IwdsLog.d(TAG, "theme downloader --> pause!!!");
        if (null != mDownloader)
            mDownloader.pause();
        REPOSITORY.remove(mId);

        if (null != mNotifyManager)
            mNotifyManager.cancel(mNoticeId);

        mThemeInfo.setDownloadStatus(ThemeDbOperation.DOWNLOAD_STATUS.PAUSE.ordinal());
        saveThemePreViewInfo(mThemeInfo);
    }

    private void downloadTheme() {
        mThemeInfo.setDownloadStatus(ThemeDbOperation.DOWNLOAD_STATUS.NO_DOWNLOAD_GET_ADDR.ordinal());
        saveThemePreViewInfo(mThemeInfo);

        String url = StringRequest.MARKET_IP + FILE_PREFIX + mThemeInfo.getId() + FILE_SUFFIX + MD5Utils.encrypt("123456");
        sendNotification();
        IwdsLog.d(TAG, "theme get addr url: " + url);
        mRequest = new StringRequest(this, url, new Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    mThemeInfo.setAddr(json.getString("addr"));
                    mThemeInfo.setSize(json.getLong("size"));
                    mThemeInfo.setMd5file(json.getString("md5file"));
                    startDownloadFile();
                } catch (Exception e) {
                    setGetDownloadAddrError(e);
                }
            }
        }, new ErrorListener() {

            @Override
            public void onErrorResponse(Exception ex) {
                setGetDownloadAddrError(ex);
            }

        });
        mRequest.start();
        IwdsLog.d(ThemePreviewActivity.this, "request download addr --> url: " + url);
    }

    private void setGetDownloadAddrError(Exception ex) {
        ex.printStackTrace();
        mThemeInfo.setDownloadStatus(ThemeDbOperation.DOWNLOAD_STATUS.NO_DOWNLOAD_GET_ADDR_FAILSE.ordinal());
        saveThemePreViewInfo(mThemeInfo);
        refreshUI();
        IwdsLog.d(ThemePreviewActivity.this, "request download addr failse--> ex: " + ex.toString());
    }

    private void startDownloadFile() {

        saveThemePreViewInfo(mThemeInfo);

        IwdsLog.d(ThemePreviewActivity.this, "launch downloader start --> down Addr: " + mThemeInfo.addr);
        mDownloader = Downloader.getOrNewDownloader(mFilePath, mThemeInfo.addr, 1, this);
        mDownloader.start();

        mThemeInfo.setDownloadStatus(ThemeDbOperation.DOWNLOAD_STATUS.DOWNLOADING.ordinal());
        IwdsLog.d(TAG, "theme downloader --> restart");
        REPOSITORY.put(mId, mThemeInfo);
    }

    private void saveThemePreViewInfo(ThemePreViewInfo info) {

        mThemeDbOperator.updateThemeDownloadStatus(info);
        sendBroadcast(new Intent(THEME_DATA_CHANGE));
    }

    public static String getFilePath(long id) {

        return Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + FIRST_PATH + File.separator + SECOND_PATH + File.separator + id + ".zip";
    }

    private ThemePreViewInfo getThemePreViewInfo(String json) {

        ThemePreViewInfo themePreViewInfo = new ThemePreViewInfo();
        try {
            JSONObject object = new JSONObject(json);
            themePreViewInfo.setId(object.getLong("id"));
            themePreViewInfo.setName(object.getString("name"));
            themePreViewInfo.setDescription(object.getString("description"));
            themePreViewInfo.setDev(object.getString("dev"));
            // themePreViewInfo.setSize(object.getLong("size"));
            themePreViewInfo.setAddtime(object.getLong("addtime"));
            themePreViewInfo.setLocale(mLocale);
            themePreViewInfo.setImages(Arrays.asList((object.getString("images").split(","))));
            IwdsLog.d(this, "imagesUrl: " + object.getString("images"));
        } catch (Exception e) {
            themePreViewInfo = null;
        }
        return themePreViewInfo;
    }

    private void sendNotification() {
        initBuilder();
        mBuilder.setProgress(MAX_PROGRESS_100, 0, true);
        Notification build = mBuilder.build();
        build.icon = R.drawable.sending;
        build.tickerText = mThemeInfo.getName();
        mNotifyManager.notify(mNoticeId, build);
    }

    private void initBuilder() {

        if (null == mNotifyManager) {
            mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mBuilder = new NotificationCompat.Builder(getApplicationContext());
            if (null != mNotificationIcon)
                mBuilder.setLargeIcon(mNotificationIcon);

            mBuilder.setContentTitle(mThemeInfo.getName());
            mBuilder.setContentText(mThemeInfo.getDescription());
            // mBuilder.setSmallIcon(R.drawable.sending);
            mBuilder.setWhen(System.currentTimeMillis());
            PendingIntent contentIntent = PendingIntent.getActivity(this, mNoticeId, getIntent(), 0);
            mBuilder.setContentIntent(contentIntent);
            mBuilder.setProgress(MAX_PROGRESS_100, 0, true);
        }
    }

    @Override
    public void onUpdateDownloadProgress(int progress, long byteCount, long fileSize) {
        mThemeInfo.setDownloadSize(byteCount);
        mThemeInfo.setSize(fileSize);
        mThemeInfo.setProgress(progress);
        refreshUI();
        if (0 == progress % 10) {
            IwdsLog.d(ThemePreviewActivity.this, "downloader progress--> progress: " + progress);
        }
        initBuilder();
        updateNotification(progress);
    }

    private void updateNotification(int progress) {
        long tempTime = System.currentTimeMillis();
        if ((tempTime - mLastSend) < 500 && 100 != progress)
            return;

        mLastSend = tempTime;
        mBuilder.setProgress(MAX_PROGRESS_100, progress, false);
        Notification build = mBuilder.build();
        build.icon = R.drawable.sending;
        if (100 == progress)
            build.flags |= Notification.FLAG_AUTO_CANCEL;
        mNotifyManager.notify(mNoticeId, build);
    }

    @Override
    public void onDownloadFinish(boolean isSuccess) {
        if (!isSuccess)
            return;
        File file = new File(mFilePath);
        String md5Str = Utils.getFileMD5(file);

        if (md5Str == null || (null != mThemeInfo.getMd5file() && !md5Str.equals(mThemeInfo.getMd5file()))) {
            if (file.exists())
                file.delete();
            mThemeInfo.setDownloadStatus(ThemeDbOperation.DOWNLOAD_STATUS.NETWORK_ERROR.ordinal());
            saveThemePreViewInfo(mThemeInfo);
            IwdsLog.d(ThemePreviewActivity.this, "downloader down md5check error --> md5: " + mThemeInfo.getMd5file() + " : " + md5Str + " : " + md5Str.length());
            return;
        }

        IwdsLog.d(ThemePreviewActivity.this, "downloader down success--> fileName: " + mThemeInfo.getName());

        mThemeInfo.setDownloadSize(mThemeInfo.getSize());

        mThemeInfo.setDownloadStatus(ThemeDbOperation.DOWNLOAD_STATUS.TO_WATCH.ordinal());
        saveThemePreViewInfo(mThemeInfo);
        refreshUI();

        REPOSITORY.remove(mId);

        sendDownloadSuccessNotification();
    }

    private void sendDownloadSuccessNotification() {
        Builder Builder = new NotificationCompat.Builder(getApplicationContext());
        if (null != mNotificationIcon)
            Builder.setLargeIcon(mNotificationIcon);

        Builder.setContentTitle(mThemeInfo.getName());
        mDownloadSuccess = getString(R.string.download_success);
        Builder.setContentText(mDownloadSuccess);
        Builder.setWhen(System.currentTimeMillis());
        PendingIntent contentIntent = PendingIntent.getActivity(this, mNoticeId, getIntent(), 0);
        Builder.setContentIntent(contentIntent);

        Notification build = Builder.build();
        build.icon = R.drawable.sending;
        build.tickerText = mDownloadSuccess;
        build.flags |= Notification.FLAG_AUTO_CANCEL;
        mNotifyManager.notify(mNoticeId, build);
    }

    @Override
    public void onDownloadError(Exception e) {
        e.printStackTrace();
        mThemeInfo.setDownloadStatus(ThemeDbOperation.DOWNLOAD_STATUS.NETWORK_ERROR.ordinal());
        saveThemePreViewInfo(mThemeInfo);
        refreshUI();
        IwdsLog.d(ThemePreviewActivity.this, "downloader down error--> cause: " + e.getMessage());

        REPOSITORY.remove(mId);
        if (null != mNotifyManager)
            mNotifyManager.cancel(mNoticeId);
    }

    @Override
    public void onPreDownload() {
    }

    @Override
    public void onStop(boolean isPauseOrRemove) {
    }

    private void commonOperate() {

        ThemePreViewInfo tempInfo = REPOSITORY.get(mId);
        if (null != tempInfo) {
            mThemeInfo.setDownloadStatus(tempInfo.getDownloadStatus());
            mThemeInfo.setDownloadSize(tempInfo.getDownloadSize());
        }
        setViews();
        loadImages(mThemeInfo.getImages());
        refreshUI();
        IwdsLog.d(this, "onCreate ---> Download Status: " + ThemeDbOperation.dic.get(mThemeInfo.getDownloadStatus()) + " == " + mThemeInfo.getDownloadStatus());
    }

    private void deleteInfoAndFile() {
        mThemeInfo.setDownloadStatus(DOWNLOAD_STATUS.NO_DOWNLOAD.ordinal());
        mThemeInfo.setDownloadSize(0);
        mThemeInfo.setProgress(0);
        mThemeDbOperator.updateThemeDownloadStatus(mThemeInfo);
        File file = new File(getFilePath(mId));
        if (file.exists())
            file.delete();
        sendBroadcast(new Intent(THEME_DATA_CHANGE));
        IwdsLog.d(ThemePreviewActivity.this, "send broadcast: " + THEME_DATA_CHANGE);
        finish();
    }
}
