/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.theme;

import java.io.FileNotFoundException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceClient.ConnectionCallbacks;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.datatransactor.DataTransactor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactorCallback;
import com.ingenic.iwds.datatransactor.FileTransactionModel;
import com.ingenic.iwds.uniconnect.ConnectionServiceManager;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.util.SimpleFileTransactionModelCallbackImpl;

public class ThemeFileSendModel {

    private static final String UUID = "a1dc19e2-17a4-0797-9362-68a0dd4bfb68";
    private static final String M_UUID = "635218be-c63a-11e4-af0f-000b2f579610";
    private static final String TAG = ThemeFileSendModel.class.getSimpleName();

    private static Context mContext;

    public static DataTransactor mTransactor;
    public static ServiceClient mServiceClient;
    public static FileTransactionModel mFileTransactionModel;;

    private static DataTransactorCallback instanceDataListener;
    private static SimpleFileTransactionModelCallbackImpl instanceModelListener;

    private static boolean sDataChanalIsAvailable;
    private static boolean sFileChanalIsAvailable;

    public static boolean sIsShowChangeDialog;

    public static void initSerivceAndModel(Context context) {
        if (null == mFileTransactionModel) {
            synchronized (ThemeFileSendModel.class) {
                if (null == mFileTransactionModel) {
                    mContext = context.getApplicationContext();
                    registerConnectedWatchBroadcast(context);
                }
            }
        }
    }

    private static void registerConnectedWatchBroadcast(Context context) {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectionServiceManager.ACTION_CONNECTED_ADDRESS);
        context.registerReceiver(new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                mServiceClient = new ServiceClient(context, ServiceManagerContext.SERVICE_CONNECTION, getServiceCallback());
                mServiceClient.connect();
                context.unregisterReceiver(this);
            }
        }, filter);
    }

    public static ConnectionCallbacks getServiceCallback() {
        return new ServiceClient.ConnectionCallbacks() {

            @Override
            public void onConnected(ServiceClient serviceClient) {

                ConnectionServiceManager manager = (ConnectionServiceManager) serviceClient.getServiceManagerContext();
                if (manager.getConnectedDeviceDescriptors().length != 0) {
                    if (null == mFileTransactionModel) {
                        mFileTransactionModel = new FileTransactionModel(mContext, getListener(), UUID);
                        mTransactor = new DataTransactor(mContext, getDataTransactorCallbackListener(), M_UUID);
                        // IwdsLog.d(TAG, "Service --> onece start ");
                    }
                    mFileTransactionModel.start();
                    mTransactor.start();
                    IwdsLog.d(TAG, "Service --> onConnected: to watch ");
                } else {
                    IwdsLog.d(TAG, "Service --> onConnected: no to watch ");
                }
            }

            @Override
            public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
                IwdsLog.d(TAG, "Service --> onDisconnected:  " + unexpected);
            }

            @Override
            public void onConnectFailed(ServiceClient serviceClient, ConnectFailedReason reason) {
                IwdsLog.d(TAG, "Service --> onDisconnected:  " + reason);
            }
        };
    }

    private static DataTransactorCallback getDataTransactorCallbackListener() {
        return new DataTransactorCallback() {

            @Override
            public void onSendResult(DataTransactResult result) {
                if (null != instanceDataListener)
                    instanceDataListener.onSendResult(result);
            }

            @Override
            public void onSendFileProgress(int progress) {
                if (null != instanceDataListener)
                    instanceDataListener.onSendFileProgress(progress);
            }

            @Override
            public void onRecvFileProgress(int progress) {
                if (null != instanceDataListener)
                    instanceDataListener.onRecvFileProgress(progress);
            }

            @Override
            public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
                if (null != instanceDataListener)
                    instanceDataListener.onLinkConnected(descriptor, isConnected);
            }

            @Override
            public void onDataArrived(Object object) {
                sIsShowChangeDialog = false;

                if (null != instanceDataListener)
                    instanceDataListener.onDataArrived(object);
            }

            @Override
            public void onChannelAvailable(boolean isAvailable) {
                sDataChanalIsAvailable = isAvailable;
                if (!isAvailable)
                    sIsShowChangeDialog = false;

                IwdsLog.d(TAG, "Service --> sDataChanalIsAvailable: " + isAvailable);
                if (null != instanceDataListener)
                    instanceDataListener.onChannelAvailable(isAvailable);
            }

            @Override
            public void onSendFileInterrupted(int index) {

            }

            @Override
            public void onRecvFileInterrupted(int index) {

            }
        };
    }

    private static SimpleFileTransactionModelCallbackImpl getListener() {
        return new SimpleFileTransactionModelCallbackImpl() {
            @Override
            public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
                if (null != instanceModelListener) {
                    instanceModelListener.onLinkConnected(descriptor, isConnected);
                }
            }

            @Override
            public void onSendResult(DataTransactResult result) {
                if (null != instanceModelListener) {
                    instanceModelListener.onSendResult(result);
                }
            }

            @Override
            public void onChannelAvailable(boolean isAvailable) {
                sFileChanalIsAvailable = isAvailable;
                IwdsLog.d(TAG, "Service --> sFileChanalIsAvailable: " + isAvailable);
                if (null != instanceModelListener) {
                    instanceModelListener.onChannelAvailable(isAvailable);
                }
            }

            @Override
            public void onSendFileProgress(int progress) {
                if (progress == ThemePreviewActivity.MAX_PROGRESS_100)
                    sIsShowChangeDialog = true;

                if (null != instanceModelListener) {
                    instanceModelListener.onSendFileProgress(progress);
                }
            }

        };
    }

    public static void register(SimpleFileTransactionModelCallbackImpl instanceModelListener2, DataTransactorCallback instanceDataListener2) {
        instanceDataListener = instanceDataListener2;
        instanceModelListener = instanceModelListener2;

        if (sFileChanalIsAvailable) {
            if (null != instanceModelListener) {
                instanceModelListener.onChannelAvailable(sFileChanalIsAvailable);
            }
        }

        if (sDataChanalIsAvailable) {
            if (null != instanceDataListener) {
                instanceDataListener.onChannelAvailable(sDataChanalIsAvailable);
            }
        }

    }

    public static void unregister() {
        ThemeFileSendModel.instanceModelListener = null;
        ThemeFileSendModel.instanceDataListener = null;
    }

    public static void requestSendFile(String path) throws FileNotFoundException {
        mFileTransactionModel.requestSendFile(path);
    }

}