/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  TangRongbing <rongbing.tang@ingenic.com, yanhuang8923@163.com>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.remotecamera;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.OrientationEventListener;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.Toast;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.remotecamera.MyHomeKey.OnHomePressedListener;

public class PhoneCameraActivity extends Activity implements
        SurfaceHolder.Callback, PreviewCallback, AutoFocusCallback,
        PictureCallback, OnHomePressedListener {

    private SurfaceView mSurfaceView;
    private SurfaceHolder mSurfaceHolder;

    private Camera mCamera;
    // 统计手机端的摄像头的数量
    private int mNumberOfCameras;
    // Camera ID currently chosen
    private int mCurrentCameraID;
    // The first rear facing camera
    private int mDefaultCameraId;
    // Camera ID that's actually acquired
    private int mCameraCurrentlyLocked;
    private final String channelUnavailable = "com.ingenic.watchmanager.remotecamera.CHANNELUNAVAILABLE";
    public static final String channelavailable = "com.ingenic.watchmanager.remotecamera.CHANNELAVAILABLE";
    private final String CAMERA_ACTION = "com.ingenic.watchmanager.remote_camera.Phone_Camera_Service";
    private CameraReceiver cameraReceiver;
    private final String TAG = "ttt";

    private final int MSG_STOP_CAMERA = 2;
    private final int MSG_SWITCH_CAMERA = 3;
    private final int MSG_TAKE_PICTURE = 4;
    private final int MSG_FROM_CAMERA_TO_SERVICE_STOP_CAMERA = 5;
    private final int MSG_FROM_CAMERA_TO_SERVICE_TAKE_PICKTURE_FAILE = 6;
    private final int MSG_FROM_CAMERA_TO_SERVICE_TAKE_PICKTURE_SUCCESS = 7;

    private final String KEY_RCBM_TO_CAMERA = "KEY_RCBM_STOP_CAMERA";
    private final String KEY_RCBM_TO_SERVICE = "KEY_RCBM_TO_SERVICE";

    private final int RCBM_DEFAULT_INT = 0;
    private final int RCBM_STOP_CAMERA = 1;
    private final int RCBM_SWITCH_CAMERA = 2;
    private final int RCBM_TAKE_PICTURE = 3;

    private final int RCBM_TO_SERVICE_STOP_CAMERA = 4;
    private final int RCBM_TO_SERVICE_TAKE_PICTURE_SUCCESS = 5;
    private final int RCBM_TO_SERVICE_TAKE_PICTURE_FAILE = 6;

    private int INIT_HEIGHT = 320;
    private int INIT_WIDTH = 240;
    private Parameters cameraParameters = null;
    private int screenOrientation = 0;
    private boolean hasPaused = false;
    private boolean isBackPressed = false;

    private boolean compressThreadRunning = false;

    private boolean keyEventAvaiable = false;

    // 屏幕方向的监听
    private OrientationEventListener mOrientationEventListener;

    // home键监听
    private MyHomeKey mHomeKey;

    public static final int FLAG_HOMEKEY_DISPATCHED = 0x80000000; // 需要自己定义标志



    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote_camera);
        if (!PhoneCameraService.channelAvaiable) {

            this.finish();
            return;
        }

        // 开启压缩线程
        startPreviewFrameCompressThread();

        hasPaused = false;

        mSurfaceView = (SurfaceView) findViewById(R.id.surfaceView_remote_camera);

        mOrientationEventListener = new OrientationEventListener(this) {
            @Override
            public void onOrientationChanged(int orientation) {
                screenOrientation = CameraUtils.roundOrientation(orientation,
                        screenOrientation);
            }
        };
        mOrientationEventListener.enable();

        registerReceiver(mPhoneCamera_BR, new IntentFilter(CAMERA_ACTION));

        // 统计手机端的摄像头的数量
        // 找到后置摄像头
        mNumberOfCameras = Camera.getNumberOfCameras();
        CameraInfo _cameraInfo = new CameraInfo();
        for (int i = 0; i < mNumberOfCameras; i++) {
            Camera.getCameraInfo(i, _cameraInfo);
            if (_cameraInfo.facing == CameraInfo.CAMERA_FACING_BACK) {
                mCurrentCameraID = mDefaultCameraId = i;
            }
        }

        // 为SurfaceHolder添加一个回调
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);

        // 为了兼容低版本的Android手机
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        PhoneStates.isStartedCamera = true;

        registerCameraReceiver();
    }

    private void registerCameraReceiver() {
        if (cameraReceiver == null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(channelUnavailable);
            intentFilter.addAction(channelavailable);
            cameraReceiver = new CameraReceiver();
            registerReceiver(cameraReceiver, intentFilter);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub

        if (!keyEventAvaiable) {
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void unRegisterCameraReceiver() {
        if (cameraReceiver != null) {
            unregisterReceiver(cameraReceiver);
        }
    }

    private class CameraReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            String action = intent.getAction();
            if (channelUnavailable.equals(action)) {
                keyEventAvaiable = true;
                finish();
            } else if (channelavailable.equals(action)) {
                keyEventAvaiable = false;
            }

        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        // home键监听
        mHomeKey = new MyHomeKey(this);
        mHomeKey.setOnHomePressedListener(this);
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();

        unRegisterCameraReceiver();
    }

    @Override
    protected void onPause() {
        compressThreadRunning = false;

        if (mHomeKey != null) {
            mHomeKey.setOnHomePressedListener(null);
        }

        // Because the Camera object is a shared resource, it's very
        // important to release it when the activity is paused.
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }

        // 此时应该主动断开 预览 链接
        // 被暂停时，应该释放摄像头的资源
        if (isBackPressed) {
            // 按返回键触发这里
            isBackPressed = false;
        } else {
            // 直接被暂停时触发这里
            if (!hasPaused) {

                Intent _intent = new Intent(CAMERA_ACTION);
                _intent.putExtra(KEY_RCBM_TO_SERVICE,
                        RemoteCameraTransactorModel.MSG_WATCH_EXIT_CAMERA);
                sendBroadcast(_intent);

                hasPaused = true;
                _intent = new Intent(CAMERA_ACTION);
                _intent.putExtra(KEY_RCBM_TO_SERVICE,
                        RCBM_TO_SERVICE_STOP_CAMERA);
                sendBroadcast(_intent);
            }

            mPhoneCameraHandler.obtainMessage(MSG_STOP_CAMERA).sendToTarget();
        }
        super.onPause();
    }

    public int[] getScreenPixels() {
        int[] _pixels = new int[] { 0, 0 };

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        _pixels[0] = dm.widthPixels;
        _pixels[1] = dm.heightPixels;

        return _pixels;
    }

    public void showToast(String msg) {
        Toast _toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        _toast.setGravity(Gravity.CENTER, 0, 0);
        _toast.show();
    }

    @Override
    public void onBackPressed() {
        // 主动断开 预览 连接
        // 通知服务，自己已主动断开了连接
        isBackPressed = true;
        compressThreadRunning = false;
        mPhoneCameraHandler.obtainMessage(
                MSG_FROM_CAMERA_TO_SERVICE_STOP_CAMERA).sendToTarget();

        Intent _intent = new Intent(CAMERA_ACTION);
        _intent.putExtra(KEY_RCBM_TO_SERVICE,
                RemoteCameraTransactorModel.MSG_WATCH_EXIT_CAMERA);
        sendBroadcast(_intent);

    }

    @Override
    public void onHomePressed() {

        Intent _intent = new Intent(CAMERA_ACTION);
        _intent.putExtra(KEY_RCBM_TO_SERVICE,
                RemoteCameraTransactorModel.MSG_WATCH_EXIT_CAMERA);
        sendBroadcast(_intent);

        if (keyEventAvaiable)
            finish();

    }

    @Override
    public void onHomeLongPressed() {

        Intent _intent = new Intent(CAMERA_ACTION);
        _intent.putExtra(KEY_RCBM_TO_SERVICE,
                RemoteCameraTransactorModel.MSG_WATCH_EXIT_CAMERA);
        sendBroadcast(_intent);

        if (keyEventAvaiable)
            finish();

    }

    // ========================================
    // SurfaceHolder的回调
    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int width,
            int height) {
        initCamera();
    }

    @Override
    public void surfaceCreated(SurfaceHolder arg0) {
        // The Surface has been created, acquire the camera and tell it where
        // to draw.
        if (mCamera != null) {
            try {
                mCamera.setPreviewDisplay(arg0);
            } catch (IOException e) {
                IwdsLog.e(TAG, "IOException caused by setPreviewDisplay() --> "
                        + e);
            }
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder arg0) {
        // Surface will be destroyed when we return, so stop the preview.
        if (mCamera != null) {
            mCamera.autoFocus(null);
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();

            try {
                mCamera.setPreviewDisplay(null);
            } catch (IOException e) {
                e.printStackTrace();
            }
            mCamera.release();
            mCamera = null;
        }
    }

    // ========================================
    // Camera预览的回调

    private int dropCount = 0;

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {

        //
        if (!PhoneCameraService.channelAvaiable) {
            isBackPressed = true;
            compressThreadRunning = false;
            mPhoneCameraHandler.obtainMessage(
                    MSG_FROM_CAMERA_TO_SERVICE_STOP_CAMERA).sendToTarget();
            return;
        }

        // 如果采样率没有设置
        if (PreviewFrameQueue.getSamplingrate() <= 0) {
            if (cameraParameters != null) {
                PreviewFrameQueue.setSamplingrate(cameraParameters);
            }
        }

        if ((++dropCount) % (PreviewFrameQueue.getSamplingrate()) == 0) {
            dropCount = 0;
            // 将原始帧入队
            PreviewFrameQueue.enqueueSourceQueue(data);
        }

        keyEventAvaiable = true;
    }

    // ========================================
    // Camera自动对焦的回调
    @Override
    public void onAutoFocus(boolean arg0, Camera arg1) {
    }

    // ========================================
    // Camera拍照的回调

    private String mPath = "";
    byte[] sendData = null;

    @Override
    public void onPictureTaken(byte[] jpegData, Camera arg1) {

        int orientation = Exif.getOrientation(jpegData);
        mPath = CameraUtils.saveJpegPicture(getBaseContext(), jpegData,
                mDateTaken, cameraParameters.getPictureSize().width,
                cameraParameters.getPictureSize().height, orientation);

        if (mPath == null) {
            return;
        }

        // 压缩
        Bitmap bitmap = CameraUtils.makeBitmap(jpegData, 50 * 1024);
        bitmap = CameraUtils.rotateBitmap(bitmap, orientation);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, stream);
        bitmap.recycle();

        sendData = stream.toByteArray();

        IwdsLog.d(TAG, "the picture which will be send is " + sendData.length
                / 1024 + "KB, " + PhoneStates.watchScreenHeight + "x"
                + PhoneStates.wathcScreenWidth * 3 / 4);

        // 发送数据:通知对方，拍照成功
        mPhoneCameraHandler
                .sendEmptyMessage(MSG_FROM_CAMERA_TO_SERVICE_TAKE_PICKTURE_SUCCESS);

        initCamera();
    }

    // ========================================
    // PhoneCameraService广播消息的接收器
    private int value_BR = RCBM_DEFAULT_INT;
    private BroadcastReceiver mPhoneCamera_BR = new BroadcastReceiver() {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            value_BR = arg1.getIntExtra(KEY_RCBM_TO_CAMERA, RCBM_DEFAULT_INT);

            switch (value_BR) {

            case RCBM_STOP_CAMERA:
                IwdsLog.d(TAG,
                        "received broadcast from service to stop camera ... ");
                mPhoneCameraHandler.obtainMessage(MSG_STOP_CAMERA)
                        .sendToTarget();
                break;

            case RCBM_SWITCH_CAMERA: {
                IwdsLog.d(TAG,
                        "received broadcast from service to switch camera ... ");
                mPhoneCameraHandler.sendEmptyMessage(MSG_SWITCH_CAMERA);
            }
                break;

            case RCBM_TAKE_PICTURE: {
                IwdsLog.d(TAG,
                        "received broadcast from service to take picture ... ");
                mPhoneCameraHandler.sendEmptyMessage(MSG_TAKE_PICTURE);
            }
                break;

            default:
                break;
            }

        }
    };

    // ========================================
    // PhoneCameraActivity的Handler
    @SuppressLint("HandlerLeak")
    private Handler mPhoneCameraHandler = new Handler() {
        public void handleMessage(Message msg) {

            switch (msg.what) {

            case MSG_STOP_CAMERA:

                if (mPhoneCamera_BR != null) {
                    unregisterReceiver(mPhoneCamera_BR);
                    mPhoneCamera_BR = null;
                }

                mOrientationEventListener.disable();
                PhoneStates.isStartedCamera = false;
                compressThreadRunning = false;
                finish();
                break;

            case MSG_FROM_CAMERA_TO_SERVICE_STOP_CAMERA: {


                Intent _intent = new Intent(CAMERA_ACTION);
                _intent.putExtra(KEY_RCBM_TO_SERVICE,
                        RCBM_TO_SERVICE_STOP_CAMERA);
                sendBroadcast(_intent);

                mPhoneCameraHandler.obtainMessage(MSG_STOP_CAMERA)
                        .sendToTarget();
            }
                break;

            // 切换摄像头
            case MSG_SWITCH_CAMERA: {
                switchCamera();
            }
                break;

            // 拍照
            case MSG_TAKE_PICTURE: {
                takePicture();
            }
                break;

            case MSG_FROM_CAMERA_TO_SERVICE_TAKE_PICKTURE_FAILE: {
                Intent _intent = new Intent(CAMERA_ACTION);
                _intent.putExtra(KEY_RCBM_TO_SERVICE,
                        RCBM_TO_SERVICE_TAKE_PICTURE_FAILE);
                sendBroadcast(_intent);
            }
                break;

            case MSG_FROM_CAMERA_TO_SERVICE_TAKE_PICKTURE_SUCCESS: {
                Intent _intent = new Intent(CAMERA_ACTION);
                _intent.putExtra(KEY_RCBM_TO_SERVICE,
                        RCBM_TO_SERVICE_TAKE_PICTURE_SUCCESS);
                _intent.putExtra("picture_data_bytes", sendData);
                sendBroadcast(_intent);
            }

            default:
                break;
            }

        };
    };

    // ========================================
    // 自定义的相关方法

    /**
     * 切换摄像头
     * */
    private void switchCamera() {
        // Release this camera -> mCameraCurrentlyLocked
        if (mCamera != null) {
            try {
                mCamera.setPreviewDisplay(null);
            } catch (IOException e) {
                e.printStackTrace();
            }
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }

        // Acquire the next camera and request Preview to reconfigure
        // parameters.
        mCurrentCameraID = (mCameraCurrentlyLocked + 1) % mNumberOfCameras;
        mCamera = Camera.open(mCurrentCameraID);
        mCameraCurrentlyLocked = mCurrentCameraID;

        // Start the preview
        initCamera();
    }

    private long mDateTaken = 0;

    private void takePicture() {

        if (!CameraUtils.checkSpace()) {
            showToast(getResources().getString(R.string.no_storage));

            // 发送数据:通知对方，拍照失败
            // sendTakeResult(false, null, null);
            mPhoneCameraHandler
                    .sendEmptyMessage(MSG_FROM_CAMERA_TO_SERVICE_TAKE_PICKTURE_FAILE);

            return;
        }

        CameraUtils.setRotationParameter(cameraParameters, mCurrentCameraID,
                screenOrientation);

        mCamera.setParameters(cameraParameters);
        mDateTaken = System.currentTimeMillis();
        try {
            mCamera.takePicture(null, null, null, this);
        } catch (RuntimeException e) {
            IwdsLog.e(TAG, "fail to capture!! ---> " + e);
            showToast(getResources().getString(R.string.take_picture_fail));

            // 发送数据:通知对方，拍照失败
            mPhoneCameraHandler
                    .sendEmptyMessage(MSG_FROM_CAMERA_TO_SERVICE_TAKE_PICKTURE_FAILE);

            initCamera();
        }
    }

    private void initCamera() {
        // 打开默认的相机
        // if (mCamera == null) {
        // Log.d("ca", "open here---------------------");
        // mCamera = Camera.open(mDefaultCameraId);
        // }
        if (mCamera == null) {

            try {

                mCamera = Camera.open(mDefaultCameraId);
            } catch (RuntimeException e1) {
                e1.printStackTrace();
                try {
                    mCamera = Camera.open(Camera.getNumberOfCameras() - 1);
                } catch (RuntimeException e2) {
                    // TODO: handle exception
                    // 打开相机失败
                    isBackPressed = true;
                    compressThreadRunning = false;


/*                    Intent _intent = new Intent(CAMERA_ACTION);
                    _intent.putExtra(KEY_RCBM_TO_SERVICE,
                            RemoteCameraTransactorModel.MSG_WATCT_LOCK_SCREEN);
                    sendBroadcast(_intent);*/

                    mPhoneCameraHandler.obtainMessage(
                            MSG_FROM_CAMERA_TO_SERVICE_STOP_CAMERA)
                            .sendToTarget();
                    e2.printStackTrace();
                }

            }
        }

        // 设置参数
        cameraParameters = mCamera.getParameters();
        cameraParameters.setPictureFormat(ImageFormat.JPEG);

        // 设置被支持的预览界面与图片的size
        List<Size> previewSizes = cameraParameters.getSupportedPreviewSizes();

        for (Size size : previewSizes) {
            IwdsLog.d(TAG, "preview_size [" + size.width + ", " + size.height
                    + "]");
        }

        Size size = CameraUtils.getCurrentScreenSize(getBaseContext(),
                previewSizes, 1);

        if (size != null) {
            cameraParameters.setPreviewSize(size.width, size.height);

            IwdsLog.d(TAG, "--------------preview_size [" + size.width + ", "
                    + size.height + "]");

            double mScale = (double) Math.max(size.height, size.width)
                    / Math.min(size.height, size.width);

            if (INIT_WIDTH > INIT_HEIGHT) {
                INIT_WIDTH = (int) (INIT_HEIGHT * mScale);
            } else {
                INIT_HEIGHT = (int) (INIT_WIDTH * mScale);
            }

        } /*
           * else { cameraParameters.setPreviewSize(320, 240);
           * IwdsLog.i(TAG,"server uses default preview size : " + 320 + "x" +
           * 240); }
           */

        List<Size> pictureSizes = cameraParameters.getSupportedPictureSizes();
        Size size2 = CameraUtils.getCameraMaxPictureSize(pictureSizes);
        if (size2 != null) {
            cameraParameters.setPictureSize(size2.width, size2.height);
        } else {
            IwdsLog.i(TAG, "setPictureSize size is null");
        }

        if (CameraUtils.isSupportedFocus(
                Parameters.FOCUS_MODE_CONTINUOUS_PICTURE,
                cameraParameters.getSupportedFocusModes())) {
            cameraParameters
                    .setFocusMode(Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }

        int mDisplayRotation = CameraUtils
                .getDisplayRotation((WindowManager) getBaseContext()
                        .getSystemService(WINDOW_SERVICE));
        int mDisplayOrientation = CameraUtils.getDisplayOrientation(
                mDisplayRotation, mCurrentCameraID);

        mCamera.setParameters(cameraParameters);
        mCamera.setDisplayOrientation(mDisplayOrientation);

        // 设置预览
        try {
            mCamera.setPreviewDisplay(mSurfaceHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 开启预览
        mCamera.startPreview();
        mCamera.setPreviewCallback(this);
    }

    private CompressRunnable mCompressRunnable;

    private void startPreviewFrameCompressThread() {
        if (compressThreadRunning) {
            return;
        }

        compressThreadRunning = true;

        if (mCompressRunnable == null) {
            mCompressRunnable = new CompressRunnable();
        }

        new Thread(mCompressRunnable).start();
    }

    private class CompressRunnable implements Runnable {
        @Override
        public void run() {
            try {
                while (!Thread.currentThread().isInterrupted()
                        && compressThreadRunning) {

                    // 原始帧队列的出队操作
                    byte[] frame = PreviewFrameQueue.dequeueSourceQueue();

                    // 压缩
                    int orientation = CameraUtils.getRotation(mCurrentCameraID,
                            screenOrientation);

                    frame = CameraUtils.compressToJpegAndRotate(frame,
                            orientation, cameraParameters,
                            PhoneStates.wathcScreenWidth,
                            PhoneStates.watchScreenHeight);

                    // 压缩帧队列的入队操作
                    PreviewFrameQueue.enqueueCompressedQueue(frame);

                }
            } catch (Exception e) {
                IwdsLog.e(TAG, "CompressRunnable.run{} ---> " + e);
            } finally {
                compressThreadRunning = false;
            }
        }
    }

}
