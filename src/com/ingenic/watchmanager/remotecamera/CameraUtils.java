/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  TangRongbing <rongbing.tang@ingenic.com, yanhuang8923@163.com>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.remotecamera;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.os.Environment;
import android.os.StatFs;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Images.ImageColumns;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.WindowManager;
import android.widget.Toast;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.R;

public class CameraUtils {
	public static final String TAG = CameraUtils.class.getName();

	// 用于处理预览帧的数据
	public static byte[] compressToJpegAndRotate(byte[] data, int degrees,
			Parameters parameters, int toWidth, int toHeight) {
		if (data == null) {
			return null;
		}

		YuvImage yuvImage = new YuvImage(data, ImageFormat.NV21,
				parameters.getPreviewSize().width,
				parameters.getPreviewSize().height, null);

		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();

		yuvImage.compressToJpeg(new Rect(0, 0,
				parameters.getPreviewSize().width,
				parameters.getPreviewSize().height), 100,
				mByteArrayOutputStream);

		byte[] jpegData = mByteArrayOutputStream.toByteArray();

		// 获取缩略图
		Bitmap bitmap = makeBitmap(jpegData, 50 * 1024);

		// 旋转图片
		bitmap = rotateBitmap(bitmap, degrees);

		ByteArrayOutputStream stream = new ByteArrayOutputStream();

		bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
		bitmap.recycle();
		return stream.toByteArray();
	}

	public static Bitmap makeBitmap(byte[] jpegData, int maxNumOfPixels) {
		try {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory
					.decodeByteArray(jpegData, 0, jpegData.length, options);
			if (options.mCancel || options.outWidth == -1
					|| options.outHeight == -1) {
				return null;
			}
			options.inSampleSize = computeSampleSize(options, -1,
					maxNumOfPixels);
			options.inJustDecodeBounds = false;

			options.inDither = false;
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			return BitmapFactory.decodeByteArray(jpegData, 0, jpegData.length,
					options);
		} catch (OutOfMemoryError ex) {
			Log.e(TAG, "Got oom exception ", ex);
			return null;
		}
	}

	public static int computeSampleSize(BitmapFactory.Options options,
			int minSideLength, int maxNumOfPixels) {
		int initialSize = computeInitialSampleSize(options, minSideLength,
				maxNumOfPixels);

		int roundedSize;
		if (initialSize <= 8) {
			roundedSize = 1;
			while (roundedSize < initialSize) {
				roundedSize <<= 1;
			}
		} else {
			roundedSize = (initialSize + 7) / 8 * 8;
		}

		return roundedSize;
	}

	private static int computeInitialSampleSize(BitmapFactory.Options options,
			int minSideLength, int maxNumOfPixels) {
		double w = options.outWidth;
		double h = options.outHeight;

		int lowerBound = (maxNumOfPixels < 0) ? 1 : (int) Math.ceil(Math.sqrt(w
				* h / maxNumOfPixels));
		int upperBound = (minSideLength < 0) ? 128 : (int) Math.min(
				Math.floor(w / minSideLength), Math.floor(h / minSideLength));

		if (upperBound < lowerBound) {
			// return the larger one when there is no overlapping zone.
			return lowerBound;
		}

		if (maxNumOfPixels < 0 && minSideLength < 0) {
			return 1;
		} else if (minSideLength < 0) {
			return lowerBound;
		} else {
			return upperBound;
		}
	}

	public static Bitmap rotateBitmap(Bitmap b, int degrees) {

		IwdsLog.d("rotateBitmap", "degrees =========" + degrees);

		if (degrees != 0 && b != null) {
			Matrix m = new Matrix();
			m.setRotate(degrees, (float) b.getWidth() / 2,
					(float) b.getHeight() / 2);
			try {
				Bitmap newBitmap = Bitmap.createBitmap(b, 0, 0, b.getWidth(),
						b.getHeight(), m, true);
				if (b != newBitmap) {
					b.recycle();
					b = newBitmap;
				}
			} catch (OutOfMemoryError ex) {

			}
		}

		return b;
	}

	public static int getRotation(int cameraId, int orientation) {
		int rotation = 0;
		if (orientation != OrientationEventListener.ORIENTATION_UNKNOWN) {
			Camera.CameraInfo info = new Camera.CameraInfo();
			Camera.getCameraInfo(cameraId, info);
			if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
				rotation = (info.orientation - orientation + 360) % 360;
			} else {
				rotation = (info.orientation + orientation) % 360;
			}
		}
		return rotation;
	}

	public static final int ORIENTATION_HYSTERESIS = 5;

	public static int roundOrientation(int orientation, int orientationHistory) {
		boolean changeOrientation = false;
		if (orientationHistory == OrientationEventListener.ORIENTATION_UNKNOWN) {
			changeOrientation = true;
		} else {
			int dist = Math.abs(orientation - orientationHistory);
			dist = Math.min(dist, 360 - dist);
			changeOrientation = (dist >= 45 + ORIENTATION_HYSTERESIS);
		}
		if (changeOrientation) {
			return ((orientation + 45) / 90 * 90) % 360;
		}
		return orientationHistory;
	}

	public static void setRotationParameter(
			android.hardware.Camera.Parameters parameters, int cameraId,
			int orientation) {
		// See android.hardware.Camera.Parameters.setRotation for
		// documentation.
		int rotation = 0;
		if (orientation != OrientationEventListener.ORIENTATION_UNKNOWN) {
			Camera.CameraInfo info = new Camera.CameraInfo();
			Camera.getCameraInfo(cameraId, info);
			if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
				rotation = (info.orientation - orientation + 360) % 360;
			} else { // back-facing camera
				rotation = (info.orientation + orientation) % 360;
			}
		}
		parameters.setRotation(rotation);
	}

	/**
	 * 获得最接近频幕宽度的尺寸
	 * 
	 * @param sizeList
	 * @param n
	 *            放大几倍 （>0)
	 * @return
	 */
	public static Size getCurrentScreenSize(Context context,
			List<Size> sizeList, int n) {

		if (sizeList == null || sizeList.size() <= 0) {
			return null;
		}

		int dstWidth = PhoneStates.wathcScreenWidth;
		int[] count = new int[sizeList.size()];

		for (int i = 0; i < sizeList.size(); i++) {
			count[i] = sizeList.get(i).width - dstWidth;
			count[i] = (count[i] >= 0) ? count[i] : -count[i];
		}

		int min = count[0];
		int minIndex = -1;

		for (int i = 0; i < count.length; i++) {
			if (min >= count[i]) {
				minIndex = i;
				min = count[i];
			}
		}

		if (minIndex < 0) {
			return null;
		}

		return sizeList.get(minIndex);

		// if (sizeList != null && sizeList.size() > 0) {
		// int screenWidth = getScreenWidth(context) * n;
		// int[] arry = new int[sizeList.size()];
		// int temp = 0;
		// for (Size size : sizeList) {
		// arry[temp++] = Math.abs(size.width - screenWidth);
		// }
		// temp = 0;
		// int index = 0;
		// for (int i = 0; i < arry.length; i++) {
		// if (i == 0) {
		// temp = arry[i];
		// index = 0;
		// } else {
		// if (arry[i] < temp) {
		// index = i;
		// temp = arry[i];
		// }
		// }
		// }
		// return sizeList.get(index);
		// }
		// return null;
	}

	/**
	 * 得到屏幕宽度
	 * 
	 * @return 单位:px
	 */
	public static int getScreenWidth(Context context) {
		int screenWidth;
		WindowManager wm = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics dm = new DisplayMetrics();
		wm.getDefaultDisplay().getMetrics(dm);
		screenWidth = dm.widthPixels;
		return screenWidth;
	}

	/**
	 * 获得相机最高分辨率
	 * 
	 * @param sizeList
	 * @param n
	 *            放大几倍 （>0)
	 * @return
	 */
	public static Size getCameraMaxPictureSize(List<Size> sizeList) {
		if (sizeList != null && sizeList.size() > 0) {
			int[] arry = new int[sizeList.size()];
			int temp = 0;
			int j = 0;
			for (Size size : sizeList) {
				arry[temp++] = size.width;
				Log.e(TAG, "pic width*height" + j++ + " = " + size.width + "*"
						+ size.height);
				Log.e(TAG, "\n");
			}
			temp = 0;
			int index = 0;
			for (int i = 0; i < arry.length; i++) {
				if (i == 0) {
					temp = arry[i];
					index = 0;
				} else {
					if (arry[i] > temp) {
						index = i;
						temp = arry[i];
					}
				}
			}
			return sizeList.get(index);
		}
		return null;
	}

	public static boolean isSupportedFocus(String value, List<String> supported) {
		return supported == null ? false : supported.indexOf(value) >= 0;
	}

	public static int getDisplayRotation(WindowManager wm) {
		int rotation = wm.getDefaultDisplay().getRotation();
		switch (rotation) {
		case Surface.ROTATION_0:
			return 0;
		case Surface.ROTATION_90:
			return 90;
		case Surface.ROTATION_180:
			return 180;
		case Surface.ROTATION_270:
			return 270;
		}
		return 0;
	}

	public static int getDisplayOrientation(int degrees, int cameraId) {
		// See android.hardware.Camera.setDisplayOrientation for
		// documentation.
		Camera.CameraInfo info = new Camera.CameraInfo();
		Camera.getCameraInfo(cameraId, info);
		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360; // compensate the mirror
		} else { // back-facing
			result = (info.orientation - degrees + 360) % 360;
		}
		return result;
	}

	public static final String PATH = Environment
			.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
			.toString()
			+ "/Camera";
	public static final long LOW_STORAGE = 50000000;

	@SuppressWarnings("deprecation")
	public static boolean checkSpace() {
		String state = Environment.getExternalStorageState();
		if (!Environment.MEDIA_MOUNTED.equals(state)) {
			Log.e(TAG, "no external storage!");
			return false;
		}

		File dir = new File(PATH);
		dir.mkdirs();
		if (!dir.isDirectory() || !dir.canWrite()) {
			return false;
		}
		long space = 0;
		try {
			StatFs stat = new StatFs(PATH);
			space = stat.getAvailableBlocks() * (long) stat.getBlockSize();
		} catch (Exception e) {
			Log.i(TAG, "Fail to access external storage", e);
		}
		return (space > LOW_STORAGE);
	}

	@SuppressLint({ "SimpleDateFormat", "InlinedApi" })
	public static String saveJpegPicture(Context context, byte[] jpeg,
			long dateTaken, int pictureWidth, int pictureHeight, int orientation) {

		SimpleDateFormat mFormat = new SimpleDateFormat(
				context.getString(R.string.image_file_name_format));
		Date date = new Date(dateTaken);
		String title = mFormat.format(date);

		String mPath = "nopath";

		if (Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())) {
			mPath = PATH + '/' + title + ".jpg";
			Log.i(TAG, "external storage:" + mPath);
		} else {
			Toast mStorageToast = Toast.makeText(context, R.string.no_storage,
					Toast.LENGTH_SHORT);
			mStorageToast.setGravity(Gravity.CENTER, 0, 0);
			mStorageToast.show();
			return null;
		}

		if (mPath.equals("nopath"))
			return null;

		String tmpPath = mPath + ".tmp";
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(tmpPath);
			out.write(jpeg);
			out.close();
			new File(tmpPath).renameTo(new File(mPath));
		} catch (Exception e) {
			Log.e(TAG, "Failed to write image", e);
			return null;
		} finally {
			try {
				out.close();
			} catch (Exception e) {
			}
		}

		// Insert into MediaStore.
		ContentValues values = new ContentValues(9);
		values.put(ImageColumns.TITLE, title);
		values.put(ImageColumns.DISPLAY_NAME, title + ".jpg");
		values.put(ImageColumns.DATE_TAKEN, dateTaken);
		values.put(ImageColumns.MIME_TYPE, "image/jpeg");
		// Clockwise rotation in degrees. 0, 90, 180, or 270.
		values.put(ImageColumns.ORIENTATION, orientation);
		values.put(ImageColumns.DATA, mPath);
		values.put(ImageColumns.SIZE, jpeg.length);
		values.put(ImageColumns.WIDTH, pictureWidth);
		values.put(ImageColumns.HEIGHT, pictureHeight);

		try {
			context.getContentResolver().insert(
					Images.Media.EXTERNAL_CONTENT_URI, values);
		} catch (Throwable th) {
			Log.e(TAG, "Failed to insert image" + th);
			return null;
		}
		Toast mToast = Toast.makeText(context, String.format(
				context.getString(R.string.picture_path_message), mPath),
				Toast.LENGTH_SHORT);
		mToast.setGravity(Gravity.CENTER, 0, 0);
		mToast.show();
		return mPath;
	}

}
