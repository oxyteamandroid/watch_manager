/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  TangRongbing <rongbing.tang@ingenic.com, yanhuang8923@163.com>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.remotecamera;

import java.util.ArrayDeque;
import java.util.Queue;

import android.hardware.Camera.Parameters;

import com.ingenic.iwds.utils.IwdsLog;

public class PreviewFrameQueue {

	public static Queue<byte[]> sourceQueue = new ArrayDeque<byte[]>(10);

	public static final int DEFAULT_FPS = 10;
	public static int dropCount = 0;// 采样周期
	public static int sizeLimit = 0;

	public static int prevSourceQueueSize = 0;

	@SuppressWarnings("deprecation")
	public static void setSamplingrate(Parameters parameters) {
		sizeLimit = dropCount = parameters.getPreviewFrameRate() / DEFAULT_FPS;
	}

	public static int getSamplingrate() {
		return dropCount;
	}

	public static void enqueueSourceQueue(byte[] sourceFrame) {
		if (sourceFrame == null) {
			return;
		}

		synchronized (sourceQueue) {
			if (sourceQueue.size() > prevSourceQueueSize) {
				// 如果队列的长度增长,则
				dropCount++;
			} else if (sourceQueue.size() < prevSourceQueueSize) {
				// 如果队列出队速度过快
				dropCount--;
			}

			prevSourceQueueSize = sourceQueue.size();
			sourceQueue.add(sourceFrame);

			IwdsLog.d("enqueue", "prevSourceQueueSize = sourceQueue.size();"
					+ sourceQueue.size());

			sourceQueue.notifyAll();
		}
	}

	public static byte[] dequeueSourceQueue() {
		synchronized (sourceQueue) {
			for (;;) {
				try {
					while (sourceQueue.size() == 0)
						sourceQueue.wait();

					return sourceQueue.poll();

				} catch (InterruptedException e) {
					IwdsLog.e("PreviewFrameQueue",
							"Exception in dequeueSourceQueue: " + e);
				}
			}
		}
	}

	// ========================================

	public static final int COMP_MAX_LEN = 50;
	public static Queue<byte[]> compressedQueue = new ArrayDeque<byte[]>(
			COMP_MAX_LEN);

	public static void enqueueCompressedQueue(byte[] frame) {
		synchronized (compressedQueue) {
			compressedQueue.add(frame);
			compressedQueue.notifyAll();
		}
	}

	public static byte[] dequeueCompressedQueue() {
		synchronized (compressedQueue) {
			for (;;) {
				try {
					while (compressedQueue.size() == 0)
						compressedQueue.wait();

					return compressedQueue.poll();

				} catch (InterruptedException e) {
					IwdsLog.e("PreviewFrameQueue",
							"Exception in dequeueCompQueue: " + e);
				}
			}
		}
	}

	// ===================================

	public static void clearAll() {
		dropCount = 0;

		synchronized (sourceQueue) {
			sourceQueue.clear();
		}
	}

}
