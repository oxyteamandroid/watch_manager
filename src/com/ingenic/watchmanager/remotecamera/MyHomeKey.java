/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  foror-fighter/elf/watch-apps/WatchManager project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.remotecamera;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class MyHomeKey {
    public static final String TAG = MyHomeKey.class.getSimpleName();
    private Context mContext;
    private OnHomePressedListener mListener;
    private IntentFilter mIntentFilter;
    private InnerRecevier mRecevier;

    public MyHomeKey(Context context) {
        if (context == null) {
            throw new ExceptionInInitializerError(
                    "The paramter Context contextg is null");
        }
        mContext = context;
        mIntentFilter = new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        mRecevier = new InnerRecevier();
    }

    public void setOnHomePressedListener(
            OnHomePressedListener onHomePressedListener) {
        mListener = onHomePressedListener;
        if (onHomePressedListener == null) {
            mContext.unregisterReceiver(mRecevier);
        } else {
            mContext.registerReceiver(mRecevier, mIntentFilter);
        }
    }

    public interface OnHomePressedListener {
        public void onHomePressed();

        public void onHomeLongPressed();
    }

    class InnerRecevier extends BroadcastReceiver {
        private final String SYSTEM_DIALOG_REASON_KEY = "reason";
        @SuppressWarnings("unused")
        private final String SYSTEM_DIALOG_REASON_GLOBAL_ACTIONS = "globalactions";
        private final String SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps";
        private final String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (!action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
                return;
            }

            String reason = intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY);

            if (reason == null) {
                return;
            }

            if (mListener == null) {
                return;
            }

            if (reason.equals(SYSTEM_DIALOG_REASON_HOME_KEY)) {

                // 短按home键
                mListener.onHomePressed();
            } else if (reason.equals(SYSTEM_DIALOG_REASON_RECENT_APPS)) {
                // 长按home键

                mListener.onHomeLongPressed();
            }
        }
    }

}
