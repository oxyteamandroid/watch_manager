/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  TangRongbing <rongbing.tang@ingenic.com, yanhuang8923@163.com>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.remotecamera;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.CameraFrameInfo;
import com.ingenic.iwds.datatransactor.elf.CameraPreviewSizeInfo;
import com.ingenic.iwds.datatransactor.elf.CameraTransactionModel;
import com.ingenic.iwds.datatransactor.elf.CameraTransactionModel.CameraTransactionModelCallback;
import com.ingenic.iwds.utils.IwdsLog;

public class PhoneCameraService extends Service implements
        CameraTransactionModelCallback {

    // 标签名
    public static final String TAG = PhoneCameraService.class.getName();

    // 与手表端的UUID一致
    public static final String PREVIEW_UUID = "08BDB878-8A44-9C6A-8542-C543B833C681";

    // 广播ACTION
    private final String CAMERA_ACTION = "com.ingenic.watchmanager.remote_camera.Phone_Camera_Service";
    private final String channelUnavailable = "com.ingenic.watchmanager.remotecamera.CHANNELUNAVAILABLE";

    private Context mContext;
    // 照相机事物层对象
    private static CameraTransactionModel mTransactionModel;

    public static final String KEY_INTNET_EXTRA_WATCH_PIXELS = "KEY_INTNET_EXTRA_WATCH_PIXELS";

    private final int MSG_START_CAMERA = 1;
    private final int MSG_STOP_CAMERA = 2;
    private final int MSG_SWITCH_CAMERA = 3;
    private final int MSG_TAKE_PICTURE = 4;
    private final int MSG_FROM_CAMERA_TO_SERVICE_STOP_CAMERA = 5;
    private final int MSG_FROM_CAMERA_TO_SERVICE_TAKE_PICKTURE_FAILE = 6;
    private final int MSG_FROM_CAMERA_TO_SERVICE_TAKE_PICKTURE_SUCCESS = 7;

    private final int MSG_CHECK_START_ACTIVITY_FAILED = 8;

    private final String KEY_RCBM_TO_CAMERA = "KEY_RCBM_STOP_CAMERA";
    private final String KEY_RCBM_TO_SERVICE = "KEY_RCBM_TO_SERVICE";

    private final int RCBM_DEFAULT_INT = 0;
    private final int RCBM_STOP_CAMERA = 1;
    private final int RCBM_SWITCH_CAMERA = 2;
    private final int RCBM_TAKE_PICTURE = 3;

    private final int RCBM_TO_SERVICE_STOP_CAMERA = 4;
    private final int RCBM_TO_SERVICE_TAKE_PICTURE_SUCCESS = 5;
    private final int RCBM_TO_SERVICE_TAKE_PICTURE_FAILE = 6;

    private boolean packetSendRunnableRunning = false;
    private PacketSendRunnable mPacketSendRunnable;
    public static boolean channelAvaiable = false;
    private boolean screen_on_off = true;

    private static RemoteCameraTransactorModel mRemoteCameraTransactorModel;

    // 手机端的远程相机的Handler
    private Handler mServiceHandler = new Handler() {
        public void handleMessage(Message msg) {
            Intent intent;
            switch (msg.what) {
            case RemoteCameraTransactorModel.MSG_WATCH_EXIT_CAMERA:
                // 手机端 主动退出相机
                exitCameraByWatch();
                break;

            case RemoteCameraTransactorModel.MSG_WATCT_LOCK_SCREEN:
                // 打开相机失败，提示用户，手机端需要屏幕解锁
                if (mRemoteCameraTransactorModel != null) {

                    mRemoteCameraTransactorModel.sendFlagToWatch(RemoteCameraTransactorModel.MSG_WATCT_LOCK_SCREEN);
                }

                break;

            case MSG_CHECK_START_ACTIVITY_FAILED:
                isClsRunning();

                break;
            case MSG_START_CAMERA:

                // 启动相机
                intent = new Intent(mContext, PhoneCameraActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);

                // 开启预览帧的封包和发送线程
                startPacketSendThread();
                mServiceHandler.removeMessages(MSG_CHECK_START_ACTIVITY_FAILED);
                mServiceHandler.sendEmptyMessageDelayed(
                        MSG_CHECK_START_ACTIVITY_FAILED, 500);

                break;

            case MSG_STOP_CAMERA:

                // 停止预览帧的发送线程
                packetSendRunnableRunning = false;

                // 关闭相机
                intent = new Intent(CAMERA_ACTION);
                intent.putExtra(KEY_RCBM_TO_CAMERA, RCBM_STOP_CAMERA);
                sendBroadcast(intent);

                break;

            case MSG_FROM_CAMERA_TO_SERVICE_STOP_CAMERA:

                //mServiceHandler.removeCallbacks(mSendRunnable);
                // 通知手表端：手机端已经关闭相机
                if (mTransactionModel != null) {
                    mTransactionModel.requestStopPreview();
                }

                mServiceHandler.removeMessages(MSG_CHECK_START_ACTIVITY_FAILED);

                // 停止预览帧的发送线程
                packetSendRunnableRunning = false;

                break;

            case MSG_SWITCH_CAMERA:

                // 处理切换摄像头请求
                intent = new Intent(CAMERA_ACTION);
                intent.putExtra(KEY_RCBM_TO_CAMERA, RCBM_SWITCH_CAMERA);
                sendBroadcast(intent);

                break;

            case MSG_TAKE_PICTURE:

                // 处理拍照请求
                intent = new Intent(CAMERA_ACTION);
                intent.putExtra(KEY_RCBM_TO_CAMERA, RCBM_TAKE_PICTURE);
                sendBroadcast(intent);

                break;

            case MSG_FROM_CAMERA_TO_SERVICE_TAKE_PICKTURE_FAILE:
                if (mTransactionModel != null) {
                    mTransactionModel.notifyRequestTakePictureFailed();
                }

                break;

            case MSG_FROM_CAMERA_TO_SERVICE_TAKE_PICKTURE_SUCCESS:
                if (mTransactionModel != null) {
                    CameraFrameInfo frameInfo = new CameraFrameInfo();
                    frameInfo.frameData = picData;
                    mTransactionModel.notifyTakePictureDone(frameInfo);
                }

                break;

            default:
                break;
            }
            intent = null;

        }

    };

    private void isClsRunning() {
        ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

        if (!"com.ingenic.watchmanager.remotecamera.PhoneCameraActivity"
                .equals(cn.getClassName())) {
            // 关闭掉手表端

            // 通知手表端：手机端已经关闭相机
            if (mTransactionModel != null) {
                mTransactionModel.requestStopPreview();
            }

            exitCameraByWatch();

            Intent intent = new Intent(channelUnavailable);
            sendBroadcast(intent);

        }

    }

    private void exitCameraByWatch() {
        // 手机端主动退出远程相机
        if (mRemoteCameraTransactorModel != null) {
            mRemoteCameraTransactorModel
                    .sendFlagToWatch(RemoteCameraTransactorModel.MSG_WATCH_EXIT_CAMERA);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate() {
        super.onCreate();

        CharSequence label = getApplication().getApplicationInfo().loadLabel(
                getPackageManager());

        Notification notification = new Notification(getApplication()
                .getApplicationInfo().icon, label, System.currentTimeMillis());

        Intent it = new Intent(
                ServiceManagerContext.ACTION_NOTIFICATION_CLICKED);
        it.setFlags(it.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, it, 0);
        notification.setLatestEventInfo(this, label, "", pendingIntent);
        notification.flags = notification.flags
                | Notification.FLAG_ONGOING_EVENT;

        startForeground(9999, notification);

        mContext = getBaseContext();
        packetSendRunnableRunning = false;

        if (mTransactionModel == null) {
            // 启动事物层对象
            mTransactionModel = new CameraTransactionModel(mContext, this,
                    PREVIEW_UUID);
            mTransactionModel.start();
        }

        if( mRemoteCameraTransactorModel == null ){
            mRemoteCameraTransactorModel = RemoteCameraTransactorModel.getInstance(PhoneCameraService.this);
            mRemoteCameraTransactorModel.startTransaction();
        }

        if (mServiceBroadcastReceiver != null) {
            // 注册广播
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(CAMERA_ACTION);
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            intentFilter.addAction("android.intent.action.SCREEN_ON");
            registerReceiver(mServiceBroadcastReceiver, intentFilter);
        }
    }

    @Override
    public void onDestroy() {
        if (mServiceBroadcastReceiver != null) {
            unregisterReceiver(mServiceBroadcastReceiver);
            mServiceBroadcastReceiver = null;
        }

        if (mTransactionModel != null) {
            mTransactionModel.stop();
        }

        if (mRemoteCameraTransactorModel == null) {

            mRemoteCameraTransactorModel.stopTransaction();
        }

        mContext = null;
        packetSendRunnableRunning = false;
        super.onDestroy();
    }

    // 相机的传输回调
    @Override
    public void onRequestStartPreview(CameraPreviewSizeInfo sizeInfo) {
        // 这里接收到手表端的开始预览的请求
        // 如果相机没有开启，则开启相机

        // 屏幕黑屏，提示用户

        KeyguardManager mKeyguardManager = (KeyguardManager) this
                .getSystemService(Context.KEYGUARD_SERVICE);
        boolean flag = mKeyguardManager.inKeyguardRestrictedInputMode();
        if (flag || !screen_on_off) {
            // 屏幕 黑屏 ，或者 锁屏
            // 提示用户，手机端需要屏幕解锁
            if (mRemoteCameraTransactorModel != null) {
                mRemoteCameraTransactorModel
                        .sendFlagToWatch(RemoteCameraTransactorModel.MSG_WATCT_LOCK_SCREEN);
            }

            mServiceHandler.obtainMessage(
                    MSG_FROM_CAMERA_TO_SERVICE_STOP_CAMERA)
                    .sendToTarget();
            return;

        }

        if (!PhoneStates.isStartedCamera) {
            PhoneStates.wathcScreenWidth = sizeInfo.width;
            PhoneStates.watchScreenHeight = sizeInfo.heigth;

            mServiceHandler.removeMessages(MSG_START_CAMERA);
            mServiceHandler.obtainMessage(MSG_START_CAMERA).sendToTarget();
        } else {
            // 在这里做切换摄像头的操作的请求
            if (sizeInfo.isFrontCamera) {

                mServiceHandler.sendEmptyMessage(MSG_SWITCH_CAMERA);
            }
        }
    }

    @Override
    public void onRequestStopPreview() {
        // 这里接收到手表端的停止预览的请求
        // 如果相机已经开启了，则关闭相机

        if (PhoneStates.isStartedCamera) {

            mServiceHandler.obtainMessage(MSG_STOP_CAMERA).sendToTarget();
        } else {

        }
    }

    @Override
    public void onRequestTakePicture() {

        // 这里接收到手表端发来的拍照请求
        if (PhoneStates.isStartedCamera) {
            mServiceHandler.sendEmptyMessage(MSG_TAKE_PICTURE);
        }
    }

    @Override
    public void onRequestStartPreviewFailed() {
        IwdsLog.e(PhoneCameraService.this, "onRequestStartPreviewFailed");

    }

    @Override
    public void onRequestStopPreviewFailed() {
        IwdsLog.e(PhoneCameraService.this, "onRequestStopPreviewFailed");

    }

    @Override
    public void onRequestTakePictureFailed() {
    }

    @Override
    public void onTakePictureDone(CameraFrameInfo frame) {
    }

    @Override
    public void onObjectArrived(CameraFrameInfo frame) {
    }

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
    }

    @Override
    public void onChannelAvailable(boolean isAvailable) {

        if (isAvailable) {
            // 禁止HOME back menu 键的操作
            channelAvaiable = true;
            Intent intent = new Intent(PhoneCameraActivity.channelavailable);
            sendBroadcast(intent);
        } else {
            channelAvaiable = false;
            Intent intent = new Intent(channelUnavailable);
            sendBroadcast(intent);
        }

    }

    @Override
    public void onSendResult(DataTransactResult result) {
        sendResult = true;
    }

    private boolean sendResult = true;

    // =========================================
    // PhoneCameraService广播消息的接收器
    // =========================================
    private int value_BR = RCBM_DEFAULT_INT;
    private byte[] picData = null;
    private BroadcastReceiver mServiceBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();

            if (CAMERA_ACTION.equals(action)) {

                value_BR = intent.getIntExtra(KEY_RCBM_TO_SERVICE,
                        RCBM_DEFAULT_INT);

                switch (value_BR) {

                case RCBM_TO_SERVICE_STOP_CAMERA:
                    mServiceHandler.obtainMessage(
                            MSG_FROM_CAMERA_TO_SERVICE_STOP_CAMERA)
                            .sendToTarget();
                    break;

                case RCBM_TO_SERVICE_TAKE_PICTURE_FAILE:
                    mServiceHandler
                            .sendEmptyMessage(MSG_FROM_CAMERA_TO_SERVICE_TAKE_PICKTURE_FAILE);
                    break;

                case RCBM_TO_SERVICE_TAKE_PICTURE_SUCCESS:
                    picData = intent.getByteArrayExtra("picture_data_bytes");
                    mServiceHandler
                            .sendEmptyMessage(MSG_FROM_CAMERA_TO_SERVICE_TAKE_PICKTURE_SUCCESS);
                    break;

                case RemoteCameraTransactorModel.MSG_WATCH_EXIT_CAMERA:
                    mServiceHandler
                            .sendEmptyMessage(RemoteCameraTransactorModel.MSG_WATCH_EXIT_CAMERA);
                    break;
                case RemoteCameraTransactorModel.MSG_WATCT_LOCK_SCREEN:
                    mServiceHandler
                            .sendEmptyMessage(RemoteCameraTransactorModel.MSG_WATCT_LOCK_SCREEN);
                    break;

                default:
                    break;
                }
            } else if ("android.intent.action.SCREEN_OFF".equals(action)) {
                screen_on_off = false;
            } else if ("android.intent.action.SCREEN_ON".equals(action)) {
                screen_on_off = true;
            }

        }
    };

    // =========================================
    // 自定义的方法
    // =========================================
    private void startPacketSendThread() {
        if (packetSendRunnableRunning) {
            return;
        }
        packetSendRunnableRunning = true;

        if (mPacketSendRunnable == null) {
            mPacketSendRunnable = new PacketSendRunnable();
        }

        new Thread(mPacketSendRunnable).start();
    }

    private class PacketSendRunnable implements Runnable {

        @Override
        public void run() {

            try {
                while (!Thread.currentThread().isInterrupted()
                        && packetSendRunnableRunning) {
                    if (packetSendRunnableRunning) {
                        byte[] frame = PreviewFrameQueue
                                .dequeueCompressedQueue();

                        CameraFrameInfo frameInfo = new CameraFrameInfo();
                        frameInfo.frameData = frame;

                        if (frame != null && sendResult) {
                            mServiceHandler
                                    .removeMessages(MSG_CHECK_START_ACTIVITY_FAILED);
                            mTransactionModel.send(frameInfo);
                            sendResult = false;
                        }
                    }
                }
            } catch (Exception e) {
                IwdsLog.e(PhoneCameraService.this,
                        "PacketSendRunnable.run{} ---> " + e);
            } finally {
                packetSendRunnableRunning = false;
                PreviewFrameQueue.clearAll();
            }
        }

    };

}
