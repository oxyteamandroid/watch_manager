/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhiqiang.zhou<zhiqiang.zhou@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.speech;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;

import com.ingenic.watchmanager.R;

public class SpeechShowOnPhoneActivity extends Activity {

    private WebView mShowWebView;
    private Button mBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speech);
        mBackButton = (Button) findViewById(R.id.speech_brower_back);
        mShowWebView = (WebView) findViewById(R.id.speech_show_on_phone);
        WebSettings webSettings = mShowWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setDomStorageEnabled(true);
        mShowWebView.setWebViewClient(new WebViewClient());
        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
        mShowWebView.loadUrl(url);
        mBackButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                if (mShowWebView.canGoBack()) {
                    mShowWebView.goBack();
                } else {
                    SpeechShowOnPhoneActivity.this.finish();
                }

            }
        });
    }

    private class WebViewClient extends android.webkit.WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return false;

        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        String url = intent.getStringExtra("url");
        mShowWebView.loadUrl(url);
        super.onStart();
        super.onNewIntent(intent);
    }

}
