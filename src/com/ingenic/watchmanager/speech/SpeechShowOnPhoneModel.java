/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhiqiang.zhou<zhiqiang.zhou@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.speech;

import android.content.Context;
import android.content.Intent;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactorCallback;
import com.ingenic.watchmanager.util.UUIDS;

/**
 * SpeechShowOnPhoneModel 接收手表发来的url， 启动SpeechShowOnPhoneActivity，
 * 并将url发送给SpeechShowOnPhoneActivity
 * @see DataTransactorCallback
 * @see SpeechShowOnPhoneActivity
 */
public class SpeechShowOnPhoneModel implements DataTransactorCallback {

    private static SpeechShowOnPhoneModel mInstance = null;
    private static DataTransactor mDataTransactor = null;
    public static final String RECEIVERSTOP = "stop_activity";
    private Context mContext;

    public static SpeechShowOnPhoneModel getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SpeechShowOnPhoneModel(context);
        }
        return mInstance;
    }

    public SpeechShowOnPhoneModel(Context context) {
        mContext = context;
        if (mDataTransactor == null) {
            mDataTransactor = new DataTransactor(mContext, this, UUIDS.SPEECH);
        }
    }

    public void startDataTransactor() {
        if (mDataTransactor != null)
            mDataTransactor.start();
    }

    public void stopDataTransactor() {
        if (mDataTransactor != null)
            mDataTransactor.stop();
    }

    private void sendDataTransactor(String cmd) {
        if (mDataTransactor != null)
            mDataTransactor.send(cmd);
    }

    @Override
    public void onChannelAvailable(boolean arg0) {
    }

    @Override
    public void onDataArrived(Object arg0) {
        String url = (String) arg0;
        startShowActivity(url);
    }

    /**
     * startShowActivity 启动SpeechShowOnPhoneActivity 并将url传递给要显示url
     * @param url
     *            手表要显示的url
     */
    private void startShowActivity(String url) {

        Intent intent = new Intent(mContext, SpeechShowOnPhoneActivity.class);
        intent.putExtra("url", url);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.getApplicationContext().startActivity(intent);

    }

    @Override
    public void onLinkConnected(DeviceDescriptor arg0, boolean arg1) {
    }

    @Override
    public void onRecvFileProgress(int arg0) {
    }

    @Override
    public void onSendFileProgress(int arg0) {
    }

    @Override
    public void onSendResult(DataTransactResult arg0) {
    }

    @Override
    public void onSendFileInterrupted(int index) {

    }

    @Override
    public void onRecvFileInterrupted(int index) {

    }

}