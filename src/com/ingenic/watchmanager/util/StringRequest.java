/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.app.Fragment;
import android.content.Context;

public class StringRequest extends Request<String> {

    public static final String MARKET_IP = "http://183.57.41.90:8810";

    public static final String UUID = "46AEE255-CB73-BF17-B60E-208B2C7E";

    public StringRequest(Context con, String url, Listener<String> lsn, ErrorListener elsn) {
        super(con, url, lsn, elsn);
    }

    public StringRequest(Fragment con, String url, Listener<String> lsn, ErrorListener elsn) {
        super(con, url, lsn, elsn);
    }

    public StringRequest(android.support.v4.app.Fragment con, String url, Listener<String> lsn, ErrorListener elsn) {
        super(con, url, lsn, elsn);
    }

    @Override
    public String handleInputStream(InputStream is) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int len = -1;
        byte[] buf = new byte[1024];
        while ((len = is.read(buf)) != -1 && !isCancel()) {
            baos.write(buf, 0, len);
        }
        if (isCancel()) {
            baos.close();
            return "";
        }

        baos.flush();
        String result = baos.toString();
        baos.close();
        return result;
    }

    public static class Range {
        private int start = 0;
        private int block = 10;
        private int count = 0;

        public Range(int s, int b) {
            start = s;
            block = b;
        }

        public Range() {
        }

        public void success() {
            count++;
        }

        public int getStart() {
            return start + count * block + 1;
        }

        public int getEnd() {
            return start + (1 + count) * block;
        }
    }
}