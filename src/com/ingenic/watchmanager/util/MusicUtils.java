package com.ingenic.watchmanager.util;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import com.ingenic.watchmanager.settings.SettingsFragment;

public class MusicUtils {

    public final static String REMOTE_MUSIC_PROPTIES = "remote_music_propties";
    public final static String PACKAGE_NAME = "packagename";
    public final static String APP_NAME = "appName";
    public final static String NO_PLAYER = "NOPlayer";

    public final static String MEDIA_BUTTON = "android.intent.action.MEDIA_BUTTON";

    public static ArrayList<String> mEntryValuesPackageName = new ArrayList<String>();
    public static ArrayList<String> mEntryLabel = new ArrayList<String>();
    public static ArrayList<String> mCanControlMusic = new ArrayList<String>();

    /**
     * 设置默认音乐播放器
     */
    public static void setDefaultMusicPlayer(Context context) {
        mCanControlMusic.clear();
        for (String musicpkg : SettingsFragment.MUSIC_PKG_LIST) {
            mCanControlMusic.add(musicpkg);
        }

        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> rs = pm.queryBroadcastReceivers(new Intent(
                MEDIA_BUTTON), 96);
        if (rs == null || rs.size() <= 0)
            return;

        mEntryValuesPackageName.clear();
        mEntryLabel.clear();
        for (int i = 0; i < rs.size(); i++) {
            if (mCanControlMusic
                    .contains(rs.get(i).activityInfo.applicationInfo.packageName)) {
                mEntryValuesPackageName
                        .add(rs.get(i).activityInfo.applicationInfo.packageName);
                mEntryLabel.add(rs.get(i).loadLabel(pm).toString());
            }
        }

        if (mEntryValuesPackageName.size() <= 0)
            return;

        SharedPreferences sharedPreferences = context.getSharedPreferences(
                REMOTE_MUSIC_PROPTIES, Context.MODE_PRIVATE);
        String packageName = sharedPreferences.getString(PACKAGE_NAME,
                NO_PLAYER);

        if (mEntryValuesPackageName.contains(packageName))
            return;

        if (mEntryValuesPackageName.contains(SettingsFragment.GOOGLE_MUSIC)) {
            Editor editor = sharedPreferences.edit();

            editor.putString(PACKAGE_NAME, SettingsFragment.GOOGLE_MUSIC);
            editor.putString(APP_NAME, mEntryLabel.get(mEntryValuesPackageName
                    .indexOf(SettingsFragment.GOOGLE_MUSIC)));

            editor.commit();
        } else {
            Editor editor = sharedPreferences.edit();

            editor.putString(PACKAGE_NAME, mEntryValuesPackageName.get(0));
            editor.putString(APP_NAME, mEntryLabel.get(0));

            editor.commit();
        }
    }
}
