package com.ingenic.watchmanager.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.renderscript.RenderScript.Priority;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

public class ImageRequest extends Request<Bitmap> {

    private boolean mIsSave;
    private String mPicturePath;

    public ImageRequest(Context con, String url, Request.Listener<Bitmap> lsn, ErrorListener elsn) {
        this(con, url, lsn, elsn, false);
    }

    public ImageRequest(Fragment con, String url, Listener<Bitmap> lsn, ErrorListener elsn) {
        super(con, url, lsn, elsn);
    }

    public ImageRequest(android.support.v4.app.Fragment con, String url, Listener<Bitmap> lsn, ErrorListener elsn) {
        super(con, url, lsn, elsn);
    }

    public ImageRequest(Context con, String url, Request.Listener<Bitmap> lsn, ErrorListener elsn, Boolean isSave) {
        super(con, url, lsn, elsn);
        mIsSave = isSave;
        if (mIsSave && null != url) {
            mPicturePath = getPicPath(url);
            if (new File(mPicturePath).exists()) {
                mResponse = BitmapFactory.decodeFile(mPicturePath);
                if (null != mResponse) {
                    Log.d(this.getClass().getSimpleName(), "pic cache hint!   url: " + mUrl);
                }
            }
        }
    }

    @Override
    public Bitmap handleInputStream(InputStream is) throws IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int len = -1;
        byte[] buf = new byte[1024];
        if (!mIsSave) {
            while ((len = is.read(buf)) != -1 && !isCancel()) {
                baos.write(buf, 0, len);
            }
        } else {
            File file = new File(mPicturePath);
            if (!file.exists()) {
                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                    file.createNewFile();
                }
            }
            OutputStream os = new FileOutputStream(mPicturePath);
            while ((len = is.read(buf)) != -1 && !isCancel()) {
                baos.write(buf, 0, len);
                os.write(buf, 0, len);
            }
            os.flush();
            os.close();
        }
        baos.flush();
        byte[] bytes = baos.toByteArray();
        baos.close();
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public static String getPicName(String url) {
        int index = url.lastIndexOf("/");
        return url.substring(index);
    }

    public static String getPicPath(String url) {
        return ROOT_PATH + SECOND_PATH + getPicName(url);
    }

    public static final String ROOT_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static final String SECOND_PATH = "/watchmanager/Images";

    @Override
    public Priority getPriority() {
        return Priority.LOW;
    }

    public interface ImageCache {

        public Bitmap getBitmap(String url);

        public void putBitmap(String url, Bitmap bitmap);

    }

    public static void loadingImage(Context context, final ImageView view, String url) {
        if (TextUtils.isEmpty(url))
            return;
        ImageRequest request = new ImageRequest(context, url, new Listener<Bitmap>() {

            @Override
            public void onResponse(Bitmap response) {
                view.setImageBitmap(response);
            }
        }, null, true);
        request.start();
    }
}
