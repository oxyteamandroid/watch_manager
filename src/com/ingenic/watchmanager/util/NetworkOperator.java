/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.json.JSONObject;

import android.content.Context;
import android.net.Uri;

public class NetworkOperator {

    private static final String DEFAULT_METHOD = "v1";
    private static final int MINIMUN_TIMEOUT = 1000;
    private static final int DEFAULT_TIMEOUT = 10000;
    private static final int VERSION_CODE = 1;
    private static final String MARKET_IP = "http://183.57.41.90:8810";

    private String mMethod = null;
    private Context mContext;
    private boolean mIsCertified = false;
    private boolean mCertifySuccess = false;

    public NetworkOperator(Context context) {
        mContext = context;
    }

    private String requestMethod(int versionCode) {
        String result = httpGet(MARKET_IP + "/version/versionCode/?code=" + versionCode);
        String method = DEFAULT_METHOD;

        try {
            JSONObject json = new JSONObject(result);
            method = json.getString("version");
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMethod = method;
        return method;
    }

    private String httpGet(String url) {
        return httpGet(url, DEFAULT_TIMEOUT);
    }

    private String httpGet(String url, int timeout) {
        String result = null;
        BufferedReader reader = null;

        try {
            HttpClient client = new DefaultHttpClient();
            timeout = Math.max(timeout, MINIMUN_TIMEOUT);
            client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout);
            client.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, timeout);

            HttpGet request = new HttpGet();
            URI uri = new URI(url);
            request.setURI(uri);
            HttpResponse response = client.execute(request);
            reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer buffer = new StringBuffer();
            String line = null;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }

            result = buffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                    reader = null;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }

        return result;
    }

    /**
     * 检查网络是否可用
     */
    public boolean checkNetwork() {
        boolean result = Utils.isNetworkConnected(mContext);
        return result;
    }

    public String request(String method, String[] params, String[] values) {
        return request(method, params, values, DEFAULT_TIMEOUT);
    }

    /**
     * 请求
     * 
     * @param method 方法名，不需要带参数、域名和方法地址
     * @param params 参数名称数组
     * @param values 参数值数组
     */
    public String request(String method, String[] params, String[] values, int timeout) {
        method += "/?";

        if (params == null) return request(method);

        if (values == null || params.length != values.length) {
            return null;
        }

        int paramCount = params.length;
        for (int i = 0; i < paramCount; i++) {
            method += params[i] + "=";
            method += values[i] + (i == params.length - 1 ? "" : "&");
        }

        return request(method, timeout);
    }

    /**
     * 请求
     * 
     * @param method 方法，带参数，不需要带域名和方法地址
     */
    public String request(String method) {
        return request(method, DEFAULT_TIMEOUT);
    }

    /**
     * 请求
     * 
     * @param method 方法， 带参数，不需要带域名和方法地址
     * @param timeout 超时
     */
    public String request(String method, int timeout) {
        boolean network = checkNetwork();
        if (!network) return null;

        if (!certify(Utils.getRemoteUuid(mContext))) {
            return null;
        }

        String result = httpGet(MARKET_IP + "/" + mMethod + method, timeout);
        return result;
    }

    /**
     * 验证设备
     * 
     * @param uuid 设备uuid
     */
    public boolean certify(String uuid) {
        if (mIsCertified) return mCertifySuccess;

        mIsCertified = true;

        if (uuid == null) return false;

        if (mMethod == null) {
            requestMethod(VERSION_CODE);
        }

        if (mMethod == null) return false;

        String result = httpGet(MARKET_IP + "/" + mMethod + "/dev/certify/?uuid=" + Uri.parse(uuid));
        int resultCode = -1;
        try {
            JSONObject json = new JSONObject(result);
            resultCode = json.getInt("resultCode");
        } catch (Exception e) {
            e.printStackTrace();
        }

        mCertifySuccess = resultCode == 1;
        return mCertifySuccess;
    }
}
