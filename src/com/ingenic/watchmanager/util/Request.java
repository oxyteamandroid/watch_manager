package com.ingenic.watchmanager.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import android.app.Fragment;
import android.content.Context;
import android.os.Handler;
import android.renderscript.RenderScript.Priority;

import com.ingenic.iwds.utils.IwdsLog;

public abstract class Request<T> implements Runnable, Comparable<Request<T>> {

    private static final Map<Object, Set<Request<?>>> repository = new LinkedHashMap<Object, Set<Request<?>>>();

    protected String mUrl;

    protected ErrorListener mErrorListener;
    protected Listener<T> mListener;

    private volatile Boolean mCancel;
    protected Context mContext;
    protected Handler mHandler;

    protected RequestQueue mQueue;

    protected int mSequence;

    protected T mResponse;

    protected HttpURLConnection mConn;

    private InputStream mInputStream;

    private Thread mThread;

    protected static final int TIMEOUT_IN_MILLIONS = 5000;

    private static final String TAG = Request.class.getSimpleName();

    public static interface ErrorListener {
        public void onErrorResponse(Exception ex);
    }

    public static interface Listener<T> {
        public void onResponse(T response);
    }

    public void cancel() {
        this.mCancel = true;
    }

    public boolean isCancel() {
        return mCancel;
    }

    public static void cancelAll(Object obj) {
        Set<Request<?>> set = repository.remove(obj);
        if (null != set) {
            int count = 0;
            for (Request<?> it : set) {
                it.cancel();
                count++;
            }
            IwdsLog.d(TAG, "cancel count: " + count);
            set.clear();
        } else {
            // IwdsLog.d(TAG, "cancel count: 没有!");
        }
    }

    public Request(Context con, String url, Listener<T> lsn, ErrorListener elsn) {
        this.mUrl = url;
        this.mListener = lsn;
        this.mErrorListener = elsn;
        this.mCancel = false;
        this.mContext = con;
        this.mHandler = new Handler(mContext.getMainLooper());
        addRepository(con);
    }

    public Request(Fragment con, String url, Listener<T> lsn, ErrorListener elsn) {
        this.mUrl = url;
        this.mListener = lsn;
        this.mErrorListener = elsn;
        this.mCancel = false;
        this.mContext = con.getActivity();
        this.mHandler = new Handler(mContext.getMainLooper());
        addRepository(con);
    }

    public Request(android.support.v4.app.Fragment con, String url, Listener<T> lsn, ErrorListener elsn) {
        this.mUrl = url;
        this.mListener = lsn;
        this.mErrorListener = elsn;
        this.mCancel = false;
        this.mContext = con.getActivity();
        this.mHandler = new Handler(mContext.getMainLooper());
        addRepository(con);
    }

    private void addRepository(Object con) {
        Set<Request<?>> set = repository.get(con);
        if (null == set) {
            set = new HashSet<Request<?>>();
            repository.put(con, set);
        }
        set.add(this);
        IwdsLog.d(TAG, "addRepository count: ++ " + 1 + " obj: " + con.hashCode());
    }

    @SuppressWarnings("unchecked")
    public void setResponse(Object response) {
        mResponse = (T) response;
    }

    @Override
    public void run() {
        try {
            if (isCancel()) {
                return;
            }
            if (null == mResponse) {
                mInputStream = getInputStreamByUrl();
                mResponse = handleInputStream(mInputStream);
            }

            callSubOnResponse(mResponse);

        } catch (Exception e) {

            callOnErrorResponse(e);

        } finally {
            if (null != mInputStream) {
                try {
                    mInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != mConn) {
                mConn.disconnect();
            }

            mHandler.post(new Runnable() {

                @Override
                public void run() {

                    Set<Request<?>> set = repository.get(mContext);
                    if (null != set) {
                        set.remove(Request.this);
                        if (set.isEmpty()) {
                            repository.remove(mContext);
                        }
                    }
                }
            });

            if (null != mQueue) {
                mQueue.finish(this);
            }
        }
    }

    public void start() {

        mThread = new Thread(this);
        mThread.start();
    }

    protected abstract T handleInputStream(InputStream is) throws IOException;

    private void callSubOnResponse(final T response) {
        if (null != mListener && !isCancel()) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (!isCancel())
                        mListener.onResponse(response);
                }
            });
        }
    }

    private void callOnErrorResponse(final Exception e) {
        if (null != mErrorListener && !isCancel()) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (!isCancel())
                        mErrorListener.onErrorResponse(e);
                }
            });
        }
    }

    private InputStream getInputStreamByUrl() throws IOException {
        URL url = new URL(mUrl);
        mConn = (HttpURLConnection) url.openConnection();
        mConn.setConnectTimeout(TIMEOUT_IN_MILLIONS);
        mConn.setReadTimeout(TIMEOUT_IN_MILLIONS);
        mConn.setRequestMethod("GET");
        mConn.setRequestProperty("accept", "*/*");
        mConn.setRequestProperty("connection", "Keep-Alive");
        if (200 != mConn.getResponseCode())
            throw new IOException("ResponseCode: " + mConn.getResponseCode());
        return mConn.getInputStream();
    }

    public void setRequestQueue(RequestQueue requstQueue) {
        mQueue = requstQueue;
    }

    public void setSequence(int sequenceNumber) {
        mSequence = sequenceNumber;
    }

    public int getSequence() {
        return mSequence;
    }

    @Override
    public int compareTo(Request<T> other) {
        Priority left = this.getPriority();
        Priority right = other.getPriority();

        return left == right ? this.mSequence - other.mSequence : right.ordinal() - left.ordinal();
    }

    public Priority getPriority() {
        return Priority.NORMAL;
    }

    public String getCacheKey() {
        return mUrl;
    }

    public Object getResponse() {
        return mResponse;
    }

}