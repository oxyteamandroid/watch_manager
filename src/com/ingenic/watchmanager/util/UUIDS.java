/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.util;

public interface UUIDS {
    String WEATHER = "1dfe1c37-e619-2b74-4d09-03b56990a5fa";
    String NOTIFICATION = "5a177e43-82e6-d483-b7e3-d7072047e346";
    String HEALTH = "461fd36c-ab50-11e4-bac2-5404a6abe086";
    String SCHEDULE = "3b11571f-64eb-4085-1e5e-50fb6f0aff83";
    String REMOTEMUSIC = "B0569AA5-53BE-F627-F7E9-2F3012920595";
    String LOCALE = "735f9b94-c67b-5a2a-20d1-79c29ed7a300";
    String SYNCTIME = "bb1609ec-d29d-11e4-ab8f-5404a6abe086";
    String OTAFILE = "439536e2-cc6a-11e4-90da-5404a6abe086";
    String TIMEADDITIONAL = "5ad8fcb6-46c7-3c03-62f6-3a1c414d9a93";
    String PONESTATE = "3e164e8e-57c9-0e70-5635-04059ed9c5ee";
    String CONTACTS = "c654f802-de98-11e4-8feb-5404a6abe086";
    String SMS = "fcb3bc51-f75a-eb06-ee1a-2907f966e1a5";
    String CALLLOG = "90cc8a7a-fb8e-11e4-a9c3-5404a6abe086";
    String FIND_PHONE = "D1E386E3-6BBF-40E0-BF84-E0C41F32B6E7";
    String SPEECH="c3da6bfc-329f-4aa9-8c79-9f4007888e0d";
}
