/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.util;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class TUtils {

    /**
     * 弹出 toast
     * 
     * @param context
     * @param stringMsg
     */
    public static void showToast(Context context, int stringMsg) {
        Toast.makeText(context, context.getString(stringMsg), Toast.LENGTH_SHORT).show();
    }

    public static void showToast(Context context, String content) {
        if (content == null)
            return;
        Toast toast = Toast.makeText(context, content, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static void showToast(Context context, int res, int position) {
        if (context != null) {
            Toast toast = Toast.makeText(context, context.getResources().getString(res), Toast.LENGTH_SHORT);
            toast.setGravity(position, 0, 0);
            toast.show();
        }
    }

}
