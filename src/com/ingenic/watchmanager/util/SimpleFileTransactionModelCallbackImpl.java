/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.watchmanager.util;

import java.io.File;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.FileInfo;
import com.ingenic.iwds.datatransactor.FileTransactionModel.FileTransactionModelCallback;

public class SimpleFileTransactionModelCallbackImpl implements FileTransactionModelCallback {

    @Override
    public void onRequestSendFile(FileInfo info) {

    }

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {

    }

    @Override
    public void onChannelAvailable(boolean isAvailable) {

    }

    @Override
    public void onSendResult(DataTransactResult result) {

    }

    @Override
    public void onFileArrived(File file) {

    }

    @Override
    public void onSendFileProgress(int progress) {

    }

    @Override
    public void onRecvFileProgress(int progress) {

    }

    @Override
    public void onConfirmForReceiveFile() {

    }

    @Override
    public void onCancelForReceiveFile() {

    }

    @Override
    public void onFileTransferError(int errorCode) {

    }

    @Override
    public void onSendFileInterrupted(int index) {

    }

    @Override
    public void onRecvFileInterrupted(int index) {

    }

}