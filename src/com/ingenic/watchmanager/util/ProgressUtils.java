/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.ingenic.watchmanager.R;

public class ProgressUtils {

    private static final int defaultResId = android.R.id.content;
    private Listener mListener;
    private Listener2 mListener2;

    private View progress;
    private View refresh;
    private ViewGroup mViewGroup;

    private Activity mActivity;
    private View mProgressView;
    protected Toast mToast;

    private boolean mIsNetwork;

    private ProgressUtils(Activity activity, Listener listener, int resId) {
        this(activity, activity.findViewById(defaultResId), listener);
    }

    @SuppressLint("ShowToast")
    public ProgressUtils(Activity activity, View viewGroup, Listener listener) {
        mViewGroup = (ViewGroup) viewGroup;
        mListener = listener;
        mActivity = activity;
        LayoutInflater inflater = LayoutInflater.from(mActivity);
        mProgressView = inflater.inflate(R.layout.waiting_progress, mViewGroup, false);
        mToast = Toast.makeText(mActivity, "", Toast.LENGTH_SHORT);
        mIsNetwork = true;
    }

    public ProgressUtils(Activity activity, View viewGroup, Listener2 listener) {
        mViewGroup = (ViewGroup) viewGroup;
        mListener2 = listener;
        mActivity = activity;
    }

    public ProgressUtils(Activity activity, Listener listener) {
        this(activity, listener, defaultResId);
    }

    public ProgressUtils(Activity activity) {
        this(activity, (Listener) activity, defaultResId);
    }

    public void start() {

        if (null != mProgressView.getParent())
            return;

        if (mActivity.findViewById(defaultResId) == mViewGroup || mViewGroup instanceof FrameLayout) {
            mViewGroup.addView(mProgressView);
        } else {
            mViewGroup.addView(mProgressView, 0);
        }

        progress = mProgressView.findViewById(R.id.progress);
        refresh = mProgressView.findViewById(R.id.refresh);

        refresh.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (checkNetwork()) {
                    if (null != mListener) {
                        mListener.request();
                    }
                    toggle(true);
                } else {
                    mToast.setText(mActivity.getResources().getString(R.string.network_error));
                    mToast.setDuration(Toast.LENGTH_SHORT);
                    mToast.show();
                }

            }
        });

        if (mIsNetwork && !checkNetwork()) {
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.network_error), Toast.LENGTH_SHORT)
                    .show();
        }
        if (null != mListener) {
            mListener.request();
        } else if (null != mListener2) {
            mListener2.request(ProgressUtils.this);
        }
    }

    public boolean checkNetwork() {
        Context context = mActivity.getApplicationContext();
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = connectivityManager.getAllNetworkInfo();
        for (int i = 0; i < networkInfo.length; i++) {
            if (networkInfo[i].getState() == NetworkInfo.State.CONNECTED) {
                return true;
            }
        }
        return false;
    }

    protected void toggle(boolean status) {
        if (status) {
            progress.setVisibility(View.VISIBLE);
            refresh.setVisibility(View.GONE);
        } else {
            progress.setVisibility(View.GONE);
            refresh.setVisibility(View.VISIBLE);
        }
    }

    public void fail() {
        toggle(false);
    }

    public void end() {
        if (null != mViewGroup.findViewById(R.id.progressUtils))
            mViewGroup.removeView(mProgressView);
    }

    public static interface Listener {
        public void request();
    }

    public static interface Listener2 {
        public void request(ProgressUtils pu);
    }

    public void setColor(int i) {
        mProgressView.setBackgroundColor(i);
    }

    public void setIsNetwork(boolean b) {
        mIsNetwork = b;
    }

}