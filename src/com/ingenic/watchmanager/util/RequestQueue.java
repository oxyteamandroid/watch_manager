package com.ingenic.watchmanager.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import android.util.Log;

public class RequestQueue {

    private static final int DEFAULT_NETWORK_THREAD_COUNT = 1;

    private AtomicInteger mCurrentConcurrentCount;
    private int mMaxConcurrentCount;

    private AtomicInteger mSequenceGenerator;

    private final PriorityBlockingQueue<Request<?>> mNetworkQueue = new PriorityBlockingQueue<Request<?>>();

    private final Map<String, Queue<Request<?>>> mWaitingRequests = new HashMap<String, Queue<Request<?>>>();

    private final Set<Request<?>> mCurrentRequests = new HashSet<Request<?>>();

    private final Executor mExecutor = Executors.newCachedThreadPool();

    public RequestQueue() {
        this(DEFAULT_NETWORK_THREAD_COUNT);
    }

    public RequestQueue(int count) {
        mSequenceGenerator = new AtomicInteger();
        mCurrentConcurrentCount = new AtomicInteger();
        mMaxConcurrentCount = count;
    }

    public void add(Request<?> request) {
        request.setSequence(mSequenceGenerator.incrementAndGet());
        request.setRequestQueue(this);

        synchronized (mCurrentRequests) {

            mCurrentRequests.add(request);
            String cacheKey = request.getCacheKey();

            if (mWaitingRequests.containsKey(cacheKey)) {
                Queue<Request<?>> stagedRequests = mWaitingRequests.get(cacheKey);
                if (stagedRequests == null) {
                    stagedRequests = new LinkedList<Request<?>>();
                }
                stagedRequests.add(request);
                mWaitingRequests.put(cacheKey, stagedRequests);

            } else {

                mNetworkQueue.add(request);
                mWaitingRequests.put(cacheKey, null);
            }
        }

        Log.d(this.getClass().getSimpleName(), "add --> net:" + mNetworkQueue.size() + ",wait:" + mWaitingRequests.size() + ",cureent:"
                + mCurrentRequests.size() + " con:" + mCurrentConcurrentCount);
        start();
    }

    public synchronized void start() {
        if (mCurrentConcurrentCount.get() < mMaxConcurrentCount) {
            Request<?> request = null;
            request = mNetworkQueue.poll();
            if (null != request) {
                mCurrentConcurrentCount.incrementAndGet();
                // request.start();
                mExecutor.execute(request);
            }
        }
    }

    public void finish(Request<?> request) {

        synchronized (mCurrentRequests) {
            mCurrentRequests.remove(request);

            String cacheKey = request.getCacheKey();
            Queue<Request<?>> waitingRequests = mWaitingRequests.remove(cacheKey);
            if (waitingRequests != null) {
                for (Request<?> req : waitingRequests) {
                    req.setResponse(request.getResponse());
                }
                mNetworkQueue.addAll(waitingRequests);
            }
        }
        mCurrentConcurrentCount.decrementAndGet();

        Log.d(this.getClass().getSimpleName(), "finish --> net:" + mNetworkQueue.size() + ",wait:" + mWaitingRequests.size() + ",cureent:"
                + mCurrentRequests.size() + " con:" + mCurrentConcurrentCount);

        start();
    }

    public void cancelAll() {
        synchronized (mCurrentRequests) {
            for (Request<?> request : mCurrentRequests) {
                request.cancel();
            }
            mWaitingRequests.clear();
            mCurrentRequests.clear();
        }
    }
}