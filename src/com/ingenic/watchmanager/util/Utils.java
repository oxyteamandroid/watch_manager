/*
 * Copyright (C) 2014 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.watchmanager.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.iwds.datatransactor.elf.ScheduleInfo;
import com.ingenic.iwds.datatransactor.elf.ScheduleInfo.Event;
import com.ingenic.iwds.datatransactor.elf.WeatherInfoArray;
import com.ingenic.watchmanager.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class Utils {
    public static final String OPEN_BT_TIP = "openbluetooth";
    public static final String SHARE_PREFERENCE = "watch-manager";
    public static final String NOTICE_SHARE_PREFERENCE = "notice_disable";
    public static final String CITY_SHARE_PREFERENCE = "offen_citys";
    public static final String SP_THEME_COLOR = "theme_color";
    public static final String SP_FIRST_USE = "first";
    public static final String SP_THEME_BACK_COLOR = "theme_back_color";
    public static final int DEFAULT_THEME_COLOR = 0xFF00ACFF;
    public static final int DEFAULT_THEME_BACK_COLOR = 0xFFFFFFFF;
    public static final String HAS_NEW_OTA_ACTION = "com.ingenic.action.HAS_NEW_OTA";
    public static final String ACTION_BACKGROUND_COLOR_CHANGED = "com.ingenic.watchmanager.action.BACKGROUND_COLOR_CHANGED";
    public static final String EXTRA_BACKGROUND_COLOR = "background_color";
    public static final DisplayImageOptions DEFAULT_OPTIONS = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.drawable.ic_launcher).showImageForEmptyUri(R.drawable.ic_launcher)
            .showImageOnFail(R.drawable.ic_launcher).cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
            .displayer(new RoundedBitmapDisplayer(20)).build();

    /**
     * 用于保存已连接设备的地址。
     */
    public static final String SP_CONNECTED_DEVICE = "device_address";

    /**
     * 已连接设备地址在保存的数据中对应的键。
     */
    public static final String KEY_DEVICE_ADDRESS = "deviceAddress";

    /**
     * 已连接设备的模型属性在保存的数据中对应的键。
     */
    public static final String KEY_MODEL = "model";

    /**
     * 已连接设备的制造者属性在保存的数据中对应的键。
     */
    public static final String KEY_FACTURE = "facture";

    /**
     * 用于保存通讯模块相关的数据。
     */
    public static final String SP_CONTACT = "contact";

    /**
     * 保存的数据中，短信同步开关状态对应的键。
     */
    public static final String KEY_SMS = "sms";

    /**
     * 保存的数据中，来电提醒开关状态对应的键。
     */
    public static final String KEY_PHONE = "phone";

    private static final HashMap<Locale, Integer> LOCALE_MAP = new HashMap<Locale, Integer>();

    private final static Map<Integer, Container> unitMap = new HashMap<Integer, Container>();

    static {
        LOCALE_MAP.put(Locale.ENGLISH, 1);
        LOCALE_MAP.put(Locale.FRENCH, 2);
        LOCALE_MAP.put(Locale.GERMAN, 3);
        LOCALE_MAP.put(Locale.ITALIAN, 4);
        LOCALE_MAP.put(Locale.JAPANESE, 5);
        LOCALE_MAP.put(Locale.KOREAN, 6);
        LOCALE_MAP.put(Locale.SIMPLIFIED_CHINESE, 7);
        LOCALE_MAP.put(Locale.TRADITIONAL_CHINESE, 8);

        unitMap.put(7, new Container(7, 10000, 10000 * 10000, 10000 * 10000 * 10000, "万", "亿", "万亿"));
        unitMap.put(8, new Container(8, 10000, 10000 * 10000, 10000 * 10000 * 10000, "萬", "億", "萬億"));
        unitMap.put(1, new Container(1, 1000, 1000 * 1000, 1000 * 1000 * 1000, "Ｋ", "Ｍ", "Ｂ"));
    }

    public static String decodeAddress(String text) {
        if (!text.contains("-")) {
            return text;
        }
        String[] decode = text.split("-");
        if (decode[1] == null) {
            return text;
        }
        char[] keys = decode[1].toCharArray();
        char[] cs = decode[0].toCharArray();
        for (int i = 0; i < cs.length; i++) {
            if (cs.length > i && keys.length > i)
                cs[i] -= keys[i];
            else
                continue;
        }
        String result = new String(cs);
        return result;
    }

    // public static void setThemeColor(Context context, int color) {
    // SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCE,
    // Context.MODE_PRIVATE);
    // sp.edit().putInt(SP_THEME_COLOR, color).commit();
    // }

    // public static int getThemeColor(Context context) {
    // SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCE,
    // Context.MODE_PRIVATE);
    // return sp.getInt(SP_THEME_COLOR, DEFAULT_THEME_COLOR);
    // }

    // public static void setThemeBackColor(Context context, int color) {
    // SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCE,
    // Context.MODE_PRIVATE);
    // sp.edit().putInt(SP_THEME_BACK_COLOR, color).commit();
    //
    // Intent it = new Intent(ACTION_BACKGROUND_COLOR_CHANGED);
    // context.sendBroadcast(it);
    // }

    /***
     * @author guanghua.shi
     * @param b
     * @return
     */
    public static Bitmap bytes2Bitmap(byte[] b) {
        if (b != null && b.length != 0) {
            return BitmapFactory.decodeByteArray(b, 0, b.length);
        } else {
            return null;
        }
    }

    private static final char HEX_DIGITS[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D',
            'E', 'F' };

    public static String toHexString(byte[] b) {
        StringBuilder sb = new StringBuilder(b.length * 2);
        for (int i = 0; i < b.length; i++) {
            sb.append(HEX_DIGITS[(b[i] & 0xf0) >>> 4]);
            sb.append(HEX_DIGITS[b[i] & 0x0f]);
        }
        return sb.toString().toLowerCase();
    }

    /**
     * 获取单个文件的MD5值！
     * 
     * @author zengyu.zhan
     * @param file
     * @return
     */

    public static String getFileMD5(File file) {
        if (file == null || !file.isFile()) {
            return null;
        }
        MessageDigest digest = null;
        FileInputStream in = null;
        byte buffer[] = new byte[1024];
        int len;
        try {
            digest = MessageDigest.getInstance("MD5");
            in = new FileInputStream(file);
            while ((len = in.read(buffer, 0, 1024)) != -1) {
                digest.update(buffer, 0, len);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        BigInteger bigInt = new BigInteger(1, digest.digest());

        String temp = bigInt.toString(16);
        for (int i = 0; i < 32 - temp.length(); i++)
            temp = "0" + temp;
        return temp;
    }

    /***
     * 根据原图,绘制圆形图片
     * 
     * @author guanghua.shi
     * @param bitmap
     * @param pixels
     * @return
     */
    public static Bitmap createCircleImage(Bitmap bitmap, int pixels) {

        if (bitmap != null) {
            return bitmap;
            /*
             * Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
             * 
             * Canvas canvas = new Canvas(output);
             * 
             * final int color = 0xff424242;
             * 
             * final Paint paint = new Paint();
             * 
             * final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
             * 
             * final RectF rectF = new RectF(rect);
             * 
             * final float roundPx = pixels;
             * 
             * paint.setAntiAlias(true);
             * 
             * canvas.drawARGB(0, 0, 0, 0);
             * 
             * paint.setColor(color);
             * 
             * canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
             * 
             * paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
             * 
             * canvas.drawBitmap(bitmap, rect, rect, paint); return output;
             */
        } else {
            return null;
        }

    }

    /**
     * @author guanghua.shi
     * @param drawable
     * @return
     */
    public static Bitmap drawableToBitmap(Drawable drawable) {

        if (drawable != null) {
            Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(),
                    drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
            Canvas canvas = new Canvas(bitmap);
            // canvas.setBitmap(bitmap);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            drawable.draw(canvas);
            return bitmap;
        } else {
            return null;
        }

    }

    /**
     * @author guanghua.shi
     * @param bm
     * @return
     */
    public static byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    // public static int getThemeBackColor(Context context) {
    // SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCE,
    // Context.MODE_PRIVATE);
    // return sp.getInt(SP_THEME_BACK_COLOR, DEFAULT_THEME_BACK_COLOR);
    // }

    public static Bitmap createWithColor(Bitmap bitmap, int color) {
        if (bitmap == null || color == 0) {
            return bitmap;
        }
        Bitmap result = bitmap.copy(bitmap.getConfig(), true);
        changeColor(result, color);
        bitmap.recycle();
        return result;
    }

    public static void changeColor(Bitmap bitmap, int color) {
        if (bitmap == null || color == 0) {
            return;
        }
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        // test by ic_launcher use time :10ms
        int[] pixels = new int[w * h];
        bitmap.getPixels(pixels, 0, w, 0, 0, w, h);
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                int c = pixels[w * i + j];
                pixels[w * i + j] = Color.argb(Color.alpha(c), Color.red(color), Color.green(color), Color.blue(color));
            }
        }
        bitmap.setPixels(pixels, 0, w, 0, 0, w, h);
        // test by ic_launcher use time :32ms
        // for (int i = 0; i < w; i++) {
        // for (int j = 0; j < h; j++) {
        // int c = result.getPixel(i, j);
        // if (c != 0) {
        // int newColor = Color.argb(Color.alpha(c), Color.red(color),
        // Color.green(color), Color.blue(color));
        // result.setPixel(i, j, newColor);
        // }
        // }
        // }
    }

    public static Object getInstance(String className, Class<?>[] paramsClasses, Object... params) throws Exception {
        Class<?> cls = Class.forName(className);
        return getInstance(cls, paramsClasses, params);
    }

    public static Object getInstance(Class<?> cls, Class<?>[] paramsClasses, Object... params) throws Exception {
        if (!checkParams(paramsClasses, params)) {
            throw new IllegalArgumentException();
        }
        Constructor<?> constructor = cls.getDeclaredConstructor(paramsClasses);
        constructor.setAccessible(true);
        return constructor.newInstance(params);
    }

    private static boolean checkParams(Class<?>[] clss, Object... params) {
        if (clss == null) {
            return true;
        }
        if (clss.length > params.length) {
            return false;
        }
        for (int i = 0; i < clss.length; i++) {
            if (params[i] != null) {
                if (clss[i].isPrimitive()) {
                    continue;
                } else if (!clss[i].isAssignableFrom(params[i].getClass()) || !clss[i].isInstance(params[i])) {
                    return false;
                }
            }
        }
        return true;
    }

    public static final Object invokeMethod(String className, Class<?>[] constructClasses, Object[] constructParams,
            String methodName, Class<?>[] clss, Object... params) throws Exception {
        Class<?> cls = Class.forName(className);
        Object obj = getInstance(cls, constructClasses, constructParams);
        if (!checkParams(clss, params)) {
            throw new IllegalArgumentException();
        }
        Method method = cls.getDeclaredMethod(methodName, clss);
        method.setAccessible(true);
        return method.invoke(obj, params);
    }

    public static String getWeatherDegress(Context context, WeatherInfoArray.WeatherInfo info) {
        int resId = "c".equalsIgnoreCase(info.tempUnit) ? R.string.centigrade : R.string.fahrenheit;
        return context.getResources().getString(resId);
    }

    public static int getWeatherIconResource(Context context, int weatherCode) {
        TypedArray a = context.getResources().obtainTypedArray(R.array.weather_icon);
        if (weatherCode >= a.length()) weatherCode = a.length() - 1;
        int iconId = a.getResourceId(weatherCode, 0);
        a.recycle();
        return iconId;
    }

    public static String getWeatherByLocal(Resources resources, String weatherCode) {
        int code = -1;
        try {
            code = Integer.parseInt(weatherCode);
        } catch (Exception e) {
            return resources.getString(R.string.wrong_weather);
        }
        String[] weathers = resources.getStringArray(R.array.weather);
        if (code >= weathers.length) {
            return resources.getString(R.string.wrong_weather);
        } else {
            return weathers[code];
        }
    }

    public static List<Event> scheduleToList(ScheduleInfo info) {
        if (info == null) {
            return null;
        }

        List<ScheduleInfo.Event> events = new ArrayList<ScheduleInfo.Event>();
        ScheduleInfo.Event[] eventArray = info.event;

        for (ScheduleInfo.Event event : eventArray) {
            events.add(event);
        }

        return events;
    }

    public static ScheduleInfo scheduleListToInfo(List<ScheduleInfo.Event> events) {
        if (events == null) {
            return null;
        }

        int count = events.size();
        ScheduleInfo info = new ScheduleInfo(count);

        for (int i = 0; i < count; i++) {
            info.event[i] = events.get(i);
        }

        return info;
    }

    public static String formatSize(int size) {
        String result = "0";
        String unit = "B";
        float resultSize = 0;
        if (size > 0) {
            if (size < 200) {
                result = String.valueOf(size);
            } else if (size < 1024 * 10) {
                resultSize = size / (float) 1024;
                unit = "KB";
            } else {
                resultSize = size / (float) (1024 * 1024);
                unit = "MB";
            }
        }

        if (resultSize > 0) {
            DecimalFormat df = new DecimalFormat("0.00");
            result = df.format(resultSize);
        }

        return result + unit;
    }

    public static void initTextColor(View view, int color) {
        if (view instanceof TextView) {
            ((TextView) view).setTextColor(color);
        } else if (view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) view;
            int count = group.getChildCount();
            for (int i = 0; i < count; i++) {
                View child = group.getChildAt(i);
                initTextColor(child, color);
            }
        }
    }

    public static String getRemoteUuid(Context context) {
        return "46AEE255-CB73-BF17-B60E-208B2C7E";
    }

    /**
     * 判断网络是否可用
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] info = cm.getAllNetworkInfo();
        for (NetworkInfo networkInfo : info) {
            if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                return true;
            }
        }
        return false;
    }

    public static class Container {

        public Container(int locale, int wan, int yi, int wanYi, String wanUnit, String yiUnit, String wanYiUnit) {
            this.locale = locale;
            this.wan = wan;
            this.yi = yi;
            this.wanYi = wanYi;
            this.wanUnit = wanUnit;
            this.yiUnit = yiUnit;
            this.wanYiUnit = wanYiUnit;
        }

        public int locale;
        public int wan;
        public int yi;
        public int wanYi;
        public String wanUnit;
        public String yiUnit;
        public String wanYiUnit;
    }

    public static String formatCount(int count) {
        Container container = null;
        int defaultLocale = getDefaultLocaleCode();
        if (unitMap.containsKey(defaultLocale)) {
            container = unitMap.get(defaultLocale);
        } else {
            container = unitMap.get(1);
        }

        if (count < container.wan) {
            return String.valueOf(count);
        }

        String unit = null;
        float result = 0;
        if (count < container.yi) {
            result = count / (float) container.wan;
            unit = container.wanUnit;
        } else if (count < container.wanYi) {
            result = count / (float) container.yi;
            unit = container.yiUnit;
        } else {
            result = count / (float) container.wanYi;
            unit = container.wanYiUnit;
        }

        if (result > 0) {
            DecimalFormat df = new DecimalFormat("0.0");
            return df.format(result) + unit;
        }
        return String.valueOf(count);
    }

    /**
     * 获取默认语言代号
     */
    public static int getDefaultLocaleCode() {
        return getLocaleCode(Locale.getDefault());
    }

    /**
     * 获取语言代号
     * 
     * @param locale
     *            语言
     */
    public static int getLocaleCode(Locale locale) {
        // IwdsLog.i("net", "locale:" + locale);
        if (LOCALE_MAP.containsKey(locale)) {
            return LOCALE_MAP.get(locale);
        } else {
            return LOCALE_MAP.get(Locale.ENGLISH);
        }
    }

    public static int dp2px(Context context, int dp) {
        float density = context.getResources().getDisplayMetrics().density;
        return (int) (dp * density);
    }

    public static float dp2px(Context context, float dp) {
        float density = context.getResources().getDisplayMetrics().density;
        return dp * density;
    }

    /**
     * 获取字符串的MD5值！
     */
    public static String getMd5(String source) {

        if (null == source) return null;

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        digest.update(source.getBytes());

        BigInteger bigInt = new BigInteger(1, digest.digest());
        return bigInt.toString(16);
    }

    public static void log(Object text) {
        if (null != text) Log.e("TAG", text.toString());
    }

    public static void toast(Context context, Object text) {
        if (null != text) Toast.makeText(context, text.toString(), Toast.LENGTH_SHORT).show();
    }

    /**
     * 把时间值转换成字符串
     * 
     * @param currtime
     * @return
     */
    public static String TimetoString(long currtime) {
        if (currtime < 0) return null;
        Scanner sc = new Scanner(currtime + "");
        if (sc.nextLine().length() == 10) {
            currtime = currtime * 1000;
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm ");
        Date curDate = new Date(currtime);
        String timestr = formatter.format(curDate);
        return timestr;
    }

    public static String queryNameByNum(String num, Context context) {
        Uri uri = Uri.parse("content://com.android.contacts/data/phones/filter/" + num);
        Cursor c = context.getContentResolver().query(uri, new String[] { "display_name" }, null, null, null);

        if (c == null) return null;

        String result;
        if (c.getCount() > 1) {
            result = null;
        } else {
            if (c.moveToFirst()) {
                result = c.getString(0);
            } else {
                result = null;
            }
        }
        c.close();
        return result;
    }

    public static Bitmap compressBitmap(Bitmap bitmap, int sampleSize) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
        BitmapFactory.Options newOpts = new BitmapFactory.Options();
        newOpts.inSampleSize = sampleSize;
        try {
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            isBm.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return BitmapFactory.decodeStream(isBm, null, newOpts);
    }

    public static Bitmap resizeBitmap(Bitmap bitmap, float scaleW, float scaleH) {
        Matrix matrix = new Matrix();
        matrix.postScale(scaleW, scaleH);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static String getNameByUrl(String url) {
        int index = url.lastIndexOf("/") + 1;
        return url.substring(index);
    }

}
