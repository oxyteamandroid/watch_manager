/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  Jiangyanbo <yanbo.jiang@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.util;

import java.util.ArrayList;
import java.util.List;

import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class ViewPagerUtils {

    private static final String TAG = ViewPagerUtils.class.getSimpleName();

    public static void bind(final ViewPager vp, View buttons, final Callback selected, final Callback notSelected) {
        final ViewGroup parent = ((ViewGroup) buttons);
        final List<View> list = initList(parent);

        vp.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                for (int i = 0; i < list.size(); i++) {
                    View view = list.get(i);
                    if (i != arg0) {
                        if (null != notSelected)
                            notSelected.onCall(view);
                    } else {
                        if (null != selected)
                            selected.onCall(view);
                    }
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
        for (int i = 0; i < list.size(); i++) {
            final int item = i;
            View view = list.get(i);
            view.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View view) {
                    if (vp.getAdapter().getCount() > item) {
                        vp.setCurrentItem(item);
                        Log.e(TAG, "onclick: " + item);
                    }
                }
            });
        }
    }

    private static List<View> initList(ViewGroup parent) {
        List<View> list = new ArrayList<View>();
        for (int i = 0; i < parent.getChildCount(); i++) {
            View view = parent.getChildAt(i);
            // if (view instanceof TextView)
            list.add(view);
        }
        return list;
    }

    public static interface Callback {
        public void onCall(View view);
    }

}
