/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/WatchManager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.cloud;

import android.content.Context;

import com.ingenic.iwds.cloud.AccountListener;
import com.ingenic.iwds.cloud.CloudServiceManager;
import com.ingenic.iwds.cloud.LoginListener;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.utils.IwdsLog;

public class WeatherCloudModel implements ServiceClient.ConnectionCallbacks {

    /**
     * 天气云服务 city_name string 城市名称
     */
    public static final String WEATHER_APP_ID = "3961173a5a3a4840bdb5326fcaef0d8a";
    public static final String WEATHER_PRODUCT_KEY = "06bfd9eef4454afd8c4600825e3bdcc8";

    private ServiceClient mClient;
    private CloudServiceManager mCloudService;

    private static WeatherCloudModel mInstance;
    private Context mContext;

    private WeatherCloudModel(Context context) {
        mContext = context;
        mClient = new ServiceClient(mContext,
                ServiceManagerContext.SERVICE_CLOUD, this);
    }

    public void startConnect() {
        if (mClient != null)
            mClient.connect();
    }

    public void disConnect() {
        if (mClient != null)
            mClient.disconnect();
    }

    public synchronized static WeatherCloudModel getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new WeatherCloudModel(context);
        }
        return mInstance;
    }

    public void registerUser(String userName, String password) {
        if (mCloudService != null)
            mCloudService.registerUser(userName, password,
                    new AccountListener() {

                        @Override
                        public void onSuccess() {
                            // TODO Auto-generated method stub
                            super.onSuccess();
                        }

                        @Override
                        public void onFailure(int errCode, String errMsg) {
                            // TODO Auto-generated method stub
                            super.onFailure(errCode, errMsg);
                        }
                    });
    }

    /**
     * 普通用户登陆
     *
     * @param userName
     * @param password
     */
    public void login(String userName, String password) {
        if (mCloudService != null) {
            mCloudService.login(userName, password, new LoginListener() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onFailure(int errCode, String errMsg) {
                    IwdsLog.d(this, "login failure: " + errMsg);
                }
            });
        }
    }

    /**
     * 匿名登陆
     */
    public void loginAnonymous() {
        if (mCloudService != null) {
            mCloudService.loginAnonymous(new LoginListener() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onFailure(int errCode, String errMsg) {
                    IwdsLog.d(this, "login failure: " + errMsg);
                }
            });
        }
    }

    /**
     * 第三方帐号登陆
     *
     * @param accountType
     * @param uid
     * @param token
     */
    public void loginWithThirdAccount(int accountType, String uid, String token) {
        if (mCloudService != null) {
            mCloudService.loginWithThirdAccount(accountType, uid, token,
                    new LoginListener() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onFailure(int errCode, String errMsg) {
                            IwdsLog.d(this, "login failure: " + errMsg);
                        }
                    });
        }
    }

    @Override
    public void onConnected(ServiceClient serviceClient) {
        if (mClient != null) {
            mCloudService = (CloudServiceManager) mClient
                    .getServiceManagerContext();
            mCloudService.init(WEATHER_APP_ID, WEATHER_PRODUCT_KEY);
        }
    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {

    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient,
            ConnectFailedReason reason) {

    }

}
