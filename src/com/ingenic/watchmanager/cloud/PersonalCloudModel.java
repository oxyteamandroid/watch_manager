/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ShiGuangHua(Kenny)<guanghua.shi@ingenic.com>
 *   
 *  Elf/WatchManager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.cloud;

import android.content.Context;

import com.ingenic.iwds.cloud.AccountListener;
import com.ingenic.iwds.cloud.CloudServiceManager;
import com.ingenic.iwds.cloud.LoginListener;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;

public class PersonalCloudModel implements ServiceClient.ConnectionCallbacks {

    /**
     * 帐号服务 针对个人帐号管理 user_name string 用户名 pass_word string 登陆密码
     */
    public static final String PERSONAL_APP_ID = "c92246fbc15a46e19d491e86720bcce3";
    public static final String PERSONAL_PRODUCT_KEY = "000c1d7dfcd34802954994240c1cba09";

    private ServiceClient mClient;
    private CloudServiceManager mCloudService;

    private static PersonalCloudModel sInstance;
    private Context mContext;
    private boolean mLoginSuccess = false;
    private boolean mIsInit = false;

    public boolean isLoginSuccess() {
        return mLoginSuccess;
    }

    public void setLoginSuccess(boolean isLoginSuccess) {
        mLoginSuccess = isLoginSuccess;
    }

    public boolean getIsInit() {
        return mIsInit;
    }

    private PersonalCloudModel(Context context) {
        mContext = context;
        mClient = new ServiceClient(mContext,
                ServiceManagerContext.SERVICE_CLOUD, this);
    }

    public void startConnect() {
        if (mClient != null)
            mClient.connect();
    }

    public void disConnect() {
        if (mClient != null)
            mClient.disconnect();
    }

    public synchronized static PersonalCloudModel getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PersonalCloudModel(context);
        }
        return sInstance;
    }

    /**
     * 普通注册
     * @param userName
     * @param password
     * @param accountListener
     */
    public void registerUser(String userName, String password,
            AccountListener accountListener) {
        if (mCloudService != null)
            mCloudService.registerUser(userName, password, accountListener);
    }

    /**
     * 邮箱地址注册
     * @param userName
     * @param password
     * @param accountListener
     */
    public void registerUserWithEmail(String userEmail, String password,
            AccountListener accountListener) {
        if (mCloudService != null)
            mCloudService.registerUserWithEmail(userEmail, password,
                    accountListener);
    }

    /**
     * 手机号码注册
     * @param userName
     * @param password
     * @param accountListener
     */
    public void registerUserWithPhone(String userPhone, String password,
            String verifyCode, AccountListener accountListener) {
        if (mCloudService != null)
            mCloudService.registerUserWithPhone(userPhone, password,
                    verifyCode, accountListener);
    }

    /**
     * 获取手机验证码
     * @param userPhone
     * @param accountListener
     */
    public void requestPhoneVerifyCode(String userPhone,
            AccountListener accountListener) {
        if (mCloudService != null)
            mCloudService.requestPhoneVerifyCode(userPhone, accountListener);
    }

    /**
     * 普通用户登陆
     * @param userName
     * @param password
     */
    public void login(String userName, String password,
            LoginListener loginListener) {
        if (mCloudService != null && !mLoginSuccess) {
            mCloudService.login(userName, password, loginListener);
        }
    }

    /**
     * 匿名登陆
     */
    public void loginAnonymous(LoginListener loginListener) {
        if (mCloudService != null) {
            mCloudService.loginAnonymous(loginListener);
        }
    }

    /**
     * 第三方帐号登陆
     * @param accountType
     * @param uid
     * @param token
     */
    public void loginWithThirdAccount(int accountType, String uid,
            String token, LoginListener loginListener) {
        if (mCloudService != null) {
            mCloudService.loginWithThirdAccount(accountType, uid, token,
                    loginListener);
        }
    }

    @Override
    public void onConnected(ServiceClient serviceClient) {
        if (mClient != null) {
            mCloudService = (CloudServiceManager) mClient
                    .getServiceManagerContext();
            mCloudService.init(PERSONAL_APP_ID, PERSONAL_PRODUCT_KEY);
            mIsInit = true;
        }
    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
        mIsInit = false;
    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient,
            ConnectFailedReason reason) {
        mIsInit = false;
    }

}
