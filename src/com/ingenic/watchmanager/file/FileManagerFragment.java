/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhengchuanshu<chuanshu.zheng@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.file;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.watchmanager.FragmentActivity;
import com.ingenic.watchmanager.R;
import com.ingenic.watchmanager.WMKeyEventCallback;

public class FileManagerFragment extends Fragment implements
        OnItemClickListener, WMKeyEventCallback, OnClickListener {
    private Context mContext;
    private ListView mListView;
    private LinearLayout mEmptyView;
    private FileListAdapter mFileAdapter;
    private LinearLayout mLinearTopNavi; // 目录导航栏
    private HorizontalScrollView mScrollTopNavi;
    private List<FileInfo> mFileList;
    private String mCurrentPath;
    private Stack<View> mTopNaviBackStack = new Stack<View>();
    private LinearLayout mToolBarNew, mToolBarSort, mToolBarRefresh,
            mToolBarSetting;
    private Button mButtonOk, mButtonCancel;
    private LinearLayout mFileOperateConfirmMenu;
    private ClipboardManager mClipboardManager;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        mClipboardManager = (ClipboardManager) mContext
                .getSystemService(Context.CLIPBOARD_SERVICE);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_filemanager, null);
        mLinearTopNavi = (LinearLayout) rootView
                .findViewById(R.id.mLinearTopNavi);
        mScrollTopNavi = (HorizontalScrollView) rootView
                .findViewById(R.id.mTopNaviScroll);
        mToolBarNew = (LinearLayout) rootView.findViewById(R.id.mToolBarNew);
        mToolBarSort = (LinearLayout) rootView.findViewById(R.id.mToolBarSort);
        mToolBarRefresh = (LinearLayout) rootView
                .findViewById(R.id.mToolBarRefresh);
        mToolBarSetting = (LinearLayout) rootView
                .findViewById(R.id.mToolBarSetting);
        mListView = (ListView) rootView.findViewById(R.id.file_list);
        mEmptyView = (LinearLayout) rootView.findViewById(R.id.empty_view);
        mButtonOk = (Button) rootView.findViewById(R.id.file_ok);
        mButtonCancel = (Button) rootView.findViewById(R.id.file_no);
        mFileOperateConfirmMenu = (LinearLayout) rootView
                .findViewById(R.id.file_operate_confirm_menu);

        mFileList = new ArrayList<FileInfo>();
        mFileAdapter = new FileListAdapter();
        mListView.setAdapter(mFileAdapter);
        mListView.setOnItemClickListener(this);
        mListView.setOnCreateContextMenuListener(this);
        registerForContextMenu(mListView);

        mToolBarNew.setOnClickListener(this);
        mToolBarSort.setOnClickListener(this);
        mToolBarRefresh.setOnClickListener(this);
        mToolBarSetting.setOnClickListener(this);
        mButtonOk.setOnClickListener(this);
        mButtonCancel.setOnClickListener(this);

        firstInit();

        return rootView;
    }

    /**
     * 返回上一级目录
     * 
     * @param path
     */
    private void up(String path, boolean upOrNavi) {
        if (mListView.getVisibility() != View.VISIBLE) {
            mEmptyView.setVisibility(View.GONE);
            mListView.setVisibility(View.VISIBLE);
        }
        mCurrentPath = path;
        if (upOrNavi) {
            View v = mTopNaviBackStack.pop();
            mLinearTopNavi.removeView(v);
        }
        mFileList = FileUtil.listFile(path, mContext);
        mFileAdapter.notifyDataSetChanged();
    }

    private void firstInit() {
        if (mListView.getVisibility() != View.VISIBLE) {
            mEmptyView.setVisibility(View.GONE);
            mListView.setVisibility(View.VISIBLE);
        }
        if (!mTopNaviBackStack.empty())
            mTopNaviBackStack.clear();
        View v = LayoutInflater.from(mContext).inflate(R.layout.file_navi_item,
                null);
        TextView textView = (TextView) v.findViewById(R.id.mTvNaviItem);
        textView.setText(getString(R.string.sdcard));
        textView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mLinearTopNavi.removeAllViews();
                firstInit();
            }
        });
        mLinearTopNavi.addView(v);

        mCurrentPath = FileUtil.ROOT_DIRECTORY;
        mFileList = FileUtil.listFile(mCurrentPath, mContext);
        mFileAdapter.notifyDataSetChanged();
    }

    /**
     * 进入下一级目录
     * 
     * @param path
     */
    private void next(final FileInfo fileInfo) {
        // String newPath = mCurrentPath + File.separator + path;
        mCurrentPath = fileInfo.path;
        View v = LayoutInflater.from(mContext).inflate(R.layout.file_navi_item,
                null);
        TextView textView = (TextView) v.findViewById(R.id.mTvNaviItem);
        textView.setText(fileInfo.name);
        textView.setTag(mCurrentPath);
        textView.setTag(R.layout.file_navi_item, v);
        textView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                while (mTopNaviBackStack.peek() != v
                        .getTag(R.layout.file_navi_item)) {
                    View view = mTopNaviBackStack.pop();
                    mLinearTopNavi.removeView(view);
                }
                String tag = ((String) v.getTag());
                if (!tag.equals(mCurrentPath)) {
                    up(tag, false);
                }
            }
        });
        mLinearTopNavi.addView(v);
        mTopNaviBackStack.push(v);
        mScrollTopNavi.post(new Runnable() {
            @Override
            public void run() {
                mScrollTopNavi.fullScroll(ScrollView.FOCUS_RIGHT);
            }
        });

        IwdsLog.e("zheng", mCurrentPath);
        mFileList = FileUtil.listFile(mCurrentPath, mContext);
        mFileAdapter.notifyDataSetChanged();
        if (mFileList.size() <= 0) {
            mEmptyView.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
        }

    }

    public void refresh() {
        mFileList = FileUtil.listFile(mCurrentPath, mContext);
        if (mFileList.size() > 0)
            mFileAdapter.notifyDataSetChanged();
        else {
            mEmptyView.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
        }
        IwdsLog.e("zheng",
                mCurrentPath + "----------------size:" + mFileList.size());
    }

    private class FileListAdapter extends BaseAdapter {

        public FileListAdapter() {

        }

        @Override
        public int getCount() {
            return mFileList.size();
        }

        @Override
        public Object getItem(int position) {
            return mFileList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            FileInfo fileInfo = mFileList.get(position);
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(mContext).inflate(
                        R.layout.file_browser_item, null);
                viewHolder.name = (TextView) convertView
                        .findViewById(R.id.file_name);
                viewHolder.icon = (ImageView) convertView
                        .findViewById(R.id.file_image);
                viewHolder.lastmodify = (TextView) convertView
                        .findViewById(R.id.modified_time);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.name.setText(fileInfo.name);
            if (fileInfo.isFolder)
                viewHolder.icon.setImageResource(R.drawable.folder);
            else
                viewHolder.icon.setImageResource(R.drawable.file_icon_default);
            viewHolder.lastmodify.setText(fileInfo.path);

            return convertView;
        }

    }

    private static class ViewHolder {
        public TextView name;
        public ImageView icon;

        @SuppressWarnings("unused")
        public TextView path;
        @SuppressWarnings("unused")
        public TextView parent;
        public TextView lastmodify;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
            long id) {
        FileInfo fileInfo = mFileList.get(position);
        if (fileInfo.isFolder) {
            next(fileInfo);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // 获得AdapterContextMenuInfo,以此来获得选择的listview项目
        AdapterContextMenuInfo menuInfo = (AdapterContextMenuInfo) item
                .getMenuInfo();
        Toast.makeText(mContext, menuInfo.position + "", Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
        case 0:
            showConfirmMenu();
            break;
        case 1:
            showConfirmMenu();
            break;
        case 2:

            break;
        case 3:
            showConfirmMenu();
            break;
        case 4:
            break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
            ContextMenuInfo menuInfo) {
        menu.setHeaderTitle(mContext.getString(R.string.file_operate));
        menu.add(0, 0, 0, mContext.getString(R.string.file_copy));
        menu.add(0, 3, 1, mContext.getString(R.string.file_move));
        menu.add(0, 1, 2, mContext.getString(R.string.file_delete));
        menu.add(0, 2, 3, mContext.getString(R.string.app_detail));
        menu.add(0, 4, 4, mContext.getString(R.string.cancel));
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && !mCurrentPath.equals(FileUtil.ROOT_DIRECTORY)) {
            up(FileUtil.getParentPath(mCurrentPath), true);
            return true;
        } else
            return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.mToolBarRefresh:
            refresh();
            break;
        case R.id.mToolBarNew:
            showNewFileDialog();
            break;
        // 更改为读取剪切板内容
        case R.id.mToolBarSort:
            readClipbord();
            break;
        case R.id.mToolBarSetting:
            Intent it = new Intent(getActivity(), FragmentActivity.class);
            it.putExtra("fragment",
                    "com.ingenic.watchmanager.file.SettingFragment");
            startActivity(it);
            break;
        case R.id.file_ok:
            disMissConfirmMenu();
            break;
        case R.id.file_no:
            disMissConfirmMenu();
            break;
        }
    }

    private void readClipbord() {
        if (!mClipboardManager.hasPrimaryClip()) {
            Toast.makeText(mContext, R.string.clipboard_is_empty,
                    Toast.LENGTH_SHORT).show();
        } else {
            ClipData data = mClipboardManager.getPrimaryClip();
            ClipData.Item item = data.getItemAt(0);
            // Toast.makeText(mContext, item.getText(),
            // Toast.LENGTH_SHORT).show();
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle(getString(R.string.new_file));
            builder.setMessage(item.coerceToText(mContext));
            builder.setPositiveButton(R.string.new_file, null)
                    .setNegativeButton(R.string.cancel, null);
            builder.show();

        }
    }

    private void showNewFileDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(getString(R.string.new_file));
        builder.setItems(R.array.filetype,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(mContext, which + "", Toast.LENGTH_SHORT).show();
                    }
                });
        builder.show();
    }

    public void disMissConfirmMenu() {
        if (mFileOperateConfirmMenu.getVisibility() != View.GONE)
            mFileOperateConfirmMenu.setVisibility(View.GONE);
    }

    public void showConfirmMenu() {
        if (mFileOperateConfirmMenu.getVisibility() != View.VISIBLE)
            mFileOperateConfirmMenu.setVisibility(View.VISIBLE);
    }

}
