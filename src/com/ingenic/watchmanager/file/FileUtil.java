/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhengchuanshu<chuanshu.zheng@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.file;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

public class FileUtil {
    public static String ROOT_DIRECTORY;

    static {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED))
            ROOT_DIRECTORY = Environment.getExternalStorageDirectory()
                    .getAbsolutePath();
        else
            ROOT_DIRECTORY = "/";
    }

    public static List<FileInfo> listFile(File path, Context context) {
        List<FileInfo> list = new ArrayList<FileInfo>();
        if (!path.isDirectory())
            return null;

        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(context);
        boolean showHidden = sp.getBoolean("cb_showHidden", false);
        File[] files;
        if (showHidden)
            files = path.listFiles();
        else
            files = path.listFiles(new FileFilter());
        for (File file : files) {
            FileInfo fileInfo = new FileInfo();
            fileInfo.name = file.getName();
            fileInfo.isFolder = (file.isDirectory());
            fileInfo.isHidden = (file.isHidden());
            fileInfo.path = (file.getPath());
            fileInfo.parent = (file.getParent());
            if (file.isFile())
                fileInfo.size = file.length();
            list.add(fileInfo);
        }

        Collections.sort(list, new FileNameCamparator());

        return list;

    }

    public static List<FileInfo> listFile(String path, Context context) {
        File srcfile = new File(path);
        return listFile(srcfile, context);
    }

    public static String getParentPath(String path) {
        int i = path.lastIndexOf(File.separator);
        return path.substring(0, i);
    }

}
