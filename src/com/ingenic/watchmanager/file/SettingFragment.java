package com.ingenic.watchmanager.file;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.ingenic.watchmanager.R;

public class SettingFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.file_setting);
    }
    
}
