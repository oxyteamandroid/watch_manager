/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhengchuanshu<chuanshu.zheng@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.watchmanager.file;

import java.util.Comparator;

public class FileNameCamparator implements Comparator<FileInfo> {

    /*
     * 目录排在文件前面,类型相同按照拼音顺序来排序
     */
    @Override
    public int compare(FileInfo file1, FileInfo file2) {
        if (file1.isFolder && !file2.isFolder)
            return -1;
        else if (file2.isFolder && !file1.isFolder)
            return 1;
        else
            return file1.name.compareTo(file2.name);
    }

}
