/*
 * Copyright (C) 2014 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/Watch-Manager Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.android.internal.telephony;

/**
 * 定义了应用控制手机通话状态的相关方法的接口。
 */
interface ITelephony {
/**
 * 挂断电话。
 * @return 成功挂断电话返回true，否则返回false。
 */
boolean endCall();

/**
 * 接听电话
 */
void answerRingingCall();
}